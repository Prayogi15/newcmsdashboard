@extends('template.template')
@section('active_listing','class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Listing Details
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li>Listing</li>
         <li>Details</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-lg-8 col-md-8 col-xs-12">
            <div class="box box-solid">
               <div class="box-body" style="margin: 20px;">
                  <div class="form-group">
                     <label for="property-link">Property Link</label>
                     <div class="input-group">
                        <input type="text" id="property-link" class="form-control" value="{{ $data['URL'] }}" readonly>
                        <a href="{{ $data['URL'] }}" target="_blank" class="input-group-addon" id="copy-to-clipboard"><i class='fa fa-clone' aria-hidden='true'></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="box box-solid" style="padding: 0px 25px;">
               <div class="box-body">
                  <h4 style="margin: 10px 0px;">Report ({{ $data["CountReport"] }})</h4>
                  @if($data["CountReport"]!=0)
                  @for($i=0;$i<count($data['Report']);$i++)
                  <div class="box">
                     <div class="box-header with-border">
                        <h4 style="margin: 0px;">{{ ucfirst(strtolower($data["Report"][$i]["Status"])) }}</h4>
                     </div>
                     <div class="box-body">
                        <table class="table table-bordered table-hover table-stripped">
                        @for($j=0;$j<count($data['Report'][$i]["Data"]);$j++)
                           <tr>
                              <td>
                                 <h4>{{ $data['Report'][$i]['Data'][$j]['subject'] }}</h4>
                                 <span><b>Description</b> :{{ $data['Report'][$i]['Data'][$j]['report'] }}</span>
                              </td>
                           </tr>
                        @endfor
                        </table>
                     </div>
                  </div>
                  @endfor
                  @else
                  <div class="box">
                     <div class="box-body" style="text-align: center;">
                        No Data
                     </div>
                  </div>
                  @endif
               </div>
            </div>
         </div>
         <div class="col-lg-4 col-md-4 col-xs-12" style="margin-top: 20px;">
            <div class="box box-solid">
               <div class="box-body" style="padding: 10px 25px;">
                  <h4 style="text-align: center;">Lister Profile</h4>
                  <div style="text-align: center;"><img src="@if($data['Data']['user']->photo_url != "null"){{ $data['Data']['user']->photo_url }}@else{{ asset('assets/images/user_001.jpg') }}@endif" class="profile-user-img img-responsive img-circle" onerror="this.src='<?=url('assets/images/user_001.jpg');?>';" style="width: 100px;"></div>
                  <div style="margin-top: 20px;">
                     <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                           Name <b class="pull-right">{{ $data['Data']['user']->firstname }} {{ $data['Data']['user']->lastname }}</b>
                        </li>
                        <li class="list-group-item">
                           Email <b class="pull-right">{{ $data['Data']['user']->username }}</b>
                        </li>
                        <li class="list-group-item">
                           User ID <b class="pull-right">{{ $data['Data']['user']->uid }}</b>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
@endsection
@section('custom-script')
<script>
   // $('#copy-to-clipboard').on('click', function(){
   //    var copyText = $('#property-link');
   //    copyText.select();
   //    document.execCommand("copy");
   //    alertify.success('Copied the text');
   // });
</script>
@endsection