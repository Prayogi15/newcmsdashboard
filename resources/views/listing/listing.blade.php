@extends('template.template')
@section('active_listing','class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Listing
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li>Listing</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  @foreach($listing_status as $status_key => $wnp_status)
                  <li @if($status_key == 0)class="active"@endif><a href="#{{ $wnp_status }}" data-toggle="tab">{{ ucfirst($wnp_status) }}</a></li>
                  @endforeach
               </ul>
               <div class="tab-content" style="padding: 10px;">
                  @foreach($listing_status as $status_key => $wnp_status)
                  <div class="tab-pane <?php if($status_key == 0){echo("active");}?>" id="{{ $wnp_status }}">
                     <table id="table-{{ $wnp_status }}" class="table table-bordered table-hover table-stripped" style="width: 100%;">
                        <thead>
                           <tr>
                              <th style="min-width: 120px;">Property ID</th>
                              <th style="min-width: 500px;">Address</th>
                              <th style="min-width: 100px;">Price</th>
                              <th style="min-width: 30px;">Status</th>
                              <th style="min-width: 50px;">Type</th>
                              <th style="min-width: 100px;">Date Created</th>
                              <th style="min-width: 30px;">Created By</th>
                              <th style="min-width: 50px;">Action</th>
                           </tr>
                        </thead>
                     </table>
                  </div>
                  @endforeach
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
@endsection
@section('custom-script')
   <script>
      var region = "{{ Session::get('region') }}";
      var listing_status = {!! json_encode($listing_status) !!};

      function ChangeStatus(id, status, tag){
         while ((tag = tag.parentElement) && tag.nodeName.toUpperCase() !== 'TABLE');
         switch(status){
            case 'active': var table_target = $("#table-active"); break;
            case 'archived': var table_target = $("#table-archived"); break;
            case 'sold': var table_target = $("#table-sold"); break;
            case 'deleted': var table_target = $("#table-deleted"); break;
         }
         $.ajax({
            url: "{{ url('/listing/update') }}/"+id+"/"+status,
            type: "PUT",
            data: {
               "_token": "{{ csrf_token() }}",
            },
            success: function(res){
               if(res.Status == "success"){
                  alertify.success(res.Message);
                  $('#'+tag.id).DataTable().ajax.reload();
                  table_target.DataTable().ajax.reload();
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error("Something went wrong.");
            }
         });
      }
   </script>
   <script src="{{ asset('js/datatables/listing-datatables.js') }}"></script>
@endsection