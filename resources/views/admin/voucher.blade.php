@extends('template.template')
@section('active_admin','active')
@section('active_admin_voucher', 'class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Admin
         <small>Voucher</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li>Admin</li>
         <li class="active">Voucher</li>
      </ol>
   </section>

   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  @foreach($voucher_status as $status_key => $wnp_status)
                  <li @if($status_key == 0)class="active"@endif><a href="#{{ $wnp_status }}" data-toggle="tab">{{ ucfirst($wnp_status) }}</a></li>
                  @endforeach
               </ul>
               <div class="tab-content" style="padding: 10px;">
                  @foreach($voucher_status as $status_key => $wnp_status)
                  <div class="tab-pane <?php if($status_key == 0){echo("active");}?>" id="{{ $wnp_status }}">
                     <table id="table-{{ $wnp_status }}" class="table table-bordered table-hover table-stripped" style="width: 100%;">
                        <thead>
                           <tr>
                              <th style="min-width: 50px;">Action</th>
                              <th style="min-width: 100px;">Claim ID</th>
                              <th style="min-width: 100px;">User ID</th>
                              <th style="min-width: 150px;">Name</th>
                              <th style="min-width: 200px;">Email</th>
                              <th style="min-width: 100px;">Phone Number</th>
                              <th style="min-width: 50px;">Date</th>
                              <th style="min-width: 50px;">Voucher Code</th>
                              <th style="min-width: 50px;">Voucher Value</th>
                           </tr>
                        </thead>
                     </table>
                  </div>
                  @endforeach
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
<form id="update">
   {!! csrf_field() !!}
   <input id="trans_id" type="hidden" name="trans_id">
   <input id="status" type="hidden" name="status" value="">
</form>
@endsection
@section('custom-script')
   <script>
      var region = "{{ Session::get('region') }}";
      var voucher_status = {!! json_encode($voucher_status) !!};
      $(document).ready(function() {
         // status -> problem
         $(document).on('click', '.problem_voucher', function(){
            var el = this;
            while ((el = el.parentElement) && el.nodeName.toUpperCase() !== 'TABLE');
            var trans_id = $(this).closest('tr').find('td:nth-child(2)').text();
            var name = $(this).closest('tr').find('td:nth-child(4)').text();
            var voucher_code = $(this).closest('tr').find('td:nth-child(8)').text();
            var info = "<br/>Code Transacation : "+trans_id+"<br/>Code Voucher : "+voucher_code+"<br/>Name : "+name;
            $('#trans_id').val(trans_id);
            $('#status').val('problem');
            alertify.confirm('Change status', 'Are you sure to change this status to Problem?'+ info, function(){
               $.ajax({
                  url: "{{ url('/admin/voucher/update_transaction') }}",
                  type: "POST",
                  data: $('#update').serialize(),
                  success: function(res){
                     if(res == "success"){
                        alertify.success('Success update transaction');
                        $('#'+el.id).DataTable().ajax.reload();
                        $('#table-problem').DataTable().ajax.reload();
                     } else {
                        alertify.error('Something went wrong.');
                     }
                  },
                  error: function(res){
                     alertify.error('Something went wrong.');
                  }
               });
            }, function(){});
            return false;
         });
         // status -> open
         $(document).on('click', '.open_voucher', function(){
            var el = this;
            while ((el = el.parentElement) && el.nodeName.toUpperCase() !== 'TABLE');
            var trans_id = $(this).closest('tr').find('td:nth-child(2)').text();
            var name = $(this).closest('tr').find('td:nth-child(4)').text();
            var voucher_code = $(this).closest('tr').find('td:nth-child(8)').text();
            var info = "<br/>Code Transacation : "+trans_id+"<br/>Code Voucher : "+voucher_code+"<br/>Name : "+name;
            $('#trans_id').val(trans_id);
            $('#status').val('processed');
            alertify.confirm('Change status', 'Are you sure to change this status to Open?'+ info, function(){
               $.ajax({
                  url: "{{ url('/admin/voucher/update_transaction') }}",
                  type: "POST",
                  data: $('#update').serialize(),
                  success: function(res){
                     if(res == "success"){
                        alertify.success('Success update transaction');
                        $('#'+el.id).DataTable().ajax.reload();
                        $('#table-open').DataTable().ajax.reload();
                     } else {
                        alertify.error('Something went wrong.');
                     }
                  },
                  error: function(res){
                     alertify.error('Something went wrong.');
                  }
               });
            }, function(){});
            return false;
         });
         // status -> complete
         $(document).on('click', '.complete_voucher', function(){
            var el = this;
            while ((el = el.parentElement) && el.nodeName.toUpperCase() !== 'TABLE');
            var trans_id = $(this).closest('tr').find('td:nth-child(2)').text();
            var name = $(this).closest('tr').find('td:nth-child(4)').text();
            var voucher_code = $(this).closest('tr').find('td:nth-child(8)').text();
            var info = "<br/>Code Transacation : "+trans_id+"<br/>Code Voucher : "+voucher_code+"<br/>Name : "+name;
            $('#trans_id').val(trans_id);
            $('#status').val('done');
            alertify.confirm('Change status', 'Are you sure to change this status to Completed?'+ info, function(){
               $.ajax({
                  url: "{{ url('/admin/voucher/update_transaction') }}",
                  type: "POST",
                  data: $('#update').serialize(),
                  success: function(res){
                     if(res == "success"){
                        alertify.success('Success update transaction');
                        $('#'+el.id).DataTable().ajax.reload();
                        $('#table-completed').DataTable().ajax.reload();
                     } else {
                        alertify.error('Something went wrong.');
                     }
                  },
                  error: function(res){
                     alertify.error('Something went wrong.');
                  }
               });
            }, function(){});
            return false;
         });
         // status -> pending
         $(document).on('click', '.pending_voucher', function(){
            var el = this;
            while ((el = el.parentElement) && el.nodeName.toUpperCase() !== 'TABLE');
            var trans_id = $(this).closest('tr').find('td:nth-child(2)').text();
            var name = $(this).closest('tr').find('td:nth-child(4)').text();
            var voucher_code = $(this).closest('tr').find('td:nth-child(8)').text();
            var info = "<br/>Code Transacation : "+trans_id+"<br/>Code Voucher : "+voucher_code+"<br/>Name : "+name;
            $('#trans_id').val(trans_id);
            $('#status').val('unprocessed');
            alertify.confirm('Change status', 'Are you sure to change this status to Pending?'+ info, function(){
               $.ajax({
                  url: "{{ url('/admin/voucher/update_transaction') }}",
                  type: "POST",
                  data: $('#update').serialize(),
                  success: function(res){
                     if(res == "success"){
                        alertify.success('Success update transaction');
                        $('#'+el.id).DataTable().ajax.reload();
                        $('#table-pending').DataTable().ajax.reload();
                     } else {
                        alertify.error('Something went wrong.');
                     }
                  },
                  error: function(res){
                     alertify.error('Something went wrong.');
                  }
               });
            }, function(){});
            return false;
         });
      });
   </script>
   <script src="{!! asset('js/datatables/voucher-datatables.js') !!}" type="text/javascript"></script>
@endsection