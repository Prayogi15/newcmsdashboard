@extends('template.template')
@section('active_admin','active')
@section('active_admin_access','class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Administrator 
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li >Administrator</li>
		  <li class="active">List</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
	  <div class="col-lg-12 col-md-12 col-xs-12">
         <div class="box box-solid">
		 
		 <div class="box-header with-border">
		<h3 class="box-title">Table List</h3>
		
		@if($profile_admin->level =="superadmin")
		<div class="box-tools pull-right">
		  <button class="btn btn-primary" data-toggle="modal" data-target="#modal_add" title="Add Data" id="add_data"> <i class="fa fa-plus"></i> Add Data</button>		  
		</div>
		@endif
		<!-- /.box-tools -->
		</div>
            <div class="box-body">
               <table id="table-admin" class="table table-bordered table-hover table-stripped" style="width: 100%;">
                  <thead>
                     <tr>
						<th style="min-width: 20px;"> </th>
                        <th style="min-width: 150px;">Name</th>
                        <th style="min-width: 70px;">Username</th>
						@if($profile_admin->level =="superadmin")	
						<th style="min-width: 50px;">Edit</th>	
						<th style="min-width: 50px;">Remove</th>	
						@endif						
                     </tr>
                  </thead>
				  <tbody>
				  <?php $n=1;?>
				  @foreach($data_admin as $row)
				  <tr>
				  <td> {{ $n }}</td>
				  <td> {{ $row->username }} </td>
				  <td>  {{ $row->fullname }} </td>	
		@if($profile_admin->level =="superadmin")				  
				  <td> <a class="btn btn-warning edit_data" onclick="edit_data_admin('{{ $row->id }}','{{ $row->username }}','{{ $row->fullname }}','{{ $row->level }}')"><i class="fa fa-edit"></i></a> </td>
				  <td> 
				  @if($row->id!=$admin_id)
				  <a class="btn btn-danger edit_data" onclick="return confirm('Are you sure to remove {{ $row->username }} from list ?')" href="{{ url('/admin/remove/')}}/{{$row->id}}"><i class="fa fa-remove"></i></a> </td>
				  @endif
			@endif		  
				  </tr>
				  <?php $n++;?>
				  @endforeach
				  </tbody>
			
               </table>
            </div>
         </div>
      </div>
	 </div> 
   </section>
   <!-- /.content -->

   
   
   <!-- modal add -->
	  <div class="modal fade" id="modal_add">
          <div class="modal-dialog">
		  	<form class="form-horizontal"  id="form1" method="post">
			{!! csrf_field() !!}
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add User</h4>
              </div>
              <div class="modal-body">
                <p>
				
				<div class="alert alert-danger alert-dismissible">
               
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
              <span id="alert_message"></span>
              </div>
				
                <div class="form-group">
                  <label for="username" class="col-sm-4 control-label">Username</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" required >
                  </div>
                </div>
			    <div class="form-group">
                  <label for="fullname" class="col-sm-4 control-label">Fullname</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Fullname" required >
                  </div>
                </div>
				 <div class="form-group">
                  <label for="level" class="col-sm-4 control-label">Acces Level</label>
                  <div class="col-sm-8">
                   <select class="form-control" name="level" id="level">
				   <option value="superadmin">Super Admin</option>
				   <option value="admin">Admin</option>
				   </select>
                  </div>
                </div>
			    <div class="form-group">
                  <label for="password" class="col-sm-4 control-label"> Password</label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" required >
                  </div>
                </div>
				</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitdata">Save</button>
              </div>
            </div>
            <!-- /.modal-content -->
		  </form>
		  </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
   
   
    <!-- modal add -->
	  <div class="modal fade" id="modal_edit">
          <div class="modal-dialog">
		  	<form class="form-horizontal" id="form2" method="post">
			{!! csrf_field() !!}
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit User</h4>
              </div>
              <div class="modal-body">
                <p>
				
				<div class="alert alert-danger alert-dismissible">
               
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
              <span id="alert_message2"> </span>
              </div>
				
				<input type="hidden" class="form-control" id="id2" name="id2" placeholder="Fullname" required >
				 
                <div class="form-group">
                  <label for="username2" class="col-sm-4 control-label">Username</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="username2" name="username" placeholder="Username" required readonly >
                  </div>
                </div>
			    <div class="form-group">
                  <label for="fullname2" class="col-sm-4 control-label">Fullname</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="fullname2" name="fullname" placeholder="Fullname" required >
                  </div>
                </div>
			  
			  <div class="form-group">
                  <label for="status2" class="col-sm-4 control-label"> Level</label>
                  <div class="col-sm-8">
                    <select name="level" id="level2" class="form-control">
					<option value="superadmin">Super Admin</option>
					<option value="admin">Admin</option>
					</select>
                  </div>
                </div>
			  
			  
			    <div class="form-group">
                  <label for="password2" class="col-sm-4 control-label"> Password</label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="password2" name="password" placeholder="Password" required >
                  </div>
                </div>

				</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitdata2">Save</button>
              </div>
            </div>
            <!-- /.modal-content -->
		  </form>
		  </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
   
   
@endsection
@section('custom-script')
   <script>
   $(document).ready(function() {
	$(".alert").hide();
	   
	   
    var t = $('#table-admin').DataTable({
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    });
	
	 t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
	
	
	$( "#add_data" ).click(function() {
		$("#username").val('');
		$("#fullname").val('');
		$("#password").val('');
		$(".alert").hide();
	});
	
	$( "#submitdata" ).click(function() {
			var data = $("#form1").serialize();
			var username = $("#username").val();
			var fullname = $("#fullname").val();
			
			$.ajax({
			type : 'POST',
			url  : "{{ url('/admin/insert/')}}",
			data : data,
			beforeSend: function()
			{	
				$("#submitdata").prop("disabled", true );
				$("#submitdata").text("Loading ..");	
			},
			success :  function(response)
			   {		
					$("#submitdata").prop("disabled", false );
					$("#submitdata").text("Save");
					var data 	= JSON.parse(response);	
					var message	= 	data['message'];
					if(data['status']=="success"){			
						 $("#alert_message").text(data['message']);
						  $(".alert").removeClass("alert-danger");
						  $(".alert").addClass( "alert-success" );
						  $( ".alert" ).show(500);
						  $('#modal_add').modal('toggle');
						  /*var rowNode = t.row.add([ '',username,fullname,"<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modal_add' title='Edit Data'>Edit</button>" ] ).draw().node();
						$(rowNode).css('color','red').animate({ color: 'black' });*/
						window.location.reload();
					}
					else if (data['status'] == "error"){
						$("#alert_message").text(data['message']);
						  $(".alert").removeClass("alert-success");
						  $(".alert").addClass( "alert-danger" );
						$( ".alert" ).show();
					}
			  }
			});
				return false;
			});
			
			
			
			$( "#submitdata2" ).click(function() {
			var data = $("#form2").serialize();
			var username = $("#username2").val();
			var fullname = $("#fullname2").val();
			
			$.ajax({
			type : 'POST',
			url  : "{{ url('/admin/update/')}}",
			data : data,
			beforeSend: function()
			{	
				$("#submitdata2").prop("disabled", true );
				$("#submitdata2").text("Loading ..");	
			},
			success :  function(response)
			   {		
					$("#submitdata2").prop("disabled", false );
					$("#submitdata2").text("Save");
					var data 	= JSON.parse(response);	
					var message	= 	data['message'];
					if(data['status']=="success"){			
						 $("#alert_message2").text(data['message']);
						  $(".alert").removeClass("alert-danger");
						  $(".alert").addClass( "alert-success" );
						  $( ".alert" ).show(500);
						  $('#modal_edit').modal('toggle');
						  /*var rowNode = t.row.add([ '',username,fullname,"<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modal_add' title='Edit Data'>Edit</button>" ] ).draw().node();
						$(rowNode).css('color','red').animate({ color: 'black' });*/
						window.location.reload();
					}
					else if (data['status'] == "error"){
						$("#alert_message2").text(data['message']);
						  $(".alert").removeClass("alert-success");
						  $(".alert").addClass( "alert-danger" );
						$( ".alert" ).show();
					}
			  }
			});
				return false;
			});
			
			
			
   });
   
   function edit_data_admin(id,username,fullname,level)
	{
		$(".alert").hide();
		$("#submitdata2").prop("disabled", false );
		$("#submitdata2").text("Save");
		$("#id2").val(id);
		$("#status2").val(status);
		$("#username2").val(username);
		$("#fullname2").val(fullname);
		$("#status2").val(status);
		$('#level2 option').filter(function(){
			return this.value === level
			}).prop('selected', true);
		
		$('#modal_edit').modal();	
	}

   </script>
  
@endsection