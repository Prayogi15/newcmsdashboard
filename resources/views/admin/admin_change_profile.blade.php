@extends('template.template')
@section('active_admin','active')
@section('active_admin_access','class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Administrator 
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li >Profile</li>
		  <li class="active">Change Profile</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
	  <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12 col-md-offset-2">
         <div class="box box-solid">
		 
		 <div class="box-header with-border">
		<h3 class="box-title">Profile Data</h3>
		<div class="box-tools pull-right">
		  		  
		</div>
		<!-- /.box-tools -->
		</div>
            <div class="box-body">
               <form class="form-horizontal" id="form2" method="post" action="{{ url('/admin/update_profile/')}}">
			{!! csrf_field() !!}
			
			 @if(session('status_failed'))
			<div class="alert alert-danger alert-dismissible"> 
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
              <span id="alert_message"> {{ session('status_failed') }}  </span>
              </div>
			@endif
			
			@if(session('status_success'))
			<div class="alert alert-success alert-dismissible"> 
                <h4><i class="icon fa fa-ban"></i> Success!</h4>
              <span id="alert_message"> {{ session('status_success') }} </span>
              </div>
			@endif
			
			
			<input type="hidden" class="form-control" id="id2" name="id2" placeholder="Fullname" required >
				 
                <div class="form-group">
                  <label for="username2" class="col-sm-4 control-label">Username</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="username2" name="username" placeholder="Username" required value="{{ $data_admin->username }}">
                  </div>
                </div>
			    <div class="form-group">
                  <label for="fullname2" class="col-sm-4 control-label">Fullname</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="fullname2" name="fullname" placeholder="Fullname" required value="{{ $data_admin->fullname }}">
                  </div>
                </div>
			  
			  
		
				<div class="form-group">
                  <label for="password2" class="col-sm-4 control-label">Old Password</label>
                  <div class="col-sm-8">
                    <div class="input-group">
					<input type="password" class="form-control" id="password3" name="oldpass" placeholder="Password">
					 <span class="input-group-addon" id="newpass_show_old" onclick="show_pass('password3','text','newpass_hide_old','newpass_show_old')"><i class="fa fa-eye"></i></span>
				<span class="input-group-addon" id="newpass_hide_old" onclick="show_pass('password3','password','newpass_show_old','newpass_hide_old')" style="display:none;"><i class="fa fa-eye-slash"></i></span>
					</div>
                  </div>
                </div>
				
				  <div class="form-group">
                  <label for="password2" class="col-sm-4 control-label"> New Password</label>
                  <div class="col-sm-8">        
				<div class="input-group">
                <input type="password" class="form-control" id="password2" name="password" placeholder="Password" >
                <span class="input-group-addon" id="newpass_show" onclick="show_pass('password2','text','newpass_hide','newpass_show')"><i class="fa fa-eye"></i></span>
				<span class="input-group-addon" id="newpass_hide" onclick="show_pass('password2','password','newpass_show','newpass_hide')" style="display:none;"><i class="fa fa-eye-slash"></i></span>
              </div>
                  </div>
                </div>
				
				
		 <div class="form-group">
        <button type="submit" class="btn btn-primary col-md-offset-5" style="width:50%"> Login </button>
     </div>
		
		
			</form>
            </div>
         </div>
      </div>
	 </div> 
   </section>
   <!-- /.content -->

@endsection
@section('custom-script')
   <script>
function show_pass(target_id,change_to,change_id,source_id)
{
	 $("#"+target_id).attr("type", change_to);	
	 $("#"+change_id).removeAttr("style");
	 $("#"+source_id).css("display", "none");
}





   </script>
  
@endsection