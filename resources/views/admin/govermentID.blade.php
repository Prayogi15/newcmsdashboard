@extends('template.template')
@section('active_admin','active')
@section('active_admin_govid', 'class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Admin
         <small>Goverment ID</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li>Admin</li>
         <li class="active">Goverment ID</li>
      </ol>
   </section>

   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  @foreach($status as $status_key => $wnp_status)
                  <li @if($status_key == 0)class="active"@endif><a href="#{{ $wnp_status }}" data-toggle="tab">{{ ucfirst($wnp_status) }}</a></li>
                  @endforeach
               </ul>
               <div class="tab-content" style="padding: 10px;">
                  @foreach($status as $status_key => $wnp_status)
                  <div class="tab-pane <?php if($status_key == 0){echo("active");}?>" id="{{ $wnp_status }}">
                     <table id="table-{{ $wnp_status }}" class="table table-bordered table-hover table-stripped" style="width: 100%;">
                        <thead>
                           <tr>
                              <th style="min-width: 50px;">User ID</th>
                              <th style="min-width: 250px;">Name</th>
                              <th style="min-width: 250px;">Email</th>
                              <th style="min-width: 150px;">Phone</th>
                              <th style="min-width: 50px;">Action</th>
                           </tr>
                        </thead>
                     </table>
                  </div>
                  @endforeach
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
<div class="modal fade" id="modal-default">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title" style="text-align: center;">Verifying</h3>
         </div>
         <div class="modal-body" style="padding: 10px 50px;">
            <div class="row">
               <div class="form-group">
                  <label for="username">Email</label>
                  <input type="text" id="username" class="form-control" disabled>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <label>Government ID</label>
                  <br>
                  <div id="box-gov-id"></div>
               </div>
               <div class="col-md-6">
                  <label>Potrait</label>
                  <br>
                  <div id="box-gov-user"></div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('custom-script')
   <script>
      var region = "{{ Session::get("region") }}";
      // var region = "indonesia";
      var status_list = {!! json_encode($status) !!};
      function viewPhotos(id){
         $("#username").val("");
         $("#box-gov-id").empty();
         $("#box-gov-user").empty();
         $.ajax({
            url: "{{ url('/admin/government_id/get_user_by_id') }}",
            type: "GET",
            data: {user_id: id},
            success: function(res){
               if(res.Status == "success"){
                  $("#username").val(res.Data.Email);
                  var gov_id_val= res.Data.Gov_ID;
                  var gov_user_val= res.Data.Gov_User;


                  if(gov_id_val.substring(0,4)!="http") //kalo tidak ada httpnya maka ambil dari thumb
                  {
                     var url_gov_id ="{{ env('IMG_THUMB') }}"+res.Data.Gov_ID;
                  }
                   else 
                   {
                     var url_gov_id =res.Data.Gov_ID; //kalo ada ambil dari http upload
                   }

                  if(gov_user_val.substring(0,4)!="http")
                  {
                     var url_gov_user ="{{ env('IMG_THUMB') }}"+res.Data.Gov_User;
                  }
                   else 
                   {
                     var url_gov_user =res.Data.Gov_User;
                   }

                   

                  $("#box-gov-id").html("<img src='"+ url_gov_id +"' style='width: 100%;' onerror='"+ res.Data.Gov_ID +"'> ");

                  $("#box-gov-user").html("<img src='"+url_gov_user+"' style='width: 100%;' onerror='"+ res.Data.Gov_ID +"'>");
               }
            }
         });
      }
      function verify(id,status_id,tag){
         while ((tag = tag.parentElement) && tag.nodeName.toUpperCase() !== 'TABLE');
         switch(status_id){
            case "2": var table_target = $("#table-pending"); break;
            case "1": var table_target = $("#table-completed"); break;
            case "3": var table_target = $("#table-open"); break;
            case "4": var table_target = $("#table-problem"); break;
         }
         $.ajax({
            url: "{{ url('/admin/government_id/change_status') }}",
            type: "POST",
            data: {
               "_token": "{{ csrf_token() }}",
               "user_id": id,
               "status_id": status_id
            },
            success: function(res){
               if(res.Code == "200"){
                  alertify.success("Success change status.");
                  $('#'+tag.id).DataTable().ajax.reload();
                  table_target.DataTable().ajax.reload();
               } else {
                  alertify.error("Something went wrong.");
               }
            }
         });
      }
   </script>
   <script src="{!! asset('js/datatables/government-datatables.js') !!}" type="text/javascript"></script>
@endsection