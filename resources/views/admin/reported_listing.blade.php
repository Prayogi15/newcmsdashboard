@extends('template.template')
@section('active_admin','active')
@section('active_admin_reportedListing', 'class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Admin
         <small>Reported Listing</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li>Admin</li>
         <li>Reported Listing</li>
      </ol>
   </section>

   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  @foreach($status as $status_key => $wnp_status)
                  <li @if($status_key == 0)class="active"@endif><a href="#{{ $wnp_status }}" data-toggle="tab">{{ ucfirst($wnp_status) }}</a></li>
                  @endforeach
               </ul>
               <div class="tab-content" style="padding: 10px;">
                  @foreach($status as $status_key => $wnp_status)
                  <div class="tab-pane <?php if($status_key == 0){echo("active");}?>" id="{{ $wnp_status }}">
                     <table id="table-{{ $wnp_status }}" class="table table-bordered table-hover table-stripped" style="width: 100%;">
                        <thead>
                           <tr>
                              <th style="min-width: 50px;">Property ID</th>
                              <th style="min-width: 150px;">Problem</th>
                              <th style="min-width: 200px;">Description</th>
                              <th style="min-width: 50px;">Reported Date</th>
                              <th style="min-width: 100px;">Reported by</th>
                              <th style="min-width: 50px;">Action</th>
                           </tr>
                        </thead>
                     </table>
                  </div>
                  @endforeach
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
<form id="update">
   {!! csrf_field() !!}
   <input id="trans_id" type="hidden" name="trans_id">
   <input id="status" type="hidden" name="status" value="">
</form>
@endsection
@section('custom-script')
   <script>
      var region = "{{ Session::get('region') }}";
      var status_report = {!! json_encode($status) !!};
      function ChangeStatus(id, status, tag){
         while ((tag = tag.parentElement) && tag.nodeName.toUpperCase() !== 'TABLE');
         switch(status){
            case 'pending': var table_target = $("#table-pending"); break;
            case 'open': var table_target = $("#table-open"); break;
            case 'completed': var table_target = $("#table-completed"); break;
            case 'problem': var table_target = $("#table-problem"); break;
         }
         $.ajax({
            url: "{{ url('/admin/reported_listing/update_report') }}/"+id+"/"+status,
            type: "PUT",
            data: {
               "_token": "{{ csrf_token() }}",
            },
            success: function(res){
               if(res.Status == "success"){
                  alertify.success(res.Message);
                  $('#'+tag.id).DataTable().ajax.reload();
                  table_target.DataTable().ajax.reload();
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error("Something went wrong.");
            }
         });
      }
   </script>
   <script src="{!! asset('js/datatables/reported-listings-datatables.js') !!}" type="text/javascript"></script>
@endsection