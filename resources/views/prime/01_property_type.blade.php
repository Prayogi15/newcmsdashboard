<div class="row">
   <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="box">
         <div class="box-header with-border">
            <h3 class="box-title">Property Type</h3>
            <div class="box-tools pull-right">
               <span><div class="rounded-popup popup" onclick="showPopup();">i <span class="popuptext" id="info-popup">Please minimize this section and don't reselect type after select 1 type</span></div></span>
               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
         </div>
         <div class="box-body" style="padding: 20px;">
            @foreach($type as $type_key => $wnp_type)
            <label class="radio-type">
               <input type="radio" name="project_type" value="{{ $wnp_type->id }}">
               <div>
                  <img class="active" src="{{ asset('/assets/icon/icon-') }}{{ str_replace(' ','', strtolower($wnp_type->type)) }}-active.svg" style="display: none;">
                  <img class="inactive" src="{{ asset('/assets/icon/icon-') }}{{ str_replace(' ','', strtolower($wnp_type->type)) }}.svg">
                  <span>{{ $wnp_type->type }}</span>
               </div>
            </label>
            @endforeach
         </div>
      </div>
   </div>
</div>