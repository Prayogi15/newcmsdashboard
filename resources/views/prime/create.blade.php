@extends('template.template')
@section('active_prime_cms','class=active')
@section('custom-css')
	<link rel="stylesheet" href="{{ asset('/css/app.css') }}">
@endsection

@section('content')

<div id="app">
	<section class="content-header">
	  	<h1>ADD PROJECT</h1>
	  	<ol class="breadcrumb">
	    	<li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
	    	<li class="active">Prime CMS</li>
	  	</ol>
	</section>

	<loading
	    :show="isLoading"
	    label="Loading...">
	</loading>

	<section class="content">
		<div class="row">
		   <div class="col-lg-12 col-md-12 col-xs-12">
		      	<div class="box">
		         	<div class="box-header with-border">
		            	<h3 class="box-title">Property Type</h3>
		            	<div class="box-tools pull-right">
		               		<span>
		               			<div class="rounded-popup popup" onclick="showPopup();">i 
		               				<span class="popuptext" id="info-popup">Please minimize this section and don't reselect type after select 1 type</span>
		               			</div>
		               		</span>
		               		<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		            	</div>
		         	</div>
		         	<div class="box-body" style="padding: 20px;">
		         		<property-type-selector :types="{{ $types }}" v-model="propertyType"></property-type-selector>
		         	</div>
		      	</div>
		   	</div>
		</div>

		<div class="row">
		   	<div class="col-xs-12">
				<div class="box">
					<div class="box-header with-border">
					  	<h3 class="box-title">Primary Details</h3>
					   	<div class="box-tools pull-right">
					      	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					   	</div>
					</div>
					<div class="box-body" style="padding: 20px;">
						<div class="row">
						   	<div class="col-xs-12">
						     	<label>Primary Photo<span class="validation-notice photos">photo minimal 1</span></label>
						     	<drop-box v-model="primaryPhotos"></drop-box>
						   	</div>
						</div>
				      	<div class="row">
				        	<div class="col-xs-12">
				            	<label>Project Status</label>
				            	<div class="row">
									<option-selector :options="{{ $statuses }}" v-model="projectStatus"></option-selector>
								</div>
				         	</div>
				      	</div>
				      	<div class="row mt-20">
				      	   	<div class="col-lg-6 col-md-6 col-xs-12">
				      	      	<div class="form-group">
				      	        	<label>Project Name</label>
				      	         	<input type="text" class="form-control" name="project_name" placeholder="Type Project Name" style="border-radius: 6px;" v-model="projectName">
				      	      	</div>
				      	   	</div>
				      	   <div class="col-lg-6 col-md-6 col-xs-12">
				      	      	<label>Price Range (IDR)</label>
				      	      	<div class="custom-input-text">
				      	        	<input type="text" class="custom-price price-input" id="project_min_price" name="project_min_price" placeholder="Min Price" onkeypress="return validate_number(event, this);" onkeyup="addComa(this);" v-model="minPrice">
				      	         	<div class="divider"> - </div>
				      	         	<input type="text" class="custom-price price-input" id="project_max_price" name="project_max_price" placeholder="Max price" onkeypress="return validate_number(event, this);" onkeyup="addComa(this);" v-model="maxPrice">
				      	      </div>
				      	   </div>
				      	</div>
				      	<div class="row mt-20">
				      	   	<div class="col-lg-12 col-md-12 col-xs-12">
				      	     	<label>Address</label>
				      	      	<div id="custom-map-control">
				      	         	<div class="autocomplete-wrapper">
				      	            	<span class="icon icon-search map-autocomplete-icon"></span>
				      	            	<input id="map-autocomplete-input" class="controls" type="text" placeholder="Find Location">
				      	         	</div>
				      	         	<div class="drag-notice-wrapper">
				      	            	<span class="icon icon-info drag-notice-icon"></span>
				      	            	<span class="drag-notice-text">Drag the map to choose correct location</span>
				      	         	</div>
				      	      	</div>
				      	      	<div id="custom-map-control-street-view">
				      	         	<label class="check-section" style="margin:0;">
				      	            	<input type="checkbox" v-model="showsStreetView">
				      	            	<span class="map-control"></span>
				      	            	<a class="map-control" style="font-weight: 500; font-size: 14px;">Show street view</a>
				      	         	</label>
				      	      	</div>
				      	      	<div class="box no-padding" style="position:relative;">
				      	         	<span id="wnp-marker" style="background: url('{{ asset('assets/images/marker-default.svg') }}');"></span>
				      	         	<div id="map" style="height: 400px;"></div>
				      	      	</div>
				      	   	</div>
				      	</div>
				      	<div class="row mt-20">
				      	  	<div class="col-lg-12 col-md-12 col-xs-12">
				      	      	<div class="form-group">
				      	      		<input type="hidden" name="lat" id="lat">
				      	      		<input type="hidden" name="lng" id="lng">
				      	         	<label>Project Detail Address (complete)</label>
				      	         	<textarea class="form-control" placeholder="Type address of project detail" style="border-radius: 6px;" id="address" v-model="projectAddress"></textarea>
				      	      	</div>
				      	   	</div>
				      	</div>
				      	<div class="row mt-20">
				      	   	<div class="col-lg-12 col-md-12 col-xs-12">
				      	      	<div class="form-group">
				      	         	<label>Address for main page (simple)</label>
				      	         	<input type="text" class="form-control" placeholder="Type address for main page" style="border-radius: 6px;" id="short_address" name="short_address" v-model="addressForMainPage">
				      	      	</div>
				      	   	</div>
				      	</div>
				      	<div class="row mt-20">
				      	   	<div class="col-lg-6 col-md-6 col-xs-12">
								<div class="form-group">
							   		<label>E-brochure</label>
							   		<file-input placeholder="Upload e-brochure pdf" v-model="eBrochurePath"></file-input>
								</div>	
				      	   	</div>
				      	   	<div class="col-lg-6 col-md-6 col-xs-12">
				      	      	<div class="form-group">
				      	         	<label>Pricelist</label>
				      	         	<file-input placeholder="Upload pricelist pdf" v-model="priceListPath"></file-input>
				      	      	</div>
				      	   	</div>
				      	</div>
					</div>
				</div>
	      	</div>
	   	</div>

	   	<div class="row">
	   		<div class="col-lg-12 col-md-12 col-xs-12">
	   			<div class="box">
	   				<div class="box-header with-border">
	   					<h3 class="box-title">Unit Type</h3>
	   					<div class="box-tools pull-right">
	   						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	   					</div>
	   				</div>
	   				<div class="box-body" style="padding: 20px;">
	   					<div class="row">
	   						<div class="col-lg-12 col-md-12 col-xs-12">
	   							<unit-types-table :unit-types="unitTypes" @clicked-edit-unit-type="editUnitType"></unit-types-table>
	   						</div>
	   					</div>
	   					<div class="row">
	   						<div class="col-lg-12 col-md-12 col-xs-12">
	   							<button type="button" class="btn btn-default" @click="addUnitType">+ Add Type</button>
	   						</div>
	   					</div>
	   				</div>
	   			</div>
	   		</div>
	   	</div>

	   	<div class="row">
	   		<div class="col-lg-12 col-md-12 col-xs-12">
	   			<div class="box">
	   				<div class="box-header with-border">
	   					<h3 class="box-title">Description</h3>
	   					<div class="box-tools pull-right">
	   						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	   					</div>
	   				</div>
	   				<div class="box-body" style="padding: 20px;">
	   					<div class="row">
	   						<div class="col-lg-12 col-md-12 col-xs-12">
	   							<div class="form-group">
	   								<label>Project Description</label>
	   								<textarea class="form-control" name="project_description" placeholder="Insert project description" autocomplete="off" style="border-radius: 6px;" rows="8" v-model="projectDescription"></textarea>
	   							</div>
	   						</div>
	   					</div>
	   					<div class="row">
	   						<div class="col-lg-12 col-md-12 col-xs-12">
	   							<div class="form-group">
	   								<label>Project Website</label>
	   								<input type="text" class="form-control" name="project_website" placeholder="Insert project website url" autocomplete="off" style="border-radius: 6px;" v-model="projectWebsite">
	   							</div>
	   						</div>
	   					</div>
	   				</div>
	   			</div>
	   		</div>
	   	</div>

	   	<div class="row">
	   		<div class="col-lg-12 col-md-12 col-xs-12">
	   			<div class="box">
	   				<div class="box-header with-border">
	   					<h3 class="box-title">Facility</h3>
	   					<div class="box-tools pull-right">
	   						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	   					</div>
	   				</div>
	   				<div class="box-body" style="padding: 20px;">
	   					<div class="row">
	   						<div class="col-lg-12 col-md-12 col-xs-12" id="facilities_section">
	   							<div class="form-group" v-for="category in visibleCategories">
	   								<label>@{{ category.name }}</label>
	   								<div class="row">
	   									<div class="col-lg-3 col-md-6 col-xs-12" v-for="facility in category.facilities">
	   										<div class="checkbox">
	   											<label><input type="checkbox" @click="selectFacility(facility)" :checked="selectedFacilities.includes(facility.id)">@{{ facility.name }}</label>
	   										</div>
	   									</div>
	   								</div>
	   							</div>
	   						</div>
	   					</div>
	   				</div>
	   			</div>
	   		</div>
	   	</div>

	   	<div class="row">
	   		<div class="col-lg-12 col-md-12 col-xs-12">
	   			<div class="box">
	   				<div class="box-header with-border">
	   					<h3 class="box-title">Project Plan</h3>
	   					<div class="box-tools pull-right">
	   						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	   					</div>
	   				</div>
	   				<div class="box-body" style="padding: 20px;">
	   					<div class="row">
				      	   	<div class="col-lg-6 col-md-6 col-xs-12">
								<div class="form-group">
							   		<label>Project Plan Image</label>
							   		<file-input placeholder="Upload project plan image" v-model="projectPlanImagePath"></file-input>
								</div>	
				      	   	</div>
	   					</div>
	   				</div>
	   			</div>
	   		</div>
	   	</div>
	   	
	   	<div class="row">
	   		<div class="col-lg-12 col-md-12 col-xs-12">
	   			<div class="box">
	   				<div class="box-header with-border">
	   					<h3 class="box-title">Developer Details</h3>
	   					<div class="box-tools pull-right">
	   						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	   					</div>
	   				</div>
	   				<div class="box-body" style="padding: 20px;">
	   					<div class="row">
	   						<div class="col-lg-6 col-md-6 col-xs-12">
   								<div class="form-group">
   							   		<label>Developer Logo (Thumbnail)</label>
   							   		<file-input placeholder="Upload developer logo" v-model="developerLogoPath"></file-input>
   								</div>
	   						</div>
	   						<div class="col-lg-6 col-md-6 col-xs-12">
   								<div class="form-group">
   							   		<label>Developer Image (Full Image)</label>
   							   		<file-input placeholder="Upload developer image" v-model="developerFullImagePath"></file-input>
   								</div>
	   						</div>
	   					</div>
	   					<div class="row">
	   						<div class="col-lg-6 col-md-6 col-xs-12">
	   							<div class="form-group">
	   								<label>Developer Name</label>
	   								<input type="text" class="form-control" name="developer_name" placeholder="Developer name" autocomplete="off" style="border-radius: 6px;" v-model="developerName">
	   							</div>
	   						</div>
	   					</div>
	   					<div class="row">
	   						<div class="col-lg-12 col-md-12 col-xs-12">
	   							<div class="form-group">
	   								<label>Developer Info</label>
	   								<textarea class="form-control" name="developer_info" placeholder="Type developer info" autocomplete="off" style="border-radius: 6px;" rows="8" v-model="developerInfo"></textarea>
	   							</div>
	   						</div>
	   					</div>
	   				</div>
	   			</div>
	   		</div>
	   	</div>
	   	
	   	<div class="row">
	   		<div class="col-lg-12 col-md-12 col-xs-12">
	   			<div class="box">
	   				<div class="box-header with-border">
	   					<h3 class="box-title">Contact Info</h3>
	   					<div class="box-tools pull-right">
	   						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	   					</div>
	   				</div>
	   				<div class="box-body" style="padding: 20px;">
	   					<div class="row">
	   						<div class="col-lg-6 col-md-6 col-xs-12">
	   							<div class="form-group">
	   								<label>Developer Phone Number</label>
	   								<input type="text" class="form-control" name="developer_phone" placeholder="Type phone number" autocomplete="off" style="border-radius: 6px;" v-model="developerPhoneNumber">
	   							</div>
	   						</div>
	   						<div class="col-lg-6 col-md-6 col-xs-12">
	   							<div class="form-group">
	   								<label>Developer Email</label>
	   								<input type="text" class="form-control" name="developer_email" placeholder="Type email" autocomplete="off" style="border-radius: 6px;" v-model="developerEmail">
	   							</div>
	   						</div>
	   					</div>
	   				</div>
	   			</div>
	   		</div>
	   	</div>

	   	<div class="row">
	   	   	<div class="col-lg-12 col-md-12 col-xs-12">
				<a href="#" class="btn btn-primary" style="padding: 10px 50px; float: right;" @click.prevent="storeProject">Create</a>
	   	   	</div>
	   	</div>
	</section>

	<unit-type-form-modal ref="createUnitTypeFormModal" @submit="storeUnitType"></unit-type-form-modal>
	<unit-type-form-modal ref="editUnitTypeFormModal" @submit="updateUnitType"></unit-type-form-modal>
</div>

@endsection

@section('custom-script')

<script>
	var categories = {!! json_encode($categories) !!};
	var buildingsFacilities = {!! json_encode($buildings_facilities) !!};
	var latLng = { lat: -6.1596475, lng: 106.8830529 };
	var global_flag = 0;

	$('input[type="hidden"]#lat').val(latLng.lat);
	$('input[type="hidden"]#lng').val(latLng.long);

	var data = {
		isLoading: false,
        propertyType: 1, // This is building_id in the database
        primaryPhotos: [],
        projectStatus: 1,
        projectName: '',
        minPrice: '',
        maxPrice: '',
        showsStreetView: false,
        projectAddress: '',
        addressForMainPage: '',
        eBrochurePath: '',
        priceListPath: '',
    	unitTypes: [],
        projectDescription: '',
        projectWebsite: '',
        selectedFacilities: [],
        projectPlanImagePath: '',
        developerLogoPath: '',
        developerFullImagePath: '',
        developerName: '',
        developerInfo: '',
        developerPhoneNumber: '',
        developerEmail: ''
    };
</script>

<script src="{{ asset('js/project.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_API_KEY')}}&callback=initMap&v=3.exp&libraries=places" async defer></script>
<script src="{{ asset('/js/map.js') }}"></script>

@endsection
