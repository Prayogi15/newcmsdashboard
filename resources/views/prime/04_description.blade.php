<div class="row">
   <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="box">
         <div class="box-header with-border">
            <h3 class="box-title">Description</h3>
            <div class="box-tools pull-right">
               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
         </div>
         <div class="box-body" style="padding: 20px;">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-xs-12">
                  <div class="form-group">
                     <label>Project Description</label>
                     <textarea class="form-control" name="project_description" placeholder="Insert project description" autocomplete="off" style="border-radius: 6px;" rows="8"></textarea>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-12 col-md-12 col-xs-12">
                  <div class="form-group">
                     <label>Project Website</label>
                     <input type="text" class="form-control" name="project_website" placeholder="Insert project website url" autocomplete="off" style="border-radius: 6px;">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>