@extends('template.template')
@section('active_prime_cms','class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Prime
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li class="active">Prime CMS</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="nav-tabs-custom">
               <a href="{{ url('/prime_cms/create_project') }}" class="btn btn-default" style="float: right; margin:5px;">+ Add Project</a>
               <ul class="nav nav-tabs">
                  @foreach($status as $status_key => $wnp_status)
                  <li @if($status_key == 0)class="active"@endif><a href="#{{ $wnp_status }}" data-toggle="tab">{{ strtoupper($wnp_status) }}</a></li>
                  @endforeach
               </ul>
               <div class="tab-content" style="padding: 10px;">
                  @foreach($status as $status_key => $wnp_status)
                  <div class="tab-pane <?php if($status_key == 0){echo("active");}?>" id="{{ $wnp_status }}">
                     <table id="table-{{ $wnp_status }}" class="table table-bordered table-hover table-stripped" style="width: 100%;">
                        <thead>
                           <tr>
                              <th style="min-width: 50px;">Action</th>
                              <th style="min-width: 200px;">Project Name</th>
                              <th style="min-width: 100px;">Project Type</th>
                              <th style="min-width: 200px;">Developer</th>
                              <th style="min-width: 100px;">Status</th>
                              <th style="min-width: 100px;">Update Terakhir</th>
                              <th style="min-width: 50px;">Project ID</th>
                           </tr>
                        </thead>
                     </table>
                  </div>
                  @endforeach
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
@endsection
@section('custom-script')
   <script>
      var region = "{{ Session::get('region') }}";
      var status_list = {!! json_encode($status) !!};
      function DeleteProject(id, tag){
         while ((tag = tag.parentElement) && tag.nodeName.toUpperCase() !== 'TABLE');
         $.ajax({
            url: "{{ url('/prime_cms/delete') }}",
            method: "POST",
            data: {project_id: id},
            success: function(res){
               if(res.Status == "success"){
                  alertify.success(res.Message);
                  $('#'+tag.id).DataTable().ajax.reload();
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error("Something went wrong.");
            }
         });
      }
      function ChangeStatus(id, status){
         $.ajax({
            url: "{{ url('/prime_cms/change_status') }}",
            method: "POST",
            data: {project_id: id, status: status},
            success: function(res){
               if(res.Status == "success"){
                  alertify.success(res.Message);
                  $('#table-active').DataTable().ajax.reload();
                  $('#table-inactive').DataTable().ajax.reload();
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error("Something went wrong.");
            }
         });
      }
   </script>
   <script src="{{ asset('js/datatables/project-datatables.js') }}"></script>
@endsection