<div class="row">
   <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="box">
         <div class="box-header with-border">
            <h3 class="box-title">Primary Details</h3>
            <div class="box-tools pull-right">
               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
         </div>
         <div class="box-body" style="padding: 20px;">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-xs-12">
                  <label>Primary Photo<span class="validation-notice photos">photo minimal 1</span></label>
               </div>
            </div>
            <drop-uploader name="project_photo"></drop-uploader>
            <div class="row">
               <div class="col-lg-12 col-md-12 col-xs-12">
                  <div class="row">
                     <div class="col-lg-4 col-md-4 col-xs-12">
                        <label>Project Status</label>
                     </div>
                  </div>
                  <div class="row">
                     @foreach($status as $status_key => $wnp_status)
                     <div class="col-lg-4 col-md-4 col-xs-12">
                        <label class="radio-status">
                           <input type="radio" name="project_status" value="{{ $wnp_status->id }}">
                           <div>
                              {{ $wnp_status->name }}
                           </div>
                        </label>
                     </div>
                     @endforeach
                  </div>
               </div>
            </div>
            <div class="row mt-20">
               <div class="col-lg-6 col-md-6 col-xs-12">
                  <div class="form-group">
                     <label>Project Name</label>
                     <input type="text" class="form-control" name="project_name" placeholder="Insert Project Name" style="border-radius: 6px;">
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-xs-12">
                  <label>Price Range (IDR)</label>
                  <div class="custom-input-text">
                     <input type="text" class="custom-price" id="project_min_price" name="project_min_price" placeholder="Min Price" onkeypress="return validate_number(event, this);" onkeyup="addComa(this);">
                     <div class="divider"> - </div>
                     <input type="text" class="custom-price" id="project_max_price" name="project_max_price" placeholder="Max price" onkeypress="return validate_number(event, this);" onkeyup="addComa(this);">
                  </div>
               </div>
            </div>
            <div class="row mt-20">
               <div class="col-lg-12 col-md-12 col-xs-12">
                  <label>Address</label>
                  <div id="custom-map-control">
                     <div class="autocomplete-wrapper">
                        <span class="icon icon-search map-autocomplete-icon"></span>
                        <input id="map-autocomplete-input" class="controls" type="text" placeholder="Find Location">
                     </div>
                     <div class="drag-notice-wrapper">
                        <span class="icon icon-info drag-notice-icon"></span>
                        <span class="drag-notice-text">Drag the map to choose correct location</span>
                     </div>
                  </div>
                  <div id="custom-map-control-street-view">
                     <label class="check-section" style="margin:0;">
                        <input type="checkbox" name="is_street_view" data-name="is_street_view">
                        <span class="map-control"></span>
                        <a class="map-control" style="font-weight: 500; font-size: 14px;">Show street view</a>
                     </label>
                  </div>
                  <div class="box no-padding" style="position:relative;">
                     <span id="wnp-marker" style="background: url('{{ asset('assets/images/marker-default.svg') }}');"></span>
                     <div id="map" style="height: 400px;"></div>
                  </div>
               </div>
            </div>
            <div class="row mt-20">
               <div class="col-lg-12 col-md-12 col-xs-12">
                  <div class="form-group">
                     <label>Project Detail Address (complete)</label>
                     <input type="text" name="lat" id="lat" style="visibility:hidden;display:none;">
                     <input type="text" name="lng" id="lng" style="visibility:hidden;display:none;">
                     <textarea class="form-control" placeholder="Insert address of project detail" style="border-radius: 6px;" id="address" name="address"></textarea>
                  </div>
               </div>
            </div>
			
		
            <div class="row mt-20">
               <div class="col-lg-12 col-md-12 col-xs-12">
                  <div class="form-group">
                     <label>Address for main page (simple)</label>
                     <input type="text" class="form-control" placeholder="Insert address for main page" style="border-radius: 6px;" id="short_address" name="short_address">
                  </div>
               </div>
            </div>
            <div class="row mt-20">
               <div class="col-lg-6 col-md-6 col-xs-12">
                  <div class="form-group">
                     <label>E-brochure</label>
                     <div class="input-group">
                        <div class="input-display">Upload e-brochure pdf</div>
                        <a href="#" class="input-group-addon file-uploader file-browser-trigger">Upload</a>
                        <input type="hidden" name="ebrochure">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-xs-12">
                  <div class="form-group">
                     <label>Pricelist</label>
                     <div class="input-group">
                        <div class="input-display">Upload pricelist pdf</div>
                        <a href="#" class="input-group-addon file-uploader file-browser-trigger">Upload</a>
                        <input type="hidden" name="pricelist">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>