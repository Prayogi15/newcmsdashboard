<!-- PROJECT PLAN -->
<div class="row">
   <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="box">
         <div class="box-header with-border">
            <h3 class="box-title">Project Plan</h3>
            <div class="box-tools pull-right">
               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
         </div>
         <div class="box-body" style="padding: 20px;">
            <div class="row">
               <div class="col-lg-6 col-md-6 col-xs-12">
                  <div class="form-group">
                     <div class="input-group">
                        <div class="input-display">Upload project plan</div>
                        <a href="#" class="input-group-addon file-uploader file-browser-trigger">Upload</a>
                        <input type="hidden" name="project_plan">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- DEVELOPER INFO -->
<div class="row">
   <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="box">
         <div class="box-header with-border">
            <h3 class="box-title">Developer Details</h3>
            <div class="box-tools pull-right">
               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
         </div>
         <div class="box-body" style="padding: 20px;">
            <div class="row">
               <div class="col-lg-6 col-md-6 col-xs-12">
                  <div class="form-group">
                     <label>Developer Logo (Thumbnail)</label>
                     <div class="input-group">
                        <div class="input-display">Upload developer logo</div>
                        <a href="#" class="input-group-addon file-uploader file-browser-trigger">Upload</a>
                        <input type="hidden" name="developer_logo">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-xs-12">
                  <div class="form-group">
                     <label>Developer Image(Full Image)</label>
                     <div class="input-group">
                        <div class="input-display">Upload developer image</div>
                        <a href="#" class="input-group-addon file-uploader file-browser-trigger">Upload</a>
                        <input type="hidden" name="developer_image">
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-6 col-md-6 col-xs-12">
                  <div class="form-group">
                     <label>Developer Name</label>
                     <input type="text" class="form-control" name="developer_name" placeholder="Developer name" autocomplete="off" style="border-radius: 6px;">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-12 col-md-12 col-xs-12">
                  <div class="form-group">
                     <label>Developer Info</label>
                     <textarea class="form-control" name="developer_info" placeholder="Insert developer info" autocomplete="off" style="border-radius: 6px;" rows="8"></textarea>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- CONTACT INFO -->
<div class="row">
   <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="box">
         <div class="box-header with-border">
            <h3 class="box-title">Contact Info</h3>
            <div class="box-tools pull-right">
               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
         </div>
         <div class="box-body" style="padding: 20px;">
            <div class="row">
               <div class="col-lg-6 col-md-6 col-xs-12">
                  <div class="form-group">
                     <label>Developer Phone Number</label>
                     <input type="text" class="form-control" name="developer_phone" placeholder="Insert phone number" autocomplete="off" style="border-radius: 6px;">
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-xs-12">
                  <div class="form-group">
                     <label>Developer Email</label>
                     <input type="text" class="form-control" name="developer_email" placeholder="Insert email" autocomplete="off" style="border-radius: 6px;">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>