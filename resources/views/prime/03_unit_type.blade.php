<div class="row">
   <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="box">
         <div class="box-header with-border">
            <h3 class="box-title">Unit Type</h3>
            <div class="box-tools pull-right">
               <button type="button" class="btn btn-default" onclick="ClearTypeProject();" style="margin-right: 20px;"><i class="fa fa-trash-o"></i> Clear Type</button>
               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
         </div>
         <div class="box-body" style="padding: 20px;">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-xs-12">
                  <table id="table-unit" class="table table-bordered table-hover table-stripped" style="width: 100%;">
                     <thead>
                        <tr>
                           <th>Action</th>
                           <th>Type Name</th>
                           <th>Price</th>
                           <th>Land Area</th>
                           <th>Floor Area</th>
                           <th><img src="{{ asset('/assets/icon/icon-bedroom.svg') }}" style="width: 22px; height: 15px;"></th>
                           <th><img src="{{ asset('/assets/icon/icon-bathroom.svg') }}" style="width: 22px; height: 15px;"></th>
                           <th><img src="{{ asset('/assets/icon/icon-parking.svg') }}" style="width: 22px; height: 15px;"></th>
                           <th>Images</th>
                           <th>Floor Plan</th>
                        </tr>
                     </thead>
                     <tbody></tbody>
                  </table>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-12 col-md-12 col-xs-12">
                  <button type="button" id="modal-button" class="btn btn-default" data-toggle="modal" data-target="#modal-unit" style="float: left;">+ Add Type</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>