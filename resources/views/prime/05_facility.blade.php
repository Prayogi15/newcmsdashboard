<div class="row">
   <div class="col-lg-12 col-md-12 col-xs-12">
      <div class="box">
         <div class="box-header with-border">
            <h3 class="box-title">Facility</h3>
            <div class="box-tools pull-right">
               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
         </div>
         <div class="box-body" style="padding: 20px;">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-xs-12" id="facilities_section">
                  <!-- FACILITIES -->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>