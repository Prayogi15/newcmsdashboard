		<?php
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header("Cache-control: private");
		header("Content-Type: application/vnd.ms-excel; name=’excel’");
		$tgl=date('dmY');
		header("Content-disposition:  attachment; filename=data_kpr_". $start."-".$end.".xls");
		echo "<h2>Data KPR</h2>";
		echo "<h4> Periode " . $start  ."-". $end ." </h4>";
		echo "<h5>";
		
		echo "<table class='table table-bordered table-hover' border='1'>
					<thead style='background-color:#ccc'>
                    <tr> 
					<th></th>
					<th width='100px'>Bank</th>
					<th width='150px'>Office</th>
					<th width='100px'>Code</th>
                    <th width='180px'>Name</th>
					<th width='150px'>Email</th>
					<th width='120px'>Phone Number</th>
					<th width='120px'>NIK</th>
					<th width='50px'>PID</th>
					<th width='90px' >Price</th>
					<th width='90px' >Loan</th>
					<th width='50px' >Period</th>	
					<th width='150px' >Submit Date</th>			
                   </tr>
                  </thead>
					<tbody>";
					$n=1;
					foreach($data_kpr as $row){
				echo "<tr><td>". $n ."</td>
					  <td>". $row->bank_name."</td>
					  <td>". $row->office."</td>
					  <td>". $row->code."</td>
					  <td>". $row->fullname."</td>
					  <td>". $row->email."</td>
					  <td>". $row->phone_number."</td>
					  <td>". $row->nik."</td>
					  <td>". $row->pid."</td>
					  <td>". $row->price."</td>
					  <td>". $row->loan."</td>
					  <td>". $row->period."</td>
					   <td>". $row->created_at."</td></tr>";
					$n++;
					}
					echo "</tbody></table>";
					
		
		?>