@extends('template.template')
@section('active_kpr','class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         KPR 
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li >KPR</li>
		  <li class="active">List</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
	  <div class="col-lg-12 col-md-12 col-xs-12">
         <div class="box box-solid">
		 
		 <div class="box-header with-border">
		<h3 class="box-title">Table List</h3>
		<div class="box-tools pull-right">
		 <button class="btn btn-info" data-toggle="modal" data-target="#modal_email" title="Download Data" id="add_data"> <i class="fa fa-envelope-o"></i> List Email Receiver</button>
		
		  <button class="btn btn-primary" data-toggle="modal" data-target="#modal_download" title="Download Data" id="add_data"> <i class="fa fa-download"></i> Download</button>		  
		</div>
		<!-- /.box-tools -->
		</div>
            <div class="box-body">
			
			
			<div class="table-responsive">
               <table id="table-kpr" class="table table-bordered table-hover table-stripped" style="width: 100%;">
                  <thead>
                    <tr> 
					<th></th>
					<th>Bank</th>
					<th>Office</th>
					<th>Code</th>
                    <th>Name</th>
					<th>Email</th>
					<th>Phone Number</th>
					<th>NIK</th>
					<th>PID</th>
					<th>Price</th>
					<th>Loan</th>
					<th>Period</th>	
					<th>Submit Date</th>	
					<th>Remove</th>						
                   </tr>
                  </thead>
				  <tbody>
				  
				  </tbody>
               </table>
			   </div>
            </div>
         </div>
      </div>
	 </div> 
   </section>
   <!-- /.content -->

   
   <!-- modal add -->
	  <div class="modal fade" id="modal_download">
          <div class="modal-dialog">
		  	<form class="form-horizontal" id="form1" method="post" action="{{ url('/kpr/download/')}}">
			
			{!! csrf_field() !!}
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Download Data KPR</h4>
              </div>
              <div class="modal-body">
                <p>
				<div class="form-group">
                <label class="col-sm-4 control-label">Date range:</label>
				<div class="col-sm-8">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="daterange" id="daterange">
                </div>
                <!-- /.input group -->
              </div>
			  </div>
				
				
				</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
				 <button type="button" class="btn btn-info" id="sendemail">Send Email</button>
                <button type="submit" class="btn btn-primary" id="submitdata">Download</button>
              </div>
            </div>
            <!-- /.modal-content -->
		  </form>
		  </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
   
 
  <!-- modal email -->
	  <div class="modal fade" id="modal_email">
          <div class="modal-dialog">
		  	<form class="form-horizontal" id="form2" method="post" action="{{ url('/kpr/savelistemail/')}}">
			{!! csrf_field() !!}
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Email Receiver</h4>
              </div>
              <div class="modal-body">
                <p>
				<div class="form-group">
                <div class="col-sm-12 control-label">
				<textarea class="form-control" name="email" id="email">{{$list_email}}</textarea>
				</div>
			  </div>
				</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="submitdata2">Save</button>
              </div>
            </div>
            <!-- /.modal-content -->
		  </form>
		  </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
@endsection
@section('custom-script')
   <script>
   $(document).ready(function() {
	 //Date range picker
    $('#daterange').daterangepicker({locale: {
            format: 'YYYY/MM/DD'
	}
	});
	  
	 $('#table-kpr').DataTable({
			"processing": true,
            "serverSide": true,
			"bProcessing": true,
            "ajax":{
                     "url": "{{ url('/kpr/getdatakpr/') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
            "columns": [
				{ "data": "no","orderable": false}, //variabel sembarang
                { "data": "bank_name" },
                { "data": "office" },
                { "data": "code" },
                { "data": "fullname" },
				{ "data": "email" },
                { "data": "phone_number" },
				{ "data": "nik" },
				{ "data": "pid" },
				{ "data": "price" },
				{ "data": "loan" },
				{ "data": "period" },
				{ "data": "created_at" },
				{ "data": "remove","orderable": false } //variabel sembarang
            ]	,
				"order": [[12, 'desc' ]]			//dari created_at
		 });
		  
		 $( "#submitdata2" ).click(function() {
			var data = $("#form2").serialize();
			$.ajax({
			type : 'POST',
			url  : "{{ url('/kpr/savelistemail/')}}",
			data : data,
			beforeSend: function()
			{	
				$("#submitdata2").prop("disabled", true );
				$("#submitdata2").text("Loading ..");	
			},
			success :  function(response)
			   {		
					$("#submitdata2").prop("disabled", false );
					$("#submitdata2").text("Save");
					var data 	= JSON.parse(response);	
					var message	= 	data['message'];
					if(data['status']=="success"){			
						  alertify.success("Data Saved");
						   $('#modal_email').modal('toggle');
					}
					else if (data['status'] == "error"){
							alertify.error("Error");
						   $('#modal_email').modal('toggle');
					}
			  }
			});
				return false;
			});
		 
		 $( "#sendemail" ).click(function() {
			var data = $("#form1").serialize();
			$.ajax({
			type : 'POST',
			url  : "{{ url('/kpr/sendemail/')}}",
			data : data,
			beforeSend: function()
			{	
				$("#sendemail").prop("disabled", true );
				$("#sendemail").text("Loading ..");	
			},
			success :  function(response)
			   {		
					$("#sendemail").prop("disabled", false );
					$("#sendemail").text("Save");
					var data 	= JSON.parse(response);	
					var message	= 	data['message'];
					if(data['message']=="success"){			
						  alertify.success("Data Sent");
						   $('#modal_download').modal('toggle');
					}
					else if (data['status'] == "error"){
							alertify.error("Error Sent");
						   $('#modal_download').modal('toggle');
					}
			  }
			});
				return false;
			});
		 
		 
		 
   });
   
   function remove(id)
   {
	 if(confirm('Are you sure to delete this data ?')){  
	  $.ajax({
      method: 'get',
      url: "{{ url('/kpr/remove/')}}/"+id,
      data: {
        //'myString': scriptString,
        'ajax': true
      },  		
      success: function(data) {
       //$('#data').text(data);
	   alertify.success("Success Deleted data");
	   $('#table-kpr').DataTable().ajax.reload();
      }

    });
	   
	 }
	   
   }
   </script>
  
@endsection