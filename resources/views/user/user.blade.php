@extends('template.template')
@section('active_user', 'class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         User
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li>User</li>
      </ol>
   </section>

   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  @foreach($status as $status_key => $wnp_status)
                  <li @if($status_key == 0)class="active"@endif><a href="#{{ $wnp_status }}" data-toggle="tab">{{ strtoupper($wnp_status) }}</a></li>
                  @endforeach
               </ul>
               <div class="tab-content" style="padding: 10px;">
                  @foreach($status as $status_key => $wnp_status)
                  <div class="tab-pane <?php if($status_key == 0){echo("active");}?>" id="{{ $wnp_status }}">
                     <table id="table-{{ $wnp_status }}" class="table table-bordered table-hover table-stripped" style="width: 100%;">
                        <thead>
                           <tr>
                              <th style="min-width: 50px;">User ID</th>
                              <th style="min-width: 250px;">Email</th>
                              <th style="min-width: 250px;">Name</th>
                              <th style="min-width: 150px;">Phone</th>
                              <th style="min-width: 50px;">Join Date</th>
                              <th style="min-width: 50px;">Status</th>
                              <th style="min-width: 50px;">Action</th>
                           </tr>
                        </thead>
                     </table>
                  </div>
                  @endforeach
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
@endsection
@section('custom-script')
   <script>
      var region = "{{ Session::get("region") }}";
      var status_list = {!! json_encode($status) !!};
      function ChangeStatus(id,status_id,tag){
         while ((tag = tag.parentElement) && tag.nodeName.toUpperCase() !== 'TABLE');
         switch(status_id){
            case "active": var table_target = $("#table-active"); break;
            case "banned": var table_target = $("#table-banned"); break;
            case "watchlist": var table_target = $("#table-watchlist"); break;
         }
         $.ajax({
            url: "{{ url('/user/change_status') }}/"+id+"/"+status_id,
            type: "PUT",
            data: {
               "_token": "{{ csrf_token() }}"
            },
            success: function(res){
               if(res.Status == "success"){
                  alertify.success("Success change status.");
                  $('#'+tag.id).DataTable().ajax.reload();
                  table_target.DataTable().ajax.reload();
                  if(tag.id != "table-all"){
                     $("#table_all").ajax.reload();
                  }
               } else {
                  alertify.error("Something went wrong.");
               }
            }
         });
      }
   </script>
   <script src="{!! asset('js/datatables/user-datatables.js') !!}" type="text/javascript"></script>
@endsection