@extends('template.template')
@section('active_user', 'class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         User Details
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li>User</li>
         <li>Details</li>
      </ol>
   </section>

   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="box box-solid">
               <div class="box-body" style="margin: 20px;">
                  <div class="col-lg-8 col-md-8 col-xs-12">
                     <h4 style="margin: 10px 0px;">Profile</h4>
                     <div class="row">
                        <div class="col-lg-4 col-md-4 col-xs-12">
                           <div style="text-align: center;"><img src="@if($data['photo_url'] != ""){{ $data['photo_url'] }} @else {{ asset('assets/images/user_001.jpg') }} @endif" class="profile-user-img img-responsive img-circle" onerror="this.src='<?=url('assets/images/user_001.jpg');?>';" style="width:70%;"></div>
                        </div>
                        <div class="col-lg-8 col-md-8 col-xs-12">
                           <div class="table-responsive">
                              <table class="table">
                                 <tbody>
                                    <tr>
                                       <td>Name</td>
                                       <td> : <b>{{ $data['first_name'] }} {{ $data['last_name'] }}</b></td>
                                    </tr>
                                    <tr>
                                       <td>Email</td>
                                       <td> : <b>{{ $data['username'] }}</b></td>
                                    </tr>
                                    <tr>
                                       <td>User ID</td>
                                       <td> : <b>{{ $data['uid'] }}</b></td>
                                    </tr>
                                    <tr>
                                       <td>Phone</td>
                                       <td> : <b>{{ $data['country_code'] }} {{ $data['phone_number'] }}</b></td>
                                    </tr>
                                    <tr>
                                       <td>Member Since</td>
                                       <td> : <b>{{ $data['created_at'] }}</b></td>
                                    </tr>
                                    <tr>
                                       <td>Status</td>
                                       <td> : <b>{!! $status !!}</b></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-xs-12">
                     <h4 style="margin: 10px 0px;">Profile</h4>
                     <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                           Government ID <span class="pull-right">@if($data['gov_verified'] == '1') <div style="width:14px; height:14px; background-color: #38cd0d; border-radius: 50%;"></div> @else <div style="width:14px; height:14px; background-color: #989898; border-radius: 50%;"></div> @endif</span>
                        </li>
                        <li class="list-group-item">
                           Facebook <span class="pull-right">@if($data['facebook_id'] != '') <div style="width:14px; height:14px; background-color: #38cd0d; border-radius: 50%;"></div> @else <div style="width:14px; height:14px; background-color: #989898; border-radius: 50%;"></div> @endif</span>
                        </li>
                        <li class="list-group-item">
                           Google <span class="pull-right">@if($data['google_id'] != '') <div style="width:14px; height:14px; background-color: #38cd0d; border-radius: 50%;"></div> @else <div style="width:14px; height:14px; background-color: #989898; border-radius: 50%;"></div> @endif</span>
                        </li>
                        <li class="list-group-item">
                           Linkedin <span class="pull-right">@if($data['linkedin_id'] != '') <div style="width:14px; height:14px; background-color: #38cd0d; border-radius: 50%;"></div> @else <div style="width:14px; height:14px; background-color: #989898; border-radius: 50%;"></div> @endif</span>
                        </li>
                        <li class="list-group-item">
                           Email <span class="pull-right">@if($data['email_verified'] != '') <div style="width:14px; height:14px; background-color: #38cd0d; border-radius: 50%;"></div> @else <div style="width:14px; height:14px; background-color: #989898; border-radius: 50%;"></div> @endif</span>
                        </li>
                        <li class="list-group-item">
                           Phone <span class="pull-right">@if($data['phone_verified'] != '') <div style="width:14px; height:14px; background-color: #38cd0d; border-radius: 50%;"></div> @else <div style="width:14px; height:14px; background-color: #989898; border-radius: 50%;"></div> @endif</span>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="box box-solid">
               <div class="box-header with-border">
                  <h4 style="margin: 10px 0px;">Current Points: <b style="color:#1477ea;">{{ $point }}</b> Points</h4>
               </div>
               <div class="box-body" style="margin: 20px;">
                  <div class="col-lg-6 col-md-6 col-xs-12">
                     <h4 style="margin: 10px 0px;">Gain Points</h4>
                     <table id="table-gain" class="table table-bordered table-hover table-stripped" style="width:100%;">
                        <thead>
                           <tr>
                              <th style="min-width: 150px;">Title</th>
                              <th style="min-width: 50px;">Date</th>
                              <th style="min-width: 50px;">Amount</th>
                           </tr>
                        </thead>
                     </table>
                  </div>
                  <div class="col-lg-6 col-md-6 col-xs-12">
                     <h4 style="margin: 10px 0px;">Claim Points</h4>
                     <table id="table-claim" class="table table-bordered table-hover table-stripped" style="width:100%;">
                        <thead>
                           <tr>
                              <th style="min-width: 150px;">Title</th>
                              <th style="min-width: 50px;">Date</th>
                              <th style="min-width: 50px;">Amount</th>
                           </tr>
                        </thead>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="box box-solid">
               <div class="box-header with-border">
                  <h4 style="margin: 10px 0px;">Listing</h4>
               </div>
               <div class="box-body" style="margin: 20px;">
                  <div class="nav-tabs-custom">
                     <ul class="nav nav-tabs">
                        @foreach($status_listing as $status_key => $wnp_status)
                        <li @if($status_key == 0)class="active"@endif><a href="#{{ $wnp_status }}" data-toggle="tab">{{ strtoupper($wnp_status) }}</a></li>
                        @endforeach
                     </ul>
                     <div class="tab-content" style="padding: 10px;">
                        @foreach($status_listing as $status_key => $wnp_status)
                        <div class="tab-pane <?php if($status_key == 0){echo("active");}?>" id="{{ $wnp_status }}">
                           <table id="table-{{ $wnp_status }}" class="table table-bordered table-hover table-stripped" style="width:100%;">
                              <thead>
                                 <tr>
                                    <th style="min-width: 100px;">Property ID</th>
                                    <th style="min-width: 350px;">Address</th>
                                    <th style="min-width: 100px;">Status</th>
                                    <th style="min-width: 100px;">Type</th>
                                    <th style="min-width: 100px;">Last Updated</th>
                                    <th style="min-width: 50px;">Action</th>
                                 </tr>
                              </thead>
                           </table>
                        </div>
                        @endforeach
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
@endsection
@section('custom-script')
   <script>
      var region = "{{ Session::get("region") }}";
      var listing_status = {!! json_encode($status_listing) !!};
      var user_id = "{!! $data["id"] !!}";
      function ChangeStatus(id, status, tag){
         while ((tag = tag.parentElement) && tag.nodeName.toUpperCase() !== 'TABLE');
         switch(status){
            case 'active': var table_target = $("#table-active"); break;
            case 'archived': var table_target = $("#table-archived"); break;
            case 'sold': var table_target = $("#table-sold"); break;
            case 'deleted': var table_target = $("#table-deleted"); break;
         }
         $.ajax({
            url: "{{ url('/listing/update') }}/"+id+"/"+status,
            type: "PUT",
            data: {
               "_token": "{{ csrf_token() }}",
            },
            success: function(res){
               if(res.Status == "success"){
                  alertify.success(res.Message);
                  $('#'+tag.id).DataTable().ajax.reload();
                  table_target.DataTable().ajax.reload();
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error("Something went wrong.");
            }
         });
      }
   </script>
   <script src="{!! asset('js/datatables/user-points-datatables.js') !!}" type="text/javascript"></script>
   <script src="{!! asset('js/datatables/user-listings-datatables.js') !!}" type="text/javascript"></script>
@endsection