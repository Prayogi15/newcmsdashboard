@extends('template.template')

@section('active_listingspage','class=active')

@section('custom-css')

<style>
    .dropdown-menu a, 
    .dropdown-menu button {
        color: #777;
        padding: 3px 20px;
        display: block;
        width: 100%;
        text-align: left;
    }

    .dropdown-menu button {
        border: 0;
    }

    .dropdown-menu a:hover,
    .dropdown-menu button:hover {
        background-color: #e1e3e9;
        color: #333;
    }

    .dropdown-menu a > .glyphicon,
    .dropdown-menu a > .fa,
    .dropdown-menu a > .ion,
    .dropdown-menu button > .glyphicon,
    .dropdown-menu button > .fa,
    .dropdown-menu button > .ion {
        margin-right: 10px;
    }
</style>

@endsection

@section('content')

<section class="content-header">
    <h1>
        Listings Page
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li>Listings Page</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="nav-tabs-custom">
                <a href="{{ route('listingspages.create') }}" class="btn btn-default" style="float:right; margin: 10px;">+ Create Listings Page</a>
                <br><br>
                <div class="nav-content" style="padding: 10px;">
                    <table id="table-places" class="table table-bordered table-hover table-stripped" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Slug</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
   
@endsection

@section('custom-script')

<script>
    (function(window, undefined) {
        $('#table-places').DataTable({
            data: {!! $listingsPages !!},
            columns: [
                { data: 'name', orderable: true },
                { data: 'slug', orderable: true },
                {
                    render: function (data, type, row) {
                        return '' +
                        '<div class="btn-group pull-right">' +
                            '<button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">' +
                                '<i class="fa fa-navicon"></i>' +
                                '<i class="fa fa-caret-down"></i>' +
                            '</button>' +
                            '<ul class="dropdown-menu pull-right" role="menu">' +
                                '<li>' +
                                    '<a href="/listingspages/' + row.slug  +'/edit"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a>' +
                                '</li>' +
                                '<li>' +
                                    '<form action="/listingspages/' + row.slug +'" method="POST">' +
                                        '<input type="hidden" name="_method" value="DELETE">' +
                                        '{{ csrf_field() }}' +
                                        '<button><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>' +
                                    '</form>' +
                                '</li>' +
                            '</ul>' +
                        '</div>';
                    }
                }
            ],
            language: {
                aria: {
                    sortAscending: ": activate to sort column ascending",
                    sortDescending: ": activate to sort column descending"
                },
                emptyTable: "No data available in table",
                infoEmpty: "No records found",
                lengthMenu: "Show _MENU_",
                search: "Search:",
                zeroRecords: "No matching records found",
                processing: "Loading...",
                paginate: {
                    previous: "Prev",
                    next: "Next",
                    last: "Last",
                    first: "First"
                }
            },
        });
    })(this);
</script>

@endsection