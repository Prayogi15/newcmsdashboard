@extends('template.template')

@section('active_listingspage','class=active')

@section('content')

<section class="content-header">
    <h1>Create Listings Page</h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="{{ route('listingspages.index') }}"><i class="fa fa-file"></i>Listings Page</a></li>
        <li>Create</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-solid">
                <div class="box-body" style="margin: 20px;">
                    <form action="{{ route('listingspages.store') }}" method="POST" enctype="multipart/form-data" role="form">
                        {{ csrf_field() }}
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control" placeholder="Type name" required>
                                @if ($errors->first('name'))
                                    <span class="text-red">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="slug">Slug</label>
                                <input type="text" name="slug" id="slug" value="{{ old('slug') }}" class="form-control" placeholder="Type slug" required>
                                @if ($errors->first('slug'))
                                    <span class="text-red">{{ $errors->first('slug') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" id="title" value="{{ old('title') }}" class="form-control" placeholder="Type title" required>
                                @if ($errors->first('title'))
                                    <span class="text-red">{{ $errors->first('title') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="metaDescription">Meta Description</label>
                                <textarea name="meta_description" id="metaDescription" cols="30" rows="5" class="form-control" placeholder="Type meta description" style="resize: vertical;" required>{{ old('meta_description') }}</textarea>
                                @if ($errors->first('meta_description'))
                                    <span class="text-red">{{ $errors->first('meta_description') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="h1Text">&lt;h1&gt; Text</label>
                                <input type="text" name="h1_text" id="h1Text" value="{{ old('h1_text') }}" class="form-control" placeholder="Type &lt;h1&gt; text" required>
                                @if ($errors->first('h1_text'))
                                    <span class="text-red">{{ $errors->first('h1_text') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="content">Content</label>
                                <textarea name="content" id="content" cols="30" rows="15" class="form-control" placeholder="Type content" style="resize: vertical;" required>{{ old('content') }}</textarea>
                                @if ($errors->first('content'))
                                    <span class="text-red">{{ $errors->first('content') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                           <div class="form-group">
                              <label for="propertyType">Property Type</label>
                              <select id="propertyType" name="property_type" class="form-control" required>
                                 <option value="" disabled{{ old('property_type') ? '' : ' selected' }}>Choose property type</option>
                                 @foreach ($listingBuildings as $listingBuilding)
                                    <option value="{{ $listingBuilding->id }}"{{ old('property_type') === $listingBuilding->id ? ' selected' : '' }}>{{ $listingBuilding->type }}</option>
                                 @endforeach
                              </select>
                              @if ($errors->first('property_type'))
                                  <span class="text-red">{{ $errors->first('property_type') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                           <div class="form-group">
                              <label for="propertyStatus">Property Status</label>
                              <select id="propertyStatus" name="property_status" class="form-control" required>
                                 <option value="" disabled{{ old('property_status') ? '' : ' selected' }}>Choose property status</option>
                                 @foreach ($types as $type)
                                    <option value="{{ $type->id }}"{{ old('property_status') === $type->id ? ' selected' : '' }}>{{ $type->name }}</option>
                                 @endforeach
                              </select>
                              @if ($errors->first('property_status'))
                                  <span class="text-red">{{ $errors->first('property_status') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="col-xs-12">
                           <div class="form-group">
                              <label for="location">Location</label>
                              <input type="text" name="location" id="location" class="form-control" placeholder="Type location" required>
                              <input type="hidden" id="lat" name="lat" value="{{ old('lat') }}">
                              <input type="hidden" id="long" name="long" value="{{ old('long') }}">
                              @if ($errors->first('lat') || $errors->first('long'))
                                  <span class="text-red">Location is required</span>
                              @endif
                           </div>
                        </div>

                        <div class="col-xs-12 text-right" style="margin-bottom: 5px;">
                            <input type="submit" value="Submit" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('custom-script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    var locationInput = document.getElementById('location'),
        latInput = document.getElementById('lat'),
        longInput = document.getElementById('long');

    displayLocationInputLoadingIfNeeded();

    function initAutocomplete() {
        var autocomplete = new google.maps.places.Autocomplete(locationInput, { types: ['geocode'] });
        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace(),
                latLng = (place.geometry.location).toString(),
                latLng = latLng.replace('(', ''),
                latLng = latLng.replace(')', ''),
                spl = latLng.split(','),
                lat = spl[0],
                lng = spl[1];

            latInput.value = lat;
            longInput.value = lng;
        });

        if (latInput.value && longInput.value) {
            displayPlaceName();
        }
    }

    function getPlaceName(lat, lng, callback) {
        var geocoder = new google.maps.Geocoder;
        var latlng = {
            lat: parseFloat(lat),
            lng: parseFloat(lng)
        };
        geocoder.geocode({'location': latlng}, function(results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    callback(results[0].formatted_address);
                } else {
                    console.log('No results found');
                }
            } else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
    }

    function displayPlaceName() {
        getPlaceName(latInput.value, longInput.value, function(name) {
            locationInput.value = name;
            locationInput.placeholder = "Type location";
        });
    }

    function displayLocationInputLoadingIfNeeded() {
        if (latInput.value && longInput.value) {
            locationInput.placeholder = "Loading location please wait...";
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&region=id&callback=initAutocomplete" async defer></script>

@endsection