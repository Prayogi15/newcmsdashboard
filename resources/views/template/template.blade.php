<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WORKnPLAY Dashboard</title>
  
  <link rel="shortcut icon" type="image/png" href="{{ asset('/assets/images/favicon.png') }}">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('assets/theme/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('assets/theme/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('assets/theme/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('assets/theme/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('assets/theme/dist/css/skins/_all-skins.min.css') }}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ asset('assets/theme/bower_components/jvectormap/jquery-jvectormap.css') }}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ asset('assets/theme/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('assets/theme/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ asset('assets/theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

  
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('assets/theme/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  
  <!-- Alertify -->
  <link rel="stylesheet" href="{{ asset('vendor/alertify/css/alertify.min.css') }}">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  @yield('custom-css')
</head>
<body class="hold-transition skin-black-light fixed sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>WNP</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="{{ asset('assets/images/worknplay-logo.png') }}" style="width: 200px;"><b>WORKnPLAY</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
     <!-- <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
-->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Region -->
          <li class="dropdown region region-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span>{{ ucfirst(Session::get('region')) }}       <i class="fa fa-caret-down"></i></span>
            </a>
            <ul class="dropdown-menu">
              <li><a href="{{ url('/change_region/idn') }}">Indonesia</a></li>
              <li><a href="{{ url('/change_region/sgp') }}">Singapore</a></li>
            </ul>
          </li>
          <!-- Notifications -->
          {{-- <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li> --}}
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="{{ url('/admin/myprofile') }}">
              <img src="@if(Session::get('admin_photo') != ""){{ Session::get('admin_photo') }} @else {{ asset('assets/images/user_001.jpg') }} @endif" class="user-image" onerror="this.src='<?=url('assets/images/user_001.jpg');?>';">
              <span class="hidden-xs">{{ Session::get('admin_name') }}</span>
            </a>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="{{ url('/logout') }}"><i class="fa fa-sign-out"></i>Signout</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
    
	 <li @yield('active_home')><a href="{{ url('/home') }}"><i class="fa fa-home"></i> <span> Overview</span></a></li>
	  <li @yield('active_user')><a href="{{ url('/user') }}"><i class="fa fa-users"></i> <span> User</span></a></li>
    <li @yield('active_listing')><a href="{{ url('/listing') }}"><i class="fa fa-file-text"></i> <span> Listing</span></a></li>
    <li class="treeview @yield('active_admin')">
      <a href="#">
        <i class="fa fa-key"></i> <span> Admin Action</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        
		 <li @yield('active_admin_access')><a href="{{ url('/admin/list') }}"><i class="fa fa-circle-o"></i> Admin Access</a></li>
		<li @yield('active_admin_voucher')><a href="{{ url('/admin/voucher') }}"><i class="fa fa-circle-o"></i> Voucher</a></li>
        <li @yield('active_admin_govid')><a href="{{ url('/admin/government_id') }}"><i class="fa fa-circle-o"></i> Government ID</a></li>
        <li @yield('active_admin_reportedListing')><a href="{{ url('/admin/reported_listing') }}"><i class="fa fa-circle-o"></i> Reported Listing</a></li>
      </ul>
    </li>

		{{-- <li class="treeview @yield('active_statistics')">
      <a href="#">
        <i class="fa fa-bar-chart"></i> <span> Statistics</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="#"><i class="fa fa-circle-o"></i> Chart 1</a></li>
        <li><a href="#"><i class="fa fa-circle-o"></i> Chart 2</a></li>
        <li><a href="#"><i class="fa fa-circle-o"></i>Chart 3</a></li>
      </ul>
    </li> --}}

		{{-- <li class="treeview @yield('active_tnc')">
      <a href="#">
        <i class="fa fa-calendar-times-o"></i> <span> Term & Condition</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li @yield('active_tnc_main')><a href="#"><i class="fa fa-circle-o"></i> Main</a></li>
        <li @yield('active_tnc_point')><a href="#"><i class="fa fa-circle-o"></i> Point</a></li>
        <li @yield('active_tnc_corporate')><a href="#"><i class="fa fa-circle-o"></i> Corporate</a></li>
        <li @yield('active_tnc_prime')><a href="#"><i class="fa fa-circle-o"></i> Prime</a></li>
      </ul>
    </li> --}}

    <li class="treeview @yield('active_setting')">
      <a href="#">
        <i class="fa fa-cog"></i> <span> Settings</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        {{-- <li @yield('active_setting_help')><a href="{{ url('/setting/help-center/0') }}"><i class="fa fa-circle-o"></i> Help Center</a></li> --}}
        <li @yield('active_setting_advertisement')><a href="{{ url('/setting/advertisement') }}"><i class="fa fa-circle-o"></i> Advertisement</a></li>
        <li @yield('active_setting_point')><a href="{{ url('/setting/point') }}"><i class="fa fa-circle-o"></i> Point</a></li>
        {{-- <li @yield('active_setting_luckydraw')><a href="#"><i class="fa fa-circle-o"></i> Lucky Draw</a></li> --}}
      </ul>
    </li>
    <li @yield('active_corporate_cms')><a href="{{ url('/corporate_cms') }}"><i class="fa fa-suitcase"></i> <span> Corporate CMS</span></a></li>
    <li @yield('active_prime_cms')><a href="{{ url('/prime_cms') }}"><i class="fa fa-building"></i> <span> Prime CMS</span></a></li>
    {{-- <li @yield('active_admin_access')><a href="#"><i class="fa fa-lock"></i> <span> Admin Access</span></a></li> --}}
	
	 <li @yield('active_kpr')><a href="{{ url('/kpr/list') }}"><i class="fa fa-home"></i> <span> KPR</span></a></li>
	
       <li @yield('active_daily')><a href="{{ url('/daily') }}"><i class="fa fa-calendar-o"></i> <span> Daily Report</span></a></li>
	   
	  <li class="treeview @yield('active_tools')">
      <a href="#">
        <i class="fa fa-cog"></i> <span> Tools</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li @yield('active_tools_xml')><a href="{{ url('/tools/data_xml') }}"><i class="fa fa-circle-o"></i> Data XML </a></li>
       
      </ul>
    </li>
	   
	   
	   
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="https://worknplay.com">worknplay.com</a>.</strong> All rights
    reserved.
  </footer>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('assets/theme/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('assets/theme/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('assets/theme/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('assets/theme/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('assets/theme/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('assets/theme/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('assets/theme/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('assets/theme/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('assets/theme/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('assets/theme/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('assets/theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('assets/theme/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('assets/theme/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/theme/dist/js/adminlte.min.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('assets/theme/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('assets/theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<!-- Alertify -->
<script src="{{ asset('vendor/alertify/alertify.min.js') }}"></script>

<script src="{{ asset('js/public.js') }}"></script>
@yield('custom-script')

</body>
</html>
