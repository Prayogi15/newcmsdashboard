@extends('template.template')
@section('active_setting','active')
@section('active_setting_advertisement', 'class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Analytics
      </h1>
      <ol class="breadcrumb">
         <li><a href="/dashboard"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li>Analytics</li>
      </ol>
   </section>

   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  @foreach($tabs as $tabs_key => $wnp_tabs)
                  <li @if($tabs_key == 0)class="active"@endif><a href="#{{ $wnp_tabs }}" data-toggle="tab">{{ ucwords(str_replace('_', ' ',$wnp_tabs)) }}</a></li>
                  @endforeach
               </ul>
               <div class="tab-content" style="padding: 10px;">
                  @foreach($tabs as $tabs_key => $wnp_tabs)
                  <div class="tab-pane <?php if($tabs_key == 0){echo("active");}?>" id="{{ $wnp_tabs }}">
                     <table id="table-{{ $wnp_tabs }}" class="table table-bordered table-hover table-stripped" style="width: 100%;">
                        <thead>
                           @switch($wnp_tabs)
                              @case("daily_create_listing")
                              <tr>
                                 <th rowspan="2" style="min-width: 70px; text-align: center;">Date</th>
                                 <th rowspan="2" style="min-width: 70px; text-align: center;">Android</th>
                                 <th rowspan="2" style="min-width: 70px; text-align: center;">Ios</th>
                                 @for($i=0;$i<count($wnp_building);$i++)
                                 <th colspan="2" style="min-width: 100px; text-align: center;">{{ $wnp_building[$i] }}</th>
                                 @endfor
                              </tr>
                              <tr>
                                 @for($j=0;$j<count($wnp_building);$j++)
                                 <th style="min-width: 50px;">Sell</th>
                                 <th style="min-width: 50px;">Rent</th>
                                 @endfor
                              </tr>
                              @break

                              @case("daily_sign_up")
                              <tr>
                                 <th style="min-width: 70px;">Date</th>
                                 <th style="min-width: 70px;">Sign Up Only</th>
                                 <th style="min-width: 70px;">New User ID</th>
                              </tr>
                              @break

                              @case("daily_listing_list")
                              <tr>
                                 <th style="min-width: 70px;">Date</th>
                                 <th style="min-width: 70px;">Property ID</th>
                                 <th style="min-width: 150px;">Price</th>
                                 <th style="min-width: 70px;">Status</th>
                                 <th style="min-width: 70px;">Type</th>
                                 <th style="min-width: 100px;">Created By</th>
                                 <th style="min-width: 70px;">User ID</th>
                              </tr>
                              @break
                           @endswitch
                        </thead>
                     </table>
                  </div>
                  @endforeach
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
@endsection
@section('custom-script')
<script>
   var region = "{{ Session::get("region") }}";
   var tabs = {!! json_encode($tabs) !!};
</script>
<script src="{!! asset('js/datatables/analytics-datatables.js') !!}" type="text/javascript"></script>
@endsection