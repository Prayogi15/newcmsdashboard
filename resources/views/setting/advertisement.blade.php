@extends('template.template')
@section('active_setting','active')
@section('active_setting_advertisement', 'class=active')

@section('custom-css')

<style>
   .pac-container {
      z-index: 2000;
   }
</style>

@endsection

@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Setting
         <small>Advertisement</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li>Settings</li>
         <li class="active">Advertisement</li>
      </ol>
   </section>

   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="nav-tabs-custom">
               <button type="button" id="modal-create" class="btn btn-default" data-toggle="modal" data-target="#modal-default" style="float: right; margin:5px;">+ Add Ads</button>
               <ul class="nav nav-tabs">
                  @foreach($position as $position_key => $wnp_position)
                  <li @if($position_key == 0)class="active"@endif><a href="#{{ $wnp_position }}" data-toggle="tab">{{ ucwords(str_replace('_', ' ',$wnp_position)) }}</a></li>
                  @endforeach
               </ul>
               <div class="tab-content" style="padding: 10px;">
                  @foreach($position as $position_key => $wnp_position)
                  <div class="tab-pane <?php if($position_key == 0){echo("active");}?>" id="{{ $wnp_position }}">
                     <table id="table-{{ $wnp_position }}" class="table table-bordered table-hover table-stripped" style="width: 100%;">
                        <thead>
                           <tr>
                              <th style="min-width: 30px;">No</th>
                              <th style="min-width: 50px;">Action</th>
                              <th style="min-width: 100px;">Order</th>
                              <th style="min-width: 250px;">Ads Name</th>
                              <th style="min-width: 70px;">Type</th>
                              <th style="min-width: 250px;">Link</th>
                              @if($wnp_position == "list_view")
                              <th style="min-width: 100px;">Title</th>
                              <th style="min-width: 250px;">Description</th>
                              @endif
                              <th style="min-width: 50px;">Image</th>
                              <th style="min-width: 50px;">Clicks</th>
                              <th style="min-width: 100px;">Date Start</th>
                              <th style="min-width: 100px;">Date End</th>
                              <th style="min-width: 100px;">Last Updated</th>
                              <th style="min-width: 50px;">Status</th>
                              
                           </tr>
                        </thead>
                     </table>
                  </div>
                  @endforeach
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
<div class="modal fade" id="modal-default">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title" id="modal-title" style="text-align: center;">ADD ADS</h3>
         </div>
         <div class="modal-body" style="padding: 10px 50px;">
            <form id="form-ads">
            {{ csrf_field() }}
            <div class="row">
               <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                     <label for="name">Name</label>
                     <input type="text" name="name" id="name" class="form-control" placeholder="Type title">
                     <input type="hidden" name="id" id="id">
                  </div>
               </div>
               <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                     <label for="position">Position</label>
                     <select id="position" name="position" class="form-control">
                        <option value="" selected disabled>Choose position</option>
                        <option value="home">Home</option>
                        <option value="listview">List View</option>
                        <option value="detail">Detail</option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                     <label for="type">Type</label>
                     <select id="type" name="type" class="form-control">
                        <option value="" selected disabled>Choose type</option>
                        <option value="view_only">View Only</option>
                        <option value="website">Website</option>
                        <option value="explore">Explore</option>
                        <option value="project">Listing Project</option>
                     </select>
                  </div>
               </div>
               <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                     <label for="platform">Device</label>
                     <select id="platform" name="platform" class="form-control">
                        <option value="" selected disabled>Choose device</option>
                        <option value="website">Website</option>
                        <option value="mobile">Mobile</option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                     <label for="image">Image</label>
                     <div style="height:0px;overflow:hidden">
                        <input type="file" id="file_image" name="file_image" accept="image/*"/>
                        <input type="text" class="form-control" name="url-img" id="url-img">
                     </div>
                     <img src="" id="img02" style="width: 40px; height: 40px; display:none; margin-right:20px;">
                     <button type="button" class="btn btn-default" id="upload-btn">Upload</button>
                  </div>
               </div>
               <div class="col-md-6 col-xs-12" id="col-project" style="display: none;">
                  <div class="form-group">
                     <label for="project">Project</label>
                     <select id="project" name="project" class="form-control">
                        <option value="" selected disabled>Choose project</option>
                        @foreach ($projects as $project)
                           <option value="{{ $project->id }}">{{ $project->name }}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="col-md-6 col-xs-12" id="col-location" style="display: none;">
                  <div class="form-group">
                     <label for="location">Location</label>
                     <input type="text" name="location" id="location" class="form-control" placeholder="Type location">
                     <input type="hidden" id="lat" name="lat">
                     <input type="hidden" id="lng" name="lng">
                  </div>
               </div>
            </div>
            <div class="row" id="row-property-type-status" style="display: none;">
               <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                     <label for="propertyType">Property Type</label>
                     <select id="propertyType" name="property_type" class="form-control">
                        <option value="" selected disabled>Choose property type</option>
                        @foreach ($listing_buildings as $listing_building)
                           <option value="{{ $listing_building->id }}">{{ $listing_building->type }}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                     <label for="propertyStatus">Property Status</label>
                     <select id="propertyStatus" name="property_status" class="form-control">
                        <option value="" selected disabled>Choose property status</option>
                        @foreach ($types as $type)
                           <option value="{{ $type->id }}">{{ $type->name }}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
            </div>
            <div class="row" id="row-price" style="display: none;">
               <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                     <label for="min_price">Min. Price (IDR)</label>
                     <input type="text" name="min_price" id="min_price" class="form-control" placeholder="Min. Price">
                  </div>
               </div>
               <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                     <label for="max_price">Max. Price (IDR)</label>
                     <input type="text" name="max_price" id="max_price" class="form-control" placeholder="Max. Price">
                  </div>
               </div>
            </div>
            <div class="row" id="row-website" style="display: none;">
               <div class="col-md-12 col-xs-12">
                  <div class="form-group">
                     <label for="link">Link</label>
                     <input type="text" name="link" id="link" class="form-control" placeholder="Type link">
                  </div>
               </div>
            </div>
            <div class="row" id="row-listview" style="display: none;">
               <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                     <label for="title">Title</label>
                     <input type="text" name="title" id="title" class="form-control" placeholder="Type title">
                  </div>
               </div>
               <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                     <label for="description">Description</label>
                     <input type="text" name="description" id="description" class="form-control" placeholder="Type title">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                     <label for="date_start">Date Start</label>
                     <div class="input-group date">
                        <input type="text" class="form-control pull-left" name="date_start" id="date_start">
                        <div class="input-group-addon">
                           <i class="fa fa-calendar"></i>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-xs-12">
                  <div class="form-group">
                     <label for="date_end">Date End</label>
                     <div class="input-group date">
                        <input type="text" class="form-control pull-left" name="date_end" id="date_end">
                        <div class="input-group-addon">
                           <i class="fa fa-calendar"></i>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" id="modal-btn" onclick="CreateAds();" class="btn btn-primary" style="padding:10px 50px;">Create</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="modal-img">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title" id="image-title" style="text-align: center;"></h3>
         </div>
         <div class="modal-body" style="padding: 10px 50px;">
            <img id="img01" style="width:100%;">
         </div>
      </div>
   </div>
</div>
@endsection
@section('custom-script')
   <script>
      var region = "{{ Session::get("region") }}";
      var array_position = {!! json_encode($position) !!};
      var current_tag;
      $('#modal-create').on('click', function(){
         $('#modal-title').html("ADD ADS");
         $('#modal-btn').attr('onclick', 'CreateAds()');
         $('#modal-btn').html("Create");
         $("#img02").hide();
         $('#form-ads')[0].reset();
      });
      $('#position').on('change', function(){
         if($('#position').val() == "listview"){
            $('#row-listview').show();
         } else {
            $('#title').val("");
            $('#description').val("");
            $('#row-listview').hide();
         }
      });

      function makeWebsiteInputsVisible(isVisible) {
         $('#link').val('');
         $('#row-website').css('display', isVisible ? 'block' : 'none');
      }

      function makeExploreInputsVisible(isVisible) {
         $('#location').val('');
         $('#propertyType').val('');
         $('#propertyStatus').val('');
         $('#min_price').val('');
         $('#max_price').val('');
         $('#col-location').css('display', isVisible ? 'block' : 'none');
         $('#row-property-type-status').css('display', isVisible ? 'block' : 'none');
         $('#row-price').css('display', isVisible ? 'block' : 'none');
      }

      function makeListingProjectInputsVisible(isVisible) {
         $('#project').val('');
         $('#col-project').css('display', isVisible ? 'block' : 'none');
      }

      $('#type').on('change', function(){
         var type = $(this).val();
         updateInputsVisibility(type);
      });

      function updateInputsVisibility(type) {
         if (type === 'website') {
            makeWebsiteInputsVisible(true);
            makeExploreInputsVisible(false);
            makeListingProjectInputsVisible(false);
         } else if (type === 'explore') {
            makeWebsiteInputsVisible(false);
            makeExploreInputsVisible(true);
            makeListingProjectInputsVisible(false);
         } else if (type === 'project') {
            makeWebsiteInputsVisible(false);
            makeExploreInputsVisible(false);
            makeListingProjectInputsVisible(true);
         } else {
            makeWebsiteInputsVisible(false);
            makeExploreInputsVisible(false);
            makeListingProjectInputsVisible(false);
         }
      }

      $('#date_start').datepicker({
         autoclose: true,
         format: "dd M yyyy"
      });
      $('#date_end').datepicker({
         autoclose: true,
         format: "dd M yyyy"
      });
      $('#upload-btn').on('click', function(){
         $("#file_image").click();
      });
      $("#file_image").on('change', function(){ 
         var myFormData = new FormData();
         myFormData.append('file_image', $('#file_image').prop('files')[0]);
         myFormData.append('public_key', 'advertise/');
         $.ajax({
            type: "POST",
            url: "{{ url('/upload_img') }}",
            data: myFormData,
            contentType: false,
            processData: false,
            cache:false,
            success: function(res){
               if(res.Status == "success"){
                  alertify.success(res.Message);
                  console.log(res.Path);
                  $("#url-img").val(res.Path);
                  $("#img02").attr('src', res.Path);
                  $("#img02").show();
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error('Something went wrong.');
            }
         });
      });
      function EditAds(id, tag){
         while ((tag = tag.parentElement) && tag.nodeName.toUpperCase() !== 'TABLE');
         current_tag = tag;
         $.ajax({
            url: "{{ url('/setting/advertisement/') }}/"+id,
            type: "GET",
            success: function(res){
               var data = res.Data;
               if (res.Status == "success"){
                  $('#modal-default').modal('show');
                  $('#modal-title').html("EDIT ADS");
                  $('#modal-btn').attr('onclick', 'UpdateAds()');

                  $('#id').val(data.id);
                  $('#name').val(data.name);
                  $("#position option[value='" + data.position +"']").attr("selected", true);
                  if (data.position == 'listview'){
                     $('#row-listview').show();
                     $('#title').val(data.title);
                     $('#description').val(data.description);
                  } else {
                     $('#title').val("");
                     $('#description').val("");
                     $('#row-listview').hide();
                  }
                  $("#type option[value='" + data.type +"']").attr("selected", true);

                  $('#type').val(data.type);
                  $('#platform').val(data.platform);

                  updateInputsVisibility(data.type);

                  if (data.type == 'website'){
                     $('#link').val(data.link);
                  } else if (data.type == 'explore') {
                     var filter = JSON.parse(data.filter);
                     $('#lat').val(filter.lat);
                     $('#lng').val(filter.long);

                     getPlaceName(filter.lat, filter.long, function(name) {
                        $('#location').val(name);
                     });

                     $('#propertyType').val(filter.building_id);
                     $('#propertyStatus').val(filter.type_id);
                     $('#min_price').val(filter.price_min);
                     $('#max_price').val(filter.price_max);
                  } else if (data.type == 'project') {
                     $('#project').val(data.project_id);
                  }

                  $('#url-img').val(data.image);
                  $('#date_start').val(data.date_start);
                  $('#date_end').val(data.date_end);
                  $('#modal-btn').html("Update");
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error('Something went wrong.');
            }
         });
      }
      function UpdateAds(){
         var data = $("#form-ads").serializeArray();
         data.push({ name : "region", value: region },{ name:"user_id", value: "{{ Session::get('admin_id') }}"});
         $.ajax({
            url: "{{ url('/setting/advertisement/update_ads') }}",
            type: "POST",
            data: data,
            success: function(res){
               console.log(res);
               var data = res.Data
               if(res.Status == "success"){
                  alertify.success(res.Message);
                  $('#modal-default').modal('hide');
                  $('#'+current_tag.id).DataTable().ajax.reload();
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res) {
               console.log(res);
               alertify.error('Something went wrong.');
            }
         });
      }
      function CreateAds(){
         var data = $("#form-ads").serializeArray();
         data.push({ name : "region", value: region },{ name:"user_id", value: "{{ Session::get('admin_id') }}"});
         console.log(data);
         $.ajax({
            url: "{{ url('/setting/advertisement/create_ads') }}",
            type: "POST",
            data: data,
            success: function(res){
               if(res.Code == 200){
                  alertify.success(res.Message);
                  $('#modal-default').modal('hide');
                  $('#table-'+$('#position').val()).DataTable().ajax.reload();
               } else {
                  console.log('res is: \n');
                  console.log(res);
                  // alertify.error(res.Message);
               }
            },
            error: function(xhr, status, error) {
               console.log(xhr);
               console.log(error);
               console.log(status);
               console.log(xhr.responseText);
               alertify.error('Something went wrong.');
            }
         });
         return false;
      }
      function DeleteAds(id, tag){
         while ((tag = tag.parentElement) && tag.nodeName.toUpperCase() !== 'TABLE');
         $.ajax({
            url: "{{ url('/setting/advertisement/delete') }}",
            type: "DELETE",
            data: { user_id : "{{ Session::get('admin_id') }}", id: id },
            success: function(res){
               if(res.Status == "success"){
                  alertify.success(res.Message);
                  $('#'+tag.id).DataTable().ajax.reload();
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error('Something went wrong.');
            }
         });
      }
      function Move(id, flag, tag){
         while ((tag = tag.parentElement) && tag.nodeName.toUpperCase() !== 'TABLE');
         $.ajax({
            url: "{{ url('/setting/advertisement/move') }}",
            type: "POST",
            data: { id : id, flag: flag },
            success: function(res){
               if(res.Status == "success"){
                  alertify.success(res.Message);
                  $('#'+tag.id).DataTable().ajax.reload();
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error('Something went wrong.');
            }
         });
      }
      function ShowImg(url, name){
         $('#image-title').html(name);
         $("#img01").attr('src', url);
      }

      function initAutocomplete() {
         var locationInput = document.getElementById('location');
         console.log(locationInput);
         var autocomplete = new google.maps.places.Autocomplete(locationInput, { types: ['geocode'] });
         autocomplete.addListener('place_changed', function() {
            let place = autocomplete.getPlace();
            let latLng = (place.geometry.location).toString()
               latLng = latLng.replace('(', ''),
               latLng = latLng.replace(')', '') ,
               spl = latLng.split(',')  ,
               lat = spl[0] ,
               lng = spl[1];

            $("#lat").val(lat)
            $("#lng").val(lng)
         });
      }

      function getPlaceName(lat, lng, callback) {
         var geocoder = new google.maps.Geocoder;
         var latlng = {
            lat: parseFloat(lat),
            lng: parseFloat(lng)
         };
         geocoder.geocode({'location': latlng}, function(results, status) {
            if (status === 'OK') {
               if (results[0]) {
                  callback(results[0].formatted_address);
               } else {
                  console.log('No results found');
               }
            } else {
               console.log('Geocoder failed due to: ' + status);
            }
         });
      }
   </script>
   <script src="{!! asset('js/datatables/ads-datatables.js') !!}" type="text/javascript"></script>
   <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&region=id&callback=initAutocomplete" async defer></script>
@endsection