@extends('template.template')
@section('active_setting','active')
@section('active_setting_point', 'class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Setting
         <small>Point</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li>Settings</li>
         <li class="active">Point</li>
      </ol>
   </section>

   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  @foreach($tabs as $tab_key => $wnp_tab)
                  <li @if($tab_key == 0)class="active"@endif><a href="#{{ $wnp_tab }}" data-toggle="tab">{{ ucwords(str_replace('_', ' ',$wnp_tab)) }}</a></li>
                  @endforeach
               </ul>
               <div class="tab-content" style="padding: 10px;">
                  @foreach($tabs as $tab_key => $wnp_tab)
                  <div class="tab-pane <?php if($tab_key == 0){echo("active");}?>" id="{{ $wnp_tab }}">
                     @if($wnp_tab == "point_gain")
                     <button type="button" class="btn btn-default btn-model" data-toggle="modal" data-target="#modal-rule" target-id="form-rule" style="float: right; margin:5px;">+ Add Rule</button>
                     @else
                     <button type="button" class="btn btn-default btn-model" data-toggle="modal" data-target="#modal-voucher" target-id="form-voucher" style="float: right; margin:5px;">+ Add Voucher</button>
                     @endif
                     <table id="table-{{ $wnp_tab }}" class="table table-bordered table-hover table-stripped" style="width: 100%;">
                        <thead>
                           <tr>
                              @if($wnp_tab == "point_gain")
                              <th style="min-width: 150px;">Title</th>
                              <th style="min-width: 50px;">Type</th>
                              <th style="min-width: 50px;">Amount</th>
                              <th style="min-width: 50px;">Status</th>
                              <th style="min-width: 50px;">Region</th>
                              <th style="min-width: 150px;">Last Updated</th>
                              <th style="min-width: 50px;">Action</th>
                              @else
                              <th style="min-width: 150px;">Title</th>
                              <th style="min-width: 100px;">Voucher Code</th>
                              <th style="min-width: 50px;">Voucher Value</th>
                              <th style="min-width: 50px;">Status</th>
                              <th style="min-width: 50px;">Action</th>
                              @endif
                           </tr>
                        </thead>
                     </table>
                  </div>
                  @endforeach
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
<!-- MODAL RULE -->
<div class="modal fade" id="modal-rule">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title" id="rule-title" style="text-align: center;">ADD POINT RULE</h3>
         </div>
         <div class="modal-body" style="padding: 10px 50px;">
            <form id="form-rule">
               {{ csrf_field() }}
               <div class="row">
                  <div class="col-md-6 col-xs-12">
                     <div class="form-group">
                        <label for="rule_title">Title</label>
                        <input type="text" name="rule_title" id="rule_title" class="form-control" placeholder="Type title">
                        <input type="hidden" name="rule_id" id="rule_id">
                     </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                     <div class="form-group">
                        <label for="rule_param_check">Parameter Check</label>
                        <input type="text" name="rule_param_check" id="rule_param_check" class="form-control" placeholder="Parameter Check">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6 col-xs-12">
                     <div class="form-group">
                        <label for="rule_amount">Amount</label>
                        <input type="text" name="rule_amount" id="rule_amount" class="form-control" placeholder="Type Amount" onkeypress="return Digits(event);">
                     </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                     <div class="form-group">
                        <label for="rule_param_operator">Parameter Operator</label>
                           <input type="text" name="rule_param_operator" id="rule_param_operator" class="form-control" placeholder="Parameter Operator">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6 col-xs-12">
                     <div class="form-group">
                        <label for="rule_img">Image</label>
                        <div style="height:0px;overflow:hidden">
                           <input type="file" id="rule_img" name="rule_img" accept="image/*" target-id="rule_url"/>
                        </div>
                        <div class="input-group">
                           <input type="text" name="rule_url" id="rule_url" class="form-control" placeholder="Image link">
                           <span class="input-group-btn">
                              <button type="button" class="btn btn-info btn-flat btn-upload" id="rule_btn" target-id="rule_img">UPLOAD</button>
                           </span>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                     <div class="form-group">
                        <label for="rule_param_value">Parameter Value</label>
                           <input type="text" name="rule_param_value" id="rule_param_value" class="form-control" placeholder="Parameter Value">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6 col-xs-12">
                     <div class="form-group">
                        <label for="rule_note">Note</label>
                        <input type="text" name="rule_note" id="rule_note" class="form-control" placeholder="Type Note">
                     </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                     <div class="form-group">
                        <label for="rule_period">Period</label>
                           <input type="text" name="rule_period" id="rule_period" class="form-control" placeholder="Period">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6 col-xs-12">
                     <div class="form-group">
                        <label for="rule_unique">Unique Object</label>
                        <input type="text" name="rule_unique" id="rule_unique" class="form-control" placeholder="Unique Object">
                     </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                     <div class="form-group">
                        <label for="rule_max_activity">Max. Activity in Period</label>
                           <input type="text" name="rule_max_activity" id="rule_max_activity" class="form-control" placeholder="Max. Activity in Period">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-offset-6 col-md-6 col-xs-12">
                     <div class="checkbox">
                        <label>
                           <input type="checkbox" name="rule_flag_max_activity" id="rule_flag_max_activity">
                           Not set max. activity period
                        </label>
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" id="modal-rule-btn" class="btn btn-primary btn-save" style="padding:10px 50px;" target-id="form-rule">Save</button>
         </div>
      </div>
   </div>
</div>
<!-- END MODAL RULE -->

<!-- MODAL VOUCHER -->
<div class="modal fade" id="modal-voucher">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title" id="voucher-title" style="text-align: center;">ADD VOUCHER</h3>
         </div>
         <div class="modal-body" style="padding: 10px 50px;">
            <form id="form-voucher">
               {{ csrf_field() }}
               <div class="row">
                  <div class="form-group">
                     <label for="voucher_code">Voucher Code</label>
                     <input type="text" name="voucher_code" id="voucher_code" class="form-control" placeholder="Type Voucher Code">
                     <input type="hidden" name="voucher_id" id="voucher_id">
                  </div>
               </div>
               <div class="row">
                  <div class="form-group">
                     <label for="voucher_value">Voucher Value</label>
                     <input type="text" name="voucher_value" id="voucher_value" class="form-control" placeholder="Type Voucher Value" onkeypress="return Digits(event);">
                  </div>
               </div>
               <div class="row">
                  <div class="form-group">
                     <label for="expired_date">Expired Date</label>
                     <div class="input-group date">
                        <input type="text" class="form-control pull-left" name="expired_date" id="expired_date">
                        <div class="input-group-addon">
                           <i class="fa fa-calendar"></i>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group">
                     <label for="voucher_url">Image</label>
                     <div style="height:0px;overflow:hidden">
                        <input type="file" id="voucher_img" name="voucher_img" accept="image/*" target-id="voucher_url"/>
                     </div>
                     <div class="input-group">
                        <input type="text" name="voucher_url" id="voucher_url" class="form-control" placeholder="Image link">
                        <span class="input-group-btn">
                           <button type="button" class="btn btn-info btn-flat btn-upload" id="voucher_btn" target-id="voucher_img">UPLOAD</button>
                        </span>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-12 col-md-12 col-xs-12">
                     <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                           @foreach($lang as $lang_key => $wnp_lang)
                           <li @if($lang_key == 0)class="active"@endif><a href="#voucher-{{ $wnp_lang }}" data-toggle="tab">{{ strtoupper($wnp_lang) }}</a></li>
                           @endforeach
                        </ul>
                        <div class="tab-content">
                           @foreach($lang as $lang_key => $wnp_lang)
                           <div class="tab-pane<?php if($lang_key == 0){echo(" active");}?>" id="voucher-{{ $wnp_lang }}">
                              <div class="row">
                                 <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group">
                                       <label for="{{ $wnp_lang }}_title">{{ strtoupper($wnp_lang) }} Title</label>
                                          <input type="text" name="{{ $wnp_lang }}_voucher_title" id="{{ $wnp_lang }}_voucher_title" class="form-control">
                                          <input type="hidden" name="{{ $wnp_lang }}_voucher_id" id="{{ $wnp_lang }}_voucher_id">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group">
                                       <label for="{{ $wnp_lang }}_voucher_detail">{{ strtoupper($wnp_lang) }} Detail</label>
                                       <textarea name="{{ $wnp_lang }}_voucher_detail" id="{{ $wnp_lang }}_voucher_detail" class="form-control"></textarea>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group">
                                       <label for="{{ $wnp_lang }}_voucher_how_to">{{ strtoupper($wnp_lang) }} How to get</label>
                                       <textarea name="{{ $wnp_lang }}_voucher_how_to" id="{{ $wnp_lang }}_voucher_how_to" class="form-control"></textarea>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group">
                                       <label for="{{ $wnp_lang }}_voucher_tnc">{{ strtoupper($wnp_lang) }} TNC</label>
                                       <textarea name="{{ $wnp_lang }}_voucher_tnc" id="{{ $wnp_lang }}_voucher_tnc" class="form-control"></textarea>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group">
                                       <label for="{{ $wnp_lang }}_voucher_note">{{ strtoupper($wnp_lang) }} Note</label>
                                       <textarea name="{{ $wnp_lang }}_voucher_note" id="{{ $wnp_lang }}_voucher_note" class="form-control"></textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           @endforeach
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" id="modal-voucher-btn" class="btn btn-primary btn-save" style="padding:10px 50px;" target-id="form-voucher">Save</button>
         </div>
      </div>
   </div>
</div>
<!-- END MODAL VOUCHER -->
@endsection
@section('custom-script')
   <script>
      $('#expired_date').datepicker({
         autoclose: true,
         format: "dd M yyyy"
      });
      var region = "{{ Session::get("region") }}";
      var tabs = {!! json_encode($tabs) !!};
      var language = {!! json_encode($lang) !!};
      var url_point = "{{ url('/setting/point/create_point') }}";
      var url_voucher = "{{ url('/setting/point/create_voucher') }}";
      function EditPoint(id){
         url_point = "{{ url('/setting/point/update_point') }}";
         $.ajax({
            type: "GET",
            url: "{{ url('/setting/point/get_point') }}/"+id,
            success: function(res){
               if(res.Status == "success"){
                  $('#modal-rule').modal('show');
                  $('#rule-title').html("EDIT POINT RULE");
                  var data = res.Data;
                  $('#rule_title').val(data.name);
                  $('#rule_id').val(id);
                  $('#rule_amount').val(data.amount);
                  $('#rule_url').val(data.picture);
                  $('#rule_note').val(data.note);
                  $('#rule_unique').val(data.object);
                  $('#rule_param_check').val(data.param_check);
                  $('#rule_param_operator').val(data.param_operator);
                  $('#rule_param_value').val(data.param_value);
                  $('#rule_period').val(data.param_value);
                  if(data.max_activity_in_period == null || data.max_activity_in_period == ''){
                     $('#rule_flag_max_activity').attr('checked', true);
                     $("#rule_max_activity").val('');
                     $("#rule_max_activity").prop('readonly', true);
                  }
               }
            }
         });
      }
      function EditVoucher(id){
         url_voucher = "{{ url('/setting/point/update_voucher') }}";
         $.ajax({
            type: "GET",
            url: "{{ url('/setting/point/get_voucher') }}/"+id,
            success: function(res){
               if(res.Status == "success"){
                  $('#modal-voucher').modal('show');
                  $('#voucher-title').html("EDIT VOUCHER");
                  var detail = res.Data.Detail;
                  console.log(detail);
                  $('#voucher_id').val(detail.id);
                  $('#voucher_code').val(detail.code);
                  $('#voucher_value').val(detail.cost);
                  $('#voucher_url').val(detail.picture);
                  $('#expired_date').val(detail.expired_at);

                  for(var i=0; i<language.length;i++){
                     var data_detail;
                     switch (language[i]){
                        case "en": data_detail = res.Data.English; break;
                        case "id": data_detail = res.Data.Indonesia; break;
                     }
                     $('#'+language[i]+'_voucher_title').val(data_detail.name);
                     $('#'+language[i]+'_voucher_id').val(data_detail.id);
                     $('#'+language[i]+'_voucher_detail').text(data_detail.details);
                     $('#'+language[i]+'_voucher_how_to').text(data_detail.how_to_get);
                     $('#'+language[i]+'_voucher_tnc').text(data_detail.tnc);
                     $('#'+language[i]+'_voucher_note').text(data_detail.note);
                  }
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error("Something went wrong.");
            }
         });
      }
      $('.btn-model').on('click', function(){
         var target = $(this).attr('target-id');
         if(target == "form-rule"){
            $('#form-rule')[0].reset();
            $('#rule-title').html("ADD POINT RULE");
            url_point = "{{ url('/setting/point/create_point') }}";
         } else if(target == "form-voucher"){
            $('#form-voucher')[0].reset();
            $('#voucher-title').html("ADD VOUCHER");
            url_voucher = "{{ url('/setting/point/create_voucher') }}";
         }
      });
      $('#rule_flag_max_activity').on('change', function(){
         if($('#rule_flag_max_activity').prop('checked') == true){
            $("#rule_max_activity").val('');
            $("#rule_max_activity").prop('readonly', true);
         } else {
            $("#rule_max_activity").prop('readonly', false);
         }
      });
      $('.btn-upload').on('click', function(){
         $('#'+$(this).attr('target-id')).click();
      });
      $('input[type=file]').on('change', function(){
         var target = $(this).attr('target-id');
         var myFormData = new FormData();
         myFormData.append('file_image', $(this).prop('files')[0]);
         myFormData.append('public_key', '');
         $.ajax({
            type: "POST",
            url: "{{ url('/upload_img') }}",
            data: myFormData,
            contentType: false,
            processData: false,
            cache: false,
            success: function(res){
               if(res.Status == "success"){
                  alertify.success(res.Message);
                  console.log(res.Path);
                  $('#'+target).val(res.Path);
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error('Something went wrong.');
            }
         });
      });
      $('#modal-rule-btn').on('click', function(){
         var myFormData = $('#form-rule').serializeArray();
         myFormData.push({ name:"user_id", value: "{{ Session::get('admin_id') }}"});
         $.ajax({
            type: "POST",
            url: url_point,
            data: myFormData,
            success: function(res){
               if(res.Status == "success"){
                  alertify.success(res.Message);
                  $('#table-point_gain').DataTable().ajax.reload();
                  $('#form-rule')[0].reset();
                  $('#modal-rule').modal('hide');
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error('Something went wrong.');
            }
         });
      });
      $('#modal-voucher-btn').on('click', function(){
         var myFormData = $('#form-voucher').serializeArray();
         myFormData.push({ name:"user_id", value: "{{ Session::get('admin_id') }}"},{ name:"region", value: region});
         $.ajax({
            type: "POST",
            url: url_voucher,
            data: myFormData,
            success: function(res){
               if(res.Status == "success"){
                  alertify.success(res.Message);
                  $('#table-voucher').DataTable().ajax.reload();
                  $('#form-voucher')[0].reset();
                  $('#modal-voucher').modal('hide');
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error('Something went wrong.');
            }
         });
      });
      function ChangeStatus(id, tag){
         var table_id;
         while ((tag = tag.parentElement) && tag.nodeName.toUpperCase() !== 'TABLE');
         switch(tag){
            case "table-point_gain": table_id = 'ms_rule'; break;
            case "table-voucher": table_id = 'ms_gift'; break;
         }
         $.ajax({
            url: "{{ url('/setting/point/change_status') }}",
            type: "POST",
            data: {id: id, table: table_id},
            success: function(res){
               if(res.Status == "success"){
                  alertify.success(res.Message);
                  $("#"+tag.id).DataTable().ajax.reload();
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error("Something went wrong.");
            }
         });
      }
      function Digits(evt){
         var charCode = (evt.which) ? evt.which : event.keyCode;
         if ((charCode >= 48 && charCode <= 57)) return true;
         return false;
      }
   </script>
   <script src="{!! asset('js/datatables/point-datatables.js') !!}" type="text/javascript"></script>
@endsection