@extends('template.template')
@section('active_setting','active')
@section('active_setting_help', 'class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Setting
         <small>Help Center</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li>Settings</li>
         <li class="active">Help Center</li>
      </ol>
   </section>

   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="box box-solid">
            <div class="box-body" style="margin: 20px;">
               <button type="button" id="btn-create-parent" class="btn btn-default" data-toggle="modal" data-target="#modal-parent" style="float: right; margin:5px;">+ Add Parent</button>
               <button type="button" id="btn-create-child" class="btn btn-default" data-toggle="modal" data-target="#modal-child" style="float: right; margin:5px;">+ Add Child</button>
               <table id="table-help-center" class="table table-bordered table-hover table-stripped" style="width: 100%">
                  <thead>
                     <tr>
                        <th style="min-width: 50px;">No.</th>
                        <th style="min-width: 250px;">Title (English)</th>
                        <th style="min-width: 250px;">Title (Indonesia)</th>
                        <th style="min-width: 100px;">Move</th>
                        <th style="min-width: 50px;">Action</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
<!-- Modal Parent -->
<div class="modal fade" id="modal-child">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title" id="child-title" style="text-align: center;">ADD CHILD</h3>
         </div>
         <div class="modal-body" style="padding: 10px 0px;">
            <form id="form-child">
               {{ csrf_field() }}
               <input type="hidden" name="child_parent_id" id="child_parent_id">
               <input type="hidden" name="child_category_id" id="child_category_id">
               <div class="row">
                  <div class="col=lg-12 col-md-12 col-xs-12">
                     <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                           <li class="active"><a href="#child-english" data-toggle="tab">English</a></li>
                           <li><a href="#child-indonesia" data-toggle="tab">Indonesia</a></li>
                        </ul>
                        <div class="tab-content" style="padding: 10px;">
                           <div class="tab-pane active" id="child-english">
                              <div class="form-group">
                                 <label for="parent_en_title">Parent EN</label>
                                 <input type="text" name="c_parent_en_title" id="c_parent_en_title" class="form-control" readonly>
                              </div>
                              <div class="form-group">
                                 <label for="child_en_title">Child Title</label>
                                 <input type="text" name="child_en_title" id="child_en_title" class="form-control" placeholder="Type child title">
                              </div>
                              <div class="form-group">
                                 <label for="child_en_content">Content</label>
                                 <textarea name="child_en_content" id="child_en_content"></textarea>
                              </div>
                           </div>
                           <div class="tab-pane" id="child-indonesia">
                              <div class="form-group">
                                 <label for="parent_id_title">Parent ID</label>
                                 <input type="text" name="c_parent_id_title" id="c_parent_id_title" class="form-control" readonly>
                              </div>
                              <div class="form-group">
                                 <label for="child_id_title">Child Title</label>
                                 <input type="text" name="child_id_title" id="child_id_title" class="form-control" placeholder="Type child title">
                              </div>
                              <div class="form-group">
                                 <label for="child_id_content">Content</label>
                                 <textarea name="child_id_content" id="child_id_content"></textarea>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" id="modal-child-btn" class="btn btn-primary btn-save" style="padding:10px 50px;" target-id="form-voucher">Save</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="modal-parent">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title" id="parent-title" style="text-align: center;">ADD PARENT</h3>
         </div>
         <div class="modal-body" style="padding: 10px 0px;">
            <form id="form-parent">
               {{ csrf_field() }}
               <input type="hidden" name="parent_id" id="parent_id" value="{{ $id }}">
               <input type="hidden" name="parent_category_id" id="parent_category_id">
               <div class="row">
                  <div class="col=lg-12 col-md-12 col-xs-12">
                     <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                           <li class="active"><a href="#parent-english" data-toggle="tab">English</a></li>
                           <li><a href="#parent-indonesia" data-toggle="tab">Indonesia</a></li>
                        </ul>
                        <div class="tab-content" style="padding: 10px;">
                           <div class="tab-pane active" id="parent-english">
                              <div class="form-group">
                                 <label for="parent_en_title">Parent Title EN</label>
                                 <input type="text" name="parent_en_title" id="parent_en_title" class="form-control" placeholder="Type parent title">
                              </div>
                           </div>
                           <div class="tab-pane" id="parent-indonesia">
                              <div class="form-group">
                                 <label for="parent_id_title">Parent Title ID</label>
                                 <input type="text" name="parent_id_title" id="parent_id_title" class="form-control" placeholder="Type parent title">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" id="modal-parent-btn" class="btn btn-primary btn-save" style="padding:10px 50px;" target-id="form-voucher">Save</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="modal-icon">
   <div class="modal-dialog modal-sm">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title" id="parent-title" style="text-align: center;">CHANGE ICON</h3>
         </div>
         <div class="modal-body" style="padding: 10px 0px;">
            <form id="form-icon">
               {{ csrf_field() }}
               <input type="hidden" name="icon_id" id="icon_id">
               <div class="row">
                  <div class="col-lg-6 col-md-6 col-xs-12" style="padding: 0px 35px;">
                     <div class="form-group">
                        <label>Icon</label>
                        <input type="file" id="icon" name="icon">
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" id="modal-icon-btn" class="btn btn-primary btn-save" style="padding:10px 50px;" target-id="form-voucher">Save</button>
         </div>
      </div>
   </div>
</div>
@endsection
@section('custom-script')
   <script>
      var category_id = "{{ $id }}";
      var url_parent = "{{ url('/setting/help-center/parent/create') }}";
      var url_child = "{{ url('/setting/help-center/child/create') }}";
      $('#btn-create-parent').on('click', function(){
         url_parent = "{{ url('/setting/help-center/parent/create') }}";
         $('#form-parent')[0].reset();
      });
      $('#btn-create-child').on('click', function(){
         url_child = "{{ url('/setting/help-center/child/create') }}";
         $('#form-child')[0].reset();
      });
      function Move(id, flag){
         $.ajax({
            url: "{{ url('/setting/help-center/move') }}/"+id+"/"+flag,
            type: "GET",
            success: function(res){
               if(res.Status == "success"){
                  alertify.success(res.Message);
                  $('#table-help-center').DataTable().ajax.reload();
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error('Something went wrong.');
            }
         });
      }
      function Edit(id){
         $.ajax({
            url: "{{ url('/setting/help-center/edit') }}/"+id,
            type: "GET",
            success: function(res){
               if(res.Status == "success"){
                  if(res.Data.IsParent == 1){
                     var current = res.Data.Title;
                     url_parent = "{{ url('/setting/help-center/parent/update') }}";
                     $('#parent_id').val(res.Data.ParentID);
                     $('#parent_category_id').val(res.Data.CategoryID);
                     $('#parent-title').html("EDIT PARENT");
                     $('#parent_id_title').val(current.id);
                     $('#parent_en_title').val(current.en);
                     $('#modal-parent').modal('show');
                  } else {
                     var data_en = res.Data.en;
                     var data_id = res.Data.id;
                     url_child = "{{ url('/setting/help-center/child/update') }}";
                     $('#child-title').html("EDIT CHILD");
                     $('#child_category_id').val(res.Data.CategoryID);
                     $('#c_parent_en_title').val(data_en.parent_title);
                     $('#child_en_title').val(data_en.child_title);
                     tinymce.get('child_en_content').getBody().innerHTML = data_en.description;
                     // $('#child_en_content').val(data_en.description);
                     $('#c_parent_id_title').val(data_id.parent_title);
                     $('#child_id_title').val(data_id.child_title);
                     tinymce.get('child_id_content').getBody().innerHTML = data_id.description;
                     // $('#child_id_content').val(data_id.description);
                     $('#modal-child').modal('show');
                  }
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error('Something went wrong.');
            }
         });
      }
      function Delete(id){
         alertify.confirm('Delete help', 'Are you sure to delete this categoy?', function(){
            $.ajax({
               url: "{{ url('/setting/help-center/delete') }}",
               type: "DELETE",
               data: { category_id: id },
               success: function(res){
                  if(res.Status == "success"){
                     alertify.success(res.Message);
                     $('#table-help-center').DataTable().ajax.reload();
                  } else {
                     alertify.error(res.Message);
                  }
               },
               error: function(res){
                  alertify.error('Something went wrong.');
               }
            });
         }, function(){});
      }
      function ChangeIcon(id){
         $('#icon_id').val(id);
         $('#modal-icon').modal('show');
      }
      $('#modal-parent-btn').on('click', function(){
         var myFormData = $('#form-parent').serializeArray();
         // myFormData.push({ name:"user_id", value: "{{ Session::get('admin_id') }}"});
         $.ajax({
            url: url_parent,
            type: "POST",
            data: myFormData,
            success: function(res){
               if(res.Status == "success"){
                  alertify.success(res.Message);
                  $('#table-help-center').DataTable().ajax.reload();
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error('Something went wrong.');
            }
         });
      });
      $('#modal-child-btn').on('click', function(){
         var myFormData = $('#form-child').serializeArray();
         myFormData.push({ name: "child_en_content", value: tinymce.get('child_en_content').getBody().innerHTML },{ name: "child_id_content", value: tinymce.get('child_id_content').getBody().innerHTML })
         // myFormData.push({ name:"user_id", value: "{{ Session::get('admin_id') }}"});
         $.ajax({
            url: url_child,
            type: "POST",
            data: myFormData,
            success: function(res){
               if(res.Status == "success"){
                  alertify.success(res.Message);
                  $('#table-help-center').DataTable().ajax.reload();
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error('Something went wrong.');
            }
         });
      });
      $('#modal-icon-btn').on('click', function(){
         var myFormData = new FormData();
         myFormData.append('icon', $('#icon').prop('files')[0]);
         myFormData.append('icon_id', $('#icon_id').val());
         $.ajax({
            url: "{{ url('/setting/help-center/change_icon') }}",
            type: "POST",
            data: myFormData,
            contentType: false,
            processData: false,
            cache:false,
            success: function(res){
               if(res.Status == "success"){
                  alertify.success(res.Message);
               } else {
                  alertify.error(res.Message);
               }
            },
            error: function(res){
               alertify.error('Something went wrong.');
            }
         });
      });
   </script>
   <script src="{!! asset('js/datatables/help-center-datatables.js') !!}" type="text/javascript"></script>
   <script src="{!! asset('vendor/tinymce/jquery.tinymce.min.js') !!}"></script>
   <script src="{!! asset('vendor/tinymce/tinymce.min.js') !!}"></script>
   <script>
      tinymce.init({
         selector: 'textarea',
         height: 200,
         plugins: 'lineheight textcolor',
         content_css: [
         '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
         '//www.tinymce.com/css/codepen.min.css'],
         toolbar: ['undo redo cut copy paste | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify |  fontselect fontsizeselect lineheightselect forecolor'],
         fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt'
      });
   </script>
@endsection