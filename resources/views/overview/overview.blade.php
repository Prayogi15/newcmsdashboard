@extends('template.template')
@section('active_home','class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Overview
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Overview</a></li>
      </ol>
   </section>

   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-lg-4 col-md-6 col-xs-12">
            <div class="row">
               <div class="col-lg-6 col-md-6 col-xs-12">
                  <div class="form-group">
                     <label>Start</label>
                     <div class="input-group date">
                        <input type="text" class="form-control pull-left overview_date1 date_start" name="date_start1" id="date_start1" autocomplete="off" value="{{ Carbon\Carbon::parse("01 Jan 2015")->format('d M Y') }}">
                        <div class="input-group-addon">
                           <i class="fa fa-calendar"></i>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-6 col-xs-12">
                  <div class="form-group">
                     <label>End</label>
                     <div class="input-group date">
                        <input type="text" class="form-control pull-left overview_date1 date_end" name="date_end1" id="date_end1" autocomplete="off" value="{{ Carbon\Carbon::now()->format('d M Y') }}">
                        <div class="input-group-addon">
                           <i class="fa fa-calendar"></i>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-lg-3 col-md-3 col-xs-6">
            <div class="small-box" style="background-color: rgba(90, 179, 69, 0.79);">
               <div class="inner" style="color: white;">
                  <h4>TOTAL LISTING</h4>
                  <h3 id="total_listing"></h3>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-3 col-xs-6">
            <div class="small-box" style="background-color: #45b39d;">
               <div class="inner" style="color: white;">
                  <h4>TOTAL USER</h4>
                  <h3 id="total_user"></h3>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-3 col-xs-6">
            <div class="small-box" style="background-color: #5499c7;">
               <div class="inner" style="color: white;">
                  <h4>LISTER</h4>
                  <h3 id="lister"></h3>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-3 col-xs-6">
            <div class="small-box" style="background-color: rgba(78, 69, 179, 0.69);">
               <div class="inner" style="color: white;">
                  <h4>NON LISTER</h4>
                  <h3 id="non_lister"></h3>
               </div>
            </div>
         </div>

         <div class="col-lg-3 col-md-3 col-xs-6">
            <div class="small-box" style="background-color: #f5b041;">
               <div class="inner" style="color: white;">
                  <h4>ACTIVE LISTING</h4>
                  <h3 id="active_listing"></h3>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-3 col-xs-6">
            <div class="small-box" style="background-color: #b39c45;">
               <div class="inner" style="color: white;">
                  <h4>ARCHIVED LISTING</h4>
                  <h3 id="archived_listing"></h3>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-3 col-xs-6">
            <div class="small-box" style="background-color: rgba(179, 110, 69, 0.92);">
               <div class="inner" style="color: white;">
                  <h4>SOLD LISTING</h4>
                  <h3 id="sold_listing"></h3>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-3 col-xs-6">
            <div class="small-box" style="background-color: #b34553;">
               <div class="inner" style="color: white;">
                  <h4>LEASED LISTING</h4>
                  <h3 id="leased_listing"></h3>
               </div>
            </div>
         </div>
      </div>
      <!-- LISTING BREAKDOWN -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="box">
               <div class="box-header with-border">
                  <h3 class="box-title">LISTING BREAKDOWN</h3>
                  <div class="box-tools pull-right">
                     <button type="button" class="btn btn-box-tool">Download <i class="fa fa-download"></i></button>
                     {{-- <button type="button" class="btn btn-box-tool" data-widget="collpase"><i class="fa fa-minus"></i></button> --}}
                  </div>
               </div><!-- end box head -->
               <div class="box-body">
                  <ul class="nav nav-tabs">
                     @foreach($listing_status as $status_key => $wnp_status)
                     <li @if($status_key ==0)class="active"@endif><a href="#listing_bd_{{ $wnp_status }}" data-toggle="tab">{{ strtoupper(str_replace('_',' & ',$wnp_status)) }}</a></li>
                     @endforeach
                  </ul>
                  <div class="tab-content">
                     @foreach($listing_status as $status_key => $wnp_status)
                     <div class="tab-pane <?php if($status_key == 0){echo("active");}?>" id="listing_bd_{{ $wnp_status }}" style="padding: 10px 0px;">
                        <div class="row"> <!-- start row for date -->
                           <div class="col-lg-4 col-md-6 col-xs-12">
                              <div class="row">
                                 <div class="col-lg-6 col-md-6 col-xs-12">
                                    <div class="form-group">
                                       <label>Start</label>
                                       <div class="input-group date">
                                          <input type="text" class="form-control pull-left overview_date2 date_start" name="date_start2_{{ $wnp_status }}" id="date_start2_{{ $wnp_status }}" autocomplete="off" value="{{ Carbon\Carbon::parse("01 Jan 2015")->format('d M Y') }}" target-id="{{ $wnp_status }}">
                                          <div class="input-group-addon">
                                             <i class="fa fa-calendar"></i>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-6 col-md-6 col-xs-12">
                                    <div class="form-group">
                                       <label>End</label>
                                       <div class="input-group date">
                                          <input type="text" class="form-control pull-left overview_date2 date_end" name="date_end2_{{ $wnp_status }}" id="date_end2_{{ $wnp_status }}" autocomplete="off" value="{{ Carbon\Carbon::now()->format('d M Y') }}" target-id="{{ $wnp_status }}">
                                          <div class="input-group-addon">
                                             <i class="fa fa-calendar"></i>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div><!-- end row for date -->
                        <div class="row">
                           <div class="col-lg-6 col-md-6 col-xs-12"><!-- div md for chart -->
                              <div id="chart1_{{ $wnp_status }}"></div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-xs-12"><!-- div md for details -->
                              <table id="table1_{{ $wnp_status }}" class="table">
                                 <thead>
                                    <tr>
                                       <th style="text-align: center;">COLOR</th>
                                       <th>TITLE</th>
                                       <th>%</th>
                                       <th>LISTING</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td style="text-align: center;"><div style="width:14px; height:14px; background-color: #3eb4e5; border-radius: 50%; margin: 0 auto;"></div></td>
                                       <td>Sell</td>
                                       <td id="percent_sell_{{ $wnp_status }}"></td>
                                       <td id="sell_{{ $wnp_status }}"></td>
                                    </tr>
                                    <tr>
                                       <td><div style="width:14px; height:14px; background-color: rgba(1, 35, 79, 0.68); border-radius: 50%; margin: 0 auto;"></div></td>
                                       <td>Rent</td>
                                       <td id="percent_rent_{{ $wnp_status }}"></td>
                                       <td id="rent_{{ $wnp_status }}"></td>
                                    </tr>
                                 </tbody>
                                 <tfoot>
                                    <tr>
                                       <th colspan="3" style="text-align: left;"><b>{{ strtoupper(str_replace('_',' & ',$wnp_status)) }} LISTING</b></th>
                                       <th id="footer_{{ $wnp_status }}"></th>
                                    </tr>
                                 </tfoot>
                              </table>
                           </div>
                        </div>
                     </div>
                     @endforeach
                  </div>
               </div><!-- end box body -->
            </div>
         </div>
      </div>
      <!-- END LISTING BREAKDOWN -->
      <!-- LISTING SEGMENTATION -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="box">
               <div class="box-header with-border">
                  <h3 class="box-title">LISTING SEGMENTATION</h3>
                  <div class="box-tools pull-right">
                     <button type="button" class="btn btn-box-tool">Download <i class="fa fa-download"></i></button>
                     {{-- <button type="button" class="btn btn-box-tool" data-widget="collpase"><i class="fa fa-minus"></i></button> --}}
                  </div>
               </div><!-- end box head -->
               <div class="box-body">
                  <ul class="nav nav-tabs">
                     @foreach($listing_status as $status_key => $wnp_status)
                     <li @if($status_key ==0)class="active"@endif><a href="#listing_seg_{{ $wnp_status }}" data-toggle="tab">{{ strtoupper(str_replace('_',' & ',$wnp_status)) }}</a></li>
                     @endforeach
                  </ul>
                  <div class="tab-content">
                     @foreach($listing_status as $status_key => $wnp_status)
                     <div class="tab-pane <?php if($status_key == 0){echo("active");}?>" id="listing_seg_{{ $wnp_status }}" style="padding: 10px 0px;">
                        <div class="row"> <!-- start row for date -->
                           <div class="col-lg-4 col-md-6 col-xs-12">
                              <div class="row">
                                 <div class="col-lg-6 col-md-6 col-xs-12">
                                    <div class="form-group">
                                       <label>Start</label>
                                       <div class="input-group date">
                                          <input type="text" class="form-control pull-left overview_date3 date_start" name="date_start3_{{ $wnp_status }}" id="date_start3_{{ $wnp_status }}" autocomplete="off" value="{{ Carbon\Carbon::parse("01 Jan 2015")->format('d M Y') }}" target-id="{{ $wnp_status }}">
                                          <div class="input-group-addon">
                                             <i class="fa fa-calendar"></i>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-6 col-md-6 col-xs-12">
                                    <div class="form-group">
                                       <label>End</label>
                                       <div class="input-group date">
                                          <input type="text" class="form-control pull-left overview_date3 date_end" name="date_end3_{{ $wnp_status }}" id="date_end3_{{ $wnp_status }}" autocomplete="off" value="{{ Carbon\Carbon::now()->format('d M Y') }}" target-id="{{ $wnp_status }}">
                                          <div class="input-group-addon">
                                             <i class="fa fa-calendar"></i>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           @if(Session::get('region') == "indonesia")
                           <div class="col-lg-2 col-md-3 col-xs-12">
                              <div class="row">
                                 <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="form-group">
                                       <label>Area</label>
                                       <select id="area_{{ $wnp_status }}" name="area_{{ $wnp_status }}" class="form-control overview_date3" target-id="{{ $wnp_status }}">
                                          <option value="all" selected>ALL</option>
                                          @foreach($area as $area_key => $wnp_area)
                                          <option value="{{ $wnp_area['id'] }}">{{ $wnp_area['name'] }}</option>
                                          @endforeach
                                       </select>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           @endif
                        </div><!-- end row for date -->
                        <div class="row">
                           <div class="col-lg-offset-6 col-lg-6 col-md-offset-6 col-md-6 col-xs-12"><!-- first table of segmentation -->
                              <table class="table">
                                 <thead>
                                    <tr>
                                       <th>{{ strtoupper(str_replace('_',' & ',$wnp_status)) }} LISTING</th>
                                       <th>SELL</th>
                                       <th>RENT</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td id="seg_total_{{ $wnp_status }}"></td>
                                       <td id="seg_total_sell_{{ $wnp_status }}"></td>
                                       <td id="seg_total_rent_{{ $wnp_status }}"></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <div class="row"><!-- ROW FOR SELL -->
                           <div class="col-lg-6 col-md-6 col-xs-12"><!-- div md for chart -->
                              <div id="chart_sell_{{ $wnp_status }}"></div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-xs-12"><!-- div md for chart -->
                              <table class="table">
                                 <thead>
                                    <tr>
                                       <th style="text-align: center;">COLOR</th>
                                       <th>TYPE</th>
                                       <th>%</th>
                                       <th>LISTING</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($listing_building as $building_key => $wnp_building)
                                    <tr>
                                       <td style="text-align: center;"><div style="width:14px; height:14px; background-color: {{ $wnp_building['color'] }}; border-radius: 50%; margin: 0 auto;"></div></td>
                                       <td>{{ $wnp_building['name'] }}</td>
                                       <td id="seg_percent_sell_{{ $wnp_status }}_{{ strtolower(str_replace(' ', '_', $wnp_building['name'])) }}">0 %</td>
                                       <td id="seg_sell_{{ $wnp_status }}_{{ strtolower(str_replace(' ', '_', $wnp_building['name'])) }}">0</td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                                 <tfoot>
                                    <tr>
                                       <th colspan="3" style="text-align: left;"><b>{{ strtoupper(str_replace('_',' & ',$wnp_status)) }} SELL</b></th>
                                       <th id="seg_footer_sell_{{ $wnp_status }}"></th>
                                    </tr>
                                 </tfoot>
                              </table>
                           </div>
                        </div><!-- END ROW FOR SELL -->
                        <div class="row"><!-- ROW FOR RENT -->
                           <div class="col-lg-6 col-md-6 col-xs-12"><!-- div md for chart -->
                              <div id="chart_rent_{{ $wnp_status }}"></div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-xs-12"><!-- div md for chart -->
                              <table class="table">
                                 <thead>
                                    <tr>
                                       <th style="text-align: center;">COLOR</th>
                                       <th>TYPE</th>
                                       <th>%</th>
                                       <th>LISTING</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($listing_building as $building_key => $wnp_building)
                                    <tr>
                                       <td style="text-align: center;"><div style="width:14px; height:14px; background-color: {{ $wnp_building['color'] }}; border-radius: 50%; margin: 0 auto;"></div></td>
                                       <td>{{ $wnp_building['name'] }}</td>
                                       <td id="seg_percent_rent_{{ $wnp_status }}_{{ strtolower(str_replace(' ', '_', $wnp_building['name'])) }}">0 %</td>
                                       <td id="seg_rent_{{ $wnp_status }}_{{ strtolower(str_replace(' ', '_', $wnp_building['name'])) }}">0</td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                                 <tfoot>
                                    <tr>
                                       <th colspan="3" style="text-align: left;"><b>{{ strtoupper(str_replace('_',' & ',$wnp_status)) }} RENT</b></th>
                                       <th id="seg_footer_rent_{{ $wnp_status }}"></th>
                                    </tr>
                                 </tfoot>
                              </table>
                           </div>
                        </div><!-- END ROW FOR RENT -->
                     </div>
                     @endforeach
                  </div>
               </div><!-- end box body -->
            </div>
         </div>
      </div>
      <!-- END LISTING SEGMENTATION -->
      <!-- TOP 20 AREA LISTING -->
      {{-- <div class="row">
         <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="box">
               <div class="box-header with-border">
                  <h3 class="box-title">TOP 20 AREA LIST</h3>
               </div>
               <div class="box-body">
                  <div class="row">
                     <div class="col-lg-6 col-md-6 col-xs-12">
                        <table class="table">
                           <thead>
                              <tr>
                                 <th>NO</th>
                                 <th>AREA</th>
                                 <th>LISTING</th>
                              </tr>
                           </thead>
                           <tbody>
                              @for($i=0;$i<10;$i++)
                              <tr>
                                 <td>{{ $top_listing[$i]['no'] }}</td>
                                 <td>{{ $top_listing[$i]['area'] }}</td>
                                 <td>{{ $top_listing[$i]['listing'] }}</td>
                              </tr>
                              @endfor
                           </tbody>
                        </table>
                     </div>
                     <div class="col-lg-6 col-md-6 col-xs-12">
                        <table class="table">
                           <thead>
                              <tr>
                                 <th>NO</th>
                                 <th>AREA</th>
                                 <th>LISTING</th>
                              </tr>
                           </thead>
                           <tbody>
                              @for($i=10;$i<20;$i++)
                              <tr>
                                 <td>{{ $top_listing[$i]['no'] }}</td>
                                 <td>{{ $top_listing[$i]['area'] }}</td>
                                 <td>{{ $top_listing[$i]['listing'] }}</td>
                              </tr>
                              @endfor
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> --}}
      <!-- END TOP 20 AREA LISTING -->
      <!-- TOTAL USER -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="box">
               <div class="box-header with-border">
                  <h3 class="box-title">TOTAL USER</h3>
                  <div class="box-tools pull-right">
                     <button type="button" class ="btn btn-box-tool">Download <i class="fa fa-download"></i></button>
                     {{-- <button type="button" class="btn btn-box-tool" data-widget="collpase"><i class="fa fa-minus"></i></button> --}}
                  </div>
               </div>
               <div class="box-body">
                  <div class="row"> <!-- row for date -->
                     <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="row">
                           <div class="col-lg-6 col-md-6 col-xs-12">
                              <div class="form-group">
                                 <label>Start</label>
                                 <div class="input-group date">
                                    <input type="text" class="form-control pull-left overview_date4 date_start4" id="date_start4" name="date_start4" autocomplete="off" value="{{ Carbon\Carbon::now()->startOfYear()->format('M Y') }}">
                                    <div class="input-group-addon">
                                       <i class="fa fa-calendar"></i>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-6 col-md-6 col-xs-12">
                              <div class="form-group">
                                 <label>End</label>
                                 <div class="input-group date">
                                    <input type="text" class="form-control pull-left overview_date4 date_end4" id="date_end4" name="date_end4" autocomplete="off" value="{{ Carbon\Carbon::now()->format('M Y') }}">
                                    <div class="input-group-addon">
                                       <i class="fa fa-calendar"></i>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div><!-- end row for date -->
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-xs-12">
                        <div id="chart_user"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- END TOTAL USER -->
      
   </section>
   <!-- /.content -->
@endsection
@section('custom-script')
   <!-- Highchart -->
   <script src="{{ asset('vendor/Highcharts/code/highcharts.js') }}"></script>
   <script>
      var region = "{{ Session::get("region") }}";
      var listing_status = {!! json_encode($listing_status) !!};
      $('.date_start').datepicker({
         "autoclose": true,
         "todayBtn": true,
         "todayHighlight": true,
         "format": "dd M yyyy",
         "defaultViewDate": "{{ Carbon\Carbon::parse("01 Jan 2015")->format('d M Y') }}",
         "startDate": "{{ Carbon\Carbon::parse("01 Jan 2015")->format('d M Y') }}",
         "endDate": "{{ Carbon\Carbon::now()->format('d M Y') }}"
      });
      $('.date_end').datepicker({
         "autoclose": true,
         "todayBtn": true,
         "todayHighlight": true,
         "format": "dd M yyyy",
         "defaultViewDate": "{{ Carbon\Carbon::now()->format('d M Y') }}",
         "startDate": "{{ Carbon\Carbon::parse("01 Jan 2015")->format('d M Y') }}",
         "endDate": "{{ Carbon\Carbon::now()->format('d M Y') }}"
      });

      $('.date_start4').datepicker({
         "autoclose": true,
         "todayBtn": true,
         "todayHighlight": true,
         "format": "M yyyy",
         "defaultViewDate": "{{ Carbon\Carbon::now()->firstOfYear()->format('M Y') }}",
         "startDate": "{{ Carbon\Carbon::parse("Jan 2015")->format('M Y') }}",
         "endDate": "{{ Carbon\Carbon::now()->format('M Y') }}"
      });
      $('.date_end4').datepicker({
         "autoclose": true,
         "todayBtn": true,
         "todayHighlight": true,
         "format": "M yyyy",
         "defaultViewDate": "{{ Carbon\Carbon::now()->format('M Y') }}",
         "startDate": "{{ Carbon\Carbon::parse("Jan 2015")->format('M Y') }}",
         "endDate": "{{ Carbon\Carbon::now()->format('M Y') }}"
      });

      function ClearSegmentation(id){
         var listing_building = {!! json_encode($listing_building) !!};
         $('#seg_total_'+id).html('0');
         $('#seg_total_sell_'+id).html('0');
         $('#seg_total_rent_'+id).html('0');
         for(var i=0; i<listing_building.length; i++){
            var current_building = listing_building[i]['name'].toLowerCase().replace(' ', '_');
            $('#seg_sell_'+id+'_'+current_building).html("0");
            $('#seg_percent_sell_'+id+'_'+current_building).html("0");
            $('#seg_rent_'+id+'_'+current_building).html("0");
            $('#seg_percent_rent_'+id+'_'+current_building).html("0");
         }
         $('#seg_footer_sell_'+id).html("0");
         $('#seg_footer_rent_'+id).html("0");
      }

      function ReloadOverview1(){
         if(new Date($('#date_start1').val()).getTime() < new Date($('#date_end1').val()).getTime() ){
            $.ajax({
               url: "{{ url('/home/get_overview1') }}",
               method: "POST",
               data: {region: region, date_start: $('#date_start1').val(), date_end: $('#date_end1').val()},
               success: function(res){
                  $('#total_listing').html(res.total_listing);
                  $('#total_user').html(res.total_user);
                  $('#lister').html(res.data_user.lister);
                  $('#non_lister').html(res.data_user.non_lister);
                  $('#active_listing').html(res.data_listing.active_listing);
                  $('#archived_listing').html(res.data_listing.archived_listing);
                  $('#sold_listing').html(res.data_listing.sold_listing);
                  $('#leased_listing').html(res.data_listing.leased_listing);
               },
               error: function(res){
                  alertify.error('Something went wrong. Please check our support.');
               }
            });
         } else {
            alertify.error('Start date is greater than End date');
         }
      }
      function ReloadOverview2(id){
         if(new Date($('#date_start2_'+id).val()).getTime() < new Date($('#date_end2_'+id).val()).getTime() ){
            var chart_data = [
               {
                  name: 'Sell',
                  y : 0,
                  color : '#3eb4e5',
               },
               {
                  name: 'Rent',
                  y : 0,
                  color : 'rgba(1, 35, 79, 0.68)',
               }
            ];
            $.ajax({
               url: "{{ url('/home/get_overview2') }}",
               method: "POST",
               data: {region: region, date_start: $('#date_start2_'+id).val(), date_end: $('#date_end2_'+id).val(), flag: id},
               success: function(res){
                  if(res.total != 0){
                     var res_data = [];
                     if(res.sell != 0){
                        var percent_sell = (res.sell / res.total) * 100;
                        $('#sell_'+id).html(res.sell);
                        $('#percent_sell_'+id).html(parseFloat(percent_sell).toFixed(2) + ' %');
                        res_data.push({name: 'Sell', y: res.sell, color: '#3eb4e5'});
                     } else {
                        $('#sell_'+id).html('0');
                        $('#percent_sell_'+id).html('0 %');
                        res_data.push({name: 'Sell', y: 0, color: '#3eb4e5'});
                     }
                     if(res.rent != 0){
                        var percent_rent = (res.rent / res.total) * 100;
                        $('#rent_'+id).html(res.rent);
                        $('#percent_rent_'+id).html(parseFloat(percent_rent).toFixed(2) + ' %');
                        res_data.push({name: 'Rent', y: res.rent, color: 'rgba(1, 35, 79, 0.68)'});
                     } else {
                        $('#rent_'+id).html('0');
                        $('#percent_rent_'+id).html('0 %');
                        res_data.push({name: 'Rent', y: 0, color: 'rgba(1, 35, 79, 0.68)'});
                     }
                     $('#footer_'+id).html(res.total);
                     Highcharts.chart('chart1_'+id, {
                        chart: {
                           type: 'pie'
                        },
                        title: {
                           text: null
                        },
                        series: [{
                           name: 'Total',
                           data: res_data,
                        }],
                     });
                  } else {
                     $('#sell_'+id).html('0');
                     $('#percent_sell_'+id).html('0 %');
                     $('#rent_'+id).html('0');
                     $('#percent_rent_'+id).html('0 %');
                     $('#footer_'+id).html('0');
                     $('#footer_'+id).html(res.total);
                     Highcharts.chart('chart1_'+id, {
                        chart: {
                           type: 'pie'
                        },
                        title: {
                           text: null
                        },
                        series: [{
                           name: 'Total',
                           data: chart_data,
                        }],
                     });
                  }
               },
               error: function(res){
                  alertify.error('Something went wrong. Please check our support.');
               }
            });
         } else {
            alertify.error('Start date is greater than End date');
         }
      }
      function ReloadOverview3(id){
         if(new Date($('#date_start3_'+id).val()).getTime() < new Date($('#date_end3_'+id).val()).getTime() ){
            $.ajax({
               url: "{{ url('/home/get_overview3') }}",
               method: "POST",
               data: {region: region, date_start: $('#date_start3_'+id).val(), date_end: $('#date_end3_'+id).val(), flag:id, area: $('#area_'+id).val()},
               success: function(res){
                  if(res.total != 0 && res.data.length != 0){
                     ClearSegmentation(id);
                     var pie_data_sell = [];
                     var pie_data_rent = [];
                     var key;
                     var data_count = res.data;
                     $('#seg_total_'+id).html(res.total);
                     $('#seg_total_sell_'+id).html(res.sell);
                     $('#seg_footer_sell_'+id).html(res.sell);
                     $('#seg_total_rent_'+id).html(res.rent);
                     $('#seg_footer_rent_'+id).html(res.rent);
                     for(key in data_count){
                        var current_building_key = key.replace('_', ' ').replace(/\b[a-z]/g, function(letter){
                           return letter.toUpperCase();
                        });
                        if(data_count[key]['sell'] != 0){
                           var percent_sell = (data_count[key]['sell'] / res.sell) * 100;
                           $('#seg_sell_'+id+'_'+key).html(data_count[key]['sell']);
                           $('#seg_percent_sell_'+id+'_'+key).html(parseFloat(percent_sell).toFixed(2) + ' %');
                           pie_data_sell.push({name: current_building_key, y: data_count[key]['sell'], color: data_count[key]['color']});
                        }
                        if(data_count[key]['rent'] != 0){
                           var percent_rent = (data_count[key]['rent'] / res.rent) * 100;
                           $('#seg_rent_'+id+'_'+key).html(data_count[key]['rent']);
                           $('#seg_percent_rent_'+id+'_'+key).html(parseFloat(percent_rent).toFixed(2) + ' %');
                           pie_data_rent.push({name: current_building_key, y: data_count[key]['rent'], color: data_count[key]['color']});
                        }
                     }
                     Highcharts.chart('chart_sell_'+id, {
                        chart: {
                           type: 'pie'
                        },
                        title: {
                           text: null
                        },
                        series: [{
                           name: 'Total',
                           data: pie_data_sell,
                        }],
                     });
                     Highcharts.chart('chart_rent_'+id, {
                        chart: {
                           type: 'pie'
                        },
                        title: {
                           text: null
                        },
                        series: [{
                           name: 'Total',
                           data: pie_data_rent,
                        }],
                     });
                  } else {
                     ClearSegmentation(id);
                  }
               },
               error: function(res){
                  alertify.error('Something went wrong. Please check our support.');
               }
            });
         } else {
            alertify.error('Start date is greater than End date');
         }
      }
      function ReloadOverview4(){
         if(new Date($('#date_start4').val()).getTime() <= new Date($('#date_end4').val()).getTime() ){
            $.ajax({
               url: "{{ url('/home/get_overview4') }}",
               method: "POST",
               data: {region: region, date_start: $('#date_start4').val(), date_end: $('#date_end4').val()},
               success: function(res){
                  Highcharts.chart('chart_user', {
                     chart: {
                        type: 'line'
                     },
                     title: {
                        text: ''
                     },
                     xAxis: {
                        categories: res.categories,
                        title: {
                           text: null
                        }
                     },
                     series: [{
                        name: 'Total',
                        color: '#ba1916',
                        data: res.total
                     },{
                        name: 'Lister',
                        color: 'rgba(96, 157, 0, 0.75)',
                        data: res.lister
                     },{
                        name: 'Non Lister',
                        color: '#006f9d',
                        data: res.non_lister
                     }]
                  });
               },
               error: function(res){
                  alertify.error('Something went wrong. Please check our support.');
               }
            });
         } else {
            alertify.error('Start date is greater than End date');
         }
      }
      $(document).ready(function(){
         ReloadOverview1();
         for(var i=0;i<listing_status.length;i++){
            ReloadOverview2(listing_status[i]);
            ReloadOverview3(listing_status[i]);
         }
         ReloadOverview4();
      });
      $('.overview_date1').on('change', function(){
         ReloadOverview1();
      });
      $('.overview_date2').on('change', function(){
         var flag = $(this).attr('target-id');
         ReloadOverview2(flag);
      });
      $('.overview_date3').on('change', function(){
         var flag = $(this).attr('target-id');
         ReloadOverview3(flag);
      });
      $('.overview_date4').on('change', function(){
         ReloadOverview4();
      });
   </script>
@endsection