@extends('front.main')
@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User
        <small>All</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Overview</a></li>
        <li class="active">User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#all" data-toggle="tab">All</a></li>
              <li><a href="#active" data-toggle="tab">Active</a></li>
              <li><a href="#watchlist" data-toggle="tab">Watchlist</a></li>
			   <li><a href="#banned" data-toggle="tab">Banned</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="all">
                <!-- Post -->
                <div class="post">
				 <table id="example2" class="table table-bordered table-hover datatables1">
                <thead>
                <tr>
                  <th>User ID</th>
                  <th>Email</th>
                  <th>Name</th>
                  <th>Phone</th>
                  <th>Join Date</th>
				  <th>Last Active</th>
				  <th>Status</th>
				  <th>Action</th>
				</tr>
                </thead>
                <tbody>
                <tr>
                   <td>J0k0</td>
                  <td>joko@gmail.com</td>
                  <td>JOko</td>
                  <td>0898989</td>
                  <td>20 Des 2016</td>
				  <td>09/04/2018, 11:06</td>
				  <td><span class="text-success"><b>Active</b></span></td>
				  <td><div class="btn-group">
                  <button type="button" class="btn btn-default"><i class="fa fa-bars"></i></button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Move to banned</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Move to watchlist</a></li>
                  </ul>
                </div></td>
                </tr>
                  <tr>
                   <td>J0k1</td>
                  <td>joko@gmail.com</td>
                  <td>JOko</td>
                  <td>0898989</td>
                  <td>20 Des 2016</td>
				  <td>09/04/2018, 11:06</td>
				  <td><span class="text-danger"><b>Banned</b></span></td>
				  <td><div class="btn-group">
                  <button type="button" class="btn btn-default"><i class="fa fa-bars"></i></button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Move to banned</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Move to watchlist</a></li>
                  </ul>
                </div></td>
                </tr>
				</tbody>
				</table>
				
                </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="active">
               <!-- Post -->
                <div class="post">
				 <table id="example2" class="table table-bordered table-hover datatables1">
                <thead>
                <tr>
                  <th>User ID</th>
                  <th>Email</th>
                  <th>Name</th>
                  <th>Phone</th>
                  <th>Join Date</th>
				  <th>Last Active</th>
				  <th>Status</th>
				  <th>Action</th>
				</tr>
                </thead>
                <tbody>
                <tr>
                   <td>J0k0</td>
                  <td>joko@gmail.com</td>
                  <td>JOko</td>
                  <td>0898989</td>
                  <td>20 Des 2016</td>
				  <td>09/04/2018, 11:06</td>
				  <td><span class="text-success"><b>Active</b></span></td>
				  <td><div class="btn-group">
                  <button type="button" class="btn btn-default"><i class="fa fa-bars"></i></button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Move to banned</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Move to watchlist</a></li>
                  </ul>
                </div></td>
                </tr>
                  <tr>
                   <td>J0k1</td>
                  <td>joko@gmail.com</td>
                  <td>JOko</td>
                  <td>0898989</td>
                  <td>20 Des 2016</td>
				  <td>09/04/2018, 11:06</td>
				  <td><span class="text-danger"><b>Banned</b></span></td>
				  <td><div class="btn-group">
                  <button type="button" class="btn btn-default"><i class="fa fa-bars"></i></button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Move to banned</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Move to watchlist</a></li>
                  </ul>
                </div></td>
                </tr>
				</tbody>
				</table>
				
                </div>
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="watchlist">
                <!-- Post -->
                <div class="post">
				 <table id="example2" class="table table-bordered table-hover datatables1">
                <thead>
                <tr>
                  <th>User ID</th>
                  <th>Email</th>
                  <th>Name</th>
                  <th>Phone</th>
                  <th>Join Date</th>
				  <th>Last Active</th>
				  <th>Status</th>
				  <th>Action</th>
				</tr>
                </thead>
                <tbody>
                <tr>
                   <td>J0k0</td>
                  <td>joko@gmail.com</td>
                  <td>JOko</td>
                  <td>0898989</td>
                  <td>20 Des 2016</td>
				  <td>09/04/2018, 11:06</td>
				  <td><span class="text-success"><b>Active</b></span></td>
				  <td><div class="btn-group">
                  <button type="button" class="btn btn-default"><i class="fa fa-bars"></i></button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Move to banned</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Move to watchlist</a></li>
                  </ul>
                </div></td>
                </tr>
                  <tr>
                   <td>J0k1</td>
                  <td>joko@gmail.com</td>
                  <td>JOko</td>
                  <td>0898989</td>
                  <td>20 Des 2016</td>
				  <td>09/04/2018, 11:06</td>
				  <td><span class="text-danger"><b>Banned</b></span></td>
				  <td><div class="btn-group">
                  <button type="button" class="btn btn-default"><i class="fa fa-bars"></i></button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Move to banned</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Move to watchlist</a></li>
                  </ul>
                </div></td>
                </tr>
				</tbody>
				</table>
				
                </div>
              </div>
              <!-- /.tab-pane -->

			  <div class="tab-pane" id="banned">
                <!-- Post -->
                <div class="post">
				 <table id="example2" class="table table-bordered table-hover datatables1">
                <thead>
                <tr>
                  <th>User ID</th>
                  <th>Email</th>
                  <th>Name</th>
                  <th>Phone</th>
                  <th>Join Date</th>
				  <th>Last Active</th>
				  <th>Status</th>
				  <th>Action</th>
				</tr>
                </thead>
                <tbody>
                <tr>
                   <td>J0k0</td>
                  <td>joko@gmail.com</td>
                  <td>JOko</td>
                  <td>0898989</td>
                  <td>20 Des 2016</td>
				  <td>09/04/2018, 11:06</td>
				  <td><span class="text-success"><b>Active</b></span></td>
				  <td><div class="btn-group">
                  <button type="button" class="btn btn-default"><i class="fa fa-bars"></i></button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Move to banned</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Move to watchlist</a></li>
                  </ul>
                </div></td>
                </tr>
                  <tr>
                   <td>J0k1</td>
                  <td>joko@gmail.com</td>
                  <td>JOko</td>
                  <td>0898989</td>
                  <td>20 Des 2016</td>
				  <td>09/04/2018, 11:06</td>
				  <td><span class="text-danger"><b>Banned</b></span></td>
				  <td><div class="btn-group">
                  <button type="button" class="btn btn-default"><i class="fa fa-bars"></i></button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Move to banned</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Move to watchlist</a></li>
                  </ul>
                </div></td>
                </tr>
				</tbody>
				</table>
				
                </div>
              </div>
			  
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
     
        
        
       
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </section>
        <!-- right col -->
			@endsection	
     