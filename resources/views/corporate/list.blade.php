@extends('template.template')
@section('active_corporate_cms','class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Corporate
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li class="active">Corporate CMS</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="box box-solid">
            <div class="box-body" style="margin: 20px;">
               <table id="table-corporate" class="table table-bordered table-hover table-stripped" style="width: 100%;">
                  <thead>
                     <tr>
                        <th style="min-width: 250px;">Corporate Name</th>
                        <th style="min-width: 50px;">Subcription</th>
                        <th style="min-width: 50px;">Total Member</th>
                        <th style="min-width: 50px;">Total Active Listing</th>
                        <th style="min-width: 100px;">Join Date</th>
                        <th style="min-width: 100px;">Last Active</th>
                        <th style="min-width: 50px;">Status</th>
                        <th style="min-width: 50px;">Action</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
<div class="modal fade" id="modal-corporate">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title" id="modal-title" style="text-align: center;">EDIT CORPORATE</h3>
            <h3 class="modal-title" id="image-title" style="text-align: center;"></h3>
         </div>
         <div class="modal-body" style="padding: 10px 50px;">
            <form id="form-corporate">
               <div class="row">
                  <div class="col-lg-6 col-md-6 col-xs-12">
                     <div class="form-group">
                        <label>Corporate Area</label>
                        <input type="text" name="corporate_area" placeholder="Insert Corporate Area">
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-xs-12">
                     <div class="form-group">
                        <label>Corporate Phone Number</label>
                        <div class="row">
                           <div class="col-lg-3 col-md-3 col-xs-3">
                              <input type="text" name="corporate_code_area">
                           </div>
                           <div class="col-lg-9 col-md-9 col-xs-9">
                              <input type="text" name="corporate_phone_number">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
@section('custom-script')
   <script>
      var region = "{{ Session::get('region') }}";
      function EditCorporate(id){
         alertify.error('Sorry. This function is under development.')
         // $.ajax({
         //    url: "{{ url('/corporate_cms/get/') }}"+id,
         //    method: "GET",
         //    success: function(res){
         //       if(res.Status == "success"){
         //          $('#'+tag.id).DataTable().ajax.reload();
         //       } else {
         //          alertify.error(res.Message);
         //       }
         //    },
         //    error: function(res){
         //       alertify.error("Something went wrong.");
         //    }
         // });
      }
      function DeleteCorporate(id){
         alertify.confirm('Confirm Delete', 'Are you sure to delete this corporate?\n(you can not undo after you confirm this)',
            function(){
               $.ajax({
                  url: "{{ url('/corporate_cms/delete_corporate') }}/"+id,
                  method: "GET",
                  success: function(res){
                     if(res.Status == "success"){
                        alertify.success(res.Message);
                        $('#table-corporate').DataTable().ajax.reload();
                     } else {
                        alertify.error(res.Message);
                     }
                  },
                  error: function(res){
                     alertify.error("Something went wrong.");
                  }
               });
            },
            function(){}
         );
         
      }
   </script>
   <script src="{{ asset('js/datatables/corporate-datatables.js') }}"></script>
@endsection