@extends('template.template')
@section('active_tools','active')
@section('active_corporate_cms','class=active')
@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Tools
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
         <li class="active">Data XML</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
	  <div class="col-lg-12 col-md-12 col-xs-12">
         <div class="box box-solid">
		 
		 <div class="box-header with-border">
		<h3 class="box-title">Table List</h3>
		<div class="box-tools pull-right">
		  <!-- Buttons, labels, and many other things can be placed here! -->
		  <!-- Here is a label for example -->
		 
		</div>
		<!-- /.box-tools -->
		</div>
		 
            <div class="box-body">
               <table id="table-corporate" class="table table-bordered table-hover table-stripped" style="width: 100%;">
                  <thead>
                     <tr>
						<th style="min-width: 20px;"> </th>
                        <th style="min-width: 150px;">Data Name</th>
                        <th style="min-width: 50px;">Total</th>
                        <th style="min-width: 50px;">Status</th>
						<th style="min-width: 50px;">Start date</th>
						<th style="min-width: 50px;">End date</th>
                        <th style="min-width: 50px;">Action</th>
                     </tr>
                  </thead>
				  <tbody>
				  <?php $n=1;?>
				  @foreach($data_xml as $row)
				  <?php 
				  $exp=explode('@',$row->activity);
				  ?>
				  <tr>
				  <td> {{ $n }}</td>
				  <td> {{ $exp[1] }} </td>
				  <td>  {{ $row->total }} </td>
				  <td>  {{ ucwords($row->status) }} </td>
				  <?php
				  if(substr($row->url,0,4)=='http')
				  {
					  $link = $row->url ;
				  }		
				  else
				  {
					 $link = "https://xml.worknplay.com/xmlfile/". $row->url; 
				  }
				  
				  ?>
				  <td> {{ $row->start_date }} </td>
				   <td> {{ $row->end_date }}  </td>
				  <td> <a href="{{ $link  }}" class="btn btn-link" target="_blank">Download</a> </td>
				  </tr>
				  <?php $n++;?>
				  @endforeach
				  </tbody>
				  
               </table>
            </div>
         </div>
      </div>
	 </div> 
   </section>
   <!-- /.content -->

@endsection
@section('custom-script')
   <script>
      var region = "{{ Session::get('region') }}";
      function EditCorporate(id){
         alertify.error('Sorry. This function is under development.')
         // $.ajax({
         //    url: "{{ url('/corporate_cms/get/') }}"+id,
         //    method: "GET",
         //    success: function(res){
         //       if(res.Status == "success"){
         //          $('#'+tag.id).DataTable().ajax.reload();
         //       } else {
         //          alertify.error(res.Message);
         //       }
         //    },
         //    error: function(res){
         //       alertify.error("Something went wrong.");
         //    }
         // });
      }
      
   </script>
  
@endsection