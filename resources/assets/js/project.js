window.Vue = require('vue');
window.axios = require('axios');

import PropertyTypeSelector from './components/PropertyTypeSelector.vue';
import DropBox from './components/DropBox.vue';
import OptionSelector from './components/OptionSelector.vue';
import FileInput from './components/FileInput.vue';
import UnitTypesTable from './components/UnitTypesTable.vue';
import UnitTypeFormModal from './components/UnitTypeFormModal.vue';
import Loading from 'vue-full-loading';

console.log('data is: ');
console.log(data);

const app = new Vue({
    el: '#app',

    components: {
    	PropertyTypeSelector, DropBox, OptionSelector, FileInput, UnitTypesTable, UnitTypeFormModal, Loading
    },

    data: data,

    computed: {
        visibleCategories() {
            const facilityIds = buildingsFacilities[this.propertyType];

            return categories
                .map(category => {
                    const clonedCategory = JSON.parse(JSON.stringify(category));
                    clonedCategory.facilities = category.facilities.filter(facility => {
                        return facilityIds.includes(facility.id);
                    });
                    return clonedCategory;
                })
                .filter(category => category.facilities.length > 0);
        }
    },

    methods: {
        // Unit
        addUnitType() {
            this.createUnitTypeFormModal.reset();
            $(this.createUnitTypeFormModal.$el).modal('show');
        },

        editUnitType(unitType) {
            this.editUnitTypeFormModal.setData(unitType);
            $(this.editUnitTypeFormModal.$el).modal('show');
        },

        storeUnitType(unitType) {
            unitType.id = this.generateID();
            this.unitTypes.push(unitType);
            $(this.createUnitTypeFormModal.$el).modal('hide');
        },

        updateUnitType(unitType) {
            const targetUnitType = this.unitTypes.find(t => t.id === unitType.id);
            Object.assign(targetUnitType, unitType);
            $(this.editUnitTypeFormModal.$el).modal('hide');
        },

        // Facility
        selectFacility(facility) {
            const index = this.selectedFacilities.indexOf(facility.id);

            if (index < 0) { // If not found, we add it to the selected list
                this.selectedFacilities.push(facility.id);
            } else { // If found, that means it was already selected, we need to toggle it.
                this.selectedFacilities.splice(index, 1);
            }
        },

        // Project
        storeProject() {
            this.isLoading = true;
            return axios.post('/prime_cms/projects', this.getData())
                .then(response => {
                    this.isLoading = false;
                    window.location = "/prime_cms";
                    console.log(response);
                })
                .catch(error => {
                    this.isLoading = false;
                    console.log(error);
                });
        },

        updateProject() {
            this.isLoading = true;
            return axios.put(`/prime_cms/projects/${this.projectId}`, this.getData())
                .then(response => {
                    this.isLoading = false;
                    window.location = "/prime_cms";
                    console.log(response);
                })
                .catch(error => {
                    this.isLoading = false;
                    console.log(error);
                });
        },

        // Helpers
        getData() {
            let data = this.$data;
            data.lat = $('input[type="hidden"]#lat').val();
            data.lng = $('input[type="hidden"]#lng').val();

            return data;
        },

        generateID() {
            return '_' + Math.random().toString(36).substr(2, 9);
        }
    },

    mounted() {
        this.createUnitTypeFormModal = this.$refs.createUnitTypeFormModal;
        this.editUnitTypeFormModal = this.$refs.editUnitTypeFormModal;
        
        $('input.price-input').keyup();
    },
});
