<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

final class ListingsPage extends Model
{
    protected $table = 'ms_listings_pages';

    /**
     * Helpers
     */
    public static function findBySlug($slug) {
        return static::where('slug', $slug)->first();    
    }
}
