<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectUnitPhoto extends Model
{
    protected $table = 'ms_project_unit_photo';

    public $timestamps = false;
}
