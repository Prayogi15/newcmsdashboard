<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportListing extends Model
{
    protected $connection = 'mysql';
    protected $table = 'ms_report_listing';
    public $timestamps = false;
}
