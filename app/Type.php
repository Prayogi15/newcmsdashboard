<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

final class Type extends Model
{
    protected $table = 'ms_type';
}
