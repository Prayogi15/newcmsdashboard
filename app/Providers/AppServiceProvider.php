<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use Cookie;
use App;
use App\Sync;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $Sync = new Sync;
        $result = $Sync->sync();
        // dd($result);
        if ($result['Code'] == 200 || $result['Code'] == 201) {
            $new_sync_data = $result['Data']['client_data_sync'];
            if ($Sync->isExist()) {
                $current_sync_data = $Sync->getAndCheckIfChanged($new_sync_data, true);
            }else{
                // first sync
                $current_sync_data = $Sync->setData($new_sync_data);
            }
            view()->share('master', $current_sync_data);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
