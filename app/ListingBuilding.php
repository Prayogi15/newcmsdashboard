<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

final class ListingBuilding extends Model
{
    protected $table = 'ms_listing_building';
}
