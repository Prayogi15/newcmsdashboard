<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectPhoto extends Model
{
    protected $table = 'ms_project_photo';
    
    public $timestamps = false;
}
