<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Admin;
use App\Http\Requests;
use Session;
class LoginController extends Controller
{
    public function __construct()
    {
       // $this->update_session();
    }

    public function Index()
    {
        return view('front.login');	
    }
	
    //GET MODULE
    public function Authenticate(Request $req){
		$user = Admin::where([
			'username' => $req->input('username'),
			'password' => md5($req->input('password')),
			'status' => "active"
		])
		->first();
		
		if ($user) {
		//	$this->guard()->login($user, $request->has('remember'));
			//return $req . "benar";
			 session(['admin_id' => $user->id, 'admin_name' => $user->fullname, 'admin_photo' => $user->photo]);
			 return redirect('/home');
		}
		else
		{
			return redirect('/')->with("status_fail", "Login Failed. Username or Password is incorrect.");
		}
	}
	public function Logout(){
		Session::flush();
		return redirect('/');
	}
}
