<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sync;
use Carbon\Carbon;
use Session;

class OverviewController extends Controller
{
	protected $base_region;
	protected $base_listing_building;
	protected $Sync;
	public function __construct(){
		parent::__construct();
		$this->Sync = new Sync;
		if($this->Sync->isExist()){
			$this->SyncData = $this->Sync->getData(true);
		} else {
			$this->SyncData = $this->Sync->setData($this->Sync->sync());
		}
		foreach($this->SyncData['ms_region'] as $keys => $value){
			$this->SyncData['ms_region'][$keys] = (array)$value;
		}
		foreach($this->SyncData['ms_listing_building'] as $keys => $value){
			$this->SyncData['ms_listing_building'][$keys] = (array)$value;
		}
		$this->base_region = collect($this->SyncData['ms_region']);
		$this->base_listing_building = collect($this->SyncData['ms_listing_building']);
	}

	private function GetRegionIDFromName($ms_region, $param_region){
		$return = "";
		foreach($ms_region as $key => $value){
			if(strtolower($value['full_name']) == strtolower($param_region)){
				$return = $value['id'];
				break;
			}
		}
		if($return == ""){
			$return = 2;
		}
		return $return;
	}

	private function GetListingBuilding($ms_listing_building, $lang){
		$return = [];
		foreach($ms_listing_building as $key => $value){
			if($value['parent_id'] == 0 && $value['lang'] == $lang){
				$color = "";
				switch($value['type']){
					case "House": $color = '#456c91';break;
					case "Apartment": $color = '#5acecd';break;
					case "HDB": $color = '#c2e286';break;
					case "Land": $color = '#ead148';break;
					case "Shophouse": $color = '#fda845';break;
					case "Retail Space": $color = '#b86c5e';break;
					case "Office Space": $color = '#5eb88d';break;
					case "Warehouse": $color = '#86a9e2';break;
					case "Factory": $color = '#d09bf2';break;
					case "Room Rental": $color = '#f29ba7';break;
				}
				array_push($return, array("name" => $value['type'], "color" => $color, "priority" => $value['priority']));
			}
		}
		return $return;
	}

	private function GetBuildingNameFromID($ms_listing_building, $param_building){
		$return = "";
		foreach($ms_listing_building as $key => $value){
			if($value['id'] == $param_building && $value['lang'] == 2){
				if($value['parent_id'] != 0){
					$parent_id = $value['parent_id'];
					foreach($ms_listing_building as $key2 => $value2){
						if($value2['id'] == $parent_id && $value2['lang'] == 2){
							$return = $value2['type'];
							break;
						}
					}
					if($return == ""){
						$return = $value['type'];
					}
				} else {
					$return = $value['type'];
				}
				break;
			}
		}
		if($return == ""){
			$return = 'House';
		}
		return $return;
	}

	private function GetListArea(){
		$area = json_decode(file_get_contents(public_path().'/data/indonesia.json'));
		$return = [];
		foreach($area as $area_key => $area_value){
			$current['id'] = $area_value->id;
			$current['name'] = $area_value->name;
			array_push($return, $current);
		}
		return $return;
	}

	public function ViewOverview(){
		$listing_status = array('active', 'archived', 'sold_leased');
		$listing_building = $this->GetListingBuilding($this->base_listing_building, 2);
		// $top_listing = $this->GetOverview5(Session::get('region'));
		if(Session::get('region') == "indonesia"){
			$area = $this->GetListArea();
		} else {
			$area = [];
		}
		return view('overview.overview')
		->with('listing_status', $listing_status)
		->with('listing_building', $listing_building)
		// ->with('top_listing', $top_listing)
		->with('area', $area);
	}

	private function CountListing($region, $date_start, $date_end){
		$url_listing = env('HOST_ELASTIC').env('ELASTIC_LISTING')."_count";
		$param_active_listing = '{
			"query": {
				"bool": {
					"must": [{
						"match_phrase": {
							"region": "'.$region.'"
						}
					},
					{
						"range": {
							"created_at": {
								"gte": "'.$date_start.'"
							}
						}
					},
					{
						"range": {
							"created_at": {
								"lte": "'.$date_end.'"
							}
						}
					},
					{
						"match_phrase": {
							"status": "active"
						}
					}],
					"must_not": {
						"exists":{
							"field":"deleted_at"
						}
					}
				}
			}
		}';
		$param_archived_listing = '{
			"query": {
				"bool": {
					"must": [{
						"match_phrase": {
							"region": "'.$region.'"
						}
					},
					{
						"range": {
							"created_at": {
								"gte": "'.$date_start.'"
							}
						}
					},
					{
						"range": {
							"created_at": {
								"lte": "'.$date_end.'"
							}
						}
					},
					{
						"match_phrase": {
							"status": "archived"
						}
					}],
					"must_not": {
						"exists":{
							"field":"deleted_at"
						}
					}
				}
			}
		}';
		$param_sold_listing = '{
			"query": {
				"bool": {
					"must": [{
						"match_phrase": {
							"region": "'.$region.'"
						}
					},
					{
						"range": {
							"created_at": {
								"gte": "'.$date_start.'"
							}
						}
					},
					{
						"range": {
							"created_at": {
								"lte": "'.$date_end.'"
							}
						}
					},
					{
						"match_phrase": {
							"status": "sold"
						}
					},
					{
						"match_phrase": {
							"type_property.id": "1"
						}
					}],
					"must_not": {
						"exists":{
							"field":"deleted_at"
						}
					}
				}
			}
		}';
		$param_leased_listing = '{
			"query": {
				"bool": {
					"must": [{
						"match_phrase": {
							"region": "'.$region.'"
						}
					},
					{
						"range": {
							"created_at": {
								"gte": "'.$date_start.'"
							}
						}
					},
					{
						"range": {
							"created_at": {
								"lte": "'.$date_end.'"
							}
						}
					},
					{
						"match_phrase": {
							"status": "sold"
						}
					},
					{
						"terms": {
							"type_property.id": ["2","3"]
						}
					}],
					"must_not": {
						"exists":{
							"field":"deleted_at"
						}
					}
				}
			}
		}';
		$result_active_listing = json_decode($this->curlPostContents($url_listing, $param_active_listing), TRUE);
		$result_archived_listing = json_decode($this->curlPostContents($url_listing, $param_archived_listing), TRUE);
		$result_sold_listing = json_decode($this->curlPostContents($url_listing, $param_sold_listing), TRUE);
		$result_leased_listing = json_decode($this->curlPostContents($url_listing, $param_leased_listing), TRUE);
		if(isset($result_active_listing['count'])){
			$return['active_listing'] = intval($result_active_listing['count']);
		} else {
			$return['active_listing'] = 0;
		}
		if(isset($result_archived_listing['count'])){
			$return['archived_listing'] = intval($result_archived_listing['count']);
		} else {
			$return['archived_listing'] = 0;
		}
		if(isset($result_sold_listing['count'])){
			$return['sold_listing'] = intval($result_sold_listing['count']);
		} else {
			$return['sold_listing'] = 0;
		}
		if(isset($result_leased_listing['count'])){
			$return['leased_listing'] = intval($result_leased_listing['count']);
		} else {
			$return['leased_listing'] = 0;
		}
		return $return;
	}

	private function CountUser($region, $date_start, $date_end, $total_user){
		$url_user = env('HOST_ELASTIC').env('ELASTIC_USER')."_count";
		$param_lister = '{
			"query":{
				"bool":{
					"must":[{
						"match_phrase": {
							"region": "'.$region.'"
						}
					},
					{
						"range": {
							"created_at": {
								"gte": "'.$date_start.'"
							}
						}
					},
					{
						"range": {
							"created_at": {
								"lte": "'.$date_end.'"
							}
						}
					},
					{
						"range": {
							"uid": {
								"gte": 1
							}
						}
					}]
				}
			}
		}';
		$result_lister = json_decode($this->curlPostContents($url_user, $param_lister), TRUE);
		if(isset($result_lister['count'])){
			$return['lister'] = intval($result_lister['count']);
		} else {
			$return['lister'] = 0;
		}
		$return['non_lister'] = $total_user - $return['lister'];
		return $return;
	}

	public function GetOverview1(Request $req){
		$region = $req->input('region');
		$region_id = $this->GetRegionIDFromName($this->base_region, $region);
		$date_start = Carbon::parse($req->input('date_start'))->startOfDay()->format('Y-m-d H:i:s');
		$date_end = Carbon::parse($req->input('date_end'))->endOfDay()->format('Y-m-d H:i:s');
		$url_listing = env('HOST_ELASTIC').env('ELASTIC_LISTING')."_count";
		$url_user = env('HOST_ELASTIC').env('ELASTIC_USER')."_count";
		// total listing, deleted juga dihitung
		$param_total_listing = '{
			"query": {
				"bool": {
					"must": [{
						"match_phrase": {
							"region": "'.$region_id.'"
						}
					},
					{
						"range": {
							"created_at": {
								"gte": "'.$date_start.'"
							}
						}
					},
					{
						"range": {
							"created_at": {
								"lte": "'.$date_end.'"
							}
						}
					}],
					"must_not": {
						"exists": {
							"field": "deleted_at"
						}
					}
				}
			}
		}';
		$param_total_user = '{
			"query": {
				"bool": {
					"must":[{
						"match_phrase": {
							"region": "'.$region_id.'"
						}
					},
					{
						"range": {
							"created_at": {
								"gte": "'.$date_start.'"
							}
						}
					},
					{
						"range": {
							"created_at": {
								"lte": "'.$date_end.'"
							}
						}
					}]
				}	
			}
		}';
		// CURL POST GET DATA
		$result_total_listing = json_decode($this->curlPostContents($url_listing, $param_total_listing), TRUE);
		$result_total_user = json_decode($this->curlPostContents($url_user, $param_total_user), TRUE);
		if(isset($result_total_listing['count'])){
			$return['total_listing'] = intval($result_total_listing['count']);
			$return['data_listing'] = $this->CountListing($region_id, $date_start, $date_end);
		} else {
			$return['total_listing'] = 0;
			$return['data_listing'] = array(
				"active_listing" => 0,
				"archived_listing" => 0,
				"sold_listing" => 0,
				"leased_listing" => 0,
			);
		}
		if(isset($result_total_user['count'])){
			$return['total_user'] = intval($result_total_user['count']);
			$return['data_user'] = $this->CountUser($region_id, $date_start, $date_end,$return['total_user']);
		} else {
			$return['total_listing'] = 0;
			$return['data_user'] = array(
				"lister" => 0,
				"non_lister" => 0,
			);
		}
		return $return;
	}

	public function GetOverview2(Request $req){
		$region = $req->input('region');
		$region_id = $this->GetRegionIDFromName($this->base_region, $region);
		switch($req->input('flag')){
			case "active": $status = "active";break;
			case "archived": $status = "archived";break;
			case "sold_leased": $status = "sold";break;
			default : $status = "active";break;
		}
		$date_start = Carbon::parse($req->input('date_start'))->startOfDay()->format('Y-m-d H:i:s');
		$date_end = Carbon::parse($req->input('date_end'))->endOfDay()->format('Y-m-d H:i:s');
		$url_listing = env('HOST_ELASTIC').env('ELASTIC_LISTING')."_search";
		$param_aggs = '{
			"size": 0,
			"query": {
				"bool": {
					"must": [{
						"match_phrase": {
							"region": "'.$region_id.'"
						}
					},
					{
						"range": {
							"created_at": {
								"gte": "'.$date_start.'"
							}
						}
					},
					{
						"range": {
							"created_at": {
								"lte": "'.$date_end.'"
							}
						}
					},
					{
						"match_phrase": {
							"status": "'.$status.'"
						}
					}],
					"must_not": {
						"exists": {
							"field": "deleted_at"
						}
					}
				}
			},
			"aggs": {
				"type_property": {
					"terms": {
						"field": "type_property.id"
					}
				}
			}
		}';
		$result_aggs = json_decode($this->curlPostContents($url_listing, $param_aggs), TRUE);
		if(isset($result_aggs['hits']['total']) && $result_aggs['hits']['total'] != 0){
			$buckets = $result_aggs['aggregations']['type_property']['buckets'];
			$sell = $rent = 0;
			for($i=0;$i<count($buckets);$i++){
				if($buckets[$i]['key'] == "1"){
					$sell = intval($buckets[$i]['doc_count']);
				} else {
					$rent += intval($buckets[$i]['doc_count']);
				}
			}
			$return['total'] = $sell+$rent;
			$return['sell'] = $sell;
			$return['rent'] = $rent;
		} else {
			$return['total'] = 0;
			$return['sell'] = 0;
			$return['rent'] = 0;
		}
		return $return;
	}

	public function GetOverview3(Request $req){
		$region = $req->input('region');
		$region_id = $this->GetRegionIDFromName($this->base_region, $region);
		switch($req->input('flag')){
			case "active": $status = "active";break;
			case "archived": $status = "archived";break;
			case "sold_leased": $status = "sold";break;
			default : $status = "active";break;
		}
		$date_start = Carbon::parse($req->input('date_start'))->startOfDay()->format('Y-m-d H:i:s');
		$date_end = Carbon::parse($req->input('date_end'))->endOfDay()->format('Y-m-d H:i:s');
		$boundary_points_query = "";
		if($region == "indonesia"){
			$area = $req->input('area');
			$boundary_points_query = "";
			if($area != "all"){
				$list_area = json_decode(file_get_contents(public_path().'/data/indonesia.json'));
				foreach($list_area as $area_key => $wnp_area){
					if($wnp_area->id == $area){
						$boundary_points = $wnp_area->boundary_points;
						break;
					}
				}
				foreach($boundary_points as $key => $value) {
					if($key == 0){
						$boundary_points_query.= '{  "lat": '.$value->lat.',
							"lon": '.$value->lng.'
						}';
					} else {
						$boundary_points_query.= ',{  "lat": '.$value->lat.',
							"lon": '.$value->lng.'
						}';
					}
				}
			}
			$area = $req->input('area');
			
			if($area != "all"){
				$list_area = json_decode(file_get_contents(public_path().'/data/indonesia.json'));
				foreach($list_area as $area_key => $wnp_area){
					if($wnp_area->id == $area){
						$boundary_points = $wnp_area->boundary_points;
						break;
					}
				}
				foreach($boundary_points as $key => $value) {
					if($key == 0){
						$boundary_points_query.= '{  "lat": '.$value->lat.',
							"lon": '.$value->lng.'
						}';
					} else {
						$boundary_points_query.= ',{  "lat": '.$value->lat.',
							"lon": '.$value->lng.'
						}';
					}
				}
			}
		}
		$url_listing = env('HOST_ELASTIC').env('ELASTIC_LISTING')."_search";
		$param_aggs = '{
			"size": 0,
			"query": {
				"bool": {
					"must": [{
						"match_phrase": {
							"region": "'.$region_id.'"
						}
					},
					{
						"range": {
							"created_at": {
								"gte": "'.$date_start.'"
							}
						}
					},
					{
						"range": {
							"created_at": {
								"lte": "'.$date_end.'"
							}
						}
					},
					{
						"match_phrase": {
							"status": "'.$status.'"
						}
					}],
					"must_not": {
						"exists": {
							"field": "deleted_at"
						}
					}';
		if(!empty($boundary_points_query)){
			$param_aggs .= ',
					"filter": {
						"geo_polygon": {
							"location": {
								"points": [
								'.$boundary_points_query.'
								]
							}
						}
					}';
		}
		$param_aggs .= '}
			},
			"aggs": {
				"building": {
					"terms": {
						"field": "building.id",
						"size": 32
					},
					"aggs": {
						"type_property":{
							"terms": {
								"field": "type_property.id"
							}
						}
					}
				}
			}
		}';
		$result_aggs = json_decode($this->curlPostContents($url_listing, $param_aggs), TRUE);
		if(isset($result_aggs['hits']['total']) && $result_aggs['hits']['total'] != 0){
			// $count_data = [];
			// $current_data = $result_aggs['aggregations']['type_property']['buckets'];
			// foreach($current_data as $current_key => $wnp_data){
			// $building_name = strtolower(str_replace(' ', '_', $this->GetBuildingNameFromID($this->base_listing_building, $wnp_data['key'])));
			// 	if(!isset($count_data[$building_name])){
			// 		$current['count'] = intval($wnp_data['doc_count']);
			// 		switch($building_name){
			// 			case "house": $current['color'] = '#456c91';break;
			// 			case "apartment": $current['color'] = '#5acecd';break;
			// 			case "hdb": $current['color'] = '#c2e286';break;
			// 			case "land": $current['color'] = '#ead148';break;
			// 			case "shophouse": $current['color'] = '#fda845';break;
			// 			case "retail_space": $current['color'] = '#b86c5e';break;
			// 			case "office_space": $current['color'] = '#5eb88d';break;
			// 			case "warehouse": $current['color'] = '#86a9e2';break;
			// 			case "factory": $current['color'] = '#d09bf2';break;
			// 			case "room_rental": $current['color'] = '#f29ba7';break;
			// 		}
			// 		$count_data[$building_name] = $current;
			// 	} else {
			// 		$count_data[$building_name]['count'] += intval($wnp_data['doc_count']);
			// 	}
			// }
			// // $return_data = [];
			// // foreach($count_data as $key_data => $wnp_count_data){
			// // 	$current = array(
			// // 		"id" => $key_data,
			// // 		"count" => $wnp_count_data['count'],
			// // 		"color" => $wnp_count_data['color'],
			// // 	);
			// // 	array_push($return_data, $current);
			// // }
			// $return['total'] = $result_aggs['hits']['total'];
			// $return['data'] = $count_data;

			$count_data = [];
			$total_sell = $total_rent = 0;
			$buckets = $result_aggs['aggregations']['building']['buckets'];
			foreach($buckets as $buckets_key => $wnp_buckets){
				$sell = $rent = 0;
				$building_name = strtolower(str_replace(' ', '_', $this->GetBuildingNameFromID($this->base_listing_building, $wnp_buckets['key'])));
				$inner_buckets = $wnp_buckets['type_property']['buckets'];
				foreach($inner_buckets as $buckets_key2 => $type_buckets){
					if($type_buckets['key'] == "1"){
						$sell += intval($type_buckets['doc_count']);
					} else {
						$rent += intval($type_buckets['doc_count']);
					}
				}
				if(!isset($count_data[$building_name])){
					switch($building_name){
						case "house": $current['color'] = '#456c91';break;
						case "apartment": $current['color'] = '#5acecd';break;
						case "hdb": $current['color'] = '#c2e286';break;
						case "land": $current['color'] = '#ead148';break;
						case "shophouse": $current['color'] = '#fda845';break;
						case "retail_space": $current['color'] = '#b86c5e';break;
						case "office_space": $current['color'] = '#5eb88d';break;
						case "warehouse": $current['color'] = '#86a9e2';break;
						case "factory": $current['color'] = '#d09bf2';break;
						case "room_rental": $current['color'] = '#f29ba7';break;
					}
					$current['sell'] = $sell;
					$current['rent'] = $rent;
					$count_data[$building_name] = $current;
				} else {
					$count_data[$building_name]['sell'] += $sell;
					$count_data[$building_name]['rent'] += $rent;
				}
				$total_sell += $sell;
				$total_rent += $rent;
			}
			$return['total'] = $total_sell+$total_rent;
			$return['sell'] = $total_sell;
			$return['rent'] = $total_rent;
			$return['data'] = $count_data;
		} else {
			$return['total'] = 0;
			$return['sell'] = 0;
			$return['rent'] = 0;
			$return['data'] = [];
		}
		return $return;
	}

	public function GetOverview4(Request $req){
		$region = $req->input('region');
		$region_id = $this->GetRegionIDFromName($this->base_region, $region);
		$date_start = Carbon::parse($req->input('date_start'))->startOfMonth()->format('Y-m-d H:i:s');
		$date_end = Carbon::parse($req->input('date_end'))->endOfMonth()->format('Y-m-d H:i:s');
		$url_user = env('HOST_ELASTIC').env('ELASTIC_USER')."_search";
		$param_aggs = '{
			"size": 0,
			"query": {
				"bool": {
					"must": [{
						"match_phrase": {
							"region": "'.$region_id.'"
						}
					},
					{
						"range": {
							"created_at": {
								"gte": "'.$date_start.'"
							}
						}
					},
					{
						"range": {
							"created_at": {
								"lte": "'.$date_end.'"
							}
						}
					}]
				}
			},
			"aggs": {
				"month_range": {
					"date_histogram": {
						"field": "created_at",
						"interval": "month"
					},
					"aggs": {
						"uid_check":{
							"filters": {
								"filters": {
									"lister":{
										"range": {
											"uid": {
												"gte": 1
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}';
		$result_aggs = json_decode($this->curlPostContents($url_user, $param_aggs), TRUE);
		if(isset($result_aggs['hits']['total']) && $result_aggs['hits']['total'] != 0){
			// $param_first = '{
			// 	"size": 0,
			// 	"query": {
			// 		"bool": {
			// 			"must": [{
			// 					"match_phrase": {
			// 						"region": "'.$region_id.'"
			// 					}
			// 				},
			// 				{
			// 					"range": {
			// 						"created_at": {
			// 							"gte": "2015-01-01 00:00:00"
			// 						}
			// 					}
			// 				},
			// 				{
			// 					"range": {
			// 						"created_at": {
			// 							"lte": "'.$date_start.'"
			// 						}
			// 					}
			// 				}
			// 			]
			// 		}
			// 	},
			// 	"aggs": {
			// 		"uid_check": {
			// 			"filters": {
			// 				"filters": {
			// 					"lister": {
			// 						"range": {
			// 							"uid": {
			// 								"gte": 1
			// 							}
			// 						}
			// 					}
			// 				}
			// 			}
			// 		}
			// 	}
			// }';
			// $first_result = json_decode($this->curlPostContents($url_user, $param_first), TRUE);
			// $total_user = intval($first_result['hits']['total']);
			// $total_lister = intval($first_result['aggregations']['uid_check']['buckets']['lister']['doc_count']);
			// $total_non_lister = $total_user - $total_lister;
			// $buckets = $result_aggs['aggregations']['month_range']['buckets'];
			// $categories = $total = $lister = $non_lister = [];
			// foreach($buckets as $bucket_key => $wnp_buckets){
			// 	$date = Carbon::parse($wnp_buckets['key_as_string'])->format('M Y');
			// 	array_push($categories, $date);
			// 	$total_user += intval($wnp_buckets['doc_count']);
			// 	array_push($total, $total_user);
			// 	$total_lister +=  intval($wnp_buckets['uid_check']['buckets']['lister']['doc_count']);
			// 	array_push($lister, $total_lister);
			// 	$total_non_lister += intval($wnp_buckets['doc_count']) -  intval($wnp_buckets['uid_check']['buckets']['lister']['doc_count']);
			// 	array_push($non_lister, $total_non_lister);
			// }
			$categories = $total = $lister = $non_lister = [];
			$buckets = $result_aggs['aggregations']['month_range']['buckets'];
			foreach($buckets as $bucket_key => $wnp_buckets){
				$date = Carbon::parse($wnp_buckets['key_as_string'])->format('M Y');
				array_push($categories, $date);
				array_push($total, intval($wnp_buckets['doc_count']));
				array_push($lister, intval($wnp_buckets['uid_check']['buckets']['lister']['doc_count']));
				array_push($non_lister, intval($wnp_buckets['doc_count'])-intval($wnp_buckets['uid_check']['buckets']['lister']['doc_count']));
			}
			$return['total'] = $total;
			$return['categories'] = $categories;
			$return['lister'] = $lister;
			$return['non_lister'] = $non_lister;
		} else {
			$return['total'] = 0;
			$return['categories'] = 0;
			$return['lister'] = 0;
			$return['non_lister'] = 0;
		}
		return $return;	
	}

	public function GetOverview5($region){
		$url_listing = env('HOST_ELASTIC').env('ELASTIC_LISTING')."_search";
		// $region = $req->input('region');
		if($region != "indonesia"){
			$return['Status'] = 'failed';
			return $return;
		}
		// ======= VERY DANGEROUS QUERY =======
		// $area_json = json_decode(file_get_contents(public_path().'/data/indonesia.json'));
		// $area = [];
		// foreach($area_json as $area_key => $area_value){
		// 	$current = [];
		// 	$current['City'] = $area_value->name;
		// 	$query_string_point = "";
		// 	foreach($area_value->boundary_points as $point_key => $point_value){
		// 		if($point_key == 0){
		// 			$query_string_point .= '{ "lat": '.$point_value->lat.', "lon": '.$point_value->lng.' }';
		// 		} else {
		// 			$query_string_point .= ',{ "lat": '.$point_value->lat.', "lon": '.$point_value->lng.' }';
		// 		}
		// 	}
		// 	$current['Points'] = $query_string_point;
		// 	array_push($area, $current);
		// }
		// $param_aggs = '{
		// 	"size": 0,
		// 	"query": {
		// 		"bool": {
		// 			"must_not": {
		// 				"exists": {
		// 					"field": "deleted_at"
		// 				}
		// 			}
		// 		}
		// 	},
		// 	"aggs": {
		// 		"listing_area": {
		// 			"filters": {
		// 				"filters":{';
		// foreach($area as $area_key => $area_value){
		// 	if($area_key == 0){
		// 		$param_aggs .= '"'.$area_value['City'].'": {
		// 			"geo_polygon": {
		// 				"location": {
		// 					"points": ['.$area_value['Points'].']
		// 				}
		// 			}
		// 		}';
		// 	} else {
		// 		$param_aggs .= ',"'.$area_value['City'].'": {
		// 			"geo_polygon": {
		// 				"location": {
		// 					"points": ['.$area_value['Points'].']
		// 				}
		// 			}
		// 		}';
		// 	}
		// }
		// $param_aggs .= '}
		// 			}
		// 		}
		// 	}
		// }';
		// ======= VERY DANGEROUS QUERY =======
		$param_aggs = '{
			"query": {
				"bool": {
					"must_not": {
						"exists": {
							"field": "deleted_at"
						}
					}
				}
			},
			"aggs": {
				"listing_area": {
					"terms": {
						"field": "province.raw",
						"size": "50",
						"order": {"_count": "desc"}
					}
				}
			}
		}';
		$result_aggs = json_decode($this->curlPostContents($url_listing, $param_aggs), TRUE);
		$return_bucket = [];
		if(isset($result_aggs['hits']['total']) && $result_aggs['hits']['total'] != 0){
			$buckets = $result_aggs['aggregations']['listing_area']['buckets'];
			usort($buckets, function($a, $b){
				return $retval = $b['doc_count'] <=> $a['doc_count'];
			});
			foreach($buckets as $bucket_key => $wnp_buckets){
				if($bucket_key == 20){
					break;
				} else {
					$current['no'] = $bucket_key+1;
					$current['area'] = ucwords($wnp_buckets['key']);
					$current['listing'] = $wnp_buckets['doc_count'];
				}
				array_push($return_bucket, $current);
			}
		}
		return $return_bucket;
	}
}
