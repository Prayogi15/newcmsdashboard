<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(){
    }
    
    protected function curlPutContents($url, $data_json){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		curl_close($ch);
		//print_r($response);exit();
		return $response;
    }

    protected function curlPostContents($url, $data_json = null)
	{	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		curl_close($ch);
		//print_r($response);exit();
		return $response;
	}
	protected function getDataFromAPIGet($url)
	{
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$output=curl_exec($ch);
		curl_close($ch);
		return $output;
	}

	public function GetUserById(Request $req){
		$user_id = $req->input('user_id');
		$urlElastic = env('HOST_ELASTIC').env('ELASTIC_USER')."_search?q=id:".$user_id;
		$data = json_decode($this->getDataFromAPIGet($urlElastic));
		$result = (array)$data->hits;
		$hits = (array)$result['hits'][0];
		$current = $hits["_source"];
		$return_data['Status'] = "success";
		$return_data['Data']['Email'] = $current->username;
		$return_data['Data']['Gov_ID'] = $current->gov_photo_id;
		$return_data['Data']['Gov_User'] = $current->gov_photo_user;
		return $return_data;
	}
	public function Development(){
		$session = Session::all();
		return $session;
	}

	public function Testing(Request $req){
		$url = env('HOST_ELASTIC').env('ELASTIC_USER')."1";
		$array_target = explode('#',$req->input('target'));
		$array_value = explode('#',$req->input('value'));
		$response = $this->UpdateElastic($url, $array_target, $array_value);
		return $response;
	}

	protected function UpdateElastic($url, $array_target, $array_value){
		$error_data["Status"] = "failed";
		$array_target = (array)$array_target;
		$array_value = (array)$array_value;
		if(count($array_target) != count($array_value)){
			$error_data["Message"] = "Please check the arrays";
		}
		// $url = env('HOST_ELASTIC').env('ELASTIC_LISTING').$id;
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$output=(array)json_decode(curl_exec($ch), TRUE);
		curl_close($ch);
		if($output['found'] == false){
			$error_data["Message"] = "ID not found";
			return $error_data;
		}
		$data = $output['_source'];
		// return $output;
		for($i=0;$i<count($array_target);$i++){
			if(strpos($array_target[$i], '.') !== false){
				$fields = explode('.', $array_target[$i]);
				$current = $output['_source'];
				for($j=0;$j<count($fields);$j++){
					if(array_key_exists($fields[$j], $current)){
						$current = $current[$fields[$j]];
					} else {
						$error_data["Message"] = "Index array not found";
						return $error_data;
					}
				}
				// hardcode vvv
				$data[$fields[0]][$fields[1]] = $array_value[$i];
			} else {
				if(array_key_exists($array_target[$i], $data)){
					$data[$array_target[$i]] = $array_value[$i];
				} else {
					$error_data["Message"] = "Index array not found";
					return $error_data;
				}
			}
		}
		$response_curl = (array)json_decode($this->curlPutContents($url, json_encode($data)),TRUE);
		if($response_curl['_shards']["failed"] != 0){
			$error_data["Message"] = "Failed update";
			return $error_data;
		} else {
			$success_data["Status"] = "success";
			$success_data["Message"] = "Success update";
			return $success_data;
		}
	}
}
