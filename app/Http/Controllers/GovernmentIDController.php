<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\User;
use App;
use App\Sync;
use Session;

class GovernmentIDController extends Controller
{
    protected $base_region;
    public function __construct(){
        $this->client = new Client(['base_uri' => env('API_URL')]);
        $this->headers = [
            // 'Authorization' => env('TOKEN_NON_LOGIN'),
            'lang' => 'en',
            'region' => 'idn',
            'version' => env('APP_VERSION'),
            'appversion' => env('APP_VERSION'),
            'noauth' => 'y',
        ];

        parent::__construct();
        $this->Sync = new Sync;
        if($this->Sync->isExist()){
            $this->SyncData = $this->Sync->getData(true);
        } else {
            $this->SyncData = $this->Sync->setData($this->Sync->sync());
        }
        foreach($this->SyncData['ms_region'] as $keys => $value){
            $this->SyncData['ms_region'][$keys] = (array)$value;
        }
        $this->base_region = collect($this->SyncData['ms_region']);
    }
    private function GetRegionIDFromName($ms_region,$param_region_name){
        $return = "";
        foreach($ms_region as $key => $value){
            if($value['full_name'] == ucfirst($param_region_name)){
                $return = $value['id'];
                break;
            }
        }
        if($return == ""){
            $return = '2';
        }
        return $return;
    }
    private function GetRegionNameFromCode($ms_region, $param_region_code){
        $return = "";
        foreach($ms_region as $key => $value){
            if($value['name'] == $param_region_code){
                $return = strtolower($value['full_name']);
                break;
            }
        }
        if($return == ""){
            $return = 'indonesia';
        }
        return $return;
    }
    public function ChangeRegion($region){
        $reg_name = $this->GetRegionNameFromCode($this->base_region, $region);
        Session::put('region', $reg_name);
        return redirect()->back();
    }
    public function ViewGovernmentID(){
    	$status = array('pending','open','completed','problem');
    	return view('admin/govermentID')
        ->with('status', $status);
    }
    public function GetListGovernmentIDElastic(Request $req){
        if($req->input('status') !== null){
            $status = $req->input('status');
        } else {
            $status = 'pending';
        }
        if($req->input('region') !== null){
            $region = $this->GetRegionIDFromName($this->base_region, $req->input('region'));
        } else {
            $region = $this->GetRegionIDFromName($this->base_region, 'indonesia');
        }
        if(!empty($_REQUEST['length'])){
            $limit = (int)$_REQUEST['length'];
        } else {
            $limit = 20;
        }
        if(!empty($_REQUEST['start'])){
            $start = (int)$_REQUEST['start'];
        } else {
            $start = 0;
        }
        if (!empty($_REQUEST['draw'])) {
            $draw = $_REQUEST['draw'];
        }else{
            $draw = 10;
        }
        if(!empty($_REQUEST['order']['0']['column'])){
            $sort_by=$_REQUEST['order']['0']['column'];
        } else {
            $sort_by = '0';
        }
        if(!empty($_REQUEST['order']['0']['dir'])){
            $order_by=$_REQUEST['order']['0']['dir'];
        } else {
            $order_by = 'desc';
        }
        if(!empty($_REQUEST['search']['value'])){
            $search = strtolower($_REQUEST['search']['value']);
        }
        $arr_column = array('uid','first_name','username','phone_number');
        $urlListing = env('HOST_ELASTIC').env('ELASTIC_USER')."_search";
       /* $paramJSON = '{
            "size": '.$limit.',
            "from": '.$start.',
            "sort": [
                {
                    "'.$arr_column[$sort_by].'" : {"order" : "'.$order_by.'"}
                }
            ],
            "query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must":[';*/
							
		$paramJSON = '{
            "size": '.$limit.',
            "from": '.$start.',
            "query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must":[';					

        if(!empty($region)){
            $paramJSON .= '{
                                "match_phrase": {
                                    "region": "'.$region.'"
                                }
                            }';
        }
        if(!empty($search)){
            $paramJSON .= ',{
                                "query_string": {
                                    "query": "*'.$search.'*",
                                    "fields": ["pid","prefered_address","address","type_property.name","building.type","user.*name"],
                                    "operator": "or"
                                }
                            }';
        }
        if(!empty($status)){
            switch($status){
                case 'pending': $paramJSON .= ',{
                                "match_phrase":{
                                    "gov_verified": "2"
                                }
                            }'; break;
                case 'open': $paramJSON .= ',{
                                "match_phrase":{
                                    "gov_verified": "3"
                                }
                            }'; break;
                case 'completed': $paramJSON .= ',{
                                "match_phrase":{
                                    "gov_verified": "1"
                                }
                            }'; break;
                case 'problem': $paramJSON .= ',{
                                "match_phrase":{
                                    "gov_verified": "4"
                                }
                            }'; break;
            }
        }
        $paramJSON .= ']}
                    }
                }
            }
        }';
        $result = json_decode($this->curlPostContents($urlListing, $paramJSON), TRUE);
        if(isset($result['hits'])){
            $user_array = array();
            $result_hits = $result['hits']['hits'];
            for($i=0;$i<count($result_hits);$i++){
                $current = $result_hits[$i]['_source'];
                $user_array[$i]['uid'] = strtoupper($current['uid']);
                $user_array[$i]['name'] = $current['first_name'].' '.$current['last_name'];
                $user_array[$i]['email'] = '<a href="#" data-toggle="modal" data-target="#modal-default" onclick="viewPhotos(`'.$current['id'].'`)">'.$current['username'].'</a>';
                $user_array[$i]['phone'] = $current['country_code'].$current['phone_number'];
                switch($status){
                    case 'pending': $user_array[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='open_gov' onclick='verify(`".$current['id']."`,`3`,this);'><i class='fa fa-folder-open-o' aria-hidden='true'></i> Mark as open</a></li><li><a href='#' class='complete_gov' onclick='verify(`".$current['id']."`,`1`,this);'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Mark as completed</a></li><li><a href='#' class='problem_gov' onclick='verify(`".$current['id']."`,`4`,this);'><i class='fa fa-exclamation-circle' aria-hidden='true'></i> Mark as problem</a></li></ul></div></td>"; break;
                    case 'open': $user_array[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='pending_gov' onclick='verify(`".$current['id']."`,`2`,this);'><i class='fa fa-question' aria-hidden='true'></i> Mark as pending</a></li><li><a href='#' class='complete_gov' onclick='verify(`".$current['id']."`,`1`,this);'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Mark as completed</a></li><li><a href='#' class='problem_gov' onclick='verify(`".$current['id']."`,`4`,this);'><i class='fa fa-exclamation-circle' aria-hidden='true'></i> Mark as problem</a></li></ul></div></td>"; break;
                    case 'completed': $user_array[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='pending_gov' onclick='verify(`".$current['id']."`,`2`,this);'><i class='fa fa-question' aria-hidden='true'></i> Mark as pending</a></li><li><a href='#' class='open_gov' onclick='verify(`".$current['id']."`,`3`,this);'><i class='fa fa-folder-open-o' aria-hidden='true'></i> Mark as open</a></li><li><a href='#' class='problem_gov' onclick='verify(`".$current['id']."`,`4`,this);'><i class='fa fa-exclamation-circle' aria-hidden='true'></i> Mark as problem</a></li></ul></div></td>"; break;
                    case 'problem': $user_array[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='pending_gov' onclick='verify(`".$current['id']."`,`2`,this);'><i class='fa fa-question' aria-hidden='true'></i> Mark as pending</a></li><li><a href='#' class='open_gov' onclick='verify(`".$current['id']."`,`3`,this);'><i class='fa fa-folder-open-o' aria-hidden='true'></i> Mark as open</a></li><li><a href='#' class='complete_gov' onclick='verify(`".$current['id']."`,`1`,this);'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Mark as completed</a></li></ul></div></td>"; break;
                }
            }
            $response = array('draw' => intval($draw),
                        'recordsTotal' => intval($result['hits']['total']),
                        'recordsFiltered' => intval($result['hits']['total']),
                        'data' => $user_array);
        } else {
            $response = array('draw' => 0,
                        'recordsTotal' => 0,
                        'recordsFiltered' => 0,
                        'data' => []);
        }
        return $response;
    }
    public function UpdateStatusGovermentVerified(Request $req){
        $id = $req->input('user_id');
        $status_id = $req->input('status_id');
        $params = [
            'user_id' => $id,
            'gov_verified' => $status_id,
            'client_id' => env('client_id'),
            'client_secret' => env('client_secret'),
            'client_source' => env('client_source'),
        ];
        try{
            $results = $this->client->put('update_gov', [
                'headers' => $this->headers,
                'form_params' => $params
            ]);
            $results = json_decode($results->getBody(), true);
        } catch (\Exception $e){
            \Log::error($e);
            dd($e);
            $results['Code'] = '401';  
        }
        return $results;
    }
}
