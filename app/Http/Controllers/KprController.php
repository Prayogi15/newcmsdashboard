<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App;
use Session;
use App\Sync;
use Carbon\Carbon;

class KprController extends Controller
{
	protected $base_region;
	public function __construct(){
	}
	
	private function GetRegionIDFromName($ms_region,$param_region_name){
		$return = "";
		foreach($ms_region as $key => $value){
			if($value['full_name'] == ucfirst($param_region_name)){
				$return = $value['id'];
				break;
			}
		}
		if($return == ""){
			$return = '2';
		}
		return $return;
	}
	
	public function index(){
		$id= Session::get('admin_id');
		$data_kpr = DB::connection('mysql5')->table('wnp_kpr.v_kpr_list')
                            ->where('statuskpr','!=',"removed")
							->orderBy('created_at', 'desc')
							->get();
		$lang = array('en','id');	
		
		$string = file_get_contents("./data/emailkpr.json");
		$emaillist = json_decode($string, true);
		
		$list_email="";
		for($i=0;$i<count($emaillist);$i++)
		{
			$list_email .= $emaillist[$i]['email'] .", ";
		}	
		$list_email=rtrim($list_email,", ");
		return view('kpr/kpr_list')
		->with('data_kpr', $data_kpr)
		->with('lang', $lang)
		->with('list_email', $list_email);
	}
	
	public function GetDataKpr(Request $req){
		if($req->input('region') !== null){
			if($req->input('region')=='indonesia'){
				$region = array('all','idn');
			} else {
				$region = array('all','sgp');
			}
		}
		if(!empty($_REQUEST['length'])){
			$limit = (int)$_REQUEST['length'];
		} else {
			$limit = 20;
		}
		if(!empty($_REQUEST['start'])){
			$start = (int)$_REQUEST['start'];
		} else {
			$start = 0;
		}
		if(!empty($_REQUEST['order']['0']['column'])){
			$sort_by=$_REQUEST['order']['0']['column'];
		} else {
			$sort_by = '12'; //diambil dari created ad. defaulnya di data
		}
		if(!empty($_REQUEST['order']['0']['dir'])){
			$order_by=$_REQUEST['order']['0']['dir'];
		} else {
			$order_by = 'desc';
		}
		if(!empty($_REQUEST['draw'])){
			$draw = $_REQUEST['draw'];
		} else {
			$draw = '10';
		}
		if(!empty($_REQUEST['search']['value'])){
			$search = strtolower($_REQUEST['search']['value']);
			$arr_search = array('v_kpr_list.bank_name','v_kpr_list.office','v_kpr_list.code','v_kpr_list.fullname','v_kpr_list.email','v_kpr_list.phone_number','v_kpr_list.nik','v_kpr_list.created_at','v_kpr_list.pid','v_kpr_list.uid','v_kpr_list.price','v_kpr_list.loan','v_kpr_list.period');
		}
		$arr_column = array(null,'v_kpr_list.bank_name','v_kpr_list.office','v_kpr_list.code','v_kpr_list.fullname','v_kpr_list.email','v_kpr_list.phone_number','v_kpr_list.nik','v_kpr_list.pid','v_kpr_list.price','v_kpr_list.loan','v_kpr_list.period','v_kpr_list.created_at',null);

					$eloquent = DB::connection('mysql5')->table('wnp_kpr.v_kpr_list as v_kpr_list')
							->where('statuskpr','!=',"removed");
			$total_all = $eloquent->count();
		if(!empty($search)){
			$eloquent = $eloquent->where(function($query) use ($search, $arr_search){
				for($i=0;$i<count($arr_search);$i++){
					//if($i == 0){
						if($i == 0){
							$query = $query->where($arr_search[$i], 'LIKE', '%'.$search.'%');
						} else {
							$query = $query->orWhere($arr_search[$i], 'LIKE', '%'.$search.'%');
						}
					//}
				}
			});
		}
		$total = $eloquent->count();
		$kpr = $eloquent->orderBy($arr_column[$sort_by], $order_by)
		->offset($start)->limit($limit)
		->select('v_kpr_list.bank_name','v_kpr_list.office','v_kpr_list.code','v_kpr_list.fullname','v_kpr_list.email','v_kpr_list.phone_number','v_kpr_list.nik','v_kpr_list.pid','v_kpr_list.price','v_kpr_list.loan','v_kpr_list.period','v_kpr_list.created_at')
		->get();
		if(count($kpr)>0){
			$n=1;
			for($i=0;$i<count($kpr);$i++){
				$items[$i]['no'] = $n;
				$items[$i]['bank_name'] 	= $kpr[$i]->bank_name;
				$items[$i]['office'] 		= $kpr[$i]->office;
				$items[$i]['code'] 			= $kpr[$i]->code;
				$items[$i]['fullname'] 		= $kpr[$i]->fullname;
				$items[$i]['email'] 		= $kpr[$i]->email;
				$items[$i]['phone_number'] 	= $kpr[$i]->phone_number;
				$items[$i]['nik'] 			= $kpr[$i]->nik;
				$items[$i]['pid'] 			= $kpr[$i]->pid;
				$items[$i]['price'] 		= number_format($kpr[$i]->price, 0, ',','.');
				$items[$i]['loan'] 			= number_format($kpr[$i]->loan, 0, ',','.');
				$items[$i]['period'] 		= $kpr[$i]->period;
				$items[$i]['created_at'] 	= $kpr[$i]->created_at;
				$items[$i]['remove'] 		= "<button class='btn btn-warning' onclick=\"remove('". $kpr[$i]->code ."')\"> <i class='fa fa-remove'></i></button>";
			$n++;
			}
			$response = array('draw' => intval($draw),
					'recordsTotal' => intval($total_all),
					'recordsFiltered' => intval($total),
					'data' => $items);
		} else {
			$response = array('draw' => 0,
					'recordsTotal' => 0,
					'recordsFiltered' => 0,
					'data' => []);
		}
		return $response;
	}
	
	public function download(Request $req){
		$exp	= explode(" - ",$req->daterange);
		$start	= $exp[0];
		$end	= $exp[1];
		$data_kpr = DB::connection('mysql5')->table('wnp_kpr.v_kpr_list')
							->where('statuskpr','!=',"removed")
							->whereBetween('created_at', array($start,$end))
                            ->get();
		
		
		return view('kpr/kpr_download')
		->with('data_kpr', $data_kpr)
		->with('start', $start)
		->with('end', $end);
		}
		
		
	public function sendemail(Request $req)
	{
	$exp	= explode(" - ",$req->daterange);
		$start	= $exp[0];
		$end	= $exp[1];
		$data_kpr = DB::connection('mysql5')->table('wnp_kpr.v_kpr_list')
							->where('statuskpr','!=',"removed")
							->whereBetween('created_at', array($start,$end))
                            ->get();	
	
	$data= "<table class='table table-bordered table-hover' border='1'>
					<thead style='background-color:#ccc'>
                    <tr> 
					<th></th>
					<th width='100px'>Bank</th>
					<th width='150px'>Office</th>
					<th width='100px'>Code</th>
                    <th width='180px'>Name</th>
					<th width='150px'>Email</th>
					<th width='120px'>Phone Number</th>
					<th width='120px'>NIK</th>
					<th width='50px'>PID</th>
					<th width='90px' >Price</th>
					<th width='90px' >Loan</th>
					<th width='50px' >Period</th>	
					<th width='150px' >Submit Date</th>			
                   </tr>
                  </thead>
					<tbody>";
					$n=1;
					foreach($data_kpr as $row){
				$data .= "<tr><td>". $n ."</td>
					  <td>". $row->bank_name."</td>
					  <td>". $row->office."</td>
					  <td>". $row->code."</td>
					  <td>". $row->fullname."</td>
					  <td>". $row->email."</td>
					  <td>". $row->phone_number."</td>
					  <td>". $row->nik."</td>
					  <td>". $row->pid."</td>
					  <td>". $row->price."</td>
					  <td>". $row->loan."</td>
					  <td>". $row->period."</td>
					   <td>". $row->created_at."</td></tr>";
					$n++;
					}
					$data .= "</tbody></table>";

		$filename="data_kpr_".str_replace('/','',$start)."_to_".str_replace('/','',$end).".xls";
		
		//print_r($filename);
		$f = fopen("./data/".$filename , 'wb');
		fwrite($f , $data );
		fclose($f);
		chmod("./data/".$filename, 0777);
		$string = file_get_contents("./data/emailkpr.json");
		$emaillist = json_decode($string, true); //sudah berbentuk array
	
		//dirubah ke dalam bentuk array 0=>gie.hermawan@gmail.com, 1=alvindarmawijaya@yahoo.com
		$kumpulan_email= array();
		for($i=0;$i<count($emaillist);$i++)
		{
			array_push($kumpulan_email,$emaillist[$i]['email']);
		}	
		
		$pass = env('sendgrid_key'); // not the key, but the token

        $url = 'https://api.sendgrid.com/';
        $json_string = array(

        'to' => $kumpulan_email,
       'category' => 'main'
       );
		//remove the user and password params - no longer needed
		$params = array(
			'x-smtpapi' => json_encode($json_string),
			'to'        => $emaillist[0]['email'],   //email pertama  
			'subject'   => 'Laporan Data KPR',
			'html'      => "Dengan hormat, berikut ini kami lampirkan laporan KPR : <a href='". env('url_link')."data/". $filename ."'> Data KPR Tanggal ". $start ." - ". $end ." </a>",
			'from'      => 'no-reply@worknplay.com',
		);

		$request =  $url.'api/mail.send.json';
		$headr = array();
		// set authorization header
		$headr[] = 'Authorization: Bearer '.$pass;

		$session = curl_init($request);
		curl_setopt ($session, CURLOPT_POST, true);
		curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
		curl_setopt($session, CURLOPT_HEADER, false);
		curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

		// add authorization header
		curl_setopt($session, CURLOPT_HTTPHEADER,$headr);

		$response = curl_exec($session);
		curl_close($session);
		echo $response;
	}	
					
	public function remove($id){
		$id			= $id;
		$admin_id	= Session::get('admin_id');
		DB::beginTransaction();
		try{
			DB::connection('mysql5')->table('wnp_kpr.tr_kpr')
			->where('code', $id)
            ->update(['status' => "removed",
					  'deleted_at' => date('Y-m-d H:i:s'),
					  'deleted_by' => $admin_id
						]);				
		}
		catch(ErrorException $e){
			$return_data["Status"] = "failed";
			$return_data["Message"] = "Something went wrong.";
			$return_data["Error"] = $e;
			DB::rollback();
			return redirect('/kpr/list')->with("status_failed", "failed");
		}
		$return_data["Status"] = "success";
		$return_data["Message"] = "Success update report ID ".$id;
		DB::commit();
		//return $return_data;
		return redirect('/kpr/list')->with("status_success", "success");
	}
	
	public function savelistemail(Request $req){
		$emails	= $req->email;
		$explode= explode(",",$emails);
		$data_email	= array();
		foreach($explode as $row)
		{
		$email["email"] =trim($row);
		array_push($data_email,$email);
		}
		$fp = fopen('./data/emailkpr.json', 'w');
		fwrite($fp, json_encode($data_email));
		fclose($fp);
		//var_dump($data_email);
		$data = array('status' => "success",'message'=>"Data Saved");
		return json_encode($data);
		}
	
	
	}