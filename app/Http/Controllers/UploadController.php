<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class UploadController extends Controller
{
	public function UploadImage(Request $req){
		$bucketName = env('s3_bucket');
		$IAM_KEY = env('aws_access_key_id');
		$IAM_SECRET = env('aws_secret_access_key');

		// CONNECT TO AWS
		try{
			$s3 = S3Client::factory(
				array(
					'credentials' => array(
						'key' => $IAM_KEY,
						'secret' => $IAM_SECRET
					),
					'version' => 'latest',
					'region' => env('aws_region')
				)
			);
		}
		catch (Exception $e){
			// return error when connect AWS
			$return_data["Status"] = "failed";
			$return_data["Error"] = $e->getMessage();
			return $return_data;
			// die("Error: " $e->getMessage());
		}

		// declare path
		$keyName = $req->input('public_key'). str_replace(" ","_",basename($_FILES["file_image"]["name"]));
		$pathInS3 = 'https://s3.us-east-2.amazonaws.com/'.$bucketName.'/'.$keyName;
		// http://s3.us-east-2.amazonaws.com/images.worknplay.com/advertise/image.jpg
		try{
			$file = $_FILES["file_image"]['name'];

			$result = $s3->putObject(
				array(
					'Bucket'=>$bucketName,
					'Key'=>$keyName,
					'SourceFile'=>$_FILES["file_image"]['tmp_name'],
					'ACL'=>'public-read',
					'ContentType'=>exif_imagetype($_FILES["file_image"]['tmp_name'])
				)
			);
		}
		catch(S3Exception $e){
			// throw $e;
			$return_data["Status"] = "failed";
			$return_data["Error"] = $e->getMessage();
			return $return_data;
		}
		catch(Exception $e){
			// throw $e;
			$return_data["Status"] = "failed";
			$return_data["Error"] = $e->getMessage();
			return $return_data;
		}
		$return_data["Status"] = "success";
		$return_data["Path"] = $result["ObjectURL"];
		$return_data["Message"] = "Success upload image.";
		return $return_data;
	}

	public function uploadFile(Request $request) {
		$file = $request->file('file');

		$file_name = rand() . '-' . time() . '.' . $file->getClientOriginalExtension();

        $s3 = \Storage::disk('s3');
        $file_path = 'temp/' . $file_name;

        try {
        	$s3->put($file_path, file_get_contents($file), 'public');
        	return $s3->url($file_path);
        } catch (S3Exception $e) {
        	return response()->json(['error' => 'Failed when uploading to S3'], 409);
        }
	}
}
