<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App;
use App\Sync;
use App\ReportListing;
use DB;
use Carbon\Carbon;

class ListingController extends Controller
{
	protected $base_currency;
	protected $base_listing_building;
	protected $base_type;
	protected $base_region;
	protected $Sync;
	public function __construct(){
		parent::__construct();
		$this->Sync = new Sync;
		if($this->Sync->isExist()){
			$this->SyncData = $this->Sync->getData(true);
		} else {
            $this->SyncData = $this->Sync->setData($this->Sync->sync());
		}
		foreach($this->SyncData["ms_currency"] as $keys => $value){
			$this->SyncData['ms_currency'][$keys] = (array)$value;
		}
		foreach($this->SyncData["ms_listing_building"] as $keys => $value){
			$this->SyncData['ms_listing_building'][$keys] = (array)$value;
		}
		foreach($this->SyncData["ms_type"] as $keys => $value){
			$this->SyncData['ms_type'][$keys] = (array)$value;
		}
		foreach($this->SyncData['ms_region'] as $keys => $value){
        	$this->SyncData['ms_region'][$keys] = (array)$value;
		}
		$this->base_currency = collect($this->SyncData["ms_currency"]);
		$this->base_listing_building = collect($this->SyncData["ms_listing_building"]);
		$this->base_type = collect($this->SyncData["ms_type"]);
		$this->base_region = collect($this->SyncData['ms_region']);
	}

	private function GetCurrency($ms_currency, $param_currency_id){
		foreach($ms_currency as $key => $value){
			if($value['id'] == $param_currency_id){
				$return = $value['name'];
				// $return["full_name"] = $value['full_name'];
				// $return["symbol"] = $value['symbol'];
				break;
			}
		}
		return $return;
	}

	private function GetBuildingName($ms_listing_building, $param_building_id, $lang){
		if($lang == ""){
			$lang = "2";
		}
		foreach($ms_listing_building as $key => $value){
			if($value['id'] == $param_building_id && $value['lang'] == $lang){
				$return = $value['type'];
				break;
			}
		}
		return $return;
	}

	private function GetTypeName($ms_type, $param_type_id, $lang){
		if($lang == ""){
			$lang = "2";
		}
		foreach($ms_type as $key => $value){
			if($value['id'] == $param_type_id && $value['lang'] == $lang){
				$return = $value['name'];
				break;
			}
		}
		if((empty($return)) && ($param_type_id == 4)){
			$return = "rent";
		}
		return $return;
	}

	public function ViewListing(){
	    	$status = array('active','archived','sold','deleted');
	    	// var_dump($status);die();
		return view('listing/listing')
		->with('listing_status', $status);
	}

	public function GetDataListing(Request $req){
		if($req->input('status') !== null){
			$status = $req->input('status');
		}
		if($req->input('region') !== null){
			if($req->input('region')=='indonesia'){
				$region = '2';
			} else {
				$region = '3';
			}
		}
		if(!empty($_REQUEST['length'])){
			$limit = (int)$_REQUEST['length'];
		} else {
			$limit = 20;
		}
		if(!empty($_REQUEST['start'])){
			$start = (int)$_REQUEST['start'];
		} else {
			$start = 0;
		}
		if(!empty($_REQUEST['order']['0']['column'])){
			$sort_by=$_REQUEST['order']['0']['column'];
		} else {
			$sort_by = '0';
		}
		if(!empty($_REQUEST['order']['0']['dir'])){
			$order_by=$_REQUEST['order']['0']['dir'];
		} else {
			$order_by = 'desc';
		}
		if(!empty($_REQUEST['draw'])){
			$draw = $_REQUEST['draw'];
		} else {
			$draw = '10';
		}
		if(!empty($_REQUEST['search']['value'])){
			$search = strtolower($_REQUEST['search']['value']);
		}
		$arr_column = array('pid','address','price','type_property.id','building.id','created_at','user.firstname');
		$urlListing = env('HOST_ELASTIC').env('ELASTIC_LISTING')."_search";
		$paramJSON = '{
			"size": '.$limit.',
			"from": '.$start.',
			"sort": [
				{
					"'.$arr_column[$sort_by].'" : {"order" : "'.$order_by.'"}
				}
			],
			"query": {
				"constant_score": {
					"filter": {
						"bool": {
							"must":[';

		if(!empty($region)){
			$paramJSON .= '{
								"match_phrase": {
									"region": "'.$region.'"
								}
							}';
		}
		if(!empty($search)){
			$paramJSON .= ',{
								"query_string": {
									"query": "*'.$search.'*",
									"fields": ["pid","prefered_address","address","type_property.name","building.type","user.*name"]
								}
							}';
		}
		if(!empty($status)){
			if($status != 'all'){
				if($status == "deleted"){
					$paramJSON .= ',{
								"exists": {
									"field": "deleted_at"
								}
							}]
						}
					}
				}
			}
		}';
				} else {
					$paramJSON .= ',{
								"match_phrase": {
									"status": "'.$status.'"
								}
							}],
							"must_not": {
								"exists": {
									"field": "deleted_at"
								}
							}
						}
					}
				}
			}
		}';
				}
			}
		} else {
			$paramJSON .= '],
							"must_not": {
								"exists": {
									"field": "deleted_at"
								}
							}
						}
					}
				}
			}
		}';
		}
		$result = json_decode($this->curlPostContents($urlListing, $paramJSON), TRUE);
		if(isset($result['hits'])){
			$listings = array();
			$listingresult = $result['hits']['hits'];
			for($i=0;$i<count($listingresult);$i++){
				$current = $listingresult[$i]['_source'];
				$listings[$i]['pid'] = '<a href="/listing/details/'.$current['id'].'" target="_blank">'.strtoupper($current['pid']).'</a>';
				if($current['prefered_address'] != ""){
					$listings[$i]['address'] = $current['prefered_address'].' '.$current['city'].' '.$current['district'].' '.$current['post_code'];
				} else if($current['prefered_address'] == ""){
					$listings[$i]['address'] = $current['address'].' '.$current['city'].' '.$current['district'].' '.$current['post_code'];
				}
				$listings[$i]['price'] = number_format($current['price'], 0, ',','.').' '.strtoupper($this->GetCurrency($this->base_currency, $current['currency']));
				if($current['type_property']['id'] == "1"){
					$listings[$i]['status'] = "Sell";
				} else if($current['type_property']['id'] == "2"){
					$listings[$i]['status'] = "Rent(Year)";
				} else {
					$listings[$i]['status'] = "Rent(Month)";
				}
				$listings[$i]['type'] = $this->GetBuildingName($this->base_listing_building, $current['building']['id'], "2");
				$listings[$i]['created_at'] = Carbon::parse($current['created_at'])->format('d M Y');
				$listings[$i]['created_by'] = $current['user']['firstname'].' '.$current['user']['lastname'];
				switch($status){
					case "active": $listings[$i]['action'] = "<div class='btn-group pull-right'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='archive_listing' onclick='ChangeStatus(`".$current['id']."`,`archived`, this);'><i class='fa fa-archive' aria-hidden='true'></i> Move to archive</a></li><li><a href='#' class='delete' onclick='ChangeStatus(`".$current['id']."`,`deleted`, this);'><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</a></li></ul></div></td>"; break;
					case "archived": $listings[$i]['action'] = "<div class='btn-group pull-right'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='activate_listing' onclick='ChangeStatus(`".$current['id']."`,`active`, this);'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Move to active</a></li><li><a href='#' class='delete' onclick='ChangeStatus(`".$current['id']."`,`deleted`, this);'><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</a></li></ul></div></td>"; break;
					case "sold": $listings[$i]['action'] = "<div class='btn-group pull-right'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='archive_listing' onclick='ChangeStatus(`".$current['id']."`,`archived`, this);'><i class='fa fa-archive' aria-hidden='true'></i> Move to archive</a></li><li><a href='#' class='delete' onclick='ChangeStatus(`".$current['id']."`,`deleted`, this);'><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</a></li></ul></div></td>"; break;
					case "deleted": $listings[$i]['action'] = "<div class='btn-group pull-right'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='activate_listing' onclick='ChangeStatus(`".$current['id']."`,`active`, this);'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Move to active</a></li><li><a href='#' class='archive_listing' onclick='ChangeStatus(`".$current['id']."`,`archived`, this);'><i class='fa fa-archive' aria-hidden='true'></i> Move to archive</a></li></ul></div></td>"; break;
				}
				// $listings[$i]['uid'] = '<a href="/user/details/'.$current['user']['id'].'" target="_blank">'.ucfirst($current['user']['uid']).'</a>'
			}
			$response = array('draw' => intval($draw),
					'recordsTotal' => intval($result['hits']['total']),
					'recordsFiltered' => intval($result['hits']['total']),
					'data' => $listings);
		} else {
			$response = array('draw' => intval($draw),
					'recordsTotal' => 0,
					'recordsFiltered' => 0,
					'data' => []);
		}
		return $response;
	}

	public function GetDetailListing($id){
		$urlElastic = env('HOST_ELASTIC').env('ELASTIC_LISTING').$id;
		$data = json_decode($this->getDataFromAPIGet($urlElastic));
		$report_data = array();
		if($data->found == true){
			$result = (array)$data->_source;
			$return_data["URL"] = env('PORTAL_URL').'properti/'.$result['url_slug'];
			$return_data["Data"] = $result;
			$return_data["CountReport"] = 0;
			$status = array('PENDING','OPEN','COMPLETED','PROBLEM');
			for($i=0;$i<count($status);$i++){
				$report_query = ReportListing::where('listing_id', $id)
				->where('status', $status[$i])
				->select('subject','report');
				$count = $report_query->count();
				if($count > 0){
					$current["Status"] = $status[$i];
					$current["Data"] = $report_query->get();
					array_push($report_data, $current);
					$return_data["CountReport"] += $count;
				}
			}
			$return_data["Report"] = $report_data;
			// var_dump($return_data["Data"]);die();
			// return $return_data;
			return view('listing/listing_details')
			->with('data', $return_data)
			->with('status', $status);
		} else {
			return redirect()->back()->with("status_fail", "Listing not found. Coba tanya ivan aja ya :)");
		}
	}

	public function ViewReportedListing(){
		$status = array('pending','open','completed','problem');
	    	// var_dump($status);die();
		return view('admin/reported_listing')
		->with('status', $status);
	}

	public function GetReportedListing(Request $req){
		if($req->input('status') !== null){
			$status = strtoupper($req->input('status'));
		}
		if($req->input('region') !== null){
			if($req->input('region')=='indonesia'){
				$region = '2';
			} else {
				$region = '3';
			}
		}
		if(!empty($_REQUEST['length'])){
			$limit = (int)$_REQUEST['length'];
		} else {
			$limit = 20;
		}
		if(!empty($_REQUEST['start'])){
			$start = (int)$_REQUEST['start'];
		} else {
			$start = 0;
		}
		if(!empty($_REQUEST['order']['0']['column'])){
			$sort_by=$_REQUEST['order']['0']['column'];
		} else {
			$sort_by = '0';
		}
		if(!empty($_REQUEST['order']['0']['dir'])){
			$order_by=$_REQUEST['order']['0']['dir'];
		} else {
			$order_by = 'desc';
		}
		if(!empty($_REQUEST['draw'])){
			$draw = $_REQUEST['draw'];
		} else {
			$draw = '10';
		}
		if(!empty($_REQUEST['search']['value'])){
			$search = strtolower($_REQUEST['search']['value']);
			$arr_search = array('ms_listing.pid','ms_report_listing.subject','ms_report_listing.report','ms_report_listing.email');
		}
		$arr_column = array('ms_listing.pid','ms_report_listing.subject','ms_report_listing.report','ms_report_listing.date','ms_report_listing.email');
		$urlListing = env('HOST_ELASTIC').env('ELASTIC_LISTING')."_search";
		$eloquent = DB::table('ms_report_listing')
		->join('ms_listing', 'ms_report_listing.listing_id', '=', 'ms_listing.id');
		if(!empty($region)){
			$eloquent = $eloquent->where('ms_listing.region', $region);
		}
		if(!empty($status)){
			$eloquent = $eloquent->where('ms_report_listing.status', $status);
		}
		if(!empty($search)){
			$eloquent = $eloquent->where(function($query) use ($search, $arr_search){
				for($i=0;$i<count($arr_search);$i++){
					if($i == 0){
						$query = $query->where($arr_search[$i], 'LIKE', '%'.$search.'%');
					} else {
						$query = $query->orWhere($arr_search[$i], 'LIKE', '%'.$search.'%');
					}
				}
			});
		}
		$count_total = $eloquent->count();
		$eloquent = $eloquent->orderBy($arr_column[$sort_by], $order_by);
		$data_reported = $eloquent->offset($start)->limit($limit)
		->select('ms_report_listing.id as id', 'ms_report_listing.listing_id as listing_id', 'ms_listing.pid as pid','ms_report_listing.subject as problem', 'ms_report_listing.report as description', 'ms_report_listing.date as date', 'ms_report_listing.user_id as user_id', 'ms_report_listing.email as email')
		->get();
		if(count($data_reported) != 0){
			$data_array = array();
			for($i=0;$i<count($data_reported);$i++){
				$current = (array)$data_reported[$i];
				$data_array[$i]['pid'] = '<a href="/listing/details/'.$current['listing_id'].'" target="_blank">'.strtoupper($current['pid']).'</a>';
				$data_array[$i]['problem'] = $current['problem'];
				$data_array[$i]['description'] = $current['description'];
				$data_array[$i]['date'] = $current['date'];
				$data_array[$i]['email'] = $current['email'];
				switch ($status){
					case "PENDING": $data_array[$i]['action'] = "<div class='btn-group pull-right'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='open_report' onclick='ChangeStatus(`".$current['id']."`,`open`,this);'><i class='fa fa-folder-open-o' aria-hidden='true'></i> Mark as open</a></li><li><a href='#' class='complete_report' onclick='ChangeStatus(`".$current['id']."`,`completed`,this);'><i class='fa fa-check' aria-hidden='true'></i> Mark as completed</a></li><li><a href='#' class='problem_report' onclick='ChangeStatus(`".$current['id']."`,`problem`,this);'><i class='fa fa-exclamation-circle' aria-hidden='true'></i> Mark as problem</a></li></ul></div></td>";
						break;
					case "OPEN": $data_array[$i]['action'] = "<div class='btn-group pull-right'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='pending_report' onclick='ChangeStatus(`".$current['id']."`,`pending`,this);'><i class='fa fa-question' aria-hidden='true'></i> Mark as pending</a></li><li><a href='#' class='complete_report' onclick='ChangeStatus(`".$current['id']."`,`completed`,this);'><i class='fa fa-check' aria-hidden='true'></i> Mark as completed</a></li><li><a href='#' class='problem_report' onclick='ChangeStatus(`".$current['id']."`,`problem`,this);'><i class='fa fa-exclamation-circle' aria-hidden='true'></i> Mark as problem</a></li></ul></div></td>";
						break;
					case "COMPLETED": $data_array[$i]['action'] = "<div class='btn-group pull-right'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='pending_report' onclick='ChangeStatus(`".$current['id']."`,`pending`,this);'><i class='fa fa-question' aria-hidden='true'></i> Mark as pending</a></li><li><a href='#' class='open_report' onclick='ChangeStatus(`".$current['id']."`,`open`,this);'><i class='fa fa-folder-open-o' aria-hidden='true'></i> Mark as open</a></li><li><a href='#' class='problem_report' onclick='ChangeStatus(`".$current['id']."`,`problem`,this);'><i class='fa fa-exclamation-circle' aria-hidden='true'></i> Mark as problem</a></li></ul></div></td>";
						break;
					case "PROBLEM": $data_array[$i]['action'] = "<div class='btn-group pull-right'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='pending_report' onclick='ChangeStatus(`".$current['id']."`,`pending`,this);'><i class='fa fa-question' aria-hidden='true'></i> Mark as pending</a></li><li><a href='#' class='open_report' onclick='ChangeStatus(`".$current['id']."`,`open`,this);'><i class='fa fa-folder-open-o' aria-hidden='true'></i> Mark as open</a></li><li><a href='#' class='complete_report' onclick='ChangeStatus(`".$current['id']."`,`completed`,this);'><i class='fa fa-check' aria-hidden='true'></i> Mark as completed</a></li></ul></div></td>";
						break;
				}
			}
			$response = array('draw' => intval($draw),
					'recordsTotal' => intval($count_total),
					'recordsFiltered' => intval($count_total),
					'data' => $data_array);
		} else {
			$response = array('draw' => intval($draw),
					'recordsTotal' => 0,
					'recordsFiltered' => 0,
					'data' => []);
		}
		return $response;
	}

	public function UpdateReport($id, $status){
		DB::beginTransaction();
		try{
			DB::table('ms_report_listing')->where('id', $id)
			->update(['status' => strtoupper($status)]);
		}
		catch(ErrorException $e){
			$return_data["Status"] = "failed";
			$return_data["Message"] = "Something went wrong.";
			$return_data["Error"] = $e;
			DB::rollback();
			return $return_data;
		}
		$return_data["Status"] = "success";
		$return_data["Message"] = "Success update report ID ".$id;
		DB::commit();
		return $return_data;
	}

	public function UpdateStatusListing($id,$status){
		$urlElastic = env('HOST_ELASTIC').env('ELASTIC_LISTING').$id;
		$array_target = array("status", "updated_at", "deleted_at");
		$time_now = Carbon::now()->format("Y-m-d H:i:s");
		// var_dump($time_now);die();
		switch ($status){
			case "active": $array_value = array("active", $time_now, null); break;
			case "archived": $array_value = array("archived", $time_now, null); break;
			case "sold": $array_value = array("sold", $time_now, null); break;
			case "deleted": $array_value = array("deleted", $time_now, $time_now); break;
		}
		DB::beginTransaction();
		try {
			$db_execute = DB::table('ms_listing')->where('id',$id);
			switch ($status){
				case "active": $db_execute->update(["status" => "active", "updated_at" => $time_now,"deleted_at" => null]); break;
				case "archived": $db_execute->update(["status" => "archived", "updated_at" => $time_now, "deleted_at" => null]); break;
				case "sold": $db_execute->update(["status" => "sold", "updated_at" => $time_now, "deleted_at" => null]); break;
				case "deleted": $db_execute->update(["status" => "deleted", "updated_at" => $time_now, "deleted_at" =>  $time_now ]); break;
			}
			$response = $this->UpdateElastic($urlElastic, $array_target, $array_value);
			if($response["Status"] == "failed"){
				DB::rollback();
				return $response;
			} else {
				DB::commit();
				return $response;
			}
		}
		catch(ErrorException $e){
			DB::rollback();
			$response["Status"] = "failed";
			$response["Message"] = "Something went wrong.";
			$response["Errors"] = $e;
			return $response;
		}

		// $data = json_decode($this->getDataFromAPIGet($urlElastic));
		// if($data->found == true){
		// 	$result = (array)$data->_source;
		// 	switch ($status){
		// 		case "active": $result['status'] = "active"; break;
		// 		case "archived": $result['status'] = "archived"; break;
		// 		case "sold": $result["status"] = "sold"; break;
		// 		case "deleted": $result["status"] = "deleted";
		// 		$result["deleted_at"] = Carbon::now(); break;
		// 	}
		// 	DB::beginTransaction();
		// 	try {
		// 		$db_execute = DB::table('ms_listing')->where('id',$id);
		// 		switch ($status){
		// 			case "active": $db_execute->update(["status" => "active"]); break;
		// 			case "archived": $db_execute->update(["status" => "archived"]); break;
		// 			case "sold": $db_execute->update(["status" => "sold"]); break;
		// 			case "deleted": $db_execute->update(["status" => "deleted", "deleted_at" =>  Carbon::now() ]); break;
		// 		}
		// 		$paramJSON = json_encode($result);
		// 		$response = json_decode($this->curlPutContents($urlElastic, $paramJSON));
		// 		if($response->result != "updated"){
		// 			DB::rollback();
		// 			return false;
		// 		} else {
		// 			return true;
		// 		}
		// 	}
		// 	catch(ErrorException $e){
		// 		DB::rollback();
		// 		return false;
		// 	}

		// } else {
		// 	return false;
		// }
	}
}
