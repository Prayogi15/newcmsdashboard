<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App;
use Session;
use App\Sync;
use Carbon\Carbon;

class AdminController extends Controller
{
	protected $base_region;
	public function __construct(){
		
	}
	
	private function GetRegionIDFromName($ms_region,$param_region_name){
		$return = "";
		foreach($ms_region as $key => $value){
			if($value['full_name'] == ucfirst($param_region_name)){
				$return = $value['id'];
				break;
			}
		}
		if($return == ""){
			$return = '2';
		}
		return $return;
	}
	
	public function index(){
		
		$admin_id	= Session::get('admin_id');
		
		$data_admin = DB::table('ms_admin')
                            ->select('id','username','fullname','status','created_at','level')
							->where('status','active')
                            ->get();
		
		$profile_admin = DB::table('ms_admin')
                            ->select('id','level')
							->where('status','active')
							->where('id',$admin_id)
                            ->first();
		
		$lang = array('en','id');					
		return view('admin/admin_list')
		->with('data_admin', $data_admin)
		->with('admin_id', $admin_id)
		->with('lang', $lang)
		->with('profile_admin',$profile_admin);
	}

	public function myprofile(){
		$admin_id	= Session::get('admin_id');
		$data_admin = DB::table('ms_admin')
                           ->select('id','username','fullname','status')
                           ->where('id', $admin_id)
						   ->first();
		$lang = array('en','id');					
		return view('admin/admin_change_profile')
		->with('data_admin', $data_admin)
		->with('lang', $lang);
	}
	
	public function access(){
		$admin_id = Session::get('admin_id');
		$data_admin = DB::table('ms_access_cms')
                            ->select('id','name')		
                            ->get();
							
							
		$lang = array('en','id');					
		return view('admin/admin_access')
		->with('data_admin', $data_admin)
		->with('lang', $lang);
	}
	
	public function insert(Request $req){
	$username		 = $req->input('username');
	$fullname		 = $req->input('fullname');
	$level			 = $req->input('level');
	$password		 = md5($req->input('password'));
	$array_admin	 			= array();
	$array_admin['username']	= $username;
	$array_admin['fullname']	= $fullname;
	$array_admin['password']	= $password;
	$array_admin['level']		= $level;
	$array_admin['status']		= "active";
	$array_admin['created_at']	= date('Y-m-d H:i:s');
	$array_admin['created_by']	= $admin_id = Session::get('admin_id');
	
	$cek_admin = DB::table('ms_admin')
						->select('id')
						->where('username', $username)
						->where('status', 'active')
						->first();
	
	$cek_users = DB::table('ms_user')
						->select('id')
						->where('username', $username)
						->first();
	
	
	
	if(isset($cek_admin))
	{
		$status="error";
		$warning="Username already used by another account";	
	}	
	
	else
	{
		if(isset($cek_users))
		{
			$status="success";
			$warning="Data saved";	
			DB::table('ms_admin')->insert($array_admin);
		}
		else
		{
			$status="error";
			$warning="Not WORKnPLAY Users ! Please input email in WORKnPLAY account";
		}		
	}
	$data = array('status' => $status,'message'=>$warning);
	return json_encode($data);
	}

	public function update(Request $req){
	$id		 					= $req->input('id2');
	$username		 			= $req->input('username');
	$fullname		 			= $req->input('fullname');
	$level		 				= $req->input('level');
	$password		 			= md5($req->input('password'));
	$cek_users = DB::table('ms_user')
						->select('id')
						->where('username', $username)
						->first();
	//kalo passwordnya diisi
	if(strlen($req->input('password'))>0)
	{
		if(isset($cek_users))
		{
			$status_msg="success";
			$warning="Data saved";	
			DB::table('ms_admin')
			->where('id', $id)
            ->update(['username' => $username,
					  'password' => $password,
					  'fullname' => $fullname,
					  'level'	 => $level,
					  'modified_at' => date('Y-m-d H:i:s'),
					  'modified_by' => Session::get('admin_id')
						]);
		}
		else
		{
			$status_msg="error";
			$warning="Not WORKnPLAY Users ! Please input email in WORKnPLAY account";
		}	
	}
	//klo passwordnya tidak diisi
		else
		{
			if(isset($cek_users))
			{
			$status_msg="success";
			$warning="Data saved";	
		
			DB::table('ms_admin')
			->where('id', $id)
            ->update(['username' => $username,
					  'fullname' => $fullname,
					  'modified_at' => date('Y-m-d H:i:s'),
					  'modified_by' => Session::get('admin_id')
						]);
			}
			else
			{
				$status_msg="error";
				$warning="Not WORKnPLAY Users ! Please input email in WORKnPLAY account";
			}			
		}
	$data = array('status' => $status_msg,'message'=>$warning);
	return json_encode($data);
	}
	
	public function update_profile(Request $req){
	$username		 			= $req->input('username');
	$fullname		 			= $req->input('fullname');
	$password		 			= md5($req->input('password'));
	$input_oldpass		 		= md5($req->input('oldpass'));
	$array_admin	 			= array();
	$array_admin['username']	= $username;
	$array_admin['fullname']	= $fullname;
	$array_admin['password']	= $password;
	$array_admin['status']		= "active";
	$array_admin['created_at']	= date('Y-m-d H:i:s');
	$array_admin['created_by']	= Session::get('admin_id');
	$id							= Session::get('admin_id');
	$cek_users = DB::table('ms_user')
						->select('id')
						->where('username', $username)
						->first();
	$cek_password = DB::table('ms_admin')
						->select('password')
						->where('id', $id)
						->first();
	
	$password_old	= $cek_password->password;
	
	
	if(strlen($req->input('password'))>0)
	{
	if($password_old==$input_oldpass)
	{
		if(isset($cek_users))
		{
			$status_msg="success";
			$warning="Data saved";	
		
			DB::table('ms_admin')
			->where('id', Session::get('admin_id'))
            ->update(['username' => $username,
					  'password' => $password,
					  'fullname' => $fullname,
					  'modified_at' => date('Y-m-d H:i:s'),
					  'modified_by' => $id
						]);			
					
		return redirect('/admin/myprofile')->with("status_success", "success");
		}
		else
		{
			return redirect('/admin/myprofile')->with("status_failed", "Username is not WORKnPLAY User");
		}	
		
		}
	else
	{
		return redirect('/admin/myprofile')->with("status_failed", "Old password is wrong");
	}	

	}
	
	else
	{
	if(isset($cek_users))
		{
			$status_msg="success";
			$warning="Data saved";	
			DB::table('ms_admin')
			->where('id', Session::get('admin_id'))
            ->update(['username' => $username,
					  'fullname' => $fullname,
					  'modified_at' => date('Y-m-d H:i:s'),
					  'modified_by' => $id
						]);			
					
		return redirect('/admin/myprofile')->with("status_success", "Data Updated");
		}
		else
		{
			return redirect('/admin/myprofile')->with("status_failed", "Username is not WORKnPLAY User");
		}		
		
		}
	}
	
	public function remove($id){
		$admin_id							= Session::get('admin_id');
		DB::beginTransaction();
		try{
			DB::table('ms_admin')
			->where('id', $id)
            ->update(['status' => "inactive",
					  'modified_at' => date('Y-m-d H:i:s'),
					  'modified_by' => $admin_id
						]);				
		}
		catch(ErrorException $e){
			$return_data["Status"] = "failed";
			$return_data["Message"] = "Something went wrong.";
			$return_data["Error"] = $e;
			DB::rollback();
			return redirect('/admin/list')->with("status_failed", "failed");
		}
		$return_data["Status"] = "success";
		$return_data["Message"] = "Success update report ID ".$id;
		DB::commit();
		//return $return_data;
		return redirect('/admin/list')->with("status_success", "success");
		
	}
	
}
