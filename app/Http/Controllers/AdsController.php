<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App;
use App\Sync;
use App\Project;
use App\ListingBuilding;
use App\Type;

class AdsController extends Controller
{
	protected $base_region;
	public function __construct(){
		$this->client = new Client([
			'base_uri' => env('API_URL'),
			// 'timeout' => env('TIMEOUT_REQUEST')
		]);
		$this->headers = [
			// 'Authorization' => env('TOKEN_NON_LOGIN'),
			'lang' => 'en',
			'region' => 'idn',
			'version' => env('APP_VERSION'),
			'appversion' => env('APP_VERSION'),
			'noauth' => 'y',
		];
		parent::__construct();
		$this->Sync = new Sync;
		if($this->Sync->isExist()){
			$this->SyncData = $this->Sync->getData(true);
		} else {
			$this->SyncData = $this->Sync->setData($this->Sync->sync());
		}
		foreach($this->SyncData['ms_region'] as $keys => $value){
			$this->SyncData['ms_region'][$keys] = (array)$value;
		}
		$this->base_region = collect($this->SyncData['ms_region']);
	}
	private function GetRegionIDFromName($ms_region,$param_region_name){
		$return = "";
		foreach($ms_region as $key => $value){
			if($value['full_name'] == ucfirst($param_region_name)){
				$return = $value['id'];
				break;
			}
		}
		if($return == ""){
			$return = '2';
		}
		return $return;
	}
	public function ViewAdvertisement(){
		$data = [
			'position' => array('home','list_view','property_details'),
			'projects' => Project::where('status', 'active')->get(),
			'listing_buildings' => ListingBuilding::all(),
			'types' => Type::all()
		];

		return view('setting/advertisement', $data);
	}
	public function GetAds(Request $req){
		switch($req->input('position')){
			case "home": $position = "home"; break;
			case "list_view": $position = "listview"; break;
			case "property_details": $position = "detail"; break;
			default: $position = "home"; break;
		}
		if($req->input('region') !== null){
			if($req->input('region')=='indonesia'){
				$region = '2';
			} else {
				$region = '3';
			}
		} else {
			$region = '1';
		}
		if(!empty($_REQUEST['length'])){
			$limit = (int)$_REQUEST['length'];
		} else {
			$limit = 20;
		}
		if(!empty($_REQUEST['start'])){
			$start = (int)$_REQUEST['start']+1;
		} else {
			$start = 1;
		}
		if(!empty($_REQUEST['order']['0']['column'])){
			$sort_by=$_REQUEST['order']['0']['column'];
		} else {
			$sort_by = '0';
		}
		if(!empty($_REQUEST['order']['0']['dir'])){
			$order_by=$_REQUEST['order']['0']['dir'];
		} else {
			$order_by = 'asc';
		}
		if(!empty($_REQUEST['draw'])){
			$draw = $_REQUEST['draw'];
		} else {
			$draw = '10';
		}
		if(!empty($_REQUEST['search']['value'])){
			$search = strtolower($_REQUEST['search']['value']);
		} else {
			$search = "";
		}
		$arr_order = array('order_no','title','image','clicks','date_start','date_end','modified_at','status');
		$params = [
			'position' => $position,
			'region' => $region,
			'search' => $search,
			'sort_by' => $arr_order[$sort_by],
			'sort_type' => $order_by,
			'limit' =>$limit,
			'page' => $start,
		];
		try{
			$results = $this->client->post('v500/ads/all', [
				'form_params' => $params
			]);
			$results = json_decode((string)$results->getBody(), true);
		} catch (\Exception $e){
			\Log::error($e);
			dd($e);
			$response['Status'] = 'failed';
			$response['Message'] = 'Something went wrong.';
		}
		if($results["Status"] == "success_ads"){
			$response['Status'] = 'success';
			$response['Message'] = 'Success update ads.';
			$max_limit = $results["Data"]["total"];
			for($i=0; $i<count($results['Data']['ads']); $i++){
				$current = $results['Data']['ads'][$i];
				$ads[$i]['no'] = $current['order_no'];
				$ads[$i]['name'] = $current['name'];
				$ads[$i]['type'] = $current['type'];
				$ads[$i]['link'] = "<a href='".$current['link']."' target='_blank'>".$current['link']."</a>";
				$ads[$i]['title'] = $current['title'];
				$ads[$i]['description'] = $current['description'];
				$ads[$i]['image'] = "<a href='#' onclick='ShowImg(`".$current['image']."`,`".$current['name']."`)' data-toggle='modal' data-target='#modal-img'><img src='".$current['image']."' style='width:30px;'></a>";
				$ads[$i]['clicks'] = 0;
				$ads[$i]['date_start'] = Carbon::parse($current['date_start'])->format('d M Y');
				$ads[$i]['date_end'] = Carbon::parse($current['date_end'])->format('d M Y');
				$ads[$i]['modified_date'] = Carbon::parse($current['modified_at'])->format('d M Y');
				if($current['status'] == '1'){
					$ads[$i]['status'] = '<span style="color: #33ba1e;">Active</span>';
				} else if($current['status'] == "2") {
					$ads[$i]['status'] = '<span style="color: #e63535;">Problem</span>';
				} else if($current['status'] == "0"){
					$ads[$i]['status'] = '<span style="color: #989898;">Inactive</span>';
				}
				if($i == 0 || $current['order_no'] == 1){
					$ads[$i]['move'] = "<button type='button' class='btn btn-default disabled'><i class='fa fa-arrow-up'></i></button><button type='button' class='btn btn-default' onclick='Move(`".$current['id']."`,`down`,this);'><i class='fa fa-arrow-down'></i></button>";
				} else if($i == count($results['Data']['ads'])-1){
					$ads[$i]['move'] = "<button type='button' class='btn btn-default' onclick='Move(`".$current['id']."`,`up`,this);'><i class='fa fa-arrow-up'></i></button><button type='button' class='btn btn-default disabled'><i class='fa fa-arrow-down'></i></button>";
				} else if($max_limit == 1){
					$ads[$i]['move'] = "<button type='button' class='btn btn-default disabled'><i class='fa fa-arrow-up'></i></button><button type='button' class='btn btn-default disabled'><i class='fa fa-arrow-down'></i></button>";
				} else {
					$ads[$i]['move'] = "<button type='button' class='btn btn-default'><i class='fa fa-arrow-up' onclick='Move(`".$current['id']."`,`up`,this);'></i></button><button type='button' class='btn btn-default' onclick='Move(`".$current['id']."`,`down`,this);'><i class='fa fa-arrow-down'></i></button>";
				}
				$ads[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-left' role='menu'><li><a href='#' class='edit_ads' onclick='EditAds(`".$current['id']."`,this)'><i class='fa fa-edit' aria-hidden='true'></i> Edit</a></li><li><a href='#' class='delete_ads' onclick='DeleteAds(`".$current['id']."`,this)'><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</a></li></ul></div></td>";
			}
			$response = array('draw'=>intval($draw),
					'recordsTotal' => intval($max_limit),
					'recordsFiltered' => intval($max_limit),
					'data' => $ads
			);
		} else {
			$response = array('draw'=>intval($draw),
					'recordsTotal' => 0,
					'recordsFiltered' => 0,
					'data' => [],
					'paramJSON' => $params,
			);
		}
		return $response;
	}
	public function CreateAds(Request $req){
		$params = [
			'name' => $req->input('name'),
			'type' => $req->input('type'),
			'platform' => $req->input('platform'),
			'position' => $req->input('position'),
			'image' => $req->input('url-img'),
			'project_id' => $req->input('project') ?? 0,
			'title' => $req->input('title'),
			'description' => $req->input('description'),
			'link' => $req->input('link'),
			'status' => '1',
			'date_start' => Carbon::parse($req->input('date_start'))->format('Y-m-d H:i:s'),
			'date_end' => Carbon::parse($req->input('date_end'))->format('Y-m-d H:i:s'),
			'region' => $this->GetRegionIDFromName($this->base_region, $req->input('region')),
			'user_id' => intval($req->input('user_id'))
		];

		if ($req->input('type') == 'explore') {
			$filter = [
				'building_id' => $req->input('property_type'),
				'type_id' => $req->input('property_status'),
				'lat' => $req->input('lat'),
				'lon' => $req->input('lng'),
				'price_min' => $req->input('min_price'),
				'price_max' => $req->input('max_price')
			];

			$params['filter'] = json_encode($filter);
		}

		try{
			$results = $this->client->post('v500/ads', [
				'form_params' => $params
			]);
			return json_decode((string)$results->getBody(), true);
		} catch (\Exception $e) {
			return response()->json($e, 400); // Status code here
		}
	}
	public function UpdateAds(Request $req){
		$id = $req->input('id');
		$params = [
			'name' => $req->input('name'),
			'type' => $req->input('type'),
			'platform' => $req->input('platform'),
			'position' => $req->input('position'),
			'image' => $req->input('url-img'),
			'title' => $req->input('title'),
			'description' => $req->input('description'),
			"link" => $req->input('link'),
			'status' => '1',
			'date_start' => Carbon::parse($req->input('date_start'))->format('Y-m-d H:i:s'),
			'date_end' => Carbon::parse($req->input('date_end'))->format('Y-m-d H:i:s'),
			'region' => $this->GetRegionIDFromName($this->base_region, $req->input('region')),
			'user_id' => intval($req->input('user_id')),
		];

		if ($req->input('type') == 'explore') {
			$filter = [
				'building_id' => $req->input('property_type'),
				'type_id' => $req->input('property_status'),
				'lat' => $req->input('lat'),
				'lon' => $req->input('lng'),
				'price_min' => $req->input('min_price'),
				'price_max' => $req->input('max_price')
			];

			$params['filter'] = json_encode($filter);
		}

		try{
			$results = $this->client->put('v500/ads/'.$id, [
				'form_params' => $params
			]);
			return json_decode((string)$results->getBody(), true);
		} catch (\Exception $e){
			return response()->json(['message' => 'Something went wrong'], 400); // Status code here
		}
	}
	public function GetSingleAds($id){
		try{
			$results = $this->client->get('v500/ads/'.$id);
			$results = json_decode((string)$results->getBody(), true);
		} catch (\Exception $e){
			\Log::error($e);
            	dd($e);
            	$response['Status'] = 'failed';
            	$response['Message'] = 'Something went wrong.';
		}
		if($results["Status"] == "success_ads"){
			$response['Status'] = 'success';
			$response['Data'] = $results['Data']['ads'];
		}
		return $response;
	}
	public function DeleteAds(Request $req){
		$id = $req->input('id');
		$user_id = $req->input('user_id');
		// return 'v500/ads/'.$id.'/'.$user_id;
		try{
			$results = $this->client->delete('v500/ads/'.$id.'/'.$user_id);
			$results = json_decode((string)$results->getBody(), true);
		} catch (\Exception $e){
			\Log::error($e);
            	dd($e);
            	$response['Status'] = 'failed';
            	$response['Message'] = 'Something went wrong.';
		}
		if($results["Status"] == "success_ads"){
			$response['Status'] = 'success';
			$response['Message'] = 'Success delete ads.';
		}else{
			$response = $results;
		}
		return $response;
	}
	public function MoveAds(Request $req){
		$id = $req->input('id');
		if($req->input('flag') !== null){
			$flag = $req->input('flag');
		} else {
			$response['Status'] = 'failed';
			$response['Message'] = 'Flag up or down not found.';
			return $response;
		}
		try{
			$results = $this->client->get('v500/ads/move/'.$id.'/'.$flag);
			$results = json_decode((string)$results->getBody(), true);
		} catch (\Exception $e){
			\Log::error($e);
            	dd($e);
            	$response['Status'] = 'failed';
            	$response['Message'] = 'Something went wrong.';
		}
		if($results["Status"] == "success_ads"){
			$response['Status'] = 'success';
			$response['Message'] = 'Success move ads.';
		}
		return $response;
	}
}
