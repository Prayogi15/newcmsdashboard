<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Gift;
use App\VoucherTransaction;
use Carbon\Carbon;
use App;

class VoucherController extends Controller
{
	public function ViewVoucher(){
        $status = array('pending','open','completed','problem');
		return view('admin/voucher')
        ->with('voucher_status', $status);
	}
    public function GetRedeemedVoucher(Request $req){
    	if($req->input('status') !== null){
            $status = $req->input('status');
        }
    	if($req->input('region') !== null){
            if($req->input('region')=='indonesia'){
                $region = 'idn';
            } else if($req->input('region')=='singapore'){
                $region = 'sgp';
            } else {
                $region = 'idn';
            }
        }
        if(!empty($_REQUEST['length'])){
            $limit = (int)$_REQUEST['length'];
        } else {
            $limit = 20;
        }
        if(!empty($_REQUEST['start'])){
            $start = (int)$_REQUEST['start'];
        } else {
            $start = 0;
        }
        if (!empty($_REQUEST['draw'])) {
            $draw = $_REQUEST['draw'];
        }else{
            $draw = 10;
        }
        if(!empty($_REQUEST['order']['0']['column'])){
            $sort_by=$_REQUEST['order']['0']['column'];
        } else {
            $sort_by = '0';
        }
        if(!empty($_REQUEST['order']['0']['dir'])){
            $order_by=$_REQUEST['order']['0']['dir'];
        } else {
            $order_by = 'desc';
        }
        if(!empty($_REQUEST['search']['value'])){
            $search = strtolower($_REQUEST['search']['value']);
            $arr_search = array('ms_user.last_name','wnp_point.tr_transaction.id','ms_user.uid','ms_user.first_name','ms_user.username','ms_user.phone_number','ms_gift.code','wnp_point.ms_gift.cost');
        }
        $arr_column = array('ms_user.last_name','wnp_point.tr_transaction.id','ms_user.uid','ms_user.first_name','ms_user.username','ms_user.phone_number','wnp_point.tr_transaction.created_at','ms_gift.code','wnp_point.ms_gift.cost');
    	// $start = (int)$req->header('start');
    	// $limit = (int)$req->header('limit');
    	$eloquent = DB::connection('mysql2')->table('wnp_point.tr_transaction')
    	->join('wnp.ms_user as ms_user', 'wnp_point.tr_transaction.user_id', '=', 'ms_user.id')
    	->join('wnp_point.ms_gift as ms_gift', 'wnp_point.tr_transaction.join_id', '=', 'ms_gift.id')
        ->join('wnp_point.ms_user as ms_user_admin', 'wnp_point.tr_transaction.user_id', '=', 'ms_user_admin.user_id')
    	->where('wnp_point.tr_transaction.type', '=', 'redeem')
    	->where('wnp_point.tr_transaction.transaction_type', '=', 'kredit')
        ->where('ms_user_admin.is_admin', '!=', '1');
        if(!empty($status)){
            $eloquent = $eloquent->where('wnp_point.tr_transaction.status', '=', $status);
        }
        if(!empty($region)){
            $eloquent = $eloquent->where('wnp_point.ms_gift.region', '=', $region);
        }
    	if(!empty($search)){
            $eloquent = $eloquent->where(function($query) use ($search, $arr_search){
                for($i=0;$i<count($arr_search);$i++){
                    if($i == 0){
                        $query = $query->where($arr_search[$i], 'LIKE', '%'.$search.'%');
                    } else {
                        $query = $query->orWhere($arr_search[$i], 'LIKE', '%'.$search.'%');
                    }
                }
            });
        }
        $total = $eloquent->count();
        $voucher = $eloquent->orderBy($arr_column[$sort_by], $order_by)
        ->offset($start)->limit($limit)
    	->select('wnp_point.tr_transaction.id as ID', 'ms_user.id as user_id', 'ms_user.uid as UID', 'ms_user.first_name as FirstName', 'ms_user.last_name as LastName', 'ms_user.username as Email', 'ms_user.country_code as PhoneCode', 'ms_user.phone_number as PhoneNumber', 'wnp_point.tr_transaction.created_at as Date', 'ms_gift.code as VoucherCode', 'wnp_point.ms_gift.cost as VoucherValue')
    	->get();
        if(count($voucher)>0){
            $items = array();
            for($i=0;$i<count($voucher);$i++){
                switch ($status){
                    case "unprocessed": $items[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-left' role='menu'><li><a href='#' class='open_voucher'><i class='fa fa-folder-open-o' aria-hidden='true'></i> Open</a></li><li><a href='#' class='complete_voucher'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Completed</a></li><li><a href='#' class='problem_voucher'><i class='fa fa-exclamation-circle' aria-hidden='true'></i> Problem</a></li></ul></div></td>";
                        break;
                    case "processed": $items[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-left' role='menu'><li><a href='#' class='pending_voucher'><i class='fa fa-question' aria-hidden='true'></i> Pending</a></li><li><a href='#' class='complete_voucher'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Completed</a></li><li><a href='#' class='problem_voucher'><i class='fa fa-exclamation-circle' aria-hidden='true'></i> Problem</a></li></ul></div></td>";
                        break;
                    case "done": $items[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-left' role='menu'><li><a href='#' class='pending_voucher'><i class='fa fa-question' aria-hidden='true'></i> Pending</a></li><li><a href='#' class='open_voucher'><i class='fa fa-folder-open-o' aria-hidden='true'></i> Open</a></li><li><a href='#' class='problem_voucher'><i class='fa fa-exclamation-circle' aria-hidden='true'></i> Problem</a></li></ul></div></td>";
                        break;
                    case "problem": $items[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-left' role='menu'><li><a href='#' class='pending_voucher'><i class='fa fa-question' aria-hidden='true'></i> Pending</a></li><li><a href='#' class='open_voucher'><i class='fa fa-folder-open-o' aria-hidden='true'></i> Open</a></li><li><a href='#' class='complete_voucher'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Completed</a></li></ul></div></td>";
                        break;
                }
                $items[$i]['id'] = $voucher[$i]->ID;
                $items[$i]['uid'] = strtoupper($voucher[$i]->UID);
                $items[$i]['name'] = $voucher[$i]->FirstName." ".$voucher[$i]->LastName;
                $items[$i]['email'] = "<a href='/user/details/".$voucher[$i]->user_id."' target='_blank'>".$voucher[$i]->Email."</a>";
                if($voucher[$i]->PhoneCode == ""){
                    $items[$i]['phone'] = "+62".$voucher[$i]->PhoneNumber;
                } else {
                    $items[$i]['phone'] = $voucher[$i]->PhoneCode.$voucher[$i]->PhoneNumber;
                }
                $items[$i]['date'] = Carbon::parse($voucher[$i]->Date)->format('d M Y');
                $items[$i]['voucher_code'] = strtoupper($voucher[$i]->VoucherCode);
                $items[$i]['voucher_value'] = $voucher[$i]->VoucherValue." Poin";
                $response = array('draw' => intval($draw),
                        'recordsTotal' => intval($total),
                        'recordsFiltered' => intval($total),
                        'data' => $items);
            }
        } else {
            $response = array('draw' => 0,
                        'recordsTotal' => 0,
                        'recordsFiltered' => 0,
                        'data' => []);
        }
    	
    	return $response;
    }
    public function ChangeStatusTransaction(Request $req){
    	$status = $req->input('status');
    	$trans_id = $req->input('trans_id');
    	if(VoucherTransaction::where('id', $trans_id)->update(['status'=>$status])){
    		return "success";
    	} else {
    		return "failed";
    	}
    }
}