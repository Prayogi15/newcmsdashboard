<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use DB;
use App\Sync;
use App;
use Carbon\Carbon;
use App\Project;
use App\ProjectPhoto;
use App\ProjectUnit;
use App\ProjectUnitPhoto;
use App\ProjectUnitFloorplan;

class PrimeController extends Controller
{
	protected $base_project_building;
	protected $base_project_status;
	protected $Sync;
	public function __construct(){
		$this->client = new Client(['base_uri' => env('API_URL')]);
		$this->headers = [
			// 'Authorization' => env('TOKEN_NON_LOGIN'),
			'lang' => 'en',
			'region' => 'idn',
			'version' => env('APP_VERSION'),
			'appversion' => env('APP_VERSION'),
			'noauth' => 'y',
		];

		parent::__construct();
		$this->Sync = new Sync;
		if($this->Sync->isExist()){
			$this->SyncData = $this->Sync->getData(true);
		} else {
            $this->SyncData = $this->Sync->setData($this->Sync->sync());
		}
		foreach($this->SyncData["ms_project_building"] as $keys => $value){
			$this->SyncData['ms_project_building'][$keys] = (array)$value;
		}
		foreach($this->SyncData["ms_project_status"] as $keys => $value){
			$this->SyncData['ms_project_status'][$keys] = (array)$value;
		}
		$this->base_project_building = collect($this->SyncData["ms_project_building"]);
		$this->base_project_status = collect($this->SyncData["ms_project_status"]);
	}

	private function GetProjectBuildingName($ms_project_building, $param_project_building_id, $lang){
		if($lang == ""){
			$lang = "2";
		}
		foreach($ms_project_building as $key => $value){
			if($value['id'] == $param_project_building_id && $value['lang'] == $lang){
				$return = $value['type'];
				break;
			}
		}
		return $return;
	}
	private function GetProjectStatusName($ms_project_status, $param_project_status_id, $lang){
		if($lang == ""){
			$lang = "2";
		}
		foreach($ms_project_status as $key => $value){
			if($value['id'] == $param_project_status_id && $value['lang'] == $lang){
				$return = $value['name'];
				break;
			}
		}
		return $return;
	}
	private function GetRandomString(){
		$length = 6;
		$str = "";
		$characters = array_merge(range('A','Z'),range('0','9'));
		for($i=0;$i<$length;$i++){
			$rand = mt_rand(0, count($characters)-1);
			$str .= $characters[$rand];
		}
		return $str;
	}
	private function RemoveComma($value){
		return intval(str_replace(",", "", $value));
	}

	public function ViewPrime(){
		$status = array('active','inactive');
		return view('prime/list')
		->with('status',$status);
	}

	public function GetListProject(Request $req){
		$lang = 2;
		if($req->input('region') !== null){
			if($req->input('region')=='indonesia'){
				$region = '2';
			} else {
				$region = '3';
			}
		}
		if($req->input('status') !== null){
			$status = $req->input('status');
		} else {
			$status = "active";
		}
		if(!empty($_REQUEST['length'])){
			$limit = (int)$_REQUEST['length'];
		} else {
			$limit = 20;
		}
		if(!empty($_REQUEST['start'])){
			$start = (int)$_REQUEST['start'];
		} else {
			$start = 0;
		}
		if(!empty($_REQUEST['order']['0']['column'])){
			$sort_by=$_REQUEST['order']['0']['column'];
		} else {
			$sort_by = '0';
		}
		if(!empty($_REQUEST['order']['0']['dir'])){
			$order_by=$_REQUEST['order']['0']['dir'];
		} else {
			$order_by = 'desc';
		}
		if(!empty($_REQUEST['draw'])){
			$draw = $_REQUEST['draw'];
		} else {
			$draw = '10';
		}
		if(!empty($_REQUEST['search']['value'])){
			$search = strtolower($_REQUEST['search']['value']);
		}
		// $arr_column = array('ms_project.created_at', 'ms_project.name', 'ms_project_building.type', 'ms_project.developer_name', 'ms_project_status.name', 'ms_project.created_at', 'ms_project.project_id');
		$arr_column = array('created_at', 'name', 'project_building.id', '\developer_name', 'project_status.id', 'updated_at', 'prid');

		$url_project = env('HOST_ELASTIC').env('ELASTIC_PROJECT')."_search";
		$paramJSON = '{
			"size": '.$limit.',
			"from": '.$start.',
			"sort": [
				{
					"'.$arr_column[$sort_by].'": {"order": "'.$order_by.'"}
				}
			],
			"query": {
				"constant_score": {
					"filter": {
						"bool": {
							"must":[
								{
									"match_phrase": {
										"status": "'.$status.'"
									}
								}
							],
							"must_not": {
								"exists": {
									"field": "deleted_at"
								}
							}
						}
					}
				}
			}
		}';
		$result = json_decode($this->curlPostContents($url_project, $paramJSON), TRUE);
		if(isset($result['hits'])){
			$projects = array();
			$project_result = $result['hits']['hits'];
			for($i=0; $i<count($project_result);$i++){
				$current = $project_result[$i]['_source'];
				// LINK PROJECT TBD
				$projects[$i]['name'] = '<a href="#">'.$current['name'].'</a>';
				$projects[$i]['type'] = $this->GetProjectBuildingName($this->base_project_building, $current['project_building']['id'], $lang);
				$projects[$i]['developer'] = $current['developer_name'];
				$projects[$i]['status'] = $this->GetProjectStatusName($this->base_project_status, $current['project_status']['id'], $lang);
				if($current['updated_at'] == null){
					$projects[$i]['updated_at'] = $current['created_at'];
				} else {
					$projects[$i]['updated_at'] = $current['updated_at'];
				}
				$projects[$i]['prid'] = $current['prid'];
				$projects[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-left' role='menu'><li><a href='".url('/prime_cms/edit_project/')."/".$current['id']."' class='edit_project'><i class='fa fa-edit' aria-hidden='true'></i> Edit Project</a></li><li><a href='#' class='delete_project' onclick='DeleteProject(`".$current['id']."`, this);'><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</a></li>";
				if($status == 'active'){
					$projects[$i]['action'] .= "<li><a href='#' class='inactivate_project' onclick='ChangeStatus(`".$current['id']."`,`inactive`);'><i class='fa fa-ban' aria-hidden='true'></i> Inactivate</a></li></ul></div></td>";
				} else {
					$projects[$i]['action'] .= "<li><a href='#' class='activate_project' onclick='ChangeStatus(`".$current['id']."`,`active`);'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Activate</a></li></ul></div></td>";
				}
			}
			$response = array('draw'=> intval($draw),
				'recordsTotal' => intval($result['hits']['total']),
				'recordsFiltered' => intval($result['hits']['total']),
				'data' => $projects
			);
		} else {
			$response = array('draw'=> intval($draw),
				'recordsTotal' => 0,
				'recordsFiltered' => 0,
				'data' => []
			);
		}
		return $response;
	}

	public function create(){
		$statuses = DB::table('ms_project_status')
			->where('lang', '2')
			->orderBy('id', 'asc')->get();

		$types = DB::table('ms_project_building')
			->select('id', 'type AS name', 'priority', 'lang') // Rename type to name
			->where('lang', '2')
			->orderBy('priority', 'asc')->get();

		$categories = DB::table('ms_category')
			->where('lang', 2)
			->get();


		$buildings_facilities = [];

		$building_facilities_db = DB::table('tr_building_facility')
			->where('lang_id', 2)
			->get();

		foreach ($types as $type) {
			$buildings_facilities[$type->id] = $building_facilities_db
				->where('building_id', $type->id)
				->pluck('facility_id')
				->toArray();
		}

		$facilities = DB::table('ms_facilities')
			->where('lang', 2)
			->get();

		foreach ($categories as $key => $category) {
			$category->facilities = $facilities
				->where('ms_category_id', $category->id)
				->values();
			/**
			 * values() will Re-key array with non-sequential numeric keys to prevent
			 * JSON to recognize it as object
			 */

			// We don't need the categories that doesn't have any facilities.
			if (!count($category->facilities)) {
				unset($categories[$key]);
			}
		}

		return view('prime.create', compact('types', 'statuses', 'categories', 'buildings_facilities'));
	}

	public function edit($id) {
		$project = Project::query()
			->with('photos', 'units', 'units.photos')
			->find($id);

		// Selected project facility ids
		$projectFacilityIds = DB::table('tr_project_facility')
			->where('project_id', $id)
			->get(['facility_id'])
			->pluck('facility_id')
			->all();

		$statuses = DB::table('ms_project_status')
			->where('lang', '2')
			->orderBy('id', 'asc')->get();

		$types = DB::table('ms_project_building')
			->select('id', 'type AS name', 'priority', 'lang') // Rename type to name
			->where('lang', '2')
			->orderBy('priority', 'asc')->get();

		$categories = DB::table('ms_category')
			->where('lang', 2)
			->get();


		$buildings_facilities = [];

		$building_facilities_db = DB::table('tr_building_facility')
			->where('lang_id', 2)
			->get();

		foreach ($types as $type) {
			$buildings_facilities[$type->id] = $building_facilities_db
				->where('building_id', $type->id)
				->pluck('facility_id')
				->toArray();
		}

		$facilities = DB::table('ms_facilities')
			->where('lang', 2)
			->get();

		foreach ($categories as $key => $category) {
			$category->facilities = $facilities
				->where('ms_category_id', $category->id)
				->values();
			/**
			 * values() will Re-key array with non-sequential numeric keys to prevent
			 * JSON to recognize it as object
			 */

			// We don't need the categories that doesn't have any facilities.
			if (!count($category->facilities)) {
				unset($categories[$key]);
			}
		}

		return view('prime.edit', compact('project', 'projectFacilityIds', 'types', 'statuses', 'categories', 'buildings_facilities'));
	}

	public function GetSpecAndFacility($id, $region){
		$spec = DB::table('tr_project_building_spec')
		->where('building_id', $id)
		->select('spec_id')
		->get();
		switch($id){
			case 5: $id = 6;
		}
		$data_facility = DB::table('tr_building_facility')
		->join('ms_facilities', 'tr_building_facility.facility_id', '=', 'ms_facilities.id')
		->join('ms_category', 'ms_facilities.ms_category_id', '=', 'ms_category.id')
		->where('ms_facilities.lang', '2')
		->where('ms_category.lang', '2')
		->where('tr_building_facility.building_id', $id)
		->select('ms_category.name as category_name','ms_facilities.ms_category_id as category_id', 'ms_facilities.id as id', 'ms_facilities.name as name')
		->get();
		if(count($data_facility) != 0){
			$facilities = array();
			$unordered_facilities = array();
			foreach($data_facility as $facility_value){
				$unordered_facilities[$facility_value->category_id][] = $facility_value;
			}
			foreach($unordered_facilities as $facility_value){
				array_push($facilities, $facility_value);
			}
		}
		if(count($spec) == 0){
			$return['Status'] = 'failed';
			$return['Message'] = 'Specification and Facilities with ID = '.$id.' not found';
		} else {
			$return['Status'] = 'success';
			$spec_data = array();
			for($i=0;$i<count($spec);$i++){
				array_push($spec_data, $spec[$i]->spec_id);
			}
			$return['Data']['Spec'] = $spec_data;
			$return['Data']['Facilities'] = $facilities;
		}
		return $return;
	}

	public function DeleteProject(Request $req){
		$id = $req->input('project_id');
		// CHECK PROJECT IS VALID OR NOT
		$project = DB::table('ms_project')->where('id', $id)
		->first();
		if($project){
			//BEGIN DELETE
			DB::table('ms_project')->where('id', $id)
			->update(['deleted_at' => Carbon::now()]);
			$results = $this->client->get('v510/update_es/project/'.$id);
			$results = json_decode($results->getBody(), true);
			if($results['Status'] != "success_listing"){
				$return['Status'] = "failed";
				$return['Message'] = "Update elastic error, please contact our support.";
			} else {
				$return['Status'] = "success";
				$return['Message'] = "Success delete project.";
			}
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Project not found. Please contact our support.";
		}
		return $return;
	}

	public function ChangeStatusProject(Request $req){
		$id = $req->input('project_id');
		$status = $req->input('status');
		// CHECK PROJECT IS VALID OR NOT
		$project = DB::table('ms_project')->where('id', $id)
		->first();
		if($project){
			//BEGIN CHANGE STATUS
			DB::table('ms_project')->where('id', $id)
			->update(['updated_at' => Carbon::now(), 'status' => $status]);
			$results = $this->client->get('v510/update_es/project/'.$id);
			$results = json_decode($results->getBody(), true);
			if($results['Status'] != "success_listing"){
				$return['Status'] = "failed";
				$return['Message'] = "Update elastic error, please contact our support.";
			} else {
				$return['Status'] = "success";
				$return['Message'] = "Success change project status.";
			}
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Project not found. Please contact our support.";
		}
		return $return;
	}

	public function store(Request $request) {
		$project = new Project;

		$this->updateProject($project, $request);
	}

	public function update(Request $request, $id) {
		$project = Project::find($id);

		$this->updateProject($project, $request);
	}

	private function updateProject(Project $project, Request $request) {
		// $this->validateInput($request); // For future improvements

		// 1. Update the project
		if (!$project->prid) {
			$project->prid = $this->generateProjectHash();
		}
		$project->project_building_id = $request->input('propertyType');
		$project->project_status_id = $request->input('projectStatus');
		$project->name = $request->input('projectName');
		$project->currency_id = 2;
		$project->price_min = $this->RemoveComma($request->input('minPrice'));
		$project->price_max = $this->RemoveComma($request->input('maxPrice'));
		$project->lat = $request->input('lat');
		$project->lng = $request->input('lng');
		$project->is_street_view = $request->input('showsStreetView');
		$project->short_address = $request->input('addressForMainPage');
		$project->address = $request->input('projectAddress');
		$project->ebrochure = $request->input('eBrochurePath');
		$project->pricelist = $request->input('priceListPath');
		$project->description = $request->input('projectDescription');
		$project->website = $request->input('projectWebsite');
		$project->project_plan = $request->input('projectPlanImagePath');
		$project->developer_logo = $request->input('developerLogoPath');
		$project->developer_image = $request->input('developerFullImagePath');
		$project->developer_name = $request->input('developerName');
		$project->developer_info = $request->input('developerInfo');
		$project->developer_phone = $request->input('developerPhoneNumber');
		$project->developer_email = $request->input('developerEmail');
		$project->save();

		// 2. Store the project photos
		$project->photos()->delete();

		$primaryPhotos = $request->input('primaryPhotos');

		$this->normalizePhotos($primaryPhotos);

		foreach ($primaryPhotos as $key => $primaryPhoto) {
			$projectPhoto = new ProjectPhoto;
			$projectPhoto->id = $primaryPhoto['image'];
			$projectPhoto->priority = $key + 1;
			$projectPhoto->url = $primaryPhoto['image'];

			$project->photos()->save($projectPhoto);
		}

		// 3. Store the project units

		// 3.1 Delete all previous units
		$project->units()->delete();

		$unitTypes = $request->input('unitTypes');

		foreach ($unitTypes as $unitType) {
			$projectUnit = new ProjectUnit;
			$projectUnit->name = $unitType['name'];
			$projectUnit->currency_id = 2;
			$projectUnit->price = $this->RemoveComma($unitType['price']);
			$projectUnit->area_metric_id = $unitType['landArea']['metric'] === 'm' ? 1 : 2;
			$projectUnit->land_area = $this->RemoveComma($unitType['landArea']['amount']);
			$projectUnit->floor_area = $this->RemoveComma($unitType['floorArea']['amount']);
			$projectUnit->bedrooms = $unitType['bedrooms'];
			$projectUnit->bathrooms = $unitType['bathrooms'];
			$projectUnit->parking = $unitType['parkings'];
			$project->units()->save($projectUnit);

			// 3.2 Store project unit's photos
			$projectUnit->photos()->delete(); // Delete previous project unit's photos

			$unitTypePhotos = $unitType['photos'];

			$this->normalizePhotos($unitTypePhotos);

			foreach ($unitTypePhotos as $key => $unitTypePhoto) {
				$projectUnitPhoto = new ProjectUnitPhoto;
				$projectUnitPhoto->id = $unitTypePhoto['image'];
				$projectUnitPhoto->project_id = $project->id;
				$projectUnitPhoto->priority = $key + 1;
				$projectUnitPhoto->url = $unitTypePhoto['image'];
				$projectUnit->photos()->save($projectUnitPhoto);
			}

			// 3.3 Store project unit's floor plan
			$projectUnit->floorplans()->delete(); // Delete previous project unit's floorplan

			$unitTypeFloorPlans = $unitType['floorPlan'];

			$this->normalizePhotos($unitTypeFloorPlans);

			foreach ($unitTypeFloorPlans as $key => $unitTypeFloorPlan) {
				$projectUnitFloorplan = new ProjectUnitFloorplan;
				$projectUnitFloorplan->id = $unitTypeFloorPlan['image'];
				$projectUnitFloorplan->project_id = $project->id;
				$projectUnitFloorplan->priority = $key + 1;
				$projectUnitFloorplan->url = $unitTypeFloorPlan['image'];
				$projectUnit->floorplans()->save($projectUnitFloorplan);
			}
		}

		// 4. Store project's facilities

		// 4.1 Delete previous facilities
		DB::table('tr_project_facility')
			->where('project_id', $project->id)
			->delete();

		// 4.2 Store new facilities
		$selectedFacilityIds = $request->input('selectedFacilities');

		foreach ($selectedFacilityIds as $selectedFacilityId) {
			DB::table('tr_project_facility')->insert([
				'project_id' => $project->id,
				'facility_id' => $selectedFacilityId
			]);
		}

		// 5. Move project files from /temp to /prime
		$this->persistProjectFiles($project);

		// 6 Update elastic
		$this->client->get('v510/update_es/project/'. $project->id);
	}

	private function persistProjectFiles(Project $project) {
		// Save after every persisting action to prevent missing link during server error.
		$project->ebrochure = $this->persistFile($project->ebrochure);
		$project->save();
		$project->pricelist = $this->persistFile($project->pricelist);
		$project->save();
		$project->project_plan = $this->persistFile($project->project_plan);
		$project->save();
		$project->developer_logo = $this->persistFile($project->developer_logo);
		$project->save();
		$project->developer_image = $this->persistFile($project->developer_image);
		$project->save();

		foreach ($project->photos as $projectPhoto) {
			$projectPhoto->url = $this->persistFile($projectPhoto->url);
			$projectPhoto->save();
		}

		foreach ($project->units as $unit) {
			foreach ($unit->photos as $unitPhoto) {
				$unitPhoto->url = $this->persistFile($unitPhoto->url);
				$unitPhoto->save();
			}

			foreach ($unit->floorplans as $floorplan) {
				$floorplan->url = $this->persistFile($floorplan->url);
				$floorplan->save();
			}
		}
	}

	/**
	 * Persist file to prevent it from being deleted by the cron.
	 *
	 * @param  string
	 * @return string new path URL
	 */
	private function persistFile(string $file_path) {
		if (!$file_path) {
			return '';
		}

		$file_name = basename($file_path);

		$s3 = \Storage::disk('s3');
		$old_file_path = 'temp/' . $file_name;
		$new_file_path = 'listings/' . $file_name;

		// If the file is stored in the temp, we move it, otherwise we do nothing.
		if (strpos($file_path, '/temp/') !== false) { // !== is deliberate, don't change it.
			$s3->move($old_file_path, $new_file_path);
		}

		// If the file is image
		if (preg_match("/\.(jpg|jpeg|gif|png)$/", $file_path)) {
			return "http://thumbnail.worknplay.com/listings/{$file_name}";
		}

		return $s3->url($new_file_path);
	}

	/**
	 * Move the cover photo to the first index.
	 *
	 * @param array
	 */
	function normalizePhotos(array &$photos) {
		foreach (range(0, count($photos)) as $index) {
			if ($photos[$index]['isCover'] === true) {
				array_unshift($photos, array_splice($photos, $index, 1)[0]);
				break;
			}
		}
	}

	/**
	 * Generate 6 digit unique hash.
	 */
	function generateProjectHash() {
		return strtoupper(substr(md5(microtime()), rand(0, 26), 6));
	}

	/**
	 * Validate request inputs.
	 * You can specify the existing listings page for edit validation.
	 *
	 * @param  \App\Project
	 * @param  \Illuminate\Http\Request
	 */
	private function validateInput(Request $request)
	{
	    $rules = [
	        'propertyType' => 'required|integer',
	        'primary_photos' => 'required|array|min:1',
	        'projectStatus' => 'required|integer',
	        'projectName' => 'required',
	        'minPrice' => 'required',
	        'maxPrice' => 'required',
	        'showsStreetView' => 'required|boolean',
	        'projectAddress' => 'required',
	        'addressForMainPage' => 'required',
	        'eBrochurePath' => 'required|url',
	        'priceListPath' => 'required|url',
	        'unitType' => 'required|array|min:1',
	        'projectDescription' => 'required',
	        'projectWebsite' => 'required',
	        'selectedFacilities' => 'required|array',
	        'projectPlanImagePath' => 'required|url',
	        'developerLogoPath' => 'required|url',
	        'developerFullImagePath' => 'required|url',
	        'developerName' => 'required',
	        'developerInfo' => 'required',
	        'developerPhoneNumber' => 'required',
	        'developerEmail' => 'required',
	        'lat' => 'required',
	        'lng' => 'required'
	    ];

	    $this->validate($request, $rules);
	}
}
