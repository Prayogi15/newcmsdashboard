<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App;
use App\Sync;
use Carbon\Carbon;

class ToolsController extends Controller
{
	protected $base_region;
	public function __construct(){
		parent::__construct();
		$this->Sync = new Sync;
		if($this->Sync->isExist()){
			$this->SyncData = $this->Sync->getData(true);
		} else {
			$this->SyncData = $this->Sync->setData($this->Sync->sync());
		}
		foreach($this->SyncData['ms_region'] as $keys => $value){
			$this->SyncData['ms_region'][$keys] = (array)$value;
		}
		$this->base_region = collect($this->SyncData['ms_region']);
	}

	private function GetRegionFullNameFromName($ms_region, $param_region){
		$return = "";
		foreach($ms_region as $key => $value){
			if($value['name'] == $param_region){
				$return = $value['full_name'];
				break;
			}
		}
		if($return == ""){
			$return = "All";
		}
		return $return;
	}

	private function GetRegionCodeFromFullName($ms_region, $param_region){
		$return = "";
		foreach($ms_region as $key => $value){
			if($value['full_name'] == $param_region){
				$return = $value['name'];
				break;
			}
		}
		if($return == ""){
			$return = "idn";
		}
		return $return;
	}

	//hanya menampilkan data, mengoverride data yang ada tiap 2 minggu sekali
	public function xml(){
		$data_xml = DB::table('ms_generate_xml')
                            ->select('id','activity','url','total','status','start_date','end_date')
							->orderBy('id', 'desc')
                            ->get();
		$lang = array('en','id');					
		return view('tools/xml')
		->with('data_xml', $data_xml)
		->with('lang', $lang);
	}

	
	
}
