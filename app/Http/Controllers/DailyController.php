<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App;
use App\Sync;
use Session;

class DailyController extends Controller
{
	protected $base_building_region;
	protected $base_listing_building;
	protected $base_region;
	protected $base_type;
	protected $base_currency;
	public function __construct(){
		$this->client = new Client([
			'base_uri' => env('API_URL'),
			// 'timeout' => env('TIMEOUT_REQUEST')
		]);
		$this->headers = [
			// 'Authorization' => env('TOKEN_NON_LOGIN'),
			'lang' => 'en',
			'region' => 'idn',
			'version' => env('APP_VERSION'),
			'appversion' => env('APP_VERSION'),
			'noauth' => 'y',
		];
		parent::__construct();
		$this->Sync = new Sync;
		if($this->Sync->isExist()){
			$this->SyncData = $this->Sync->getData(true);
		} else {
			$this->SyncData = $this->Sync->setData($this->Sync->sync());
		}
		foreach($this->SyncData['tr_building_region'] as $keys => $value){
			$this->SyncData['tr_building_region'][$keys] = (array)$value;
		}
		foreach($this->SyncData['ms_listing_building'] as $keys => $value){
			$this->SyncData['ms_listing_building'][$keys] = (array)$value;
		}
		foreach($this->SyncData['ms_region'] as $keys => $value){
			$this->SyncData['ms_region'][$keys] = (array)$value;
		}
		foreach($this->SyncData['ms_type'] as $keys => $value){
			$this->SyncData['ms_type'][$keys] = (array)$value;
		}
		foreach($this->SyncData['ms_currency'] as $keys => $value){
			$this->SyncData['ms_currency'][$keys] = (array)$value;
		}
		$this->base_building_region = collect($this->SyncData['tr_building_region']);
		$this->base_listing_building = collect($this->SyncData['ms_listing_building']);
		$this->base_region = collect($this->SyncData['ms_region']);
		$this->base_type = collect($this->SyncData['ms_type']);
		$this->base_currency = collect($this->SyncData['ms_currency']);
	}

	private function GetCurrencyByID($ms_currency, $currency_id){
		$return = "";
		foreach($ms_currency as $key => $value){
			if($value['id'] == $currency_id){
				$return = $value['symbol'];
				break;
			}
		}
		return $return;
	}

	private function GetListBuildingNameFromRegion($ms_listing_building, $tr_building_region, $ms_region, $region){
		$region_id = "";
		foreach($ms_region as $key => $value){
			if($value['full_name'] == ucfirst($region)){
				$region_id = $value['id'];
				break;
			}
		}
		if($region_id == ""){
			$region_id = '2';
		}
		$array_building_name = [];
		for($i=0;$i<count($tr_building_region);$i++){
			if($tr_building_region[$i]['region'] == $region_id){
				$building_id = $tr_building_region[$i]['id'];
				foreach($ms_listing_building as $key => $value){
					if($value['id'] == $building_id && $value['lang'] == '2'){
						array_push($array_building_name, $value['type']);
						break;
					}
				}
			}
		}
		return $array_building_name;
	}

	private function GetTypeFromID($ms_type, $param_id){
		$type = "";
		foreach($ms_type as $key => $value){
			if($value['id'] == $param_id && $value['lang'] == "2"){
				$type = $value['name'];
				break;
			}
		}
		if($type == ""){
			$type = "Sell";
		}
		return $type;
	}

	public function ViewDailyReport(){
		// $tabs = array('daily_create_listing','daily_sign_up','daily_listing_list');
		$tabs = array('daily_create_listing', 'daily_listing_list');
		$wnp_building = $this->GetListBuildingNameFromRegion($this->base_listing_building, $this->base_building_region, $this->base_region, Session::get('region'));
		return view('daily/daily')
		->with('tabs', $tabs)
		->with('wnp_building', $wnp_building);
	}

	public function GetDailyReport(Request $req){
		$date = explode("-", Carbon::parse($req->input('date'))->format('Y-m-d'));
		// $date = explode("-", Carbon::now()->format('Y-m-d'));
		if($req->input('region') !== null){
			if($req->input('region')=='indonesia'){
				$region = '2';
			} else {
				$region = '3';
			}
		} else {
			$region = '1';
		}
		if(!empty($_REQUEST['draw'])){
			$draw = $_REQUEST['draw'];
		} else {
			$draw = '10';
		}
		switch($req->input('tab')){
			case "daily_create_listing": $url = "v510/report/daily_create_listing/".$date[0]."/".$date[1]; break;
			case "daily_sign_up": $url = "sitemap/v510/user/".$date[0]."/".$date[1]; break;
			case "daily_listing_list": $url = "sitemap/v510/listing/".$date[0]."/".$date[1]; break;
		}
		try{
			$results = $this->client->get($url);
			$results = json_decode((string)$results->getBody(), true);
		} catch(\Exception $e){
			\Log::error($e);
			dd($e);
			$response['Status'] = 'failed';
			$response['Message'] = 'Something went wrong.';
		}
		if($results['Status'] == "success_sync_listing"){
			switch($req->input('tab')){
				case "daily_create_listing":
					$max_date = Carbon::parse($req->input('date'))->endOfMonth()->format('d');
					// $max_date = intval(Carbon::now()->endOfMonth()->format('d'));
					$total_all = $total_web = $total_android = $total_ios = 0;
					$wnp_building = $this->GetListBuildingNameFromRegion($this->base_listing_building, $this->base_building_region, $this->base_region, $req->input('region'));
					$array_count = 0;
					for($i=$max_date; $i>0; $i--){
					// for($i=0; $i<$max_date; $i++){
						$total_current = 0;
						$day = $i;
						if($day<10){
							$day = "0".$day;
						}
						$date = Carbon::parse($req->input('date'))->format('Ym').$day;
						// $date = Carbon::now()->format('Ym').$day;
						$current_building = $results['Data']['building'][$date];
						$current_client = $results['Data']['client'][$date];
						$data[$array_count]['date'] = Carbon::parse($req->input('date'))->format('Y-m-').$day;
						if(isset($current_client['web'])){
							$data[$array_count]['web'] = intval($current_client['web']);
							$total_current += intval($current_client['web']);
							$total_web += intval($current_client['web']);
						} else {
							$data[$array_count]['web'] = "-";
						}
						if(isset($current_client['android'])){
							$data[$array_count]['android'] = intval($current_client['android']);
							$total_current += intval($current_client['android']);
							$total_android += intval($current_client['android']);
						} else {
							$data[$array_count]['android'] = "-";
						}
						if(isset($current_client['ios'])){
							$data[$array_count]['ios'] = intval($current_client['ios']);
							$total_current += intval($current_client['ios']);
							$total_ios += intval($current_client['ios']);
						} else {
							$data[$array_count]['ios'] = "-";
						}
						if($total_current == 0){
							$data[$array_count]['total'] = "-";
						} else {
							$data[$array_count]['total'] = $total_current;
							$total_all += $total_current;
						}
						
						for($j=0;$j<count($wnp_building);$j++){
							if(isset($current_building[$wnp_building[$j]])){
								$current_building_data = $current_building[$wnp_building[$j]];
								$current_building_name = str_replace(' ','_', strtolower($wnp_building[$j]));
								if(isset($current_building_data['sell'])){
									$data[$array_count][$current_building_name.'_sell'] = intval($current_building_data['sell']);
								} else {
									$data[$array_count][$current_building_name.'_sell'] = "-";
								}
								if(isset($current_building_data['rent'])){
									$data[$array_count][$current_building_name.'_rent'] = intval($current_building_data['rent']);
								} else {
									$data[$array_count][$current_building_name.'_rent'] = "-";
								}
							} else {
								$current_building_name = str_replace(' ','_', strtolower($wnp_building[$j]));
								$data[$array_count][$current_building_name.'_sell'] = "-";
								$data[$array_count][$current_building_name.'_rent'] = "-";
							}
						}
						$array_count++;
					}
				break;
				case "daily_sign_up":
					for($i=0; $i<count($results['Data']['listing']);$i++){
						$current = $results['Data']['listing'][$i];
						$data[$i]['date'] = Carbon::parse($current['created_at'])->format("Y-m-d");
						$data[$i]['non_uid'] = "0";
						$data[$i]['uid'] = "0";
					}
				break;
				case "daily_listing_list":
					for($i=0; $i<count($results['Data']['listing']);$i++){
						$current = $results['Data']['listing'][$i];
						$data[$i]['date'] = Carbon::parse($current['created_at'])->format("Y-m-d");
						$data[$i]['pid'] = $current['pid'];
						$data[$i]['price'] = $this->GetCurrencyByID($this->base_currency, $current['currency']).' '.number_format($current['price']);
						$data[$i]['status'] = ucwords($this->GetTypeFromID($this->base_type, $current['ms_listing_type']));
						$data[$i]['type'] = $current['building_id'];
						$data[$i]['created_by'] = "Bambang";
						$data[$i]['uid'] = "WXYZ";
					}
				break;
			}
			$response = array('draw'=>intval($draw),
					'data' => $data,
			);
		} else {
			$response = array('draw'=>intval($draw),
					'data' => [],
			);
		}
		return $response;
	}
}
