<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App;
use App\Sync;
use Carbon\Carbon;

class PointController extends Controller
{
	protected $base_region;
	public function __construct(){
		parent::__construct();
		$this->Sync = new Sync;
		if($this->Sync->isExist()){
			$this->SyncData = $this->Sync->getData(true);
		} else {
			$this->SyncData = $this->Sync->setData($this->Sync->sync());
		}
		foreach($this->SyncData['ms_region'] as $keys => $value){
			$this->SyncData['ms_region'][$keys] = (array)$value;
		}
		$this->base_region = collect($this->SyncData['ms_region']);
	}

	private function GetRegionFullNameFromName($ms_region, $param_region){
		$return = "";
		foreach($ms_region as $key => $value){
			if($value['name'] == $param_region){
				$return = $value['full_name'];
				break;
			}
		}
		if($return == ""){
			$return = "All";
		}
		return $return;
	}

	private function GetRegionCodeFromFullName($ms_region, $param_region){
		$return = "";
		foreach($ms_region as $key => $value){
			if($value['full_name'] == $param_region){
				$return = $value['name'];
				break;
			}
		}
		if($return == ""){
			$return = "idn";
		}
		return $return;
	}

	public function ViewPoint(){
		$tabs = array('point_gain','voucher');
		$lang = array('en','id');
		return view('setting/point')
		->with('tabs', $tabs)
		->with('lang', $lang);
	}

	public function GetDataPoint(Request $req){
		if($req->input('region') !== null){
			if($req->input('region')=='indonesia'){
				$region = array('all','idn');
			} else {
				$region = array('all','sgp');
			}
		}
		if(!empty($_REQUEST['length'])){
			$limit = (int)$_REQUEST['length'];
		} else {
			$limit = 20;
		}
		if(!empty($_REQUEST['start'])){
			$start = (int)$_REQUEST['start'];
		} else {
			$start = 0;
		}
		if(!empty($_REQUEST['order']['0']['column'])){
			$sort_by=$_REQUEST['order']['0']['column'];
		} else {
			$sort_by = '0';
		}
		if(!empty($_REQUEST['order']['0']['dir'])){
			$order_by=$_REQUEST['order']['0']['dir'];
		} else {
			$order_by = 'desc';
		}
		if(!empty($_REQUEST['draw'])){
			$draw = $_REQUEST['draw'];
		} else {
			$draw = '10';
		}
		if(!empty($_REQUEST['search']['value'])){
			$search = strtolower($_REQUEST['search']['value']);
			$arr_search = array('ms_rule.name','ms_rule.type','ms_rule.amount','ms_rule.status');
		}
		$arr_column = array('ms_rule.name','ms_rule.type','ms_rule.amount','ms_rule.status','ms_rule.region','ms_rule.updated_at');

		$eloquent = DB::connection('mysql2')->table('wnp_point.ms_rule as ms_rule');
		if(!empty($region)){
			$eloquent = $eloquent->whereIn('ms_rule.region', $region);
		}
		if(!empty($search)){
			$eloquent = $eloquent->where(function($query) use ($search, $arr_search){
				for($i=0;$i<count($arr_search);$i++){
					if($i == 0){
						if($i == 0){
							$query = $query->where($arr_search[$i], 'LIKE', '%'.$search.'%');
						} else {
							$query = $query->orWhere($arr_search[$i], 'LIKE', '%'.$search.'%');
						}
					}
				}
			});
		}
		$total = $eloquent->count();
		$point = $eloquent->orderBy($arr_column[$sort_by], $order_by)
		->offset($start)->limit($limit)
		->select('ms_rule.id as id', 'ms_rule.name as name', 'ms_rule.type as type', 'ms_rule.amount as amount', 'ms_rule.status as status', 'ms_rule.region as region', 'ms_rule.updated_at as updated_at')
		->get();
		if(count($point)>0){
			for($i=0;$i<count($point);$i++){
				$items[$i]['title'] = $point[$i]->name;
				$items[$i]['type'] = ucwords($point[$i]->type);
				$items[$i]['amount'] = $point[$i]->amount." Points";
				if($point[$i]->status == "active"){
					$items[$i]['status'] = '<span style="color:#33ba1e;">'.ucfirst($point[$i]->status).'</span>';
					$symbol = "fa-toggle-off";
				} else {
					$items[$i]['status'] = '<span style="color:#989898;">'.ucfirst($point[$i]->status).'</span>';
					$symbol = "fa-toggle-on";
				}
				$items[$i]['region'] = $this->GetRegionFullNameFromName($this->base_region, $point[$i]->region);
				$items[$i]['last_updated'] = Carbon::parse($point[$i]->updated_at)->format('d M Y');
				$items[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='change_status' onclick='ChangeStatus(`".$point[$i]->id."`, this);'><i class='fa ".$symbol."' aria-hidden='true'></i> Change Status</a></li><li><a href='#' class='edit_point' onclick='EditPoint(`".$point[$i]->id."`);'><i class='fa fa-edit' aria-hidden='true'></i> Edit</a></li></ul></div></td>";
			}
			$response = array('draw' => intval($draw),
					'recordsTotal' => intval($total),
					'recordsFiltered' => intval($total),
					'data' => $items);
		} else {
			$response = array('draw' => 0,
					'recordsTotal' => 0,
					'recordsFiltered' => 0,
					'data' => []);
		}
		return $response;
	}

	public function GetSinglePoint($id){
		$point = DB::connection('mysql2')->table('wnp_point.ms_rule as ms_rule')
		->where('ms_rule.id', $id)->first();
		if(!empty($point)){
			$return["Status"] = "success";
			$return["Data"] =(array)$point;
		} else {
			$return["Status"] = "failed";
			$return["Message"] = "Failed get Data or Data not found.";
		}
		return $return;
	}

	public function CreatePoint(Request $req){
		$validation = $req->validate([
			'rule_title' => 'required',
			'rule_amount' => 'required|int',
			'rule_url' => 'required',
			'rule_note' => 'required',
			'user_id' => 'required'
		]);
		if($validation == true){
			$arrayofinput['activity'] = strtolower(str_replace(' ', '_', $req->input('rule_title')));
			$arrayofinput['name'] = $req->input('rule_title');
			$arrayofinput['type'] = 'point';
			$arrayofinput['region'] = 'all';
			$arrayofinput['amount'] = $req->input('rule_amount');
			$arrayofinput['picture'] = $req->input('rule_url');
			$arrayofinput['note'] = $req->input('rule_note');
			$arrayofinput['object'] = $req->input('rule_unique');
			$arrayofinput['param_check'] = $req->input('rule_param_check');
			$arrayofinput['param_operator'] = $req->input('rule_param_operator');
			$arrayofinput['param_value'] = $req->input('rule_param_value');
			$arrayofinput['period'] = $req->input('rule_period');
			$arrayofinput['max_activity_in_period'] = $req->input('rule_max_activity');
			$arrayofinput['status'] = 'active';
			$arrayofinput['created_at'] = Carbon::now();
			$arrayofinput['created_by'] = $req->input('user_id');
			if(DB::connection('mysql2')->table('wnp_point.ms_rule')->insert($arrayofinput)){
				$return['Status'] = "success";
				$return['Message'] = "Success create point rule.";
			} else {
				$return['Status'] = "failed";
				$return['Message'] = "Something went wrong, internal server error.";
			}
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Something went wrong, please check again.";
		}
		return $return;
	}

	public function UpdatePoint(Request $req){
		$validation = $req->validate([
			'rule_id' => 'required',
			'rule_title' => 'required',
			'rule_amount' => 'required|int',
			'rule_url' => 'required',
			'rule_note' => 'required',
			'user_id' => 'required'
		]);
		if($validation == true){
			$arrayofinput['activity'] = strtolower(str_replace(' ', '_', $req->input('rule_title')));
			$arrayofinput['name'] = $req->input('rule_title');
			$arrayofinput['type'] = 'point';
			$arrayofinput['region'] = 'all';
			$arrayofinput['amount'] = $req->input('rule_amount');
			$arrayofinput['picture'] = $req->input('rule_url');
			$arrayofinput['note'] = $req->input('rule_note');
			$arrayofinput['object'] = $req->input('rule_unique');
			$arrayofinput['param_check'] = $req->input('rule_param_check');
			$arrayofinput['param_operator'] = $req->input('rule_param_operator');
			$arrayofinput['param_value'] = $req->input('rule_param_value');
			$arrayofinput['period'] = $req->input('rule_period');
			$arrayofinput['max_activity_in_period'] = $req->input('rule_max_activity');
			$arrayofinput['status'] = 'active';
			$arrayofinput['created_at'] = Carbon::now();
			$arrayofinput['created_by'] = $req->input('user_id');
			if(DB::connection('mysql2')->table('wnp_point.ms_rule as ms_rule')->where('ms_rule.id', $req->input('rule_id'))->update($arrayofinput)){
				$return['Status'] = "success";
				$return['Message'] = "Success create point rule.";
			} else {
				$return['Status'] = "failed";
				$return['Message'] = "Something went wrong, internal server error.";
			}
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Something went wrong, please check again.";
		}
		return $return;
	}

	public function GetDataVoucher(Request $req){
		if($req->input('region') !== null){
			if($req->input('region')=='indonesia'){
				$region = 'idn';
			} else {
				$region = 'sgp';
			}
		}
		if(!empty($_REQUEST['length'])){
			$limit = (int)$_REQUEST['length'];
		} else {
			$limit = 20;
		}
		if(!empty($_REQUEST['start'])){
			$start = (int)$_REQUEST['start'];
		} else {
			$start = 0;
		}
		if(!empty($_REQUEST['order']['0']['column'])){
			$sort_by=$_REQUEST['order']['0']['column'];
		} else {
			$sort_by = '0';
		}
		if(!empty($_REQUEST['order']['0']['dir'])){
			$order_by=$_REQUEST['order']['0']['dir'];
		} else {
			$order_by = 'desc';
		}
		if(!empty($_REQUEST['draw'])){
			$draw = $_REQUEST['draw'];
		} else {
			$draw = '10';
		}
		if(!empty($_REQUEST['search']['value'])){
			$search = strtolower($_REQUEST['search']['value']);
		}
		$arr_column = array('ms_gift_details.name','ms_gift.code','ms_gift.cost','ms_gift.status');

		$eloquent = DB::connection('mysql2')->table('wnp_point.ms_gift as ms_gift')
		->join('wnp_point.ms_gift_details as ms_gift_details', 'ms_gift.code', '=', 'ms_gift_details.gift_code');
		if(!empty($region)){
			$eloquent = $eloquent->where('ms_gift.region', $region);
		}
		if(!empty($search)){
			$eloquent = $eloquent->where(function($query) use ($search, $arr_column){
				for($i=0;$i<count($arr_column);$i++){
					if($i == 0){
						if($i == 0){
							$query = $query->where($arr_column[$i], 'LIKE', '%'.$search.'%');
						} else {
							$query = $query->orWhere($arr_column[$i], 'LIKE', '%'.$search.'%');
						}
					}
				}
			});
		}
		$total = $eloquent->count();
		$voucher = $eloquent->where('ms_gift_details.lang', 'en')
		->orderBy($arr_column[$sort_by], $order_by)
		->offset($start)->limit($limit)
		->select('ms_gift.id as id', 'ms_gift_details.name as name', 'ms_gift.code as code', 'ms_gift.cost as cost', 'ms_gift.status')
		->get();
		if(count($voucher)>0){
			for($i=0;$i<count($voucher);$i++){
				$items[$i]['title'] = $voucher[$i]->name;
				$items[$i]['code'] = strtoupper($voucher[$i]->code);
				$items[$i]['cost'] = $voucher[$i]->cost." Points";
				if($voucher[$i]->status == "active"){
					$items[$i]['status'] = '<span style="color:#33ba1e;">'.ucfirst($voucher[$i]->status).'</span>';
					$symbol = "fa-toggle-off";
				} else {
					$items[$i]['status'] = '<span style="color:#989898;">'.ucfirst($voucher[$i]->status).'</span>';
					$symbol = "fa-toggle-on";
				}
				$items[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='change_status' onclick='ChangeStatus(`".$voucher[$i]->id."`, this);'><i class='fa ".$symbol."' aria-hidden='true'></i> Change Status</a></li><li><a href='#' class='edit_voucher' onclick='EditVoucher(`".$voucher[$i]->id."`);'><i class='fa fa-edit' aria-hidden='true'></i> Edit</a></li><li><a href='#' class='delete_voucher' onclick='DeleteVoucher(`".$voucher[$i]->id."`, this);'><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</a></li></ul></div></td>";
			}
			$response = array('draw' => intval($draw),
					'recordsTotal' => intval($total),
					'recordsFiltered' => intval($total),
					'data' => $items);
		} else {
			$response = array('draw' => 0,
					'recordsTotal' => 0,
					'recordsFiltered' => 0,
					'data' => []);
		}
		return $response;
	}

	public function GetSingleVoucher($id){
		$eloquent = DB::connection('mysql2')->table('wnp_point.ms_gift as ms_gift')
		->join('wnp_point.ms_gift_details', 'ms_gift.code', '=', 'ms_gift_details.gift_code')
		->where('ms_gift.id', $id);
		$voucher["Detail"] = $eloquent->select('ms_gift.code', 'ms_gift.cost', 'ms_gift.picture','ms_gift.expired_at')->first();
		$eloquent = $eloquent->select('ms_gift_details.id', 'ms_gift_details.name', 'ms_gift_details.details', 'ms_gift_details.how_to_get', 'ms_gift_details.tnc');
		$voucher["English"] = $eloquent->where('ms_gift_details.lang', 'en')->first();

		// gak tau kenapa harus connect ulang, gak bisa reuse eloquent
		$voucher["Indonesia"] = DB::connection('mysql2')->table('wnp_point.ms_gift as ms_gift')
		->join('wnp_point.ms_gift_details', 'ms_gift.code', '=', 'ms_gift_details.gift_code')
		->where('ms_gift.id', $id)->where('ms_gift_details.lang', 'id')->first();
		$voucher['Detail']->expired_at = Carbon::parse($voucher['Detail']->expired_at)->format('d M Y');
		if(!empty($voucher)){
			$return["Status"] = "success";
			$return["Data"] =(array)$voucher;
		} else {
			$return["Status"] = "failed";
			$return["Message"] = "Failed get Data or Data not found.";
		}
		return $return;
	}
	public function CreateVoucher(Request $req){
		$validation = $req->validate([
			'voucher_code' => 'required',
			'voucher_value' => 'required|int',
			'voucher_url' => 'required',
			'expired_date' => 'required',
			'en_voucher_title' => 'required',
			'id_voucher_title' => 'required',
			'en_voucher_tnc' => 'required',
			'id_voucher_tnc' => 'required',
			'en_voucher_note' => 'required',
			'id_voucher_note' => 'required',
		]);
		if($validation == true){
			$arrayofinput['app_id'] = 0;
			$arrayofinput['vendor_id'] = 1;
			$arrayofinput['code'] = $req->input('voucher_code');
			$arrayofinput['cost'] = $req->input('voucher_value');
			$arrayofinput['picture'] = $req->input('voucher_url');
			$arrayofinput['picture_history'] = "https://s3-ap-southeast-1.amazonaws.com/images.worknplay.com/point/history/claim_voucher.svg";
			$arrayofinput['picture_history'] = "https://s3-ap-southeast-1.amazonaws.com/images.worknplay.com/point/history/claim_voucher.svg";
			$arrayofinput['region'] = $this->GetRegionCodeFromFullName($this->base_region, $req->input('region'));
			$arrayofinput['status'] = 'active';
			$arrayofinput['expired_at'] = Carbon::parse($req->input('expired_date'));
			$arrayofinput['expired_at'] = $req->input('voucher_expired');
			$arrayofinput['created_at'] = Carbon::now();

			$array_details = array(
				array(
					"gift_code" => $req->input('voucher_code'),
					"lang" => "en",
					"name" => $req->input('en_voucher_title'),
					"details" => $req->input('en_voucher_detail'),
					"how_to_get" => $req->input('en_voucher_how_to'),
					"tnc" => $req->input('en_voucher_tnc'),
					"note" => $req->input('en_voucher_note'),
				),
				array(
					"gift_code" => $req->input('voucher_code'),
					"lang" => "id",
					"name" => $req->input('id_voucher_title'),
					"details" => $req->input('id_voucher_detail'),
					"how_to_get" => $req->input('id_voucher_how_to'),
					"tnc" => $req->input('id_voucher_tnc'),
					"note" => $req->input('id_voucher_note'),
				),
			);
			DB::beginTransaction();
			try{
				DB::connection('mysql2')->table('wnp_point.ms_gift')->insert($arrayofinput);
				DB::connection('mysql2')->table('wnp_point.ms_gift_details')->insert($array_details);
			}
			catch(Exception $e){
				$return['Status'] = "failed";
				$return['Message'] = "Something went wrong. Please contact our support.";
				$return['ErrorMessage'] = $e;
				DB::rollback();
				return $return;
			}
			$return['Status'] = "success";
			$return['Message'] = "Success create voucher.";
			DB::commit();
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Something went wrong. Please check again.";
		}
		return $return;
	}

	public function UpdateVoucher(Request $req){
		$validation = $req->validate([
			'voucher_code' => 'required',
			'voucher_value' => 'required|int',
			'voucher_url' => 'required',
			'expired_date' => 'required',
			'en_voucher_id' => 'required',
			'id_voucher_id' => 'required',
			'en_voucher_title' => 'required',
			'id_voucher_title' => 'required',
			'en_voucher_tnc' => 'required',
			'id_voucher_tnc' => 'required',
			'en_voucher_note' => 'required',
			'id_voucher_note' => 'required',
		]);
		if($validation == true){
			$arrayofinput['code'] = $req->input('voucher_code');
			$arrayofinput['cost'] = $req->input('voucher_value');
			$arrayofinput['picture'] = $req->input('voucher_url');
			$arrayofinput['status'] = 'active';
			$arrayofinput['expired_at'] = Carbon::parse($req->input('expired_date'));

			$en_details = array(
				"gift_code" => $req->input('voucher_code'),
				"name" => $req->input('en_voucher_title'),
				"details" => $req->input('en_voucher_detail'),
				"how_to_get" => $req->input('en_voucher_how_to'),
				"tnc" => $req->input('en_voucher_tnc'),
				"note" => $req->input('en_voucher_note'),
				"updated_at" => Carbon::now(),
				"updated_by" => $req->input('user_id'),
			);

			$id_details = array(
				"gift_code" => $req->input('voucher_code'),
				"name" => $req->input('id_voucher_title'),
				"details" => $req->input('id_voucher_detail'),
				"how_to_get" => $req->input('id_voucher_how_to'),
				"tnc" => $req->input('id_voucher_tnc'),
				"note" => $req->input('id_voucher_note'),
				"updated_at" => Carbon::now(),
				"updated_by" => $req->input('user_id'),
			);
			DB::beginTransaction();
			try{
				DB::connection('mysql2')->table('wnp_point.ms_gift as ms_gift')
				->where('ms_gift.id', $req->input('voucher_id'))
				->update($arrayofinput);
				DB::connection('mysql2')->table('wnp_point.ms_gift_details')
				->where('ms_gift_details.id', $req->input('en_voucher_id'))
				->update($en_details);
				DB::connection('mysql2')->table('wnp_point.ms_gift_details')
				->where('ms_gift_details.id', $req->input('id_voucher_id'))
				->update($id_details);
			}
			catch(Exception $e){
				$return['Status'] = "failed";
				$return['Message'] = "Something went wrong. Please contact our support.";
				$return['ErrorMessage'] = $e;
				DB::rollback();
				return $return;
			}
			$return['Status'] = "success";
			$return['Message'] = "Success update voucher.";
			DB::commit();
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Something went wrong. Please check again.";
		}
		return $return;
	}

	public function ChangeStatusPoint(Request $req){
		$validation = $req->validate([
			'id' => 'required',
			'table' => 'required',
		]);
		if($validation == true){
			$check = DB::connection('mysql2')->table($req->input('table'))
			->where('id', $req->input('id'))
			->first();
			if($check->status == "active"){
				$status = "inactive";
			} else {
				$status = "active";
			}
			if(DB::connection('mysql2')->table($req->input('table'))->where('id', $req->input('id'))->update(["status" => $status])){
				$return['Status'] = "success";
				$return['Message'] = "Success change status.";
			} else {
				$return['Status'] = "failed";
				$return['Message'] = "Something went wrong. Please contact our support.";
			}
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Something went wrong. Please refresh this page.";
		}
		return $return;
	}

	// public function DeleteVoucher(Request $req){
	// 	$validation = $req->validate([
	// 		'id' => 'required',
	// 		'user_id' => 'required',
	// 	]);
	// 	if($validation == true){
	// 		if(DB::connection('mysql2')->table("wnp_point.ms_gift")->where('id', $req->input('id'))->update(["deleted_at" => Carbon::now(), 'deleted_by' => $req->input('user_id')])){
	// 			$return['Status'] = "success";
	// 			$return['Message'] = "Success delete voucher.";
	// 		} else {
	// 			$return['Status'] = "failed";
	// 			$return['Message'] = "Something went wrong. Please contact our support.";
	// 		}
	// 	} else {
	// 		$return['Status'] = "failed";
	// 		$return['Message'] = "Something went wrong. Please refresh this page.";
	// 	}
	// 	return $return;
	// }
}
