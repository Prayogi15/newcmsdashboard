<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Intervention\Image\ImageManager;

class HelpCenterController extends Controller
{
	public function ViewHelpCenter($id){
		return view('setting/help-center')
		->with('id', $id);
	}

	public function GetDataHelpCenter(Request $req){
		if(!empty($_REQUEST['length'])){
			$limit = (int)$_REQUEST['length'];
		} else {
			$limit = 20;
		}
		if(!empty($_REQUEST['start'])){
			$start = (int)$_REQUEST['start'];
		} else {
			$start = 0;
		}
		if(!empty($_REQUEST['draw'])){
			$draw = $_REQUEST['draw'];
		} else {
			$draw = '10';
		}
		$db_support = DB::connection('mysql3');
		$eloquent = $db_support->table('helpcenter_setting as helpcenter_setting')
		->where('helpcenter_setting.parent_id', $req->input('category_id'))
		->whereNull('helpcenter_setting.deleted_at')
		->orderBy('helpcenter_setting.no', 'asc');
		$total = $eloquent->count();
		$order = $eloquent->offset($start)->limit($limit)
		->get();
		$title_en = $db_support->table('helpcenter_setting as helpcenter_setting')
		->join('helpcenter_category as helpcenter_category', 'helpcenter_setting.category_id', '=', 'helpcenter_category.category_id')
		->where('helpcenter_setting.parent_id', $req->input('category_id'))
		->whereNull('helpcenter_category.deleted_at')
		->select('helpcenter_category.name')
		->orderBy('helpcenter_setting.no', 'asc')
		->where('helpcenter_category.lang', 'en')
		->offset($start)->limit($limit)
		->get();
		$title_id = $db_support->table('helpcenter_setting as helpcenter_setting')
		->join('helpcenter_category as helpcenter_category', 'helpcenter_setting.category_id', '=', 'helpcenter_category.category_id')
		->where('helpcenter_setting.parent_id', $req->input('category_id'))
		->whereNull('helpcenter_category.deleted_at')
		->select('helpcenter_category.name')
		->orderBy('helpcenter_setting.no', 'asc')
		->where('helpcenter_category.lang', 'id')
		->offset($start)->limit($limit)
		->get();
		if($total>0){
			for($i=0;$i<$total;$i++){
				$data[$i]['no'] = $order[$i]->no;
				if($order[$i]->isParent == 1){
					$data[$i]['id'] = "<a href='".url('/setting/help-center')."/".$order[$i]->category_id."'>".$title_id[$i]->name."</a>";
					$data[$i]['en'] = "<a href='".url('/setting/help-center')."/".$order[$i]->category_id."'>".$title_en[$i]->name."</a>";
				} else {
					$data[$i]['id'] = $title_id[$i]->name;
					$data[$i]['en'] = $title_en[$i]->name;
				}
				if($i == 0 || $order[$i]->no == 1){
					$data[$i]['move'] = "<button type='button' class='btn btn-default disabled'><i class='fa fa-arrow-up'></i></button><button type='button' class='btn btn-default' onclick='Move(`".$order[$i]->category_id."`,`down`);'><i class='fa fa-arrow-down'></i></button>";
				} else if($i == count($order)-1){
					$data[$i]['move'] = "<button type='button' class='btn btn-default' onclick='Move(`".$order[$i]->category_id."`,`up`);'><i class='fa fa-arrow-up'></i></button><button type='button' class='btn btn-default disabled'><i class='fa fa-arrow-down'></i></button>";
				} else if($total == 1){
					$data[$i]['move'] = "<button type='button' class='btn btn-default disabled'><i class='fa fa-arrow-up'></i></button><button type='button' class='btn btn-default disabled'><i class='fa fa-arrow-down'></i></button>";
				} else {
					$data[$i]['move'] = "<button type='button' class='btn btn-default'><i class='fa fa-arrow-up' onclick='Move(`".$order[$i]->category_id."`,`up`);'></i></button><button type='button' class='btn btn-default' onclick='Move(`".$order[$i]->category_id."`,`down`);'><i class='fa fa-arrow-down'></i></button>";
				}
				$data[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='edit' onclick='Edit(`".$order[$i]->category_id."`)'><i class='fa fa-edit' aria-hidden='true'></i> Edit</a></li>";
				if($req->input('category_id') == 0){
					$data[$i]['action'] .= "<li><a href='#' class='change_icon' onclick='ChangeIcon(`".$order[$i]->category_id."`)'><i class='fa fa-refresh' aria-hidden='true'></i> Change Icon</a></li>";
				}
				$data[$i]['action'] .= "<li><a href='#' class='delete' onclick='Delete(`".$order[$i]->category_id."`)'><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</a></li></ul></div></td>";
			}
			$response = array('draw'=>intval($draw),
					'recordsTotal' => intval($total),
					'recordsFiltered' => intval($total),
					'data' => $data
			);
		} else {
			$response = array('draw'=>intval($draw),
					'recordsTotal' => 0,
					'recordsFiltered' => 0,
					'data' => [],
			);
		}
		return $response;
	}

	public function GetSingleHelpCenter($id){
		$db_support = DB::connection('mysql3');
		$check = $db_support->table('helpcenter_setting')
		->where('category_id', $id)
		->first();
		if($check){
			if($check->isParent == 1){
				$title_en = $db_support->table('helpcenter_category')
				->where('category_id', $id)
				->whereNull('deleted_at')
				->where('lang', 'en')
				->first();
				$title_id = $db_support->table('helpcenter_category')
				->where('category_id', $id)
				->whereNull('deleted_at')
				->where('lang', 'id')
				->first();
				$title['en'] = $title_en->name;
				$title['id'] = $title_id->name;
				$data['Title'] = $title;
				$data['ParentID'] = $check->parent_id;
				$data['CategoryID'] = $id;
				$data['IsParent'] = 1;
			} else {
				$parent_en = $db_support->table('helpcenter_category')
				->where('category_id', $check->parent_id)
				->whereNull('deleted_at')
				->where('lang', 'en')
				->first();
				$parent_id = $db_support->table('helpcenter_category')
				->where('category_id', $check->parent_id)
				->whereNull('deleted_at')
				->where('lang', 'id')
				->first();
				$child_en = $db_support->table('helpcenter_category')
				->join('helpcenter_content', 'helpcenter_category.category_id', '=', 'helpcenter_content.category_id')
				->where('helpcenter_category.category_id', $id)
				->where('helpcenter_category.lang', 'en')
				->where('helpcenter_content.lang', 'en')
				->whereNull('helpcenter_category.deleted_at')
				->whereNull('helpcenter_content.deleted_at')
				->first();
				$child_id = $db_support->table('helpcenter_category')
				->join('helpcenter_content', 'helpcenter_category.category_id', '=', 'helpcenter_content.category_id')
				->where('helpcenter_category.category_id', $id)
				->where('helpcenter_category.lang', 'id')
				->where('helpcenter_content.lang', 'id')
				->whereNull('helpcenter_category.deleted_at')
				->whereNull('helpcenter_content.deleted_at')
				->first();
				$data_en['parent_title'] = $parent_en->name;
				$data_en['child_title'] = $child_en->name;
				$data_en['description'] = $child_en->content;
				$data_id['parent_title'] = $parent_id->name;
				$data_id['child_title'] = $child_id->name;
				$data_id['description'] = $child_id->content;
				$data['en'] = $data_en;
				$data['id'] = $data_id;
				$data['IsParent'] = 0;
				$data['CategoryID'] = $id;
			}
			$return['Status'] = "success";
			$return['Data'] = $data;
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Something went wrong. Please contact our support.";
		}
		return $return;
	}

	public function MoveHelpCenter($id, $flag){
		DB::beginTransaction();
		try{
			$db_support = DB::connection('mysql3');
			$check = $db_support->table('helpcenter_setting')
			->where('category_id', '=', $id)
			->whereNull('deleted_at')
			->first();
			$parent_id = $check->parent_id;
			if($flag == "up"){
				$newno1 = intval($check->no) - 1;
				$newno2 = intval($check->no);
			} else {
				$newno1 = intval($check->no) + 1;
				$newno2 = intval($check->no);
			}
			if($newno1 == 0){
				$return['Status'] = "failed";
				$return['Message'] = "Something went wrong. Please contact our support.";
				$return['Error'] = "Cannot move number 1 to 0";
				return $return;
			}
			$check2 = $db_support->table('helpcenter_setting')
			->where('parent_id', '=', $parent_id)
			->where('no', '=', $newno1)
			->whereNull('deleted_at')
			->first();
			if(empty($check2)){
				$db_support->table('helpcenter_setting')
				->where('id', $check->id)
				->whereNull('deleted_at')
				->update(['no' => $newno1]);
			} else {
				$db_support->table('helpcenter_setting')
				->where('id', $check->id)
				->whereNull('deleted_at')
				->update(['no' => $newno1]);
				$db_support->table('helpcenter_setting')
				->where('id', $check2->id)
				->whereNull('deleted_at')
				->update(['no' => $newno2]);
			}
		}
		catch(Exception $e){
			DB::rollback();
			if(!$return['Status']){
				$return['Status'] = "failed";
				$return['Message'] = "Something went wrong. Please contact our support.";
				$return['Error'] = $e;
			}
			return $return;
		}
		DB::commit();
		$return['Status'] = 'success';
		$return['Message'] = "Success move category.";
		return $return;
	}

	public function CreateParent(Request $req){
		$validation = $req->validate([
			'parent_id' => 'required',
			'parent_en_title' => 'required',
			'parent_id_title' => 'required',
		]);
		if($validation == true){
			DB::beginTransaction();
			try{
				$db_support = DB::connection('mysql3');
				$last_id = $db_support->table('helpcenter_setting')
				->where('parent_id', $req->input('parent_id'))
				->whereNull('deleted_at')
				->orderBy('category_id', 'desc')
				->first();
				$last_no = $db_support->table('helpcenter_setting')
				->where('parent_id', $req->input('parent_id'))
				->whereNull('deleted_at')
				->orderBy('no', 'desc')
				->first();
				if($last_id && $last_no){
					$new_id = $last_id->category_id++;
					$new_no = $last_no->no++;
					$arrayofinput['category_id'] = $new_id;
					$arrayofinput['parent_id'] = $req->input('parent_id');
					$arrayofinput['no'] = $new_no;
					$arrayofinput['isParent'] = 1;
					$arrayofinput['isHidden'] = 0;
					// insert helpcenter_setting
					$db_support->table('helpcenter_setting')->insert($arrayofinput);
					//insert helpcenter_category
					$arrayofinput2 = array(
						array(
							"category_id" => $new_id,
							"lang"=> "en",
							"name"=> $req->input('parent_en_title'),
						),
						array(
							"category_id" => $new_id,
							"lang"=> "id",
							"name"=> $req->input('parent_id_title'),
						),
					);
					$db_support->table('helpcenter_category')->insert($arrayofinput2);
				} else {
					$return['Status'] = "failed";
					$return['Message'] = "Something went wrong. Please contact our support.";
					$return['Error'] = $e;
					return $return;
				}
			}
			catch(Exception $e){
				DB::rollback();
				$return['Status'] = "failed";
				$return['Message'] = "Something went wrong. Please contact our support.";
				return $return;
			}
			DB::commit();
			$return['Status'] = "success";
			$return['Message'] = "Success create parent.";
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Something went wrong. Please check again.";
		}
		return $return;
	}

	public function UpdateParent(Request $req){
		$validation = $req->validate([
			'parent_id' => 'required',
			'parent_category_id' => 'required',
			'parent_en_title' => 'required',
			'parent_id_title' => 'required',
		]);
		if($validation == true){
			DB::beginTransaction();
			try{
				$db_support = DB::connection('mysql3');
				$db_support->table('helpcenter_category')
				->where('category_id', $req->input('parent_category_id'))
				->where('lang', 'en')
				->update(['name' => $req->input('parent_en_title')]);
				$db_support->table('helpcenter_category')
				->where('category_id', $req->input('parent_category_id'))
				->where('lang', 'id')
				->update(['name' => $req->input('parent_id_title')]);
			}
			catch(Exception $e){
				DB::rollback();
				$return['Status'] = "failed";
				$return['Message'] = "Something went wrong. Please contact our support.";
				$return['Error'] = $e;
				return $return;
			}
			DB::commit();
			$return['Status'] = "success";
			$return['Message'] = "Success update parent.";
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Something went wrong. Please check again.";
		}
		return $return;
	}

	public function CreateChild(Request $req){
		$validation = $req->validate([
			'child_parent_id' => 'required',
			'child_en_title' => 'required',
			'child_en_content' => 'required',
			'child_id_title' => 'required',
			'child_id_content' => 'required',
		]);
		if($validation == true){
			DB::beginTransaction();
			try{
				$db_support = DB::connection('mysql3');
				$last_id = $db_support->table('helpcenter_setting')
				->where('parent_id', $req->input('child_parent_id'))
				->orderBy('category_id', 'desc')
				->first();
				$last_no = $db_support->table('helpcenter_setting')
				->where('parent_id', $req->input('child_parent_id'))
				->orderBy('category_id', 'desc')
				->first();
				if($last_id && $last_no){
					$new_id = $last_id->category_id++;
					$new_no = $last_no->category_id++;
					$arrayofinput['category_id'] = $new_id;
					$arrayofinput['parent_id'] = $req->input('child_parent_id');
					$arrayofinput['no'] = $new_no;
					$arrayofinput['isParent'] = 0;
					$arrayofinput['isHidden'] = 0;
					$db_support->table('helpcenter_setting')->insert($arrayofinput);
					$arrayofinput2 = array(
						array(
							"category_id" => $new_id,
							"lang" => "en",
							"name" => $req->input('child_en_title'),
						),
						array(
							"category_id" => $new_id,
							"lang" => "id",
							"name" => $req->input('child_id_title'),
						),
					);
					$db_support->table('helpcenter_category')->insert($arrayofinput2);
					$arrayofinput3 = array(
						array(
							"category_id" => $new_id,
							"content" => $req->input('child_en_content'),
							"lang" => "en",
						),
						array(
							"category_id" => $new_id,
							"content" => $req->input('child_id_content'),
							"lang" => "id"
						),
					);
					$db_support->table('helpcenter_content')->insert($arrayofinput3);
				}
			}
			catch(Exception $e){
				DB::rollback();
				$return['Status'] = "failed";
				$return['Message'] = "Something went wrong. Please contact our support.";
				$return['Error'] = $e;
				return $return;
			}
			DB::commit();
			$return['Status'] = "success";
			$return['Message'] = "Success create child.";
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Something went wrong. Please check again.";
		}
		return $return;
	}

	public function UpdateChild(Request $req){
		$validation = $req->validate([
			'child_category_id' => 'required',
			'child_en_title' => 'required',
			'child_en_content' => 'required',
			'child_id_title' => 'required',
			'child_id_content' => 'required',
		]);
		if($validation == true){
			DB::beginTransaction();
			try{
				$db_support = DB::connection('mysql3');
				$db_support->table('helpcenter_category')
				->where('category_id', $req->input('child_category_id'))
				->where('lang', 'en')
				->update(['name' => $req->input('child_en_title')]);
				$db_support->table('helpcenter_category')
				->where('category_id', $req->input('child_category_id'))
				->where('lang', 'id')
				->update(['name' => $req->input('child_id_title')]);
				$db_support->table('helpcenter_content')
				->where('category_id', $req->input('child_category_id'))
				->where('lang', 'en')
				->update(['content' => $req->input('child_en_content')]);
				$db_support->table('helpcenter_content')
				->where('category_id', $req->input('child_category_id'))
				->where('lang', 'id')
				->update(['content' => $req->input('child_id_content')]);
			}
			catch(Exception $e){
				DB::rollback();
				$return['Status'] = "failed";
				$return['Message'] = "Something went wrong. Please contact our support.";
				$return['Error'] = $e;
				return $return;
			}
			DB::commit();
			$return['Status'] = "success";
			$return['Message'] = "Success update child.";
		} else {
			$return['Status'] = "failed";
			$return['Message'] = "Something went wrong. Please check again.";
		}
		return $return;
	}

	public function DeleteHelpCenter(Request $req){
		$category_id = $req->input('category_id');
		DB::beginTransaction();
		try{
			$db_support = DB::connection('mysql3');
			$check = $db_support->table('helpcenter_setting')
			->where('category_id', $category_id)
			->first();
			if($check->isParent != 0){
				//delete parent
				$db_support->table('helpcenter_setting')
				->where('category_id', $category_id)
				->update(['deleted_at' => Carbon::now()]);
				$db_support->table('helpcenter_category')
				->where('category_id', $category_id)
				->update(['deleted_at' => Carbon::now()]);
				//get child data
				$children = $db_support->table('helpcenter_setting')
				->where('parent_id', $category_id)
				->get();
				$child_id_array = [];
				foreach($children as $child_key => $wnp_child){
					array_push($child_id_array, $wnp_child->category_id);
				}
				//delete child
				$db_support->table('helpcenter_setting')
				->whereIn('category_id', $child_id_array)
				->update(['deleted_at' => Carbon::now()]);
				$db_support->table('helpcenter_category')
				->whereIn('category_id', $child_id_array)
				->update(['deleted_at' => Carbon::now()]);
				$db_support->table('helpcenter_content')
				->whereIn('category_id', $child_id_array)
				->update(['deleted_at' => Carbon::now()]);
			} else {
				//delete parent
				$db_support->table('helpcenter_setting')
				->where('category_id', $category_id)
				->update(['deleted_at' => Carbon::now()]);
				$db_support->table('helpcenter_category')
				->where('category_id', $category_id)
				->update(['deleted_at' => Carbon::now()]);
			}
		}
		catch(Exception $e){
			DB::rollback();
			$return['Status'] = "failed";
			$return['Message'] = "Something went wrong. Please contact our support.";
			$return['Error'] = $e;
			return $return;
		}
		DB::commit();
		$return['Status'] = "success";
		$return['Message'] = "Success delete category.";
		return $return;
	}

	public function ChangeIcon(Request $req){
		$db_support = DB::connection('mysql3');
		$base_url = env('icon_upload');
		DB::beginTransaction();
		try{
			$check = $_FILES['icon']['name'];
			$manager = new ImageManager(array('driver' => 'gd'));
			$img = $manager->make($_FILES['icon']['tmp_name']);
			if($img->filesize() > 30000000){
				$return['Status'] = "failed";
				$return['Message'] = "Image file size is too large.";
				return $return;
			}
			if(!in_array($img->mime(),['image/png','image/svg','image/jpg','image/jpeg','image/jpg','image/img'])){
				$return['Status'] = "failed";
				$return['Message'] = "Only .png, .svg, .jpeg, .jpg, .img are allowed.";
				return $return;
			}
			$upload = $img->save($base_url);
			if($upload){
				$url = env('img_url')."/".$check;
				$db_support->table('helpcenter_setting')
				->where('category_id', $req->input('icon_id'))
				->update(['icon' => $url]);
			} else {
				$return['Status'] = "failed";
				$return['Message'] = "Upload failed. Something went wrong.";
				return $return;
			}
		}
		catch(Exception $e){
			DB::rollback();
			$return['Status'] = "failed";
			$return['Message'] = "Something went wrong. Please contact our support.";
			$return['Error'] = $e;
			return $return;
		}
		DB::commit();
		$return['Status'] = "success";
		$return['Message'] = "Success update icon.";
		return $return;
	}
}