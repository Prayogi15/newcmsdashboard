<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class CorporateController extends Controller
{
	public function __construct(){
		$this->client = new Client(['base_uri' => env('API_URL')]);
		$this->headers = [
			// 'Authorization' => env('TOKEN_NON_LOGIN'),
			'lang' => 'en',
			'region' => 'idn',
			'version' => env('APP_VERSION'),
			'appversion' => env('APP_VERSION'),
			'noauth' => 'y',
		];
	}

	public function ViewCorporate(){
		return view('corporate/list');
	}
	public function GetDataCorporate(Request $req){
		if(!empty($_REQUEST['length'])){
			$limit = (int)$_REQUEST['length'];
		} else {
			$limit = 20;
		}
		if(!empty($_REQUEST['start'])){
			$start = (int)$_REQUEST['start'];
		} else {
			$start = 0;
		}
		if(!empty($_REQUEST['order']['0']['column'])){
			$sort_by=$_REQUEST['order']['0']['column'];
		} else {
			$sort_by = '5';
		}
		if(!empty($_REQUEST['order']['0']['dir'])){
			$order_by=$_REQUEST['order']['0']['dir'];
		} else {
			$order_by = 'desc';
		}
		if(!empty($_REQUEST['draw'])){
			$draw = $_REQUEST['draw'];
		} else {
			$draw = '10';
		}
		if(!empty($_REQUEST['search']['value'])){
			$search = strtolower($_REQUEST['search']['value']);
		}
		$arr_column = array('name', 'type', 'total_member', 'total_listing.active', 'created_at', 'id', 'status');

		$url_corporate = env('HOST_ELASTIC').env('ELASTIC_CORPORATE')."_search";
		$paramJSON = '{
			"size": '.$limit.',
			"from": '.$start.',
			"sort": [
				{
					"'.$arr_column[$sort_by].'": {"order": "'.$order_by.'"}
				}
			],
			"query": {
				"constant_score": {
					"filter": {
						"bool": {
							"must_not": {
								"exists": {
									"field": "deleted_at"
								}
							}
						}
					}
				}
			}
		}';
		$result = json_decode($this->curlPostContents($url_corporate, $paramJSON), TRUE);
		if(isset($result['hits'])){
			$corporate = array();
			$corporate_result = $result['hits']['hits'];
			for($i=0; $i<count($corporate_result);$i++){
				$current = $corporate_result[$i]['_source'];
				// LINK PROJECT TBD
				$corporates[$i]['name'] = '<a href="#">'.$current['name'].'</a>';
				$corporates[$i]['subscription'] = $current['type'];
				$corporates[$i]['total_member'] = $current['total_member'];
				$corporates[$i]['total_active_listing'] = $current['total_listing']['active'];
				$corporates[$i]['join_date'] = $current['total_listing']['active'];
				$corporates[$i]['last_active'] = $current['total_listing']['active'];
				if($current['deleted_at'] != null){
					$status = 'deleted';
					$corporates[$i]['status'] = "<span style='color: #ff0000;'>Deleted</span>";
				}else if($current['status'] == 'active'){
					$status = 'active';
					$corporates[$i]['status'] = "<span style='color: #33ba1e;'>".ucfirst($current['status'])."</span>";
				} else if($current['status'] == 'inactive'){
					$status = 'inactive';
					$corporates[$i]['status'] = "<span style='color: #363f45;'>Pending</span>";
				}
				$corporates[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-left' role='menu'><li><a href='#' class='edit_corporate' onclick='EditCorporate(`".$current['id']."`);'><i class='fa fa-edit' aria-hidden='true'></i> Edit Corporate</a></li>";
				if($status != 'deleted'){
					$corporates[$i]['action'] .= "<li><a href='#' class='delete_corporate' onclick='DeleteCorporate(`".$current['id']."`);'><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</a></li>";
				}
				$corporates[$i]['action'] .= "</ul></div></td>";
			}
			$response = array('draw'=> intval($draw),
				'recordsTotal' => intval($result['hits']['total']),
				'recordsFiltered' => intval($result['hits']['total']),
				'data' => $corporates
			);
		} else {
			$response = array('draw'=> intval($draw),
				'recordsTotal' => 0,
				'recordsFiltered' => 0,
				'data' => []
			);
		}
		return $response;
	}
	public function DeleteCorporate($id){
		DB::connection('mysql4')->table('ms_corporate')->where('id', $id)
		->update(['deleted_at' => Carbon::now(), 'status' => 'banned']);
		DB::connection('mysql4')->table('ms_member')->where('corporate_id', $id)
		->update(['deleted_at' => Carbon::now(), 'status' => 'banned']);
		$result_corporate = $this->client->get('v510/update_es/corporate/'.$id);
		$result_corporate = json_decode($result_corporate->getBody(), true);
		$result_member = $this->client->get('v510/update_es/member_by_corporate/'.$id);
		$result_member = json_decode($result_member->getBody(), true);
		if($result_corporate['Status'] != "success_listing" || $result_member['Status'] != "success_listing"){
			$return['Status'] = "failed";
			$return['Message'] = "Update elastic error, please contact our support.";
		} else {
			$return['Status'] = "success";
			$return['Message'] = "Success delete project.";
		}
		return $return;
	}
	// public function GetSingleCorporate($id){
	// 	$url_corporate = env('HOST_ELASTIC').env('ELASTIC_CORPORATE').$id;
	// 	$data = json_decode($this->getDataFromAPIGet($url_corporate));
	// 	if($data->founc == true){
	// 		$result = (array)$data->source;
	// 		$data['area'] = $result['area'];
	// 		$data['phone_number'] = $result['telephone'];
	// 	}
	// }
}
