<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Sync;
use Carbon\Carbon;
use DB;

class UserController extends Controller
{
	protected $base_region;
	protected $base_listing_building;
	public function __construct(){
		parent::__construct();
		$this->Sync = new Sync;
		if($this->Sync->isExist()){
			$this->SyncData = $this->Sync->getData(true);
		} else {
			$this->SyncData = $this->Sync->setData($this->Sync->sync());
		}
		foreach($this->SyncData['ms_region'] as $keys => $value){
			$this->SyncData['ms_region'][$keys] = (array)$value;
		}
		foreach($this->SyncData["ms_listing_building"] as $keys => $value){
			$this->SyncData['ms_listing_building'][$keys] = (array)$value;
		}
		$this->base_region = collect($this->SyncData['ms_region']);
		$this->base_listing_building = collect($this->SyncData["ms_listing_building"]);
    }

    private function GetBuildingName($ms_listing_building, $param_building_id, $lang){
		if($lang == ""){
			$lang == "2";
		}
		foreach($ms_listing_building as $key => $value){
			if($value['id'] == $param_building_id && $value['lang'] == $lang){
				$return = $value['type'];
				break;
			}
		}
		return $return;
	}

	private function GetRegionIDFromName($ms_region,$param_region_name){
		$return = "";
		foreach($ms_region as $key => $value){
			if($value['full_name'] == ucfirst($param_region_name)){
				$return = $value['id'];
				break;
			}
		}
		if($return == ""){
			$return = '2';
		}
		return $return;
	}

    public function ViewUser(){
		$status = array('all','active','watchlist','banned');
		return view('user/user')
		->with('status',$status);
    }

    public function GetDataUser(Request $req){
		if($req->input('status') !== null){
			$status = $req->input('status');
		}
		if($req->input('region') !== null){
			$region = $this->GetRegionIDFromName($this->base_region, $req->input('region'));
		}
		if(!empty($_REQUEST['length'])){
			$limit = (int)$_REQUEST['length'];
		} else {
			$limit = 20;
		}
		if(!empty($_REQUEST['start'])){
			$start = (int)$_REQUEST['start'];
		} else {
			$start = 0;
		}
		if(!empty($_REQUEST['order']['0']['column'])){
			$sort_by=$_REQUEST['order']['0']['column'];
		} else {
			$sort_by = '0';
		}
		if(!empty($_REQUEST['order']['0']['dir'])){
			$order_by=$_REQUEST['order']['0']['dir'];
		} else {
			$order_by = 'desc';
		}
		if(!empty($_REQUEST['search']['value'])){
			$search = strtolower($_REQUEST['search']['value']);
		}
		if(!empty($_REQUEST['draw'])){
			$draw = $_REQUEST['draw'];
		} else {
			$draw = '10';
		}
		$arr_column = array('uid','username','first_name','phone_number','created_at','status','last_name');
		$urlUser = env('HOST_ELASTIC').env('ELASTIC_USER')."_search";
		$paramJSON = '{
			"size": '.$limit.',
			"from": '.$start.',
			"sort": [
				{
					"'.$arr_column[$sort_by].'" : {"order" : "'.$order_by.'"}
				}
			],
			"query": {
				"constant_score": {
					"filter": {
						"bool": {
							"must":[';
		if(!empty($region)){
			$paramJSON .= '{
									"match_phrase": {
										"region": "'.$region.'"
									}
								}';
		}
		if(!empty($search)){
			$paramJSON .= ',{
									"query_string": {
										"query": "*'.$search.'*",
										"fields": ["uid","username","first_name","last_name","phone_number","country_code","status"]
									}
								}';
		}
		if(!empty($status)){
			if($status != 'all'){
				if($status != 'watchlist'){
					$paramJSON .= ',{
									"match_phrase": {
										"status": "'.$status.'"
									}
								}';
				} else {
					$paramJSON .= ',{
									"exists": {
										"field": "watch_date"
									}
								}';
					// $query_get_ids = DB::table('tr_watch_list')
					// ->where('end_date','=',NULL)->select('ms_user_id');
					// if($query_get_ids->count() == 0){
					// 	$response = array('draw' => intval($draw),
					// 				'recordsTotal' => 0,
					// 				'recordsFiltered' => 0,
					// 				'data' => []
					// 	);
					// 	return $response;
					// }
					// $array_id = array();
					// $watch_list_array = $query_get_ids->get();
					// for($x=0;$x<$query_get_ids->count();$x++){
					// 	array_push($array_id, $watch_list_array[$x]->ms_user_id);
					// }
					// $paramJSON .= ',{
					// 				"terms": {
					// 					"id": ["'.join('","', $array_id).'"]
					// 				}
					// 			}';
				}
			}
		}
		$paramJSON .= ']
						}
					}
				}
			}
		}';
		$result = json_decode($this->curlPostContents($urlUser, $paramJSON), TRUE);
		if(isset($result['hits'])){
			$users = array();
			$user_result = $result['hits']['hits'];
			for($i=0;$i<count($user_result);$i++){
				$current = $user_result[$i]['_source'];
				$users[$i]['uid'] = $current["uid"];
				$users[$i]['email'] = '<a href="/user/details/'.$current['id'].'" target="_blank">'.$current['username'].'</a>';
				$users[$i]['name'] = $current['first_name'].' '.$current['last_name'];
				$users[$i]['phone'] = $current['country_code'].$current['phone_number'];
				$users[$i]['created_at'] = Carbon::parse($current['created_at'])->format('d M Y');
				if($current['status'] == 'active'){
					$users[$i]['status'] = '<span style="color: #33ba1e;">Active</span>';
				} else if($current['status'] == "banned") {
					$users[$i]['status'] = '<span style="color: #e63535;">Banned</span>';
				} else if($current['status'] == "unverified"){
					$users[$i]['status'] = '<span style="color: #989898;">Unverified</span>';
				}
				switch($status){
					case "all": if($current['status'] == "banned"){
							$users[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='activate_user' onclick='ChangeStatus(`".$current['id']."`,`active`,this);'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Move to active</a></li></ul></div></td>";
						} else {
							$users[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='ban_user' onclick='ChangeStatus(`".$current['id']."`,`banned`,this);''><i class='fa fa-ban' aria-hidden='true'></i> Move to Banned</a></li><li><a href='#' class='watch_user' onclick='ChangeStatus(`".$current['id']."`,`watchlist`,this);'><i class='fa fa-eye' aria-hidden='true'></i> Move to Watchlist</a></li></ul></div></td>";
						} break;
					case "active": $users[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='ban_user' onclick='ChangeStatus(`".$current['id']."`,`banned`,this);'><i class='fa fa-ban' aria-hidden='true'></i> Move to Banned</a></li><li><a href='#' class='watch_user' onclick='ChangeStatus(`".$current['id']."`,`watchlist`,this);'><i class='fa fa-eye' aria-hidden='true'></i> Move to Watchlist</a></li></ul></div></td>"; break;
					case "watchlist": $users[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='activate_user' onclick='ChangeStatus(`".$current['id']."`,`active`,this);'><i class='fa fa-ban' aria-hidden='true'></i> Remove from Watchlist</a></li></ul></div></td>";
					case "banned": $users[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='activate_user' onclick='ChangeStatus(`".$current['id']."`,`active`,this);'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Move to active</a></li></ul></div></td>";
				}
			}
			$response = array('draw' => intval($draw),
					'recordsTotal' => intval($result['hits']['total']),
					'recordsFiltered' => intval($result['hits']['total']),
					'data' => $users);
		} else {
			$result['paramJSON'] = $paramJSON;
			$response = $result;
		}
		return $response;
	}

	public function GetDetailUser($id){
		$urlElastic = env('HOST_ELASTIC').env('ELASTIC_USER').$id;
		$data = json_decode($this->getDataFromAPIGet($urlElastic));
		if($data->found == true){
			$result = (array)$data->_source;
			$check = DB::table('tr_watch_list')->where('ms_user_id', $id)->where('end_date', '=', null)->count();
			if($check == 0){
				if($result['status'] == 'active'){
					$status = '<span style="color: #38cd0d;">Active</span>';
				} else if($result['status'] == 'banned'){
					$status = '<span style="color: #e63535;">Banned</span>';
				} else if($result['status'] == 'unverified'){
					$status = '<span style="color: #989898;">Unverified</span>';
				}
			} else {
				$status = '<span style="color: #fdbd39;">Watchlist</span>';
			}
			$point_data = DB::table('wnp_point.ms_user')->where('user_id', $id)->first();
			if($point_data){
				$point = $point_data->point;
			} else {
				$point = 0;
			}
			// return $result;
			$status_listing = array('active','archived','sold','deleted');
			return view('user.user_details')
			->with('data', $result)
			->with('status', $status)
			->with('status_listing', $status_listing)
			->with('point', $point);
		} else {
			return redirect()->back()->with("status_fail", "User not found. Coba tanya ivan aja ya :)");
		}
	}

	public function GetPointUser(Request $req){
		$table = DB::table('wnp_point.tr_transaction');
		if($req->input('type') == "gain"){
			$data_point = $table->join('wnp_point.ms_rule as ms_rule', 'wnp_point.tr_transaction.join_id', '=', 'ms_rule.id')
			->where('wnp_point.tr_transaction.user_id', $req->input('user_id'))
			->where('wnp_point.tr_transaction.type', $req->input('type'))
			->select('ms_rule.name as name', 'wnp_point.tr_transaction.created_at as date', 'ms_rule.amount as amount')
			->orderBy('wnp_point.tr_transaction.created_at', 'desc')
			->get();
		} else {
			$data_point = $table->join('wnp_point.ms_gift as ms_gift', 'wnp_point.tr_transaction.join_id', '=', 'ms_gift.id')
			->join('wnp_point.ms_gift_details as ms_gift_details', 'ms_gift.code', '=', 'ms_gift_details.gift_code')
			->where('wnp_point.tr_transaction.user_id', $req->input('user_id'))
			->where('wnp_point.tr_transaction.type', $req->input('type'))
			->where('ms_gift_details.lang', 'en')
			->select('ms_gift_details.name as name', 'wnp_point.tr_transaction.created_at as date', 'ms_gift.cost as amount')
			->orderBy('wnp_point.tr_transaction.created_at', 'desc')
			->get();
		}
		if(count($data_point) != 0){
			$point_array = array();
			for($i=0;$i<count($data_point);$i++){
				$current = (array)$data_point[$i];
				$point_array[$i]['name'] = $current['name'];
				if($req->input('type') == "gain"){
					$point_array[$i]['amount'] = "<b style='color:#1477ea;'>+ ".$current['amount']."</b> Points";
				} else {
					$point_array[$i]['amount'] = "<b style='color:#e54242;'>- ".$current['amount']."</b> Points";
				}
				$point_array[$i]['date'] = Carbon::parse($current['date'])->format('d M Y');
			}
			$response = array('data' => $point_array);
		} else {
			$response = array('data' => []);
		}
		return $response;
	}

	public function GetListingUser(Request $req){
		$id = $req->input('user_id');
		if($req->input('status') !== null){
			$status = $req->input('status');
		}
		if($req->input('region') !== null){
			if($req->input('region')=='indonesia'){
				$region = '2';
			} else {
				$region = '3';
			}
		}
		if(!empty($_REQUEST['length'])){
			$limit = (int)$_REQUEST['length'];
		} else {
			$limit = 20;
		}
		if(!empty($_REQUEST['start'])){
			$start = (int)$_REQUEST['start'];
		} else {
			$start = 0;
		}
		if(!empty($_REQUEST['order']['0']['column'])){
			$sort_by=$_REQUEST['order']['0']['column'];
		} else {
			$sort_by = '0';
		}
		if(!empty($_REQUEST['order']['0']['dir'])){
			$order_by=$_REQUEST['order']['0']['dir'];
		} else {
			$order_by = 'desc';
		}
		if(!empty($_REQUEST['draw'])){
			$draw = $_REQUEST['draw'];
		} else {
			$draw = '10';
		}
		if(!empty($_REQUEST['search']['value'])){
			$search = strtolower($_REQUEST['search']['value']);
		}
		$arr_column = array('pid','prefered_address','type_property.id','building.id','updated_at');
		$urlListing = env('HOST_ELASTIC').env('ELASTIC_LISTING')."_search";
		$paramJSON = '{
			"size": '.$limit.',
			"from": '.$start.',
			"sort": [
				{
					"'.$arr_column[$sort_by].'" : {"order" : "'.$order_by.'"}
				}
			],
			"query": {
				"constant_score": {
					"filter": {
						"bool": {
							"must":[{
								"match_phrase": {
									"user.id": "'.$id.'"
								}
							}';

		if(!empty($region)){
			$paramJSON .= ',{
								"match_phrase": {
									"currency": "'.$region.'"
								}
							}';
		}
		if(!empty($search)){
			$paramJSON .= ',{
								"query_string": {
									"query": "*'.$search.'*",
									"fields": ["pid","prefered_address","address","type_property.name","building.type","user.*name"]
								}
							}';
		}
		if(!empty($status)){
			if($status != 'all'){
				if($status == "deleted"){
					$paramJSON .= ',{
								"exists": {
									"field": "deleted_at"
								}
							}]
						}
					}
				}
			}
		}';
				} else {
					$paramJSON .= ',{
								"match_phrase": {
									"status": "'.$status.'"
								}
							}],
							"must_not": {
								"exists": {
									"field": "deleted_at"
								}
							}
						}
					}
				}
			}
		}';
				}
			}
		} else {
			$paramJSON .= '],
							"must_not": {
								"exists": {
									"field": "deleted_at"
								}
							}
						}
					}
				}
			}
		}';
		}
		$result = json_decode($this->curlPostContents($urlListing, $paramJSON), TRUE);
		if(isset($result['hits'])){
			$listings = array();
			$listingresult = $result['hits']['hits'];
			for($i=0;$i<count($listingresult);$i++){
				$current = $listingresult[$i]['_source'];
				$listings[$i]['pid'] = '<a href="/listing/details/'.$current['id'].'" target="_blank">'.strtoupper($current['pid']).'</a>';
				if($current['prefered_address'] != ""){
					$listings[$i]['address'] = $current['prefered_address'].' '.$current['city'].' '.$current['district'].' '.$current['post_code'];
				} else if($current['prefered_address'] == ""){
					$listings[$i]['address'] = $current['address'].' '.$current['city'].' '.$current['district'].' '.$current['post_code'];
				}
				if($current['type_property']['id'] == "1"){
					$listings[$i]['status'] = "Sell";
				} else if($current['type_property']['id'] == "2"){
					$listings[$i]['status'] = "Rent(Year)";
				} else {
					$listings[$i]['status'] = "Rent(Month)";
				}
				$listings[$i]['type'] = $this->GetBuildingName($this->base_listing_building, $current['building']['id'], "2");
				$listings[$i]['updated_at'] = Carbon::parse($current['updated_at'])->format('d M Y');
				switch($status){
					case "active": $listings[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='archive_listing' onclick='ChangeStatus(`".$current['id']."`,`archived`, this);'><i class='fa fa-archive' aria-hidden='true'></i> Move to archive</a></li><li><a href='#' class='delete' onclick='ChangeStatus(`".$current['id']."`,`deleted`, this);'><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</a></li></ul></div></td>"; break;
					case "archived": $listings[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='activate_listing' onclick='ChangeStatus(`".$current['id']."`,`active`, this);'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Move to active</a></li><li><a href='#' class='delete' onclick='ChangeStatus(`".$current['id']."`,`deleted`, this);'><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</a></li></ul></div></td>"; break;
					case "sold": $listings[$i]['action'] = ""; break;
					case "deleted": $listings[$i]['action'] = "<div class='btn-group pull-left'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-navicon'></i> <i class='fa fa-caret-down'></i></button><ul class='dropdown-menu pull-right' role='menu'><li><a href='#' class='activate_listing' onclick='ChangeStatus(`".$current['id']."`,`active`, this);'><i class='fa fa-check-circle-o' aria-hidden='true'></i> Move to active</a></li><li><a href='#' class='archive_listing' onclick='ChangeStatus(`".$current['id']."`,`archived`, this);'><i class='fa fa-archive' aria-hidden='true'></i> Move to archive</a></li></ul></div></td>"; break;
				}
				// $listings[$i]['uid'] = '<a href="/user/details/'.$current['user']['id'].'" target="_blank">'.ucfirst($current['user']['uid']).'</a>'
			}
			$response = array('draw' => intval($draw),
					'recordsTotal' => intval($result['hits']['total']),
					'recordsFiltered' => intval($result['hits']['total']),
					'data' => $listings);
		} else {
			$response = "failed";
		}
		return $response;
	}

	public function UpdateStatusUser($id,$status){
		$urlElastic = env('HOST_ELASTIC').env('ELASTIC_USER').$id;
		$time_now = Carbon::now()->format("Y-m-d H:i:s");
		DB::beginTransaction();
		try{
			switch($status){
				case "active": $array_target = array("status","watch_date");
				$array_value = array("active",null);
				$current_watch_list = DB::table('tr_watch_list')->where('ms_user_id', $id)->where('end_date', '!=', null);
				if($current_watch_list->count() > 0){
					$current_watch_list->update(['end_date'=>$time_now]);
				}
				break;
				case "watchlist": $array_target = array("watch_date");
				$array_value= array($time_now);
				$current_watch_list = DB::table('tr_watch_list')->where('ms_user_id', $id)->where('end_date', '!=', null);
				if($current_watch_list->count() == 0){
					DB::table('tr_watch_list')->insert(['ms_user_id' => $id, 'watch_date' => $time_now]);
				}
				break;
				case "banned": $array_target = array("status","watch_date");
				$array_value = array("banned",null);
				$current_watch_list = DB::table('tr_watch_list')->where('ms_user_id', $id)->where('end_date', '!=', null);
				if($current_watch_list->count() > 0){
					DB::table('tr_watch_list')->update(['end_date'=>$time_now]);
				}
				break;
			}
			$response = $this->UpdateElastic($urlElastic, $array_target, $array_value);
			if($response["Status"] == "failed"){
				DB::rollback();
				return $response;
			} else {
				DB::commit();
				return $response;
			}
		}
		catch(ErrorException $e){
			DB::Rollback();
			$response["Status"] = "failed";
			$response["Message"] = "Something went wrong.";
			$response["Errors"] = $e;
			return $response;
		}
	}
}
