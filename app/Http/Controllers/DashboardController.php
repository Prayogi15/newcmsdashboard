<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\EditPricingRequest;
use App\User;
use Carbon\Carbon;
use App\Http\Requests;
use DB;
use Session;
class DashboardController extends Controller
{
    public function __construct()
    {
       // $this->update_session();
    }

    public function Dashboard(){
        return view('front.home');
    }
}
