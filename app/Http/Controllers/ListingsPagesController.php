<?php

namespace App\Http\Controllers;

use App\ListingsPage;
use App\ListingBuilding;
use App\Type;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

final class ListingsPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) 
    {
    	$listingsPages = ListingsPage::all();

    	return view('listingspages.index', compact('listingsPages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$data = [
	    	'listingBuildings' => ListingBuilding::all(),
	    	'types' => Type::all()
    	];

        return view('listingspages.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateInput($request);

        $listingsPage = new ListingsPage();
        $this->updateListingsPage($listingsPage, $request);

        return redirect()->route('listingspages.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $data = [
            'listingsPage' => ListingsPage::findBySlug($slug),
            'listingBuildings' => ListingBuilding::all(),
            'types' => Type::all()
        ];
        
        return view('listingspages.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $slug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $listingsPage = ListingsPage::findBySlug($slug);

        $this->validateInput($request, $listingsPage);
        $this->updateListingsPage($listingsPage, $request);

        return redirect()->route('listingspages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $listingsPage = ListingsPage::findBySlug($slug);
        $listingsPage->delete();

        return redirect()->back();
    }

    /**
     * Update listings page.
     *
     * @param  \App\ListingsPage
     * @param  \Illuminate\Http\Request
     */
    private function updateListingsPage(ListingsPage $listingsPage, Request $request)
    {
    	$listingsPage->name = $request->input('name');
    	$listingsPage->slug = $request->input('slug');
    	$listingsPage->title = $request->input('title');
    	$listingsPage->meta_description = $request->input('meta_description');
    	$listingsPage->h1_text = $request->input('h1_text');
    	$listingsPage->content = $request->input('content');
    	$listingsPage->building_id = $request->input('property_type');
    	$listingsPage->type_id = $request->input('property_status');
    	$listingsPage->lat = $request->input('lat');
    	$listingsPage->long = $request->input('long');

        $listingsPage->save();
    }

    /**
     * Validate request inputs.
     * You can specify the existing listings page for edit validation.
     *
     * @param  \App\ListingsPage
     * @param  \Illuminate\Http\Request
     */
    private function validateInput(Request $request, ListingsPage $listingsPage = null)
    {
        $slugRule = Rule::unique('ms_listings_pages');

        if ($listingsPage) {
            $slugRule = $slugRule->ignore($listingsPage->id);
        }

        $rules = [
            'name' => 'required',
            'slug' => [
                'required',
                'alpha_dash',
                $slugRule
            ],
            'title' => 'required',
            'meta_description' => 'required',
            'h1_text' => 'required',
            'content' => 'required',
            'property_type' => 'required|integer',
            'property_status' => 'required|integer',
            'lat' => 'required',
            'long' => 'required'
        ];

        $this->validate($request, $rules);
    }
}
