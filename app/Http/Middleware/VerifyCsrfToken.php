<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/listing/get_listing_data',
        '/testing',
        '/user/get_user_data',
        '/user/get_points',
        '/user/get_listings',
        '/admin/reported_listing/get_reported_listing',
        '/admin/voucher/get_redeemed_voucher',
        '/upload_img',
        '/upload_file',
        '/setting/advertisement/get_ads',
        '/setting/advertisement/move',
        '/setting/advertisement/delete',
        '/setting/point/get_point',
        '/setting/point/get_voucher',
        '/setting/help-center/get_data',
        '/home/get_overview1',
        '/home/get_overview2',
        '/home/get_overview3',
        '/home/get_overview4',
        '/home/get_overview5',
        '/setting/help-center/delete',
        '/setting/help-center/change_icon',
        '/daily/get_daily',
        '/prime_cms/get_project',
        '/prime_cms/delete',
        '/prime_cms/change_status',
        '/corporate_cms/get_corporate'
    ];
}
