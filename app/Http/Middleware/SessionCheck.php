<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;
use Illuminate\Support\Facades\DB;
class SessionCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(strlen($request->session()->get('region')) == 0){
            $request->session()->put('region', 'indonesia');
        }
        if($request->session()->has('admin_id')) 
        {    
		$data_admin = DB::table('ms_admin')
                           ->select('id')
                           ->where('id',$request->session()->get('admin_id'))
						   ->where('status',"active")
						   ->first();
		if($data_admin==null)
		{
		return redirect('/logout');
		}		
		
		return $next($request);
		}
        else{
            return redirect('/')->with("status_fail", "Your session is expired. Please login again.");
        }
    }
}
