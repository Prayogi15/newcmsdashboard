<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

final class Project extends Model
{
    protected $table = 'ms_project';

    /**
     * Relations
     */
    public function photos()
    {
        return $this->hasMany('App\ProjectPhoto', 'project_id');
    }

    public function units()
    {
        return $this->hasMany('App\ProjectUnit', 'project_id');
    }
}
