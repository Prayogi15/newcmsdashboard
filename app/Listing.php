<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

final class Listing extends Model
{
    protected $table = 'ms_listing';

    /**
     * Relations
     */
    public function building() 
    {
        return $this->hasOne('App\ListingBuilding', 'building_id');
    }
}