<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoucherTransaction extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'tr_transaction';
    public $timestamps = false;
}
