<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectUnitFloorplan extends Model
{
    protected $table = 'ms_project_unit_floorplan';

    public $timestamps = false;
}
