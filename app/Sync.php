<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Cookie;
use App;

use Illuminate\Contracts\Encryption\Encrypter;

class Sync extends Model
{
  public function __construct()
  {
  	$this->client = new Client(['base_uri' => env('API_URL')]);
  	$this->headers = [
      'Authorization' => 'bearer '.env('TOKEN_NON_LOGIN'),
      'lang' => 'en',
      'region' => 'idn',
      'version' => env('APP_VERSION'),
      'appversion' => env('APP_VERSION'),
    ];
    parent::__construct();
  }
  
  public function sync()
  {
    $params = [
      'client_last_sync' => env('DEFAULT_FIRST_SYNC')
    ];
    if (!empty(Cookie::get('WNP_LAST_SYNC'))) {
        $params['client_last_sync'] = Cookie::get('WNP_LAST_SYNC');
    }
    try {
        $results = $this->client->post('sync', [
            'headers' => $this->headers,
            'form_params' => $params
        ]);
        $statuscode = $results->getStatusCode();
        $results = json_decode($results->getBody(), true);
        Cookie::queue('WNP_LAST_SYNC', date('Y-m-d H:i:s'), 2628000);
    } catch (\Exception $e) {
        \Log::error($e);
        dd($e);
        $results['Code'] = '401';   
    }
    return $results;
  }

  public function isExist()
  {
    if (Storage::disk('local')->exists('public/' . 'web_sync')) {
      return true;
    }
    return false;
  }

  public function getData($encrypt = false)
  {
    $current_sync_data = (array)json_decode(Storage::disk('local')->get('public/' . 'web_sync'));
    return $this->sortData($current_sync_data, $encrypt);
  }

  public function getAndCheckIfChanged($new_sync_data, $encrypt = false)
  {
    $current_sync_data = (array)json_decode(Storage::disk('local')->get('public/' . 'web_sync'));
    foreach ($new_sync_data as $key => $my_sync_data) {
      if (!empty($my_sync_data)) {
        $current_sync_data[$key] = $my_sync_data;
      }
    }
    if (!(json_encode($current_sync_data) == Storage::disk('local')->get('public/' . 'web_sync'))) {
      Storage::disk('local')->put('public/' . 'web_sync', json_encode($current_sync_data));
    }
    return $this->sortData($current_sync_data, $encrypt);
  }

  public function setData($new_sync_data)
  {
    Storage::disk('local')->put('public/' . 'web_sync', json_encode($new_sync_data));
    return $this->sortData($new_sync_data);
  }

  private function sortData($sync_data, $encrypt = false)
  {
    // build collection
      $ms_area_metric = collect($sync_data["ms_area_metric"]);
      $ms_category = collect($sync_data["ms_category"]);
      $ms_currency = collect($sync_data["ms_currency"]);
      $ms_detail = collect($sync_data["ms_detail"]);
      $ms_facilities = collect($sync_data["ms_facilities"]);
      $ms_lang = collect($sync_data["ms_lang"]);
      $ms_listing_building = collect($sync_data["ms_listing_building"]);
      $ms_nearby_facility = collect($sync_data["ms_nearby_facility"]);
      $ms_price = collect($sync_data["ms_price"]);
      $ms_project_building = collect($sync_data["ms_project_building"]);
      $ms_project_status = collect($sync_data["ms_project_status"]);
      $ms_province = collect($sync_data["ms_province"]);
      $ms_regency = collect($sync_data["ms_regency"]);
      $ms_region = collect($sync_data["ms_region"]);
      $ms_sort = collect($sync_data["ms_sort"]);
      $ms_spec = collect($sync_data["ms_spec"]);
      $ms_type = collect($sync_data["ms_type"]);
      $tr_building_detail = collect($sync_data["tr_building_detail"]);
      $tr_building_facility = collect($sync_data["tr_building_facility"]);
      $tr_building_price_type = collect($sync_data["tr_building_price_type"]);
      $tr_building_region = collect($sync_data["tr_building_region"]);
      $tr_building_spec = collect($sync_data["tr_building_spec"]);

    // App::setLocale('en');

    $lang = (array) $ms_lang->where("name", App::getLocale())->first();
    if (empty(Cookie::get('WNP_REGION'))) {
      return $sync_data;
    }
    if ($encrypt) {
      $region = (array)$ms_region->where("name", decrypt(Cookie::get('WNP_REGION')))->first();
    }else{
      $region = (array)$ms_region->where("name", Cookie::get('WNP_REGION'))->first();
    }

    $base_region_id = [1, $region["id"]];
    $currency = $ms_currency->whereIn("region", $base_region_id)->first();

    // listing building
    $building_id = $tr_building_region->whereIn("region", $base_region_id)->pluck('building_id')->toArray();
    $sync_data['ms_listing_building'] = $ms_listing_building->where('lang', $lang["id"])->whereIn('id', $building_id)->sortBy('priority')->toArray();

    //sort type
    $sync_data['ms_sort'] = $ms_sort->where('lang', $lang["id"])->sortBy('priority')->toArray();

    // price filter
    // $sync_data['ms_price'] = $ms_price->where('currency_id', $currency["id"])->sortBy('priority')->toArray();
    $sync_data['ms_price'] = $ms_price->where('currency_id', 2)->sortBy('priority')->toArray();

    // type
    $sync_data['ms_type'] = $ms_type->where('lang', $lang["id"])->toArray();
    
    // area metric
    $sync_data['ms_area_metric'] = $ms_area_metric->where('lang', $lang["id"])->toArray();

    // spec
    $sync_data['tr_building_spec'] = $tr_building_spec->whereIn('building_id', $building_id)->groupBy('building_id')->toArray();

    // ms_category
    $categories = $ms_category->where('lang', $lang["id"])->pluck('id')->toArray();
    $sync_data['ms_category'] = $ms_category->where('lang', $lang["id"])->toArray();
    
    // ms_detail
    $details = $ms_detail->where('lang', $lang["id"])->whereIn('region', $base_region_id)->pluck('id')->toArray();
    $sync_data['ms_detail'] = $ms_detail->where('lang', $lang["id"])->whereIn('region', $base_region_id)->whereIn('ms_category_id', $categories)->toArray();

    // ms_facilities
    $facilities = $ms_facilities->where('lang', $lang["id"])->whereIn('region', $base_region_id)->whereIn('ms_category_id', $categories)->pluck('id')->toArray();
    $sync_data['ms_facilities'] = $ms_facilities->where('lang', $lang["id"])->whereIn('region', $base_region_id)->whereIn('ms_category_id', $categories)->toArray();

    // tr_building_detail
    // $sync_data['tr_building_detail'] = $tr_building_detail->where('lang', $lang["id"])->whereIn('building_id', $building_id)->whereIn('detail_id', $details)->toArray();
    $sync_data['tr_building_detail'] = $tr_building_detail->whereIn('building_id', $building_id)->whereIn('detail_id', $details)->toArray();

    // tr_building_facility
    $sync_data['tr_building_facility'] = $tr_building_facility->whereIn('building_id', $building_id)->whereIn('facility_id', $facilities)->toArray();

    $sync_data["current_lang"] = $lang;
    $sync_data["current_region"] = $region;
    $sync_data["current_currency"] = $currency;
    return $sync_data;
  }
}