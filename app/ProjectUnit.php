<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectUnit extends Model
{
    protected $table = 'ms_project_unit';

    public $timestamps = false;

    /**
     * Relations
     */
    public function photos()
    {
        return $this->hasMany('App\ProjectUnitPhoto', 'unit_id');
    }

    public function floorplans()
    {
        return $this->hasMany('App\ProjectUnitFloorplan', 'unit_id');
    }
}
