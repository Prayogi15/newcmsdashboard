<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get("/", 'HomeController@index');

// untuk penghubung ke vue
//Route::any('{all?}', 'HomeController@index') ->where('all', '.+'); 

// Route::any('/landing', 'HomeController@index') ->where('all', '.+');
try {
    DB::connection()->getPdo();
} catch (\Exception $e) {
    die("Could not connect to the database.  Please check your configuration.");
}
Route::get('/', function () {
	if(Session::get('admin_id'))
	{
		return redirect('/home');
	} else {
		return view('front.login');
	}
});
Route::post('/signin','LoginController@Authenticate');
Route::get('/logout','LoginController@Logout');

Route::group(['middleware' => 'SessionCheck'], function () {
	Route::get('/home','OverviewController@ViewOverview');

	Route::get('/user', 'UserController@ViewUser');
	Route::get('/user/details/{id}', 'UserController@GetDetailUser');

	Route::get('/listing', 'ListingController@ViewListing');
	Route::get('/listing/details/{id}', 'ListingController@GetDetailListing');

	Route::get('/admin/voucher', 'VoucherController@ViewVoucher');
	Route::get('/admin/government_id', 'GovernmentIDController@ViewGovernmentID');
	Route::get('/admin/reported_listing', 'ListingController@ViewReportedListing');

	Route::get('/setting/advertisement', 'AdsController@ViewAdvertisement');
	Route::get('/setting/point', 'PointController@ViewPoint');
	Route::get('/setting/help-center/{id}', 'HelpCenterController@ViewHelpCenter');

	Route::get('/corporate_cms', 'CorporateController@ViewCorporate');
	Route::get('/prime_cms', 'PrimeController@ViewPrime');
	Route::get('/prime_cms/create_project', 'PrimeController@create');
	Route::get('/prime_cms/edit_project/{project_id}', 'PrimeController@edit');

	Route::get('/daily', 'DailyController@ViewDailyReport');
	
	Route::get('/admin/list', 'AdminController@index');
	Route::get('/admin/view', 'AdminController@access');
	Route::get('/admin/myprofile', 'AdminController@myprofile');
	Route::get('/kpr/list', 'KprController@index');
	Route::get('/kpr/listemail', 'KprController@listemail');
	Route::get('/kpr/remove/{id}', 'KprController@remove');
	Route::get('/tools/data_xml', 'ToolsController@xml');

	
});

//Overview
Route::post('/home/get_overview1', 'OverviewController@GetOverview1');
Route::post('/home/get_overview2', 'OverviewController@GetOverview2');
Route::post('/home/get_overview3', 'OverviewController@GetOverview3');
Route::post('/home/get_overview4', 'OverviewController@GetOverview4');
Route::post('/home/get_overview5', 'OverviewController@GetOverview5');

//User
Route::post('/user/get_user_data', 'UserController@GetDataUser');
Route::put('/user/change_status/{id}/{status}', 'UserController@UpdateStatusUser');
Route::post('/user/get_points', 'UserController@GetPointUser');
Route::post('/user/get_listings', 'UserController@GetListingUser');

//Listing
Route::post('/listing/get_listing_data', 'ListingController@GetDataListing');
Route::put('/listing/update/{id}/{status}', 'ListingController@UpdateStatusListing');

//Voucher
Route::post('/admin/voucher/get_redeemed_voucher', 'VoucherController@GetRedeemedVoucher');
Route::post('/admin/voucher/update_transaction', 'VoucherController@ChangeStatusTransaction');
//Goverment ID
Route::get('/admin/government_id/get_data', 'GovernmentIDController@GetListGovernmentIDElastic');
Route::get('/admin/government_id/get_user_by_id','Controller@GetUserById');
Route::post('/admin/government_id/change_status','GovernmentIDController@UpdateStatusGovermentVerified');
//Reported Listing
Route::post('/admin/reported_listing/get_reported_listing', 'ListingController@GetReportedListing');
Route::put('/admin/reported_listing/update_report/{id}/{status}', 'ListingController@UpdateReport');

//HELP CENTER
Route::get('/setting/help-center/edit/{id}', 'HelpCenterController@GetSingleHelpCenter');
Route::post('/setting/help-center/get_data', 'HelpCenterController@GetDataHelpCenter');
Route::get('/setting/help-center/move/{id}/{flag}', 'HelpCenterController@MoveHelpCenter');
Route::post('/setting/help-center/parent/create', 'HelpCenterController@CreateParent');
Route::post('/setting/help-center/parent/update', 'HelpCenterController@UpdateParent');
Route::post('/setting/help-center/child/create', 'HelpCenterController@CreateChild');
Route::post('/setting/help-center/child/update', 'HelpCenterController@UpdateChild');
Route::delete('/setting/help-center/delete','HelpCenterController@DeleteHelpCenter');
Route::post('/setting/help-center/change_icon', 'HelpCenterController@ChangeIcon');

//ADS
Route::post('/setting/advertisement/get_ads', 'AdsController@GetAds');
Route::post('/setting/advertisement/create_ads', 'AdsController@CreateAds');
Route::post('/setting/advertisement/update_ads', 'AdsController@UpdateAds');
Route::get('/setting/advertisement/{id}', 'AdsController@GetSingleAds');
Route::delete('/setting/advertisement/delete', 'AdsController@DeleteAds');
Route::post('/setting/advertisement/move', 'AdsController@MoveAds');

//POINT SETTING
Route::post('/setting/point/get_point', 'PointController@GetDataPoint');
Route::get('/setting/point/get_point/{id}', 'PointController@GetSinglePoint');
Route::post('/setting/point/create_point', 'PointController@CreatePoint');
Route::post('/setting/point/update_point', 'PointController@UpdatePoint');
Route::post('/setting/point/get_voucher', 'PointController@GetDataVoucher');
Route::get('/setting/point/get_voucher/{id}', 'PointController@GetSingleVoucher');
Route::post('/setting/point/create_voucher', 'PointController@CreateVoucher');
Route::post('/setting/point/update_voucher', 'PointController@UpdateVoucher');
Route::post('/setting/point/change_status', 'PointController@ChangeStatusPoint');

// DAILY REPORT
Route::post('/daily/get_daily', 'DailyController@GetDailyReport');



//Listing
Route::post('/listing/get_listing_data', 'ListingController@GetDataListing');
Route::put('/listing/update/{id}/{status}', 'ListingController@UpdateStatusListing');




// UPLOAD IMAGE
Route::post('/upload_img', 'UploadController@UploadImage');
Route::post('/upload_file', 'UploadController@uploadFile');
// Route::post('/upload_file', 'UploadController@UploadFile');
// Route::post('/upload_file', 'UploadController@UploadMultiple');
// Route::get('/setting/upload_image', 'UploadController@viewpage');

Route::get('/session','Controller@Development');
Route::get('/session','Controller@Development');
Route::get('/change_region/{region}', 'GovernmentIDController@ChangeRegion');

// CORPORATE CMS
Route::post('/corporate_cms/get_corporate', 'CorporateController@GetDataCorporate');
Route::get('/corporate_cms/delete_corporate/{id}', 'CorporateController@DeleteCorporate');

// PRIME CMS
Route::post('/prime_cms/get_project', 'PrimeController@GetListProject');
Route::post('/prime_cms/delete', 'PrimeController@DeleteProject');
Route::post('/prime_cms/change_status', 'PrimeController@ChangeStatusProject');
Route::get('/prime_cms/create_project/get_spec_and_facility/{id}/{region}', 'PrimeController@GetSpecAndFacility');
Route::post('/prime_cms/projects', 'PrimeController@store');
Route::put('/prime_cms/projects/{project_id}', 'PrimeController@update');

//ADMINISTRATOR
Route::post('/admin/insert', 'AdminController@insert');
Route::post('/admin/update', 'AdminController@update');
Route::post('/admin/update_profile', 'AdminController@update_profile');
Route::get('/admin/remove/{id}', 'AdminController@remove');
//KPR
Route::post('/kpr/getdatakpr', 'KprController@GetDataKpr');
Route::post('/kpr/download', 'KprController@download');
Route::post('/kpr/savelistemail', 'KprController@savelistemail');
Route::post('/kpr/sendemail', 'KprController@sendemail');









