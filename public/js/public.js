function validate_number(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if ((charCode >= 48 && charCode <= 57)) return true;
	return false;
}
function addComa(element) {
	element.value = commafy(element.value.replace(/,/g, ""))
}
function addComaValue(value){
	return commafy(value);
}
function commafy(num) {
	var str = num.toString().split('.');
	if (str[0].length >= 4) {
		str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
	}
	if (str[1] && str[1].length >= 4) {
		str[1] = str[1].replace(/(\d{3})/g, '$1 ');
	}
	return str.join('.');
}
function removeComma(string) {
	return string.replace(/,/g , "");
}