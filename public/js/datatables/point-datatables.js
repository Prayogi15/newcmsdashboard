for (var j = 0; j < tabs.length; j++) {
    var array_column;
    var url;
    if(tabs[j] == "point_gain"){
        array_column = [
            { data: 'title', "orderable": true },
            { data: 'type', "orderable": true },
            { data: 'amount', "orderable": true },
            { data: 'status', "orderable": true },
            { data: 'region', "orderable": true },
            { data: 'last_updated', "orderable": true },
            { data: 'action', "orderable": false }
        ];
        url = "/setting/point/get_point"
    } else {
        array_column = [
            { data: 'title', "orderable": true },
            { data: 'code', "orderable": true },
            { data: 'cost', "orderable": true },
            { data: 'status', "orderable": true },
            { data: 'action', "orderable": false }
        ];
        url = "/setting/point/get_voucher";
    }
    $('#table-' + tabs[j]).DataTable({
        language: {
            aria: {
                sortAscending: ": activate to sort column ascending",
                sortDescending: ": activate to sort column descending"
            },
            emptyTable: "No data available in table",
            info: "Showing _START_ to _END_ of _TOTAL_ records",
            infoEmpty: "No records found",
            infoFiltered: "(filtered1 from _MAX_ total records)",
            lengthMenu: "Show _MENU_",
            search: "Search:",
            zeroRecords: "No matching records found",
            processing: "Loading...",
            paginate: {
                previous: "Prev",
                next: "Next",
                last: "Last",
                first: "First"
            }
        },
        lengthMenu: [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        paging: true,
        pagingType: "first_last_numbers",
        serverSide: true,
        processing: true,
        retrieve: true,
        searching: { "regex": true },
        ajax: {
            url: url,
            type: "POST",
            data: { region: region }
        },
        deferRender: true,
        scrollX: true,
        scrollY: 400,
        columns: array_column
    });
}