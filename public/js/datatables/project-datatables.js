for (var j = 0; j < status_list.length; j++) {
    $('#table-' + status_list[j]).DataTable({
        language: {
            aria: {
                sortAscending: ": activate to sort column ascending",
                sortDescending: ": activate to sort column descending"
            },
            emptyTable: "No data available in table",
            info: "Showing _START_ to _END_ of _TOTAL_ records",
            infoEmpty: "No records found",
            infoFiltered: "(filtered1 from _MAX_ total records)",
            lengthMenu: "Show _MENU_",
            search: "Search:",
            zeroRecords: "No matching records found",
            processing: "Loading...",
            paginate: {
                previous: "Prev",
                next: "Next",
                last: "Last",
                first: "First"
            }
        },
        lengthMenu: [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        paging: true,
        pagingType: "first_last_numbers",
        order: [
            [1, "asc"]
        ],
        serverSide: true,
        processing: true,
        retrieve: true,
        searching: { "regex": true },
        ajax: {
            url: "/prime_cms/get_project",
            type: "POST",
            data: { status: status_list[j], region: region }
        },
        deferRender: true,
        scrollX: true,
        scrollY: 400,
        ordering: false,
        columns: [
            { data: 'action', "orderable": false },
            { data: 'name', "orderable": true },
            { data: 'type', "orderable": true },
            { data: 'developer', "orderable": true },
            { data: 'status', "orderable": true },
            { data: 'updated_at', "orderable": true },
            { data: 'prid', "orderable": true }
        ]
    });
}