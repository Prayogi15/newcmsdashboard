for (var j = 0; j < status_report.length; j++) {
    $('#table-' + status_report[j]).DataTable({
        language: {
            aria: {
                sortAscending: ": activate to sort column ascending",
                sortDescending: ": activate to sort column descending"
            },
            emptyTable: "No data available in table",
            info: "Showing _START_ to _END_ of _TOTAL_ records",
            infoEmpty: "No records found",
            infoFiltered: "(filtered1 from _MAX_ total records)",
            lengthMenu: "Show _MENU_",
            search: "Search:",
            zeroRecords: "No matching records found",
            processing: "Loading...",
            paginate: {
                previous: "Prev",
                next: "Next",
                last: "Last",
                first: "First"
            }
        },
        lengthMenu: [
            [10, 20, 50, -1],
            [10, 25, 50, "All"]
        ],
        paging: true,
        pagingType: "first_last_numbers",
        order: [
            [3, "desc"]
        ],
        serverSide: true,
        processing: true,
        retrieve: true,
        searching: { "regex": true },
        ajax: {
            url: "/admin/reported_listing/get_reported_listing",
            type: "POST",
            data: { region: region, status: status_report[j] }
        },
        deferRender: true,
        scrollX: true,
        scrollY: 400,
        columns: [
            { data: 'pid', "orderable": true },
            { data: 'problem', "orderable": true },
            { data: 'description', "orderable": true },
            { data: 'date', "orderable": true },
            { data: 'email', "orderable": true },
            { data: 'action', "orderable": false },
        ]
    });
}