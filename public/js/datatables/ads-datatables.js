for (var j = 0; j < array_position.length; j++) {
    var array_column;
    if(array_position[j] == "list_view"){
        array_column = [
            { data: 'no' },
            { data: 'action' },
            { data: 'move' },
            { data: 'name' },
            { data: 'type' },
            { data: 'link' },
            { data: 'title' },
            { data: 'description'},
            { data: 'image' },
            { data: 'clicks' },
            { data: 'date_start' },
            { data: 'date_end' },
            { data: 'modified_date' },
            { data: 'status' }
        ];
    } else {
        array_column = [
            { data: 'no' },
            { data: 'action' },
            { data: 'move' },
            { data: 'name' },
            { data: 'type' },
            { data: 'link' },
            { data: 'image' },
            { data: 'clicks' },
            { data: 'date_start' },
            { data: 'date_end' },
            { data: 'modified_date' },
            { data: 'status' }
        ];
    }
    $('#table-' + array_position[j]).DataTable({
        language: {
            aria: {
                sortAscending: ": activate to sort column ascending",
                sortDescending: ": activate to sort column descending"
            },
            emptyTable: "No data available in table",
            info: "Showing _START_ to _END_ of _TOTAL_ records",
            infoEmpty: "No records found",
            infoFiltered: "(filtered1 from _MAX_ total records)",
            lengthMenu: "Show _MENU_",
            search: "Search:",
            zeroRecords: "No matching records found",
            processing: "Loading...",
            paginate: {
                previous: "Prev",
                next: "Next",
                last: "Last",
                first: "First"
            }
        },
        lengthMenu: [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        paging: true,
        pagingType: "first_last_numbers",
        order: [
            [1, "asc"]
        ],
        serverSide: true,
        processing: true,
        retrieve: true,
        searching: { "regex": true },
        ajax: {
            url: "/setting/advertisement/get_ads",
            type: "POST",
            data: { position: array_position[j], region: region }
        },
        deferRender: true,
        scrollX: true,
        scrollY: 400,
        ordering: false,
        columns: array_column
    });
}