var array_column1 = [
    { data: 'date' },
    { data: 'total' },
    { data: 'web' },
    { data: 'android' },
    { data: 'ios' }
];
for(var q = 0; q <array_building.length; q++){
    var str_new = array_building[q].toLowerCase().replace(" ", "_")
    array_column1.push({ data : str_new+'_sell' });
    array_column1.push({ data : str_new+'_rent' });
}

$('#table-daily_create_listing').DataTable({
    searching: false,
    paging: false,
    ordering: false,
    language: {
        emptyTable: "No data available in table",
        processing: "Loading...",
    },
    bInfo: false,
    processing: true,
    ajax: {
        url: "/daily/get_daily",
        type: "POST",
        data: function (d){
            d.tab = "daily_create_listing";
            d.region = region;
            d.date = $('#date_daily_create_listing').val();
        }
    },
    scrollX: true,
    scrollY: 400,
    columns: array_column1
});

// var array_column2 = [
//     { data: 'date' },
//     { data: 'non_uid' },
//     { data: 'uid' }
// ];

// $('#table-daily_sign_up').DataTable({
//     searching: false,
//     paging: false,
//     ordering: false,
//     language: {
//         emptyTable: "No data available in table",
//         processing: "Loading...",
//     },
//     bInfo: false,
//     processing: true,
//     ajax: {
//         url: "/daily/get_daily",
//         type: "POST",
//         data: function (d){
//             d.tab = "daily_create_listing";
//             d.region = region;
//             d.date = $('#date_daily_sign_up').val();
//         }
//     },
//     scrollX: true,
//     scrollY: 400,
//     columns: array_column2
// });

var array_column3 = [
    { data: 'date' },
    { data: 'pid' },
    { data: 'price' },
    { data: 'status' },
    { data: 'type' },
    { data: 'created_by' },
    { data: 'uid' }
];

$('#table-daily_listing_list').DataTable({
    searching: false,
    paging: false,
    ordering: false,
    language: {
        emptyTable: "No data available in table",
        processing: "Loading...",
    },
    bInfo: false,
    processing: true,
    ajax: {
        url: "/daily/get_daily",
        type: "POST",
        data: function (d){
            d.tab = "daily_listing_list";
            d.region = region;
            d.date = $('#date_daily_listing_list').val();
        }
    },
    scrollX: true,
    scrollY: 400,
    columns: array_column3
});