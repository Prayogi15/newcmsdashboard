$('#table-help-center').DataTable({
    language: {
        aria: {
            sortAscending: ": activate to sort column ascending",
            sortDescending: ": activate to sort column descending"
        },
        emptyTable: "No data available in table",
        info: "Showing _START_ to _END_ of _TOTAL_ records",
        infoEmpty: "No records found",
        infoFiltered: "(filtered1 from _MAX_ total records)",
        lengthMenu: "Show _MENU_",
        search: "Search:",
        zeroRecords: "No matching records found",
        processing: "Loading...",
        paginate: {
            previous: "Prev",
            next: "Next",
            last: "Last",
            first: "First"
        }
    },
    lengthMenu: [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
    ],
    paging: true,
    pagingType: "first_last_numbers",
    serverSide: true,
    processing: true,
    retrieve: true,
    searching: false,
    ajax: {
        url: "/setting/help-center/get_data",
        type: "POST",
        data: { category_id: category_id }
    },
    deferRender: true,
    scrollX: true,
    scrollY: 400,
    columns: [
        { data: 'no' },
        { data: 'id' },
        { data: 'en' },
        { data: 'move' },
        { data: 'action' }
    ],
    ordering: false
});