$('#table-corporate').DataTable({
    language: {
        aria: {
            sortAscending: ": activate to sort column ascending",
            sortDescending: ": activate to sort column descending"
        },
        emptyTable: "No data available in table",
        info: "Showing _START_ to _END_ of _TOTAL_ records",
        infoEmpty: "No records found",
        infoFiltered: "(filtered1 from _MAX_ total records)",
        lengthMenu: "Show _MENU_",
        search: "Search:",
        zeroRecords: "No matching records found",
        processing: "Loading...",
        paginate: {
            previous: "Prev",
            next: "Next",
            last: "Last",
            first: "First"
        }
    },
    lengthMenu: [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
    ],
    paging: true,
    pagingType: "first_last_numbers",
    serverSide: true,
    processing: true,
    retrieve: true,
    searching: { "regex": true },
    ajax: {
        url: "/corporate_cms/get_corporate",
        type: "POST"
    },
    deferRender: true,
    scrollX: true,
    scrollY: 400,
    columns: [
        { data: 'name', "orderable": true },
        { data: 'subscription', "orderable": true },
        { data: 'total_member', "orderable": true },
        { data: 'total_active_listing', "orderable": true },
        { data: 'join_date', "orderable": true },
        { data: 'last_active', "orderable": true },
        { data: 'status', "orderable": true },
        { data: 'action', "orderable": false }
    ]
});