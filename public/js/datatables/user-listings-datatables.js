for (var j = 0; j < listing_status.length; j++) {
    $('#table-' + listing_status[j]).DataTable({
        language: {
            aria: {
                sortAscending: ": activate to sort column ascending",
                sortDescending: ": activate to sort column descending"
            },
            emptyTable: "No data available in table",
            info: "Showing _START_ to _END_ of _TOTAL_ records",
            infoEmpty: "No records found",
            infoFiltered: "(filtered1 from _MAX_ total records)",
            lengthMenu: "Show _MENU_",
            search: "Search:",
            zeroRecords: "No matching records found",
            processing: "Loading...",
            paginate: {
                previous: "Prev",
                next: "Next",
                last: "Last",
                first: "First"
            }
        },
        lengthMenu: [
            [10, 20, 50, -1],
            [10, 25, 50, "All"]
        ],
        paging: true,
        pagingType: "first_last_numbers",
        order: [
            [4, "desc"]
        ],
        serverSide: true,
        processing: true,
        retrieve: true,
        searching: { "regex": true },
        ajax: {
            url: "/user/get_listings",
            type: "POST",
            data: { region: region, status: listing_status[j], user_id: user_id }
        },
        deferRender: true,
        scrollX: true,
        scrollY: 400,
        columns: [
            { data: 'pid', "orderable": true },
            { data: 'address', "orderable": true },
            { data: 'status', "orderable": true },
            { data: 'type', "orderable": true },
            { data: 'updated_at', "orderable": true },
            { data: 'action', "orderable": false },
        ]
    });
}