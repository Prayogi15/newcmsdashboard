for (var j = 0; j < tabs.length; j++) {
    var array_column;
    switch(tabs[j]){
        case "daily_create_listing": 
            array_column = [
                { data: 'date' },
                { data: 'total' },
                { data: 'web' },
                { data: 'android' },
                { data: 'ios' }
            ];
            for(var q = 0; q <array_building.length; q++){
                var str_new = array_building[q].toLowerCase().replace(" ", "_")
                array_column.push({ data : str_new+'_sell' });
                array_column.push({ data : str_new+'_rent' });
            }
            break;
        case "daily_sign_up":
            array_column = [
                { data: 'date' },
                { data: 'non_uid' },
                { data: 'uid' }
            ];
        break;
        case "daily_listing_list":
            array_column = [
                { data: 'date' },
                { data: 'pid' },
                { data: 'price' },
                { data: 'status' },
                { data: 'type' },
                { data: 'created_by' },
                { data: 'uid' }
            ];
    }
    $('#table-' + tabs[j]).DataTable({
        searching: false,
        paging: false,
        ordering: false,
        language: {
            emptyTable: "No data available in table",
            processing: "Loading...",
        },
        bInfo: false,
        processing: true,
        ajax: {
            url: "/daily/get_daily",
            type: "POST",
            data: { tab: tabs[j], region: region, date: $('#date_'+tabs[j]).val() }
        },
        scrollX: true,
        scrollY: 400,
        columns: array_column
    });
}