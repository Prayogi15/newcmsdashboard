for (var j = 0; j < voucher_status.length; j++) {
    var selected_status;
    switch (voucher_status[j]) {
        case "pending":
            selected_status = "unprocessed";
            break;
        case "open":
            selected_status = "processed";
            break;
        case "completed":
            selected_status = "done";
            break;
        case "problem":
            selected_status = "problem";
            break;
    }
    $('#table-' + voucher_status[j]).DataTable({
        language: {
            aria: {
                sortAscending: ": activate to sort column ascending",
                sortDescending: ": activate to sort column descending"
            },
            emptyTable: "No data available in table",
            info: "Showing _START_ to _END_ of _TOTAL_ records",
            infoEmpty: "No records found",
            infoFiltered: "(filtered1 from _MAX_ total records)",
            lengthMenu: "Show _MENU_",
            search: "Search:",
            zeroRecords: "No matching records found",
            processing: "Loading...",
            paginate: {
                previous: "Prev",
                next: "Next",
                last: "Last",
                first: "First"
            }
        },
        lengthMenu: [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        paging: true,
        pagingType: "first_last_numbers",
        order: [
            [6, "desc"]
        ],
        serverSide: true,
        processing: true,
        retrieve: true,
        searching: { "regex": true },
        ajax: {
            url: "/admin/voucher/get_redeemed_voucher",
            type: "POST",
            data: { region: region, status: selected_status }
        },
        deferRender: true,
        scrollX: true,
        scrollY: 400,
        columns: [
            { data: 'action', "orderable": false },
            { data: 'id', "orderable": true },
            { data: 'uid', "orderable": true },
            { data: 'name', "orderable": true },
            { data: 'email', "orderable": true },
            { data: 'phone', "orderable": true },
            { data: 'date', "orderable": true },
            { data: 'voucher_code', "orderable": true },
            { data: 'voucher_value', "orderable": true }
        ],
        fixedColumns: {
            leftColumns: 1
        }
    });
}