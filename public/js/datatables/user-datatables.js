for (var j = 0; j < status_list.length; j++) {
    $('#table-' + status_list[j]).DataTable({
        language: {
            aria: {
                sortAscending: ": activate to sort column ascending",
                sortDescending: ": activate to sort column descending"
            },
            emptyTable: "No data available in table",
            info: "Showing _START_ to _END_ of _TOTAL_ records",
            infoEmpty: "No records found",
            infoFiltered: "(filtered1 from _MAX_ total records)",
            lengthMenu: "Show _MENU_",
            search: "Search:",
            zeroRecords: "No matching records found",
            processing: "Loading...",
            paginate: {
                previous: "Prev",
                next: "Next",
                last: "Last",
                first: "First"
            }
        },
        lengthMenu: [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        paging: true,
        pagingType: "first_last_numbers",
        order: [
            [4, "desc"]
        ],
        serverSide: true,
        processing: true,
        retrieve: true,
        searching: { "regex": true },
        ajax: {
            url: "/user/get_user_data",
            type: "POST",
            data: { region: region, status: status_list[j] }
        },
        deferRender: true,
        scrollX: true,
        scrollY: 500,
        columns: [
            { data: 'uid', "orderable": true },
            { data: 'email', "orderable": true },
            { data: 'name', "orderable": true },
            { data: 'phone', "orderable": true },
            { data: 'created_at', "orderable": true },
            { data: 'status', "orderable": true },
            { data: 'action', "orderable": false }
        ]
    });
}