$('#table-gain').DataTable({
    searching: false,
    paging: false,
    ordering: false,
    info: false,
    ajax: {
        url: "/user/get_points",
        type: "POST",
        data: { user_id: user_id, type: "gain" }
    },
    scrollX: true,
    scrollY: 200,
    columns: [
        { data: 'name' },
        { data: 'date' },
        { data: 'amount' }
    ]
});

$('#table-claim').DataTable({
    searching: false,
    paging: false,
    ordering: false,
    info: false,
    ajax: {
        url: "/user/get_points",
        type: "POST",
        data: { user_id: user_id, type: "redeem" }
    },
    scrollX: true,
    scrollY: 200,
    columns: [
        { data: 'name' },
        { data: 'date' },
        { data: 'amount' }
    ]
});