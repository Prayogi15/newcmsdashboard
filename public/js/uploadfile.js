(function($, window, undefined) {

   var uploadFile = function(file) {
      var formData = new FormData();
      formData.append('file', file);

      return $.ajax({
         url: '/upload_file',
         type: 'POST',
         processData: false,
         contentType: false,
         data: formData
      });
   };

   var uploadDidSuccess = function(fileName) {
      $(this).siblings('.input-display').html(fileName);
      $(this).siblings('input[type=hidden]').val(fileName);
   };

   var uploadDidFail = function(data) {
      var message = data.responseJSON.error;
      alertify.error(message);
   };

   var displayFileBrowser = function() {
      var fileSelector = document.createElement('input');
      fileSelector.setAttribute('type', 'file');

      fileSelector.onchange = (function(e) {
         uploadFile(fileSelector.files[0])
            .done(uploadDidSuccess.bind(this))
            .fail(uploadDidFail.bind(this));

         e.preventDefault();
      }).bind(this);

      fileSelector.click();
   };

   $('.file-browser-trigger').click(function(e) {
      displayFileBrowser.bind(this)();
      e.preventDefault();
   });

})(jQuery, window);