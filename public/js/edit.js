$(document).ready(function(){
	//localStorage.clear();
	$('input[type=text][name=project_id]').val(data_project['id']);
	$('input[type=radio][name=project_type][value='+data_project['project_building']['id']+']').attr('checked', true);
	$('input[type=radio][name=project_status][value='+data_project['project_status']['id']+']').attr('checked', true);
	$('input[type=text][name=project_name]').val(data_project['name']);
	$('input[type=text][name=project_min_price]').val(addComaValue(data_project['price_min']));
	$('input[type=text][name=project_max_price]').val(addComaValue(data_project['price_max']));
	$('input[name=lat]').val(data_project['location']['lat']); //lat adalah object
	$('input[name=lng]').val(data_project['location']['lon']); //lon adalah object
	$('textarea[name=address]').val(data_project['address']);
	$('input[type=text][name=short_address]').val(data_project['short_address']);
	$('input[type=text][name=ebrochure]').val(data_project['ebrochure']);
	$('input[type=text][name=pricelist]').val(data_project['pricelist']);
		// DESCRIPTION
	$('textarea[name=project_description]').val(data_project['description']);
	$('input[type=text][name=project_website').val(data_project['website']);
	$('#map-autocomplete-input').val(data_project['address']);
	
	// PROJECT PLAN
	$('input[type=text][name=project_plan]').val(data_project['project_plan']);

	// DEVELOPER DETAILS
	$('input[type=text][name=developer_logo]').val(data_project['developer_logo']);
	$('input[type=text][name=developer_image]').val(data_project['developer_image']);
	$('input[type=text][name=developer_name]').val(data_project['developer_name']);
	$('textarea[name=developer_info]').val(data_project['developer_info']);

	// CONTACT INFO
	$('input[type=text][name=developer_phone]').val(data_project['developer_phone']);
	$('input[type=text][name=developer_email]').val(data_project['developer_email']);
	
	
	// PRIMARY DETAILS
	
	var data_primary_photo = data_project['project_photos'];
	var index_primary_photo = "";
	for(var i=0;i<data_primary_photo.length;i++){
		if(i==0){
			index_primary_photo += data_primary_photo[i]['id'];
			primary_photo.push(data_primary_photo[i]['id']);
			let photo_element = `<div class="col-lg-3 col-md-3 col-xs-6" style="padding:0 15px; margin-bottom:15px;"><div class="block-img" style="overflow:hidden; height:150px; margin-bottom:15px;text-align:center"><button type='button' id=` + data_primary_photo[i]['id'] + ` class="remove-img" style="position: absolute;background: red;color: white;top: -8px;right: 10px;border-radius:50%;border:none;padding:7px 10px;cursor:pointer">X</button><img src=` + data_primary_photo[i]['url'] + ` style="height:100%"></div><button type="button" id=` + data_primary_photo[i]['id'] + ` class="button small-expanded button-set-cover disabled" style="width: 100%;" data-id="primary_photo">Cover</button></div>`;
			$("#result-primary_photo").append(photo_element);
		} else {
			index_primary_photo += ','+data_primary_photo[i]['id'];
			primary_photo.push(data_primary_photo[i]['id']);
			let photo_element = `<div class="col-lg-3 col-md-3 col-xs-6" style="padding:0 15px; margin-bottom:15px;"><div class="block-img" style="overflow:hidden; height:150px; margin-bottom:15px;text-align:center"><button type='button' id=` + data_primary_photo[i]['id'] + ` class="remove-img" style="position: absolute;background: red;color: white;top: -8px;right: 10px;border-radius:50%;border:none;padding:7px 10px;cursor:pointer">X</button><img src=` + data_primary_photo[i]['url'] + ` style="height:100%"></div><button type="button" id=` + data_primary_photo[i]['id'] + ` class="button small-expanded button-set-cover" style="width: 100%;" data-id="primary_photo">Set as Cover</button></div>`;
			$("#result-primary_photo").append(photo_element);
		}
	}
	$('input[type=text][name=project_photo]').val(index_primary_photo);
	
	
	// UNIT TYPE
	var data_project_units = data_project['project_units'];
	

	
	  $('#test').on('click', function(e){
         //e.preventDefault();
		console.log(data_project_units);
	  
	  });
	
	
	var project_units = [];
	for(var j=0;j<data_project_units.length;j++){
		var index_unit_photo = "";
		for(var k=0;k<data_project_units[j]['photos'].length;k++){
			if(k==0){
				index_unit_photo += data_project_units[j]['photos'][k]['id'];
			} else {
				index_unit_photo += ','+data_project_units[j]['photos'][k]['id'];
			}
		}
		var index_unit_floor_plan = "";
		for(var k=0;k<data_project_units[j]['floorplans'].length;k++){
			if(k==0){
				index_unit_floor_plan += data_project_units[j]['floorplans'][k]['id'];
			} else {
				index_unit_floor_plan += ','+data_project_units[j]['floorplans'][k]['id'];
			}
		}
		var data = {
			'name': data_project_units[j]['name'],
			'price' : data_project_units[j]['price'],
			'area_metric_id' : data_project_units[j]['area_metric']['id'],
			'land_area' : data_project_units[j]['land_area'],
			'floor_area' : data_project_units[j]['floor_area'],
			'bedrooms' : data_project_units[j]['bedrooms'],
			'bathrooms' : data_project_units[j]['bathrooms'],
			'parking' : data_project_units[j]['parking'],
			'photo' : index_unit_photo,
			'floor_plan' : index_unit_floor_plan
		};
		project_units.push(data);
	}
	localStorage.setItem('unit_data', JSON.stringify(project_units));
	UpdateDataTable();


	// FACILITY
	var project_facilities = [];
	var region = 2;//default indonesia
	for(var j=0; j<data_project['project_facilities'].length; j++){
		var current_data_facilities = data_project['project_facilities'][j];
		for(var k=0; k<current_data_facilities['item'].length; k++){
			project_facilities.push(current_data_facilities['item'][k]['item_id']);
		}
	}
	$.ajax({
		type: "GET",
		url: url_api+"/"+data_project['project_building']['id']+"/"+region,
		success: function(res){
			if(res.Status == 'success'){
				$('.spec').attr('disabled', true);
				$('#facilities_section').html("");
				for(var i=0;i<res.Data.Spec.length;i++){
					$('.spec[spec-id='+res.Data.Spec[i]+']').attr('disabled', false);
				}
				for(var j=0;j<res.Data.Facilities.length;j++){
					var current_facilities = res.Data.Facilities[j];
					$('#facilities_section').append(`<div class="form-group"><label>`+current_facilities[0]['category_name']+`</label><div class="row" id="category`+current_facilities[0]['category_id']+`"></div></div>`);
					for(var k=0;k<current_facilities.length;k++){
						if(jQuery.inArray(current_facilities[k]["id"], project_facilities) !== -1){
							$('#category'+current_facilities[0]['category_id']).append(`<div class="col-lg-3 col-md-6 col-xs-12"><div class="checkbox"><label><input type="checkbox" name="project_facilities[]" value="`+current_facilities[k]["id"]+`" checked>`+current_facilities[k]["name"]+`</label></div></div>`);
						} else {
							$('#category'+current_facilities[0]['category_id']).append(`<div class="col-lg-3 col-md-6 col-xs-12"><div class="checkbox"><label><input type="checkbox" name="project_facilities[]" value="`+current_facilities[k]["id"]+`">`+current_facilities[k]["name"]+`</label></div></div>`);
						}
					}
				}
			} else {
				alertify.error(res.Message);
			}
		},
		error: function(res){
			alertify.error('Something went wrong.');
		}
	});

});

