
let map;
let map_style = [
  {
  "elementType": "geometry",
  "stylers": [
    {
    "color": "#f5f5f5"
    }
  ]
  },
  {
  "elementType": "labels.icon",
  "stylers": [
    {
    "visibility": "off"
    }
  ]
  },
  {
  "elementType": "labels.text.fill",
  "stylers": [
    {
    "color": "#616161"
    }
  ]
  },
  {
  "elementType": "labels.text.stroke",
  "stylers": [
    {
    "color": "#f5f5f5"
    }
  ]
  },
  {
  "featureType": "administrative.country",
  "elementType": "labels.text.fill",
  "stylers": [
    {
    "color": "#424242"
    }
  ]
  },
  {
  "featureType": "administrative.land_parcel",
  "elementType": "labels.text.fill",
  "stylers": [
    {
    "color": "#bdbdbd"
    }
  ]
  },
  {
  "featureType": "administrative.province",
  "elementType": "labels.text.fill",
  "stylers": [
    {
    "color": "#424242"
    }
  ]
  },
  {
  "featureType": "landscape.man_made",
  "elementType": "geometry.fill",
  "stylers": [
    {
    "color": "#c7c7c7"
    },
    {
    "saturation": 5
    },
    {
    "lightness": 65
    }
  ]
  },
  {
  "featureType": "landscape.natural",
  "elementType": "geometry.fill",
  "stylers": [
    {
    "color": "#ebebeb"
    }
  ]
  },
  {
  "featureType": "poi",
  "elementType": "geometry",
  "stylers": [
    {
    "color": "#eeeeee"
    }
  ]
  },
  {
  "featureType": "poi",
  "elementType": "geometry.fill",
  "stylers": [
    {
    "color": "#d6d6d6"
    }
  ]
  },
  {
  "featureType": "poi",
  "elementType": "labels.icon",
  "stylers": [
    {
    "visibility": "on"
    }
  ]
  },
  {
  "featureType": "poi",
  "elementType": "labels.text.fill",
  "stylers": [
    {
    "color": "#005493"
    }
  ]
  },
  {
  "featureType": "poi",
  "elementType": "labels.text.stroke",
  "stylers": [
    {
    "color": "#ffffff"
    }
  ]
  },
  {
  "featureType": "poi.park",
  "elementType": "geometry",
  "stylers": [
    {
    "color": "#e5e5e5"
    }
  ]
  },
  {
  "featureType": "poi.park",
  "elementType": "geometry.fill",
  "stylers": [
    {
    "color": "#14ad3c"
    },
    {
    "saturation": -15
    },
    {
    "lightness": 60
    }
  ]
  },
  {
  "featureType": "poi.park",
  "elementType": "labels.text.fill",
  "stylers": [
    {
    "color": "#005493"
    }
  ]
  },
  {
  "featureType": "poi.park",
  "elementType": "labels.text.stroke",
  "stylers": [
    {
    "color": "#ffffff"
    }
  ]
  },
  {
  "featureType": "road",
  "elementType": "geometry",
  "stylers": [
    {
    "color": "#ffffff"
    }
  ]
  },
  {
  "featureType": "road.arterial",
  "elementType": "labels.text.fill",
  "stylers": [
    {
    "color": "#797979"
    }
  ]
  },
  {
  "featureType": "road.highway",
  "elementType": "geometry",
  "stylers": [
    {
    "color": "#dadada"
    }
  ]
  },
  {
  "featureType": "road.highway",
  "elementType": "geometry.fill",
  "stylers": [
    {
    "color": "#6699cc"
    },
    {
    "saturation": -30
    },
    {
    "lightness": 15
    }
  ]
  },
  {
  "featureType": "road.highway",
  "elementType": "geometry.stroke",
  "stylers": [
    {
    "color": "#d6d6d6"
    },
    {
    "weight": 1.5
    }
  ]
  },
  {
  "featureType": "road.highway",
  "elementType": "labels.text.fill",
  "stylers": [
    {
    "color": "#5e5e5e"
    }
  ]
  },
  {
  "featureType": "road.highway",
  "elementType": "labels.text.stroke",
  "stylers": [
    {
    "color": "#ebebeb"
    }
  ]
  },
  {
  "featureType": "road.local",
  "elementType": "labels.text.fill",
  "stylers": [
    {
    "color": "#797979"
    }
  ]
  },
  {
  "featureType": "transit.line",
  "elementType": "geometry",
  "stylers": [
    {
    "color": "#e5e5e5"
    }
  ]
  },
  {
  "featureType": "transit.line",
  "elementType": "labels.text.fill",
  "stylers": [
    {
    "color": "#797979"
    }
  ]
  },
  {
  "featureType": "transit.station",
  "elementType": "geometry",
  "stylers": [
    {
    "color": "#eeeeee"
    }
  ]
  },
  {
  "featureType": "water",
  "elementType": "geometry",
  "stylers": [
    {
    "color": "#c9c9c9"
    }
  ]
  },
  {
  "featureType": "water",
  "elementType": "geometry.fill",
  "stylers": [
    {
    "color": "#afcaf3"
    },
    {
    "saturation": 100
    }
  ]
  },
  {
  "featureType": "water",
  "elementType": "labels.icon",
  "stylers": [
    {
    "visibility": "on"
    }
  ]
  },
  {
  "featureType": "water",
  "elementType": "labels.text.fill",
  "stylers": [
    {
    "color": "#0096ff"
    },
    {
    "saturation": -30
    }
  ]
  }
];
let input = (document.getElementById('map-autocomplete-input'));
input.addEventListener("keydown", function(e){ 
  var key = e.charCode || e.keyCode || 0; 
  if (key == 13) {
    e.preventDefault();
  }
});
let countryRestrict = {'country': ['id','sg']};

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: latLng,
    zoom: 15,
    styles: map_style,
    clickableIcons: false,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: false
  });

  // add custom element
  let custom_control = document.getElementById('custom-map-control');
  let map_control_street_view = document.getElementById('custom-map-control-street-view');
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(custom_control);
  map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(map_control_street_view);

  // get current location
  var controlDiv = document.createElement('div');          
  var firstChild = document.createElement('button');
  firstChild.style.backgroundColor = '#fff';
  firstChild.style.border = 'none';
  firstChild.style.outline = 'none';
  firstChild.style.width = '28px';
  firstChild.style.height = '28px';
  firstChild.style.borderRadius = '2px';
  firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
  firstChild.style.cursor = 'pointer';
  firstChild.style.marginRight = '10px';
  firstChild.style.padding = '0px';
  firstChild.title = 'Your Location';
  firstChild.type = 'button';
  controlDiv.appendChild(firstChild);
  
  var secondChild = document.createElement('div');
  secondChild.style.margin = '5px';
  secondChild.style.width = '18px';
  secondChild.style.height = '18px';
  secondChild.style.backgroundImage = 'url(https://maps.gstatic.com/tactile/mylocation/mylocation-sprite-1x.png)';
  secondChild.style.backgroundSize = '180px 18px';
  secondChild.style.backgroundPosition = '0px 0px';
  secondChild.style.backgroundRepeat = 'no-repeat';
  secondChild.id = 'you_location_img';
  firstChild.appendChild(secondChild);
  
  google.maps.event.addListener(map, 'dragend', function() {
    $('#you_location_img').css('background-position', '0px 0px');
  });

  firstChild.addEventListener('click', function() {
    // var imgX = '0';
    // var animationInterval = setInterval(function(){
    //   if(imgX == '-18') imgX = '0';
    //   else imgX = '-18';
    //   $('#you_location_img').css('background-position', imgX+'px 0px');
    // }, 500);
    
    if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        map.setCenter(latlng);
        // clearInterval(animationInterval);
        // $('#you_location_img').css('background-position', '-144px 0px');
      });
    }
    else{
      // clearInterval(animationInterval);
      // $('#you_location_img').css('background-position', '0px 0px');
    }
    getAddress(map.getCenter().lat(), map.getCenter().lng());
  });
  controlDiv.index = 1;
  map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(controlDiv);

  // get user location if allowed, if not => default kelapa gading
  if(global_flag == 0){
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        latLng = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
      }, function() {
        console.log("cant get location");
      });
    } else {
        console.log("geolocation not supported");
    }
  }
  
  map.setCenter(latLng);
  // init autocomplete
  let autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.bindTo('bounds', map);
  
  // get current address
  // getAddress(map.getCenter().lat(), map.getCenter().lng());

  // events
  map.addListener('dragend', function() {
    getAddress(map.getCenter().lat(), map.getCenter().lng());
  });

  autocomplete.addListener('place_changed', function() {
    let place = autocomplete.getPlace();
    if (!place.geometry) {
      console.log("Autocomplete's returned place contains no geometry");
      return;
    }
    map.setZoom(15);
    map.setCenter(place.geometry.location);

    var lat = map.getCenter().lat();
    var lng = map.getCenter().lng();
    $('input[type="hidden"][id="lat"]').val(lat);
    $('input[type="hidden"][id="lng"]').val(lng);
    getAddress(lat, lng);
  });
}

function getAddress(lat, lng) {
  $.ajax({
    type: "GET",
    url: "https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&sensor=false"
  }).done(function(response) {
    if (response.results.length>0) {
      let data = response.results[0].address_components;
      let country, province, city, district, postal_code, address;
      for (let i = 0; i < data.length; i++) {
        if (data[i].types.includes("country")) {
          country = data[i].long_name;
        } else if ((data[i].types.includes("administrative_area_level_1")) || (data[i].types.includes("locality"))) {
          province = data[i].long_name;
        } else if (data[i].types.includes("administrative_area_level_2")) {
          city = data[i].long_name;
        } else if (data[i].types.includes("administrative_area_level_3")) {
          district = data[i].long_name;
        } else if (data[i].types.includes("postal_code")) {
          postal_code = data[i].long_name;
        } else if ((data[i].types.includes("street_address")) || (data[i].types.includes("route"))) {
          address = data[i].long_name;
        }
      }
      $('textarea[id="address"]').val(address+', '+district+', '+city+', '+province+', '+postal_code+', '+country);

      $('input[type="text"]#short_address').val(district+', '+province);

      $('input[type="text"]#map-autocomplete-input').val(address);

      
      
      // $('[data-name="country"]').val(country);
    }
  });
}